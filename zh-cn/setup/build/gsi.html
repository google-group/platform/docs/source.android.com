<html devsite><head>
    <title>常规系统映像 (GSI)</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>

  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>常规系统映像 (GSI) 是包含已针对 Android 设备调整配置的系统映像。这种映像被视为所有 Android 设备应该都能顺利运行且包含未经修改的 Android 开源项目 (AOSP) 代码的“纯 Android”实现。
</p>

<p>GSI 中的内容不依赖于供应商映像。为了验证 GSI 的独立性，Android 设备的系统映像会被替换为 GSI，然后通过<a href="/compatibility/vts/">供应商测试套件 (VTS)</a> 和<a href="/compatibility/cts/">兼容性测试套件 (CTS)</a> 进行全面测试。同样，您可以使用 GSI 替换自己的系统映像，以验证 Android 设备能够正确实现供应商接口。
</p>

<h2 id="gsi-configuration-and-variances">GSI 配置和差异</h2>

<p>GSI 的目标是为所有 Android 设备提供特定的通用配置，同时允许不同的供应商设备之间存在差异。当前的 GSI 基于 Android 9。
</p>

<p>当前的 GSI 具有以下配置：</p>

<ul>
  <li><strong>Treble</strong>。GSI 完全支持 Android 8.0 中引入的<a href="/devices/architecture/#hidl">基于 HIDL 的架构更改</a>（也称为“Treble”），其中包括对 <a href="/reference/hidl/">HIDL 接口</a>的支持。您可以在任何使用 HIDL 供应商接口的 Android 设备上使用 GSI（如需了解详情，请参阅<a href="/devices/architecture/#resources">关于架构的资源</a>）。</li>
  <li><strong>验证启动。</strong> GSI 不包含验证启动解决方案（<a href="/security/verifiedboot/">vboot 1.0</a>、<a href="/security/verifiedboot/avb">AVB</a> 等）。要将 GSI 刷写到 Android 设备上，此设备必须具有停用验证启动的方法。</li>
  <li><strong>编译变体</strong>。GSI 始终凭借 <code>userdebug</code> 编译变体来支持运行 VTS 和 CTS。将系统映像替换为 GSI 后，您可以对设备进行 Root 操作，然后使用 <code>user</code> 版本的供应商映像和 <code>userdebug</code> 版本的系统映像进行测试。</li>
  <li><strong>文件系统和映像格式</strong>。GSI 使用具有稀疏映像格式的 ext4 文件系统。</li>
</ul>

<p>当前的 GSI 包含以下主要差异：</p>

<ul>
  <li><strong>版本</strong>。支持 Android 8.0、Android 8.1 和 Android 9。</li>
  <li><strong>CPU 架构</strong>。支持不同的 CPU 指令（ARM、x86 等）和 CPU 位数（32 位或 64 位）。</li>
  <li><strong>分区布局</strong>。可以使用 <a href="/devices/bootloader/system-as-root">system-as-root</a> 或非 system-as-root 分区布局。</li>
  <li>支持 binder 接口位数。</li>
</ul>

<h2 id="gsi-types">GSI 类型</h2>

<p>GSI 类型取决于设备搭载的 Android 版本。Android 9 支持以下 GSI：</p>

<table>
  <tbody><tr>
   <th>GSI 名称</th>
   <th>说明</th>
   <th>产品名称</th>
  </tr>
  <tr>
   <td>Android GSI</td>
   <td>适用于搭载 Android 9 的设备</td>
   <td><code>aosp_$arch</code></td>
  </tr>
  <tr>
   <td>旧版 GSI</td>
   <td>适用于搭载 Android 8.0 或 Android 8.1 的设备</td>
   <td><code>aosp_$arch_a(b)</code></td>
  </tr>
</tbody></table>

<aside class="note">
  <strong>注意</strong>：要为某个设备确定合适的 GSI，请将此设备连接到工作站，然后运行 <code>adb shell getprop ro.product.first_api_level</code>。如果 API 级别为 28，则使用 Android GSI；如果 API 级别为 27，则使用旧版 GSI。如果没有系统属性，则说明此设备可能不受支持。
</aside>

<p>所有 GSI 都是从 Android 9 代码库编译的。是否支持旧版 GSI 取决于设备的供应商接口实现，并非所有搭载 Android 8.0 或 Android 8.1 的设备都能使用旧版 GSI。有关详情，请参阅<a href="#vendor-binaries-and-vndk-dependencies">供应商二进制文件和 VNDK 依赖项</a>。
</p>

<h3 id="changes-in-p-gsis">Android 9 GSI 变更</h3>

<p>搭载 Android 9 的设备必须使用 Android 9 GSI，Android 9 GSI 与早期 GSI 相比存在以下主要变化：</p>

<ul>
  <li><strong>合并 GSI 和模拟器</strong>。GSI 是根据模拟器产品（例如，<code>aosp_arm64</code>、<code>aosp_x86</code> 等）的系统映像编译的。</li>
  <li><strong>System-as-root</strong>。在以前的 Android 版本中，不支持 A/B 更新的设备可以在 <code>/system</code> 目录下装载系统映像。在 Android 9 中，系统映像的 root 作为设备的 root 装载。</li>
    <li><strong>64 位 binder 接口</strong>。在 Android 8.x 中，32 位 GSI 使用 32 位 binder 接口。Android 9 不支持 32 位 binder 接口，因此 32 位 GSI 和 64 位 GSI 都使用 64 位 binder 接口。</li>
    <li><strong>强制执行 VNDK</strong>。在 Android 8.1 中，VNDK 是可选的。在 Android 9 中，VNDK 是强制性的，这意味着 <code>BOARD_VNDK_RUNTIME_DISABLE</code> <strong>不可</strong>设置 (<code>BOARD_VNDK_RUNTIME_DISABLE :=  # must not be set</code>)。</li>
    <li><strong>兼容的系统属性</strong>。Android 9 支持对兼容的系统属性进行访问检查：<code>PRODUCT_COMPATIBLE_PROPERTY_OVERRIDE := true</code>。</li>
</ul>

<p>要通过 cts-on-gsi 测试搭载 Android 9 的设备，请使用 <a href="#p-gsi-build-targets">Android 9 GSI 的编译目标</a>。</p>

<h3 id="changes-in-legacy-gsis">Android 9 旧版 GSI 变更</h3>

<p>升级到 Android 9 的设备可以使用名称带后缀 <code>_ab</code> 或 <code>_a</code> 的旧版 GSI 产品（例如，<code>aosp_arm64_ab</code>、<code>aosp_x86_a</code>）。此 GSI 支持以下升级用例：</p>

<ul>
  <li>具有 Android 8.1 供应商接口实现的设备</li>
  <li>更新到 Android 9 供应商接口实现的设备</li>
</ul>

<p>旧版 GSI 是从 Android 9 源代码树编译的，但包含以下针对升级设备的向后兼容配置：</p>

<ul>
  <li><strong>非 system-as-root</strong>。不支持系统作为 root 的设备可以继续使用 <code>_a</code> 产品（例如 <code>aosp_arm_a</code>）。</li>
  <li><strong>32 位用户空间 + 32 位 binder 接口。</strong>32 位 GSI 可以继续使用 32 位 binder 接口。</li>
  <li><strong>8.1 VNDK</strong>。设备可以使用随附的 8.1 VNDK。</li>
  <li><strong>装载目录</strong>。一些旧版设备使用目录作为装载指针（例如，<code>/bluetooth</code>、<code>/firmware/radio</code>、<code>/persist</code> 等）。</li>
</ul>

<p>要通过 cts-on-gsi 测试升级到 Android 9 的设备，请使用<a href="#legacy-gsi-build-targets">旧版 GSI 的编译目标</a>。</p>

<aside class="note">
  <strong>注意</strong>：如果 Android 9 之前的设备实现了 Android 9 供应商接口并满足 Android 9 中引入的所有要求，请不要使用旧版 GSI，而是将 Android 9 GSI 用于 VTS 和 cts-on-gsi。
</aside>

<h3 id="changes-to-keymaster-behavior">Android 9 Keymaster 变更</h3>

<p>在早期版本的 Android 中，实现 Keymaster 3 或更早版本的设备需要验证并确保运行系统报告的版本信息（<code>ro.build.version.release</code> 和 <code>ro.build.version.security_patch</code>）与引导加载程序报告的版本信息匹配。此类信息通常可以从引导映像标头中获取。
</p>

<p>在 Android 9 中，为了让供应商引导 GSI，已经更改了此要求：具体来说，Keymaster 不应该执行验证，因为 GSI 报告的版本信息可能与供应商的引导加载程序报告的版本信息不匹配。对于实现 Keymaster 3 或更低版本的设备，供应商必须修改 Keymaster 实现以跳过验证（或升级到 Keymaster 4）。有关 Keymaster 的详细信息，请参阅<a href="/security/keystore/">由硬件支持的 Keystore</a>。
</p>

<h2 id="vendor-binaries-and-vndk-dependencies">供应商二进制文件和 VNDK 依赖项</h2>

<p>升级到 Android 9 的设备具有不同的升级路径，具体取决于设备上使用的供应商二进制文件的版本以及用于编译设备的 VNDK 相关配置。下表总结了针对升级设备的旧版 GSI 支持情况：</p>

<table>
  <tbody><tr>
   <th>用例</th>
   <th>供应商二进制文件版本</th>
   <th><code>BOARD_VNDK_VERSION</code></th>
   <th><code>BOARD_VNDK_RUNTIME_DISABLE</code></th>
   <th>旧版 GSI 系统二进制文件版本</th>
   <th>旧版 GSI 支持</th>
  </tr>
  <tr>
   <td>0</td>
   <td>8.0</td>
   <td>（任何）</td>
   <td>(不适用)</td>
   <td>9</td>
   <td>否</td>
  </tr>
  <tr>
   <td>1.a</td>
   <td>8.1</td>
   <td>（空）</td>
   <td>（任何）</td>
   <td>9</td>
   <td>否</td>
  </tr>
  <tr>
   <td>1.b</td>
   <td>8.1</td>
   <td><code>current</code></td>
   <td><code>true</code></td>
   <td>9</td>
   <td>否</td>
  </tr>
  <tr>
   <td>2</td>
   <td>8.1</td>
   <td><code>current</code></td>
   <td>（空）</td>
   <td>9</td>
   <td>是</td>
  </tr>
  <tr>
   <td>3</td>
   <td>9</td>
   <td><code>current</code></td>
   <td><code>true</code></td>
   <td>9</td>
   <td>是</td>
  </tr>
  <tr>
   <td>4</td>
   <td>9</td>
   <td><code>current</code></td>
   <td>（空）</td>
   <td>9</td>
   <td>是</td>
  </tr>
</tbody></table>

<p>最常见的受支持用例是 2，其中旧版 GSI 支持运行 Android 8.1 且使用 <code>BOARD_VNDK_VERSION</code> 进行编译（而未通过设置 <code>BOARD_VNDK_RUNTIME_DISABLE</code> 进行编译，即，未停用运行时强制执行）的设备。
</p>

<p>两个不受支持的用例是 1.a 和 1.b，其中旧版 GSI 不支持运行 Android 8.1 且未使用 <code>BOARD_VNDK_VERSION</code> 或通过设置 <code>BOARD_VNDK_RUNTIME_DISABLE</code> 进行编译（即，运行时强制执行已停用）的设备。这些设备不受支持，因为它们的供应商二进制文件依赖于 Android 8.1 非 VNDK 共享库，这些库未包含在旧版 GSI 中。</p>

<p>要使这些设备与旧版 GSI 兼容，供应商必须执行以下操作之一：</p>

<ul>
  <li>启用 <code>BOARD_VNDK_VERSION</code>，但未设置 <code>BOARD_VNDK_RUNTIME_DISABLE</code>（用例 2）<br /><br />或<br /><br /></li>
  <li>移植/升级供应商二进制文件，以便依赖于 Android 9 中的共享库（用例 3 和用例 4）。</li>
</ul>

<h2 id="building-gsis">编译 GSI</h2>

<p>自 Android 9 开始，每个 Android 版本都在 AOSP 上拥有一个名为 <code><var>DESSERT</var>-gsi</code> 的 GSI 分支（例如，<code>pie-gsi</code> 是 Android 9 的 GSI 分支）。GSI 分支包含应用了所有<a href="/security/bulletin/">安全补丁程序</a>和 <a href="#contributing-to-a-gsi">GSI 补丁程序</a> 的 Android 内容。
</p>

<p>要编译 GSI，请从 GSI 分支进行<a href="/setup/build/downloading">下载</a>，然后<a href="/setup/build/building#choose-a-target">选择 GSI 编译目标</a>，从而设置 Android 源代码树。请根据以下编译目标表确定设备应使用哪个 GSI 版本。编译完成后，GSI 便会成为系统映像（例如 <code>system.img</code>）并显示在输出文件夹 <code>out/target/product/<strong>generic_arm64_ab</strong></code> 中。编译还会输出 <code>vbmeta.img</code>；对于使用 <a href="/security/verifiedboot/avb">Android 验证启动</a>的设备，您可以将其用于停用验证启动。
</p><p>

</p><p>示例：以下命令可在 GSI 分支 <code>pie-gsi</code> 上编译旧版 GSI 编译目标 <code>aosp_arm64_ab-userdebug</code>：</p>

<pre class="prettyprint">
$ repo init -u https://android.googlesource.com/platform/manifest -b pie-gsi
$ repo sync -cq
$ source build/envsetup.sh
$ lunch aosp_arm64_ab-userdebug
$ make -j4
</pre>

<h3 id="p-gsi-build-targets">Android 9 GSI 编译目标</h3>

<p>以下 GSI 编译目标适用于搭载 Android 9 的设备。由于架构之间的差异减少，Android 9 仅包含四个 GSI 产品。
</p>

<table>
  <tbody><tr>
   <th>GSI 名称</th>
   <th>CPU 架构</th>
   <th>Binder 接口位数</th>
   <th>System-as-root</th>
   <th>产品名称</th>
  </tr>
  <tr>
   <td><code>aosp_arm</code></td>
   <td><code>ARM</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm64</code></td>
   <td><code>ARM64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm64-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86</code></td>
   <td><code>x86</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_64</code></td>
   <td><code>x86-64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86_64-userdebug</code></td>
  </tr>
</tbody></table>

<h3 id="legacy-gsi-build-targets">Android 9 旧版 GSI 编译目标</h3>

<p>以下旧版 GSI 编译目标适用于升级到 Android 9 的设备。旧版 GSI 名称包含后缀 <code>_ab</code> 或 <code>_a</code>，以区别于 Android 9 GSI 名称。
</p>

<table>
  <tbody><tr>
   <th>GSI 名称</th>
   <th>CPU 架构</th>
   <th>Binder 接口位数</th>
   <th>System-as-root</th>
   <th>产品名称</th>
  </tr>
  <tr>
   <td><code>aosp_arm_a</code></td>
   <td><code>ARM</code></td>
   <td><code>32</code></td>
   <td><code>N</code></td>
   <td><code>aosp_arm_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm_ab</code></td>
   <td><code>ARM</code></td>
   <td><code>32</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>**NA</code></td>
   <td><code>ARM</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td></td>
  </tr>
  <tr>
   <td><code>aosp_arm_64b_ab</code></td>
   <td><code>ARM</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm_64b_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm64_a</code></td>
   <td><code>ARM64</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td><code>aosp_arm64_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm64_ab</code></td>
   <td><code>ARM64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm64_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_a</code></td>
   <td><code>x86</code></td>
   <td><code>32</code></td>
   <td><code>N</code></td>
   <td><code>aosp_x86_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_ab</code></td>
   <td><code>x86</code></td>
   <td><code>32</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>**NA</code></td>
   <td><code>x86</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td></td>
  </tr>
  <tr>
   <td><code>**NA</code></td>
   <td><code>x86</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td></td>
  </tr>
  <tr>
   <td><code>aosp_x86_64_a</code></td>
   <td><code>x86-64</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td><code>aosp_x86_64_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_64_ab</code></td>
   <td><code>x86-64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86_64_ab-userdebug</code></td>
  </tr>
</tbody></table>
<em>**可以根据要求添加</em>

<aside class="aside">
  <strong>注意</strong>：在未来的 Android 版本中可能会移除这些编译目标。
</aside>

<h2 id="flashing-gsis">刷写 GSI</h2>

<p>Android 设备可能具有不同的设计，因此不可能通过单个命令或单组指令将 GSI 刷写到特定设备上。请使用以下常规步骤作为指南：</p>

<ol>
  <li>确保设备具备以下条件：
    <ul>
      <li>支持 HIDL-HAL 接口。</li>
      <li>用于解锁设备的方法（以便能够使用 <code>fastboot</code> 对其进行刷写）。</li>
      <li>用于停用验证启动的方法（例如 <a href="/security/verifiedboot/">vboot 1.0</a>、<a href="/security/verifiedboot/avb">AVB</a> 等）。</li>
      <li>解锁设备，使其可通过 <code>fastboot</code> 进行刷写。</li>
      <aside class="note">
        <strong>注意</strong>：请通过 Android 源代码树来编译 <code>fastboot</code>，以确保您拥有其最新版本。</aside>
    </ul>
  </li>
  <li>停用验证启动。</li>
  <li>清空当前系统分区，然后将 GSI 刷写到系统分区。</li>
  <li>擦除用户数据，并清除来自其他必要分区的数据（例如元数据）。</li>
  <li>重新启动设备。</li>
</ol>

<p>例如，要将 GSI 刷写到 Pixel 2 设备，请执行以下操作：</p>

<ol>
  <li><a href="/setup/build/running#booting-into-fastboot-mode">启动到引导加载程序模式</a>，然后<a href="/setup/build/running#unlocking-the-bootloader">解锁引导加载程序</a>。</li>
  <li>通过刷写 <code>vbmeta.img</code> 来停用验证启动 (AVB)：<pre class="prettyprint">$ fastboot flash vbmeta vbmeta.img</pre></li>
  <li>清空系统分区，然后将 GSI 刷写到系统分区：<pre class="prettyprint">
$ fastboot erase system
$ fastboot flash system system.img
</pre></li>
  <li>擦除用户数据并清除其他必要分区的数据：<pre class="prettyprint">$ fastboot -w</pre></li>
  <li>重新启动：<pre class="prettyprint">$ fastboot reboot</pre></li>
</ol>

<h2 id="contributing-to-gsis">为改进 GSI 积极贡献力量</h2>

<p>Android 欢迎您为 GSI 开发贡献自己的力量。您可以通过以下方式参与开发并帮助改进 GSI：</p>

<ul>
  <li><strong>创建 GSI 补丁程序</strong>。由于 <code><var>DESSERT</var>-gsi</code> 并<strong>不是</strong>开发分支，并且仅接受从 AOSP master 分支择优挑选的补丁程序，因此要提交 GSI 补丁程序，您必须：<ol>
      <li>将补丁程序提交到 <a href="https://android-review.googlesource.com" class="external">AOSP</a> master 分支。</li>
      <li>择优挑选要提交到 <code><var>DESSERT</var>-gsi</code> 的补丁程序。</li>
      <li>提交错误，以便让择优挑选的补丁程序接受审核。</li>
    </ol>
  </li>
  <li><strong>报告 GSI 错误</strong>或提出其他建议。查看<a href="/setup/contribute/report-bugs#platform">报告错误</a>中的说明，然后浏览或提交 GSI 错误（在“平台”表中查找“常规系统映像”）。</li>
</ul>

</body></html>