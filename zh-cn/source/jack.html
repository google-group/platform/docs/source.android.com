<html devsite><head>
    <title>使用 Jack 编译</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<aside class="warning"><strong>警告</strong>：截止到 <a href="https://android-developers.googleblog.com/2017/03/future-of-java-8-language-feature.html" class="external">2017 年 3 月 14 日公告</a>发布之时，Jack 工具链已被弃用。虽然您可以继续使用 Jack，但我们强烈建议您使用最新的 <a href="https://developer.android.com/studio/preview/index.html" class="external">Android Studio 预览版</a>，该版本可为内置到默认工具链中的 <a href="https://developer.android.com/studio/preview/features/java8-support.html" class="external">Java 8 语言功能</a>提供更好的支持。</aside>

<p>Jack 是一种 Android 工具链，用于将 Java 源代码编译成 Android dex 字节码。它取代了之前由 javac、ProGuard、jarjar 和 dx 等多种工具组成的 Android 工具链。由于 Jack 是 Android 6.x 的默认 Android 编译工具链，因此要使用 Jack，您只需使用标准的 Makefile 命令来编译源代码树或您的项目即可，无需进行其他任何操作。</p>

<h2 class="overview">Jack 简介</h2>
<p>Jack 工具链具有以下优势：</p>

<img src="./images/jack_overview.png" alt="Jack 概览"/>
<figcaption><strong>图 1.</strong> Jack 概览。</figcaption>

<ul>
<li><strong>完全开放的源代码</strong>：Jack 是在 AOSP 中提供的；我们欢迎用户贡献内容。</li>
<li><strong>提高编译速度</strong>：Jack 能够利用 dex 预处理、增量编译和 Jack 编译服务器来缩短编译时间。</li>
<li><strong>能够处理以下任务：压缩、混淆、重新打包和多 dex 处理</strong>。不再需要使用单独的软件包（如 ProGuard）。</li>
</ul>

<p>从 Android 7.0 起，Jack 支持使用 JaCoCo 衡量代码覆盖率。如需了解详情，请参阅<a href="https://android.googlesource.com/platform/prebuilts/sdk/+/master/tools/README-jack-code-coverage.md" class="external">使用 JaCoCo 衡量代码覆盖率</a>和 <a href="https://developer.android.com/preview/j8-jack.html" class="external">Java 8 语言功能</a>。</p>

<h3 id="jack_library">Jack 库格式</h3>

<p>Jack 具有自己的 .jack 文件格式，其中包含相应库的预编译 dex 代码，可实现更快速的编译（dex 预处理）。</p>

<img src="./images/jack_library.png" alt="Jack 库文件内容"/>
<figcaption><strong>图 2.</strong> Jack 库文件内容。</figcaption>

<h3 id="jill">Jill</h3>

<p>Jill 工具可将现有的 .jar 库转换为新的库格式，如下图所示。</p>

<img src="./images/jack_jill.png" alt="使用 Jill 导入 .jar 库"/>
<figcaption><strong>图 3.</strong> 导入现有 .jar 库的工作流程。</figcaption>

<h2 id="using_jack">Jack 编译服务器</h2>

<aside class="note"><strong>注意</strong>：以下说明仅适用于在 Android 6.x 中使用 Jack 的情况；如需关于在 Android 7.x 及更高版本中使用 Jack 的说明，请参阅 <a href="https://android.googlesource.com/platform/prebuilts/sdk/+/master/tools/README-jack-server.md" class="external">Jack 服务器文档</a>。</aside>

<p>首次使用 Jack 时，它会在您的计算机上启动一个本地 Jack 编译服务器。该服务器：</p>

<ul>
<li>能够实现内在加速，因为它可以避免在每次编译时都启动新的主机 JRE JVM、加载 Jack 代码、初始化 Jack 以及准备 JIT。此外，它还会在小规模编译期间（例如增量模式下）实现非常快的编译速度。</li>
<li>是短期内控制并行 Jack 编译数量的解决方案。该服务器可以避免计算机过载（内存或磁盘问题），因为它会限制并行编译的数量。</li>
</ul>

<p>如果没有任何编译工作，在空闲一段时间之后，Jack 服务器会自行关闭。它使用 localhost 接口上的两个 TCP 端口，因此无法从外部访问。您可以通过修改 <code>$HOME/.jack</code> 文件来修改所有参数（并行编译的数量、超时、端口号等）。</p>

<h3 id="home_jack_file">$HOME/.jack 文件</h3>

<p><code>$HOME/.jack</code> 文件包含以下针对 Jack 服务器变量的设置，采用纯 bash 语法：</p>

<ul>
<li><code>SERVER=true</code>：启用 Jack 的服务器功能。</li>
<li><code>SERVER_PORT_SERVICE=8072</code>：设置该服务器上用于编译的 TCP 端口号。</li>
<li><code>SERVER_PORT_ADMIN=8073</code>：设置该服务器上用于管理的 TCP 端口号。</li>
<li><code>SERVER_COUNT=1</code>：未使用。
</li><li><code>SERVER_NB_COMPILE=4</code>：设置允许的最大并行编译数量。</li>
<li><code>SERVER_TIMEOUT=60</code>：设置无编译工作时服务器在自行关闭之前必须等待的空闲秒数。</li>
<li><code>SERVER_LOG=${SERVER_LOG:=$SERVER_DIR/jack-$SERVER_PORT_SERVICE.log}</code>：设置服务器日志写入到的文件。默认情况下，此变量可被环境变量重载。</li>
<li><code>JACK_VM_COMMAND=${JACK_VM_COMMAND:=java}</code>：设置用于在主机上启动 JVM 的默认命令。默认情况下，此变量可被环境变量重载。</li>
</ul>

<h3 id="jack_troubleshooting">Jack 编译问题排查</h3>

<table>
<tbody><tr>
<th>问题</th>
<th>措施</th>
</tr>
<tr>
<td>您的计算机在编译期间无响应，或者 Jack 编译因“Out of memory error”（内存不足错误）而失败</td>
<td>您可以通过修改 <code>$HOME/.jack</code> 并将 <code>SERVER_NB_COMPILE</code> 改为较低的值来减少同时进行的 Jack 编译的数量，从而改善这种情况。</td>
</tr>
<tr>
<td>编译因“Cannot launch background server”（无法启动后台服务器）而失败</td>
<td>最可能的原因是您计算机上的 TCP 端口都被占用了。您可以通过修改 <code>$HOME/.jack</code>（<code>SERVER_PORT_SERVICE</code> 和 <code>SERVER_PORT_ADMIN</code> 变量）来更改端口。

<br /><br />如果上述方法无法解决问题，请报告该错误（务必附上您的编译日志和 <a href="#jack_log">Jack 服务器日志</a>）。要解决这种情况，请通过修改 <code>$HOME/.jack</code> 并将 <code>SERVER</code> 更改为 false 来停用 Jack 编译服务器。遗憾的是，这将大大降低编译速度，并可能会迫使您使用加载控制（<code>make</code> 的选项 <code>-l</code> ）启动 <code>make -j</code>。
</td>
</tr>
<tr>
<td>编译卡住了，没有任何进展</td>
<td>报告并提供以下信息（如果可能）：
<br />
<ul>
<li>卡住时所在的命令行。</li>
<li>此命令行的输出。</li>
<li>执行 <code>jack-admin server-stat</code> 获得的结果。</li>
<li><code>$HOME/.jack</code> 文件。</li>
<li><a href="#jack_log">Jack 服务器日志</a>（已转储服务器状态）的内容。要获取服务器日志，请执行以下操作：
<ul>
 <li>通过运行 <code>jack-admin list-server</code> 查找 Jack 后台服务器进程。</li>
 <li>向该服务器发送 <code>kill -3</code> 命令，将其状态转储到日志文件中。</li>
 </ul>
</li>
<li>执行 <code>ls -lR $TMPDIR/jack-$USER</code> 获得的结果。</li>
<li>运行 <code>ps j -U $USER</code> 获得的结果。</li>
</ul>

要解决这种情况，请使用 <code>jack-admin kill-server</code> 停止 Jack 后台服务器，然后移除临时目录（<code>/tmp</code> 或 <code>$TMPDIR</code>）的 <code>jack-$USER</code> 中包含的临时目录。
</td>
</tr>
<tr>
<td>其他问题</td>
<td>要报告错误或请求功能，请使用 <a href="http://b.android.com" class="external">http://b.android.com</a> 中的公开问题跟踪工具。使用 <a href="https://code.google.com/p/android/issues/entry?template=Jack%20bug%20report" class="external">Jack 工具错误报告</a>或 <a href="https://code.google.com/p/android/issues/entry?template=Jack%20feature%20request" class="external">Jack 工具功能请求</a>模板，并记得将 Jack 日志附到错误报告中。</td>
</tr>
</tbody></table>

<h3 id="jack_log">查找 Jack 日志</h3>
如果您曾针对 dist 目标运行 <code>make</code> 命令，则 Jack 日志位于 <code>$ANDROID_BUILD_TOP/out/dist/logs/jack-server.log</code> 中。如果没有，则您可以通过运行 <code>jack-admin server-log</code> 找到该日志。对于可重现的 Jack 错误，您可以通过设置以下变量来获取更详细的日志：<p></p>

<pre class="devsite-terminal devsite-click-to-copy">
export ANDROID_JACK_EXTRA_ARGS="--verbose debug --sanity-checks on -D sched.runner=single-threaded"
</pre>

<p>使用标准 Makefile 命令编译源代码树（或您的项目），并附上标准输出和错误。要移除详细的编译日志，请运行以下命令：</p>

<pre class="devsite-terminal devsite-click-to-copy">
unset ANDROID_JACK_EXTRA_ARGS
</pre>

<h3 id="jack_limitations">Jack 的使用限制</h3>

<ul>
<li>默认情况下，Jack 服务器为单用户模式，一台计算机上只能有一位用户使用。要支持更多用户，请为每位用户选择不同的端口号，并相应地调整 <code>SERVER_NB_COMPILE</code>。您还可以通过在 <code>$HOME/.jack</code> 中设置 <code>SERVER=false</code> 来停用 Jack 服务器。</li>
<li>当前的 <code>vm-tests-tf</code> 集成方案会导致 CTS 编译速度较慢。
</li><li>不支持字节码处理工具（如 JaCoCo）。</li>
</ul>

<h2 id="using_jack_features">使用 Jack</h2>

<p>Jack 支持 Java 编程语言 1.7，并集成了下述附加功能。</p>

<h3 id="predexing">dex 预处理</h3>

<p>在生成 Jack 库文件时，系统会生成该库的 .dex 文件并将其作为 dex 预处理文件存储在 .jack 库文件中。在进行编译时，Jack 会重复使用每个库中的 dex 预处理文件。所有库均经过 dex 预处理：</p>

<img src="./images/jack_predex.png" alt="包含 dex 预处理文件的 Jack 库"/>
<figcaption><strong>图 4.</strong> 包含 dex 预处理文件的 Jack 库。</figcaption>

<p>如果在编译过程中使用了压缩、混淆或重新打包功能，则 Jack 不会重复使用库的 dex 预处理文件。</p>

<h3 id="incremental_compilation">增量编译</h3>

<p>增量编译指的是，仅重新编译自上次编译后出现过更改的组件及其依赖项。当只有少数组件出现过更改时，进行增量编译可能比完整编译快得多。</p>

<p>增量编译默认处于未启用状态（当压缩、混淆、重新打包或旧版多 dex 处理功能启用后，增量编译会自动被停用）。要启用增量编译，请将以下行添加到您要进行增量编译的项目的 <code>Android.mk</code> 文件中：</p>

<pre class="devsite-click-to-copy">LOCAL_JACK_ENABLED := incremental</pre>

<aside class="note"><strong>注意</strong>：首次使用 Jack 编译项目时，如果某些依赖项未编译，请使用 <code>mma</code> 对其进行编译，之后您可以使用标准编译命令。</aside>

<h3 id="shrinking_and_obfuscation">压缩和混淆</h3>

<p>Jack 使用 proguard 配置文件来实现压缩和混淆功能。</p>

<p>常用选项包括：</p>

<ul>
  <li> <code>@</code>
  </li><li> <code>-include</code>
  </li><li> <code>-basedirectory</code>
  </li><li> <code>-injars</code>
  </li><li> <code>-outjars // only 1 output jar supported</code>
  </li><li> <code>-libraryjars</code>
  </li><li> <code>-keep</code>
  </li><li> <code>-keepclassmembers</code>
  </li><li> <code>-keepclasseswithmembers</code>
  </li><li> <code>-keepnames</code>
  </li><li> <code>-keepclassmembernames</code>
  </li><li> <code>-keepclasseswithmembernames</code>
  </li><li> <code>-printseeds</code>
</li></ul>

<p>压缩选项包括：</p>

<ul>
  <li><code>-dontshrink</code>
</li></ul>

<p>混淆选项包括：</p>

<ul>
  <li> <code>-dontobfuscate</code>
  </li><li> <code>-printmapping</code>
  </li><li> <code>-applymapping</code>
  </li><li> <code>-obfuscationdictionary</code>
  </li><li> <code>-classobfuscationdictionary</code>
  </li><li> <code>-packageobfuscationdictionary</code>
  </li><li> <code>-useuniqueclassmembernames</code>
  </li><li> <code>-dontusemixedcaseclassnames</code>
  </li><li> <code>-keeppackagenames</code>
  </li><li> <code>-flattenpackagehierarchy</code>
  </li><li> <code>-repackageclasses</code>
  </li><li> <code>-keepattributes</code>
  </li><li> <code>-adaptclassstrings</code>
</li></ul>

<p>忽略的选项包括：</p>

<ul>
  <li> <code>-dontoptimize // Jack does not optimize</code>
  </li><li> <code>-dontpreverify // Jack does not preverify</code>
  </li><li> <code>-skipnonpubliclibraryclasses</code>
  </li><li> <code>-dontskipnonpubliclibraryclasses</code>
  </li><li> <code>-dontskipnonpubliclibraryclassmembers</code>
  </li><li> <code>-keepdirectories</code>
  </li><li> <code>-target</code>
  </li><li> <code>-forceprocessing</code>
  </li><li> <code>-printusage</code>
  </li><li> <code>-whyareyoukeeping</code>
  </li><li> <code>-optimizations</code>
  </li><li> <code>-optimizationpasses</code>
  </li><li> <code>-assumenosideeffects</code>
  </li><li> <code>-allowaccessmodification</code>
  </li><li> <code>-mergeinterfacesaggressively</code>
  </li><li> <code>-overloadaggressively</code>
  </li><li> <code>-microedition</code>
  </li><li> <code>-verbose</code>
  </li><li> <code>-dontnote</code>
  </li><li> <code>-dontwarn</code>
  </li><li> <code>-ignorewarnings</code>
  </li><li> <code>-printconfiguration</code>
  </li><li> <code>-dump</code>
</li></ul>

<aside class="note"><strong>注意</strong>：其他选项会引发错误。</aside>

<h3 id="repackaging">重新打包</h3>

<p>Jack 使用 jarjar 配置文件来进行重新打包。虽然 Jack 与“rule”规则类型兼容，但与“zap”或“keep”规则类型不兼容。如果您需要使用“zap”或“keep”规则类型，请提交功能请求，并在其中说明您在应用中如何使用该功能。</p>

<h3 id="multidex_support">多 dex 处理支持</h3>

<p>Jack 支持本地多 dex 处理和旧版多 dex 处理。由于 dex 文件的方法数上限为 65K，因此方法数超过 65K 的应用必须拆分成多个 dex 文件。有关详情，请参阅<a href="http://developer.android.com/tools/building/multidex.html" class="external">构建方法数超过 65K 的应用</a>。</p>

</body></html>