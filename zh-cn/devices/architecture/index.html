<html devsite><head>
    <title>架构</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
Android 系统架构包含以下组件：
</p>

<img src="../images/ape_fwk_all.png"/>
<figcaption><strong>图 1.</strong> Android 系统架构</figcaption>

<ul>
<li>
    <strong>应用框架</strong>。应用框架最常被应用开发者使用。作为硬件开发者，您应该非常了解开发者 API，因为很多此类 API 都可以直接映射到底层 HAL 接口，并可提供与实现驱动程序相关的实用信息。
</li>
<li>
    <strong>Binder IPC</strong>。Binder 进程间通信 (IPC) 机制允许应用框架跨越进程边界并调用 Android 系统服务代码，这使得高级框架 API 能与 Android 系统服务进行交互。在应用框架级别，开发者无法看到此类通信的过程，但一切似乎都在“按部就班地运行”。
</li>
<li>
    <strong>系统服务</strong>。系统服务是专注于特定功能的模块化组件，例如窗口管理器、搜索服务或通知管理器。
    应用框架 API 所提供的功能可与系统服务通信，以访问底层硬件。Android 包含两组服务：“系统”（诸如窗口管理器和通知管理器之类的服务）和“媒体”（与播放和录制媒体相关的服务）。<em></em><em></em>
</li>
<li>
    <strong>硬件抽象层 (HAL)</strong>。HAL 可定义一个标准接口以供硬件供应商实现，这可让 Android 忽略较低级别的驱动程序实现。借助 HAL，您可以顺利实现相关功能，而不会影响或更改更高级别的系统。HAL 实现会被封装成模块，并会由 Android 系统适时地加载。有关详情，请参阅<a href="/devices/architecture/hal.html">硬件抽象层 (HAL)</a> 一文。
</li>
<li>
    <strong>Linux 内核</strong>。开发设备驱动程序与开发典型的 Linux 设备驱动程序类似。Android 使用的 Linux 内核版本包含几个特殊的补充功能，例如：Low Memory Killer（一种内存管理系统，可更主动地保留内存）、唤醒锁定（一种 <a href="https://developer.android.com/reference/android/os/PowerManager.html" class="external"><code>PowerManager</code></a> 系统服务）、Binder IPC 驱动程序以及对移动嵌入式平台来说非常重要的其他功能。这些补充功能主要用于增强系统功能，不会影响驱动程序开发。您可以使用任意版本的内核，只要它支持所需功能（如 Binder 驱动程序）即可。不过，我们建议您使用 Android 内核的最新版本。有关详情，请参阅<a href="/setup/building-kernels.html">编译内核</a>一文。
</li>
</ul>

<h2 id="hidl">HAL 接口定义语言 (HIDL)</h2>

<p>
    Android 8.0 重新设计了 Android 操作系统框架（在一个名为“Treble”<em></em>的项目中），以便让制造商能够以更低的成本更轻松、更快速地将设备更新到新版 Android 系统。在这种新架构中，HAL 接口定义语言（HIDL，发音为“hide-l”）指定了 HAL 和其用户之间的接口，让用户能够替换 Android 框架，而无需重新编译 HAL。
</p>

<aside class="note">
    <strong>注意</strong>：要详细了解 Treble 项目，请参阅博文 <a href="https://android-developers.googleblog.com/2017/05/here-comes-treble-modular-base-for.html" class="external">Treble 现已推出：Android 的模块化基础</a>和<a href="https://android-developers.googleblog.com/2018/05/faster-adoption-with-project-treble.html" class="external">借助 Treble 项目更快速地采用新系统</a>。
</aside>

<p>利用新的供应商接口，HIDL 将供应商实现（由芯片制造商编写的设备专属底层软件）与 Android 操作系统框架分离开来。供应商或 SOC 制造商构建一次 HAL，并将其放置在设备的 <code>/vendor</code> 分区中；框架可以在自己的分区中通过<a href="/devices/tech/ota/">无线下载 (OTA) 更新</a>进行替换，而无需重新编译 HAL。
</p>
<p>
    旧版 Android 架构与当前基于 HIDL 的架构的区别在于对供应商接口的使用：
</p>

<ul>
<li>
    Android 7.x 及更早版本中没有正式的供应商接口，因此设备制造商必须更新大量 Android 代码才能将设备更新到新版 Android 系统：<br /><br />

<img src="images/treble_blog_before.png"/>
<figcaption><strong>图 2.</strong> 旧版 Android 更新环境</figcaption>
</li>
<li>Android 8.0 及更高版本提供了一个稳定的新供应商接口，因此设备制造商可以访问 Android 代码中特定于硬件的部分，这样一来，设备制造商只需更新 Android 操作系统框架，即可跳过芯片制造商直接提供新的 Android 版本：<br /><br />

<img src="images/treble_blog_after.png"/>
<figcaption><strong>图 3.</strong> 当前 Android 更新环境</figcaption>
</li>
</ul>

<p>所有搭载 Android 8.0 及更高版本的新设备都可以利用这种新架构。为了确保供应商实现的前向兼容性，供应商接口会由<a href="/devices/tech/vts/index.html">供应商测试套件 (VTS)</a> 进行验证，该套件类似于<a href="/compatibility/cts/">兼容性测试套件 (CTS)</a>。您可以使用 VTS 在旧版 Android 架构和当前 Android 架构中自动执行 HAL 和操作系统内核测试。
</p>

<h2 id="resources">架构资源</h2>

<p>
    要详细了解 Android 架构，请参阅以下部分：
</p>

<ul>
<li>
    <a href="/devices/architecture/hal-types.html">HAL 类型</a>：提供了关于绑定式 HAL、直通 HAL、Same-Process (SP) HAL 和旧版 HAL 的说明。
</li>
<li>
    <a href="/devices/architecture/hidl/index.html">HIDL（一般信息）</a>：包含与 HAL 和其用户之间的接口有关的一般信息。
</li>
<li>
    <a href="/devices/architecture/hidl-cpp/index.html">HIDL (C++)</a>：包含关于为 HIDL 接口创建 C++ 实现的详情。
</li>
<li>
    <a href="/devices/architecture/hidl-java/index.html">HIDL (Java)</a>：包含关于 HIDL 接口的 Java 前端的详情。
</li>
<li>
    <a href="/devices/architecture/configstore/index.html">ConfigStore HAL</a>：提供了关于可供访问用于配置 Android 框架的只读配置项的 API 的说明。
</li>
<li>
    <a href="/devices/architecture/dto/index.html">设备树叠加层</a>：提供了关于在 Android 中使用设备树叠加层 (DTO) 的详情。
</li>
<li>
    <a href="/devices/architecture/vndk/index.html">供应商原生开发套件 (VNDK)</a>：提供了关于一组可供实现供应商 HAL 的供应商专用库的说明。
</li>
<li>
    <a href="/devices/architecture/vintf/index.html">供应商接口对象 (VINTF)</a>：提供了关于收集设备的相关信息并通过可查询 API 提供这些信息的对象的说明。</li>
<li>
    <a href="/security/selinux/images/SELinux_Treble.pdf">SELinux for Android 8.0</a>：提供了关于 SELinux 变更和自定义的详情。
</li>
</ul>

</body></html>