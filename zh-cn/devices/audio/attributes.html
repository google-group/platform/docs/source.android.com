<html devsite><head>
    <title>音频属性</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>

  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
音频播放器支持定义音频系统如何处理指定来源的导向、音量和焦点决策的属性。应用可以将属性附加到音频播放（例如流式传输服务播放的音乐或新电子邮件通知）上，然后将音频源的属性传递给框架，此时音频系统会使用这些属性做出混音决策并将系统状态通知给应用。
</p>

<aside class="note"><strong>注意</strong>：应用还可以将属性附加到音频录制（例如在视频录制中截取的音频）上，但此功能不会在公共 API 中提供。
</aside>

<p>
在 Android 4.4 及更早版本中，框架仅使用音频流类型做出混音决策。不过，基于流类型做出这些决策过于受限，无法在多个应用和设备上产生优质输出。例如，在移动设备上，某些应用（例如 Google 地图）基于 STREAM_MUSIC 流类型播放驾驶方向；但是，在采用投影仪模式（例如 Android Auto）的移动设备上，应用不能将驾驶方向与其他媒体流混合在一起播放。
</p>

<p>
应用可以使用<a href="http://developer.android.com/reference/android/media/AudioAttributes.html" class="external">音频属性 API</a> 为音频系统提供有关特定音频源的详细信息，包括用法（播放来源的原因）、内容类型（播放来源的类型）、标记（来源的播放方式）以及上下文（Android 9 中的新变化）。语法如下所示：
</p>

<pre class="prettyprint">
AudioAttributes {
    mUsage
    mContentType
    mSource
    mFlags
    mTags / mFormattedTags / mBundle    (key value pairs)
}
</pre>

<ul>
  <li><strong>用法</strong>。指定播放来源的原因，并控制导向、焦点和音量决策。</li>
  <li><strong>内容类型</strong>。指定播放来源的类型（音乐、电影、语音、发音、未知）。</li>
  <li><strong>上下文</strong>。提取到 Audio HAL 的用法值。</li><li><strong>标记</strong>。指定来源的播放方式。包括对可听性强制执行（一些国家/地区要求发出相机快门提示音）和硬件音频/视频同步的支持。</li>
</ul>

<p>
对于动态处理，应用必须区分电影、音乐和语音内容。关于数据本身的信息也可能非常重要，例如响度和峰值采样值。
</p>

<h2 id="using">使用属性</h2>

<p>
用法指定了在何种上下文中使用相关流类型，并提供有关播放相应声音的原因以及该声音用法的信息。用法信息比流类型信息更丰富，并且可让平台或导向策略优化音量或导向决策。
</p>

<p>
为任何实例提供以下用法值之一：
</p>

<ul>
<li>USAGE_UNKNOWN</li>
<li>USAGE_MEDIA</li>
<li>USAGE_VOICE_COMMUNICATION</li>
<li>USAGE_VOICE_COMMUNICATION_SIGNALLING</li>
<li>USAGE_ALARM</li>
<li>USAGE_NOTIFICATION</li>
<li>USAGE_NOTIFICATION_TELEPHONY_RINGTONE</li>
<li>USAGE_NOTIFICATION_COMMUNICATION_REQUEST</li>
<li>USAGE_NOTIFICATION_COMMUNICATION_INSTANT</li>
<li>USAGE_NOTIFICATION_COMMUNICATION_DELAYED</li>
<li>USAGE_NOTIFICATION_EVENT</li>
<li>USAGE_ASSISTANCE_ACCESSIBILITY</li>
<li>USAGE_ASSISTANCE_NAVIGATION_GUIDANCE</li>
<li>USAGE_ASSISTANCE_SONIFICATION</li>
<li>USAGE_GAME</li>
<li>USAGE_VIRTUAL_SOURCE</li>
<li>USAGE_ASSISTANT</li>
</ul>

<p>
音频属性用法值是互斥的。有关示例，请参阅 <code><a href="http://developer.android.com/reference/android/media/AudioAttributes.html#USAGE_MEDIA" class="external">USAGE_MEDIA</a></code> 和 <code><a href="http://developer.android.com/reference/android/media/AudioAttributes.html#USAGE_ALARM" class="external">USAGE_ALARM</a></code> 定义；有关例外情况，请参阅 <code><a href="http://developer.android.com/reference/android/media/AudioAttributes.Builder.html" class="external">AudioAttributes.Builder</a></code> 定义。
</p>

<h2 id="content-type">内容类型</h2>

<p>
内容类型定义了声音是什么，并指明内容的常规类别，如电影、语音或提示音/铃声。音频框架使用内容类型信息来选择性地配置音频处理后块。尽管提供内容类型是可选的，但只要内容类型已知，您就应该包含类型信息，例如针对电影流式传输服务使用 <code>CONTENT_TYPE_MOVIE</code>，或针对音乐播放应用使用 <code>CONTENT_TYPE_MUSIC</code>。
</p>

<p>
为任何实例提供以下内容类型值之一：
</p>

<ul>
<li><code>CONTENT_TYPE_UNKNOWN</code>（默认）</li>
<li><code>CONTENT_TYPE_MOVIE</code></li>
<li><code>CONTENT_TYPE_MUSIC</code></li>
<li><code>CONTENT_TYPE_SONIFICATION</code></li>
<li><code>CONTENT_TYPE_SPEECH</code></li>
</ul>

<p>
音频属性内容类型值是互斥的。有关内容类型的详细信息，请参阅<a href="http://developer.android.com/reference/android/media/AudioAttributes.html" class="external">音频属性 API</a>。
</p>

<h2 id="contexts">上下文</h2>

<p>
Android 中的每段声音都由相应的应用和声音生成的原因来识别；Android 设备会使用这些信息来确定如何呈现声音。在 Android 8.x 及更低版本中，应用可以使用旧版流类型（如 <code>AudioSystem.STREAM_MUSIC</code>）或 <code>AudioAttributes</code> 报告声音生成的原因。在 Android 9 中，<code>AudioAttributes.usage</code> 值在 HAL 级别被提取为上下文<em></em>。
</p>

<table>
<thead>
<tr>
<th>HAL 音频上下文</th>
<th>AudioAttributes 用法</th>
</tr>
</thead>
<tbody>
<tr>
<td>MUSIC</td>
<td>MEDIA</td>
</tr>
<tr>
<td>VOICE_COMMAND</td>
<td>USAGE_ASSISTANT</td>
</tr>
<tr>
<td>NAVIGATION</td>
<td>ASSISTANCE_NAVIGATION_GUIDANCE</td>
</tr>
<tr>
<td>CALL</td>
<td>VOICE_COMMUNICATION</td>
</tr>
<tr>
<td>RINGTONE</td>
<td>NOTIFICATION_RINGTONE</td>
</tr>
<tr>
<td>NOTIFICATION</td>
<td>NOTIFICATION</td>
</tr>
<tr>
<td>ALARM</td>
<td>ALARM</td>
</tr>
<tr>
<td>SYSTEM_SOUND</td>
<td>ASSISTANCE_SONIFICATION</td>
</tr>
<tr>
<td>UNKNOWN</td>
<td>UNKNOWN</td>
</tr>
</tbody>
</table>

<p>
您可以为任何实例提供以下 <code>CONTEXT_NUMBER</code> 值之一：
</p>

<ul>
<li>MUSIC_CONTEXT           // 音乐播放</li>
<li>NAVIGATION_CONTEXT      // 导航路线</li>
<li>VOICE_COMMAND_CONTEXT   // 语音指令会话</li>
<li>CALL_RING_CONTEXT       // 语音呼叫响铃</li>
<li>CALL_CONTEXT            // 语音通话</li>
<li>ALARM_CONTEXT           // Android 闹钟铃声</li>
<li>NOTIFICATION_CONTEXT    // 通知</li>
<li>SYSTEM_SOUND_CONTEXT    // 用户交互声音（按钮点击声音等）</li>
</ul>

<h2 id="flags">标记</h2>

<p>
标记会指定音频框架如何对音频播放应用效果。请为一个实例提供以下一个或多个标记：
</p>

<ul>
  <li><code>FLAG_AUDIBILITY_ENFORCED</code>。向系统发出请求，以确保声音的可听性。用于满足旧版 <code>STREAM_SYSTEM_ENFORCED</code> 的需求（如强制要求发出相机快门提示音）。
  </li>
  <li><code>HW_AV_SYNC</code>。向系统发出请求，以选择支持硬件 A/V 同步的输出流。</li>
</ul>

<p>
音频属性标记是非专有的（可以组合使用）。要详细了解这些标记，请参阅<a href="http://developer.android.com/reference/android/media/AudioAttributes.html" class="external">音频属性 API</a>。
</p>

<h2 id="example">示例</h2>

<p>
在以下示例中，<code>AudioAttributes.Builder</code> 定义了将由一个新的 <code>AudioTrack</code> 实例使用的 <code>AudioAttributes</code>：</p>

<pre class="prettyprint">
AudioTrack myTrack = new AudioTrack(
  new AudioAttributes.Builder()
 .setUsage(AudioAttributes.USAGE_MEDIA)
    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
    .build(),
  myFormat, myBuffSize, AudioTrack.MODE_STREAM, mySession);
</pre>

<h2 id="compatibility">兼容性</h2>

<p>
应用开发者在为 Android 5.0 或更高版本创建或更新应用时，应使用音频属性。不过，应用并非必须要利用这些属性；它们可以仅处理旧版流类型，或一直不知道属性（即，对其播放的内容一无所知的常规媒体播放器）。
</p>

<p>
在此类情况下，框架通过自动将旧版音频流类型转换为音频属性，保持对旧设备和 Android 版本的向后兼容性。不过，框架不会针对各个设备、制造商或 Android 版本强制要求或保证具备这种映射。
</p>

<p>
兼容性映射：
</p>

<table>
<tbody><tr>
  <th>Android 5.0 及更高版本</th>
  <th>Android 4.4 及更早版本</th>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SPEECH</code><br />
  <code>USAGE_VOICE_COMMUNICATION</code>
  </td>
  <td>
  <code>STREAM_VOICE_CALL</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_ASSISTANCE_SONIFICATION</code>
  </td>
  <td>
  <code>STREAM_SYSTEM</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_NOTIFICATION_RINGTONE</code>
  </td>
  <td>
  <code>STREAM_RING</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_MUSIC</code><br />
  <code>USAGE_UNKNOWN</code><br />
  <code>USAGE_MEDIA</code><br />
  <code>USAGE_GAME</code><br />
  <code>USAGE_ASSISTANCE_ACCESSIBILITY</code><br />
  <code>USAGE_ASSISTANCE_NAVIGATION_GUIDANCE</code>
  </td>
  <td>
  <code>STREAM_MUSIC</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_ALARM</code>
  </td>
  <td>
  <code>STREAM_ALARM</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_NOTIFICATION</code><br />
  <code>USAGE_NOTIFICATION_COMMUNICATION_REQUEST</code><br />
  <code>USAGE_NOTIFICATION_COMMUNICATION_INSTANT</code><br />
  <code>USAGE_NOTIFICATION_COMMUNICATION_DELAYED</code><br />
  <code>USAGE_NOTIFICATION_EVENT</code>
  </td>
  <td>
  <code>STREAM_NOTIFICATION</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SPEECH</code>
  </td>
  <td>
  (@hide)<code> STREAM_BLUETOOTH_SCO</code>
  </td>
</tr>
<tr>
  <td>
  <code>FLAG_AUDIBILITY_ENFORCED</code>
  </td>
  <td>
  (@hide)<code> STREAM_SYSTEM_ENFORCED</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_VOICE_COMMUNICATION_SIGNALLING</code>
  </td>
  <td>
  (@hide)<code> STREAM_DTMF</code>
  </td>
</tr>
</tbody></table>

<aside class="note"><strong>注意</strong>：@hide 流在框架内部使用，不是公共 API 的一部分。
</aside>

<h2 id="deprecated">已弃用的流类型</h2>

<p>
Android 9 弃用了以下用于汽车的流类型：
</p>

<ul>
<li>STREAM_DEFAULT</li>
<li>STREAM_VOICE_CALL</li>
<li>STREAM_SYSTEM</li>
<li>STREAM_RING</li>
<li>STREAM_MUSIC</li>
<li>STREAM_ALARM</li>
<li>STREAM_NOTIFICATION</li>
<li>STREAM_BLUETOOTH_SCO</li>
<li>STREAM_SYSTEM_ENFORCED</li>
<li>STREAM_DTMF</li>
<li>STREAM_TTS</li>
<li>STREAM_ACCESSIBILITY</li>
</ul>

<p>有关详情，请参阅<a href="/devices/automotive/audio">汽车音频</a>。
</p>

</body></html>