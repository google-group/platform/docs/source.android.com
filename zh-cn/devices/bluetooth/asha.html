<html devsite><head>
    <title>使用蓝牙 LE 的助听器音频支持</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
    <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License.  You may obtain a
      copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
      License for the specific language governing permissions and limitations
      under the License.
    -->

    <p>
      使用通过蓝牙低功耗 (BLE) 进行通信的连接导向型 L2CAP 通道 (CoC)，有助于改进助听设备 (HA) 在 Android 移动设备上的无障碍功能。CoC 使用多个音频数据包的弹性缓冲区来维持稳定的音频流（即使存在数据包丢失的情况也是如此）。该缓冲区可提供助听设备的音频质量，但会产生延迟。
    </p>

    <p>
      CoC 的设计参考了<a href="https://www.bluetooth.com/specifications/bluetooth-core-specification">蓝牙核心规范版本 5</a> (BT)。为了与核心规范保持一致，本页面上的所有多字节值都应以小端字节序的形式读取。
    </p>

    <h2 id="terminology">术语</h2>

      <ul>
        <li>
          <strong>中央设备</strong> - 通过蓝牙扫描广告的 Android 设备。
        </li>
        <li>
          <strong>外围设备</strong> - 通过蓝牙发送广告包的助听器。
        </li>
      </ul>

    <h2 id="network-topology-and-system-architecture">
      网络拓扑和系统架构
    </h2>

      <p>
        针对助听器使用 CoC 时，网络拓扑会假设存在一个中央设备和两个外围设备（一个在左侧，一个在右侧），如<strong>图 1</strong> 所示。蓝牙音频系统会将左右外围设备分别视为一个音频接收器。如果由于单耳选配或连接中断而导致某个外围设备缺失，则中央设备会混合左右声道，并将音频传输到剩余的那个外围设备。如果中央设备与这两个外围设备之间的连接均中断，则中央设备会认为指向音频接收器的链接发生中断。在这些情况下，中央设备会将音频路由到其他输出设备。
      </p>

      <p><img src="/devices/bluetooth/images/bt_asha_topology.png"/><br />
        <strong>图 1. </strong> 用于使用支持 BLE 的 CoC 将助听器与 Android 移动设备配对的拓扑
      </p>

      <p>
        如果中央设备未将音频数据流式传输到外围设备，且可以保持 BLE 连接，那么中央设备应该不会与外围设备断开连接。保持连接可以与位于外围设备上的 GATT 服务器进行数据通信。
      </p>

      <aside class="note">
        <strong>注意</strong>：中央设备和外围设备之间没有音频反向链接。在通话期间，中央麦克风用于语音输入。
      </aside>

      <p>
        在配对和连接助听设备时，中央设备应该：
      </p>

      <ul>
        <li>
          跟踪最近配对的左右外围设备。这两个外围设备应被视为音频接收器。
        </li>
        <li>
          如果存在有效配对，则假设这些外围设备正在使用中。当连接中断时，中央设备应尝试与已配对的设备建立连接或重新建立连接。
        </li>
        <li>
          如果删除了配对，则假设这些外围设备已不在使用中状态。
        </li>
      </ul>

      <p>
        在上述情况中，配对是指在操作系统中使用给定的 UUID 和左/右指示符注册一组助听器的操作，而不是指蓝牙配对流程。<em></em>
      </p>

    <h2 id="system-requirements">系统要求</h2>

      <p>
        要正确实施 CoC 以获得良好的用户体验，中央设备和外围设备中的蓝牙系统应该：</p>

      <ul>
        <li>
          实现兼容的 BT 4.2 或更高版本的控制器</li>
        <li>
          支持至少 2 个同步 LE 链路（包含<a href="#audio-packet-format-and-timing">音频数据包格式和时间设置</a>中所述的参数）。
        </li>
        <li>
          拥有基于 LE 信用的流控制 [BT 第 3 卷，A 部分，第 10.1 节]。
          设备应该在 CoC 上支持至少 240 个字节的 MTU 和 MPS 大小，并且最多能够缓冲 8 个数据包。
        </li>
        <li>
          具有 LE 数据长度扩展 [BT 第 6 卷，B 部分，第 5.1.9 节]，负载至少为 87 个字节。建议数据长度至少为 250 个字节。
        </li>
        <li>
          让中央设备支持 HCI LE 连接更新命令，并遵循非零 minimum_CE_Length 参数。
        </li>
        <li>
          使用<a href="#audio-packet-format-and-timing">音频数据包格式和时间设置</a>中的连接时间间隔和负载大小，保持与两个不同外围设备之间的两个 LE CoC 连接的数据吞吐量。
        </li>
        <li>
          让外围设备将 <code>LL_LENGTH_REQ</code> 或 <code>LL_LENGTH_RSP</code> 帧中的 <code>MaxRxOctets</code> 和 <code>MaxRxTime</code> 参数设置为这些规范所需的最小必需值。这样，中央设备可以在计算接收帧所需的时间长度时优化其时间调度程序。
        </li>
      </ul>

      <p>
        外围设备和中央设备可以实现 BT 5 中指定的 2 Mbit PHY。中央设备应该同时在 1 Mbit 和 2 Mbit PHY 上支持高达 64 kbit/s 的音频链路，但可以选择将对需要高于 64 kbit/s 的链路的支持限制为 2 Mbit PHY，以便改善与其他 2.4 GHz 设备的共存性。请勿使用 BLE 远程 PHY。
      </p>

      <p>
        CoC 使用标准蓝牙机制实现链路层加密和跳频。
      </p>

    <h2 id="asha-gatt-services">ASHA GATT 服务</h2>

      <p>
        外围设备应实现下文所述的 Audio Streaming for Hearing Aid (ASHA) GATT 服务器服务。外围设备应在一般可检测模式下播发此服务，以便中央设备识别音频接收器。任何 LE 音频流式传输操作都需要加密。BLE 音频流式传输包含以下特征：
      </p>

      <table>
        <tbody><tr>
          <th>特征</th>
          <th>属性</th>
          <th>说明</th>
        </tr>
        <tr>
          <td>ReadOnlyProperties</td>
          <td>读取</td>
          <td>请参见 <a href="#readonlyproperties">ReadOnlyProperties</a>。</td>
        </tr>
        <tr>
          <td>AudioControlPoint</td>
          <td>写入（无响应）</td>
          <td>
            音频流的控制点。请参见 <a href="#audiocontrolpoint">AudioControlPoint</a>。
          </td>
        </tr>
        <tr>
          <td>AudioStatusPoint</td>
          <td>读取/通知</td>
          <td>
            音频控制点的状态报告字段。操作码是：
            <ul>
              <li><strong>0</strong> - 状态正常</li>
              <li><strong>-1</strong> - 未知命令</li>
              <li><strong>-2</strong> - 非法参数</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>Volume</td>
          <td>写入（无响应）</td>
          <td>
            介于 -128 和 0 之间的字节，表示以分贝为单位的音量。-128 应解析为静音。在轨间流式传输正弦音调的 0 dB 应表示助听器上的 100 dBSPL 输入等效值。中央设备应按标称全标度进行流式传输，并使用此变量在外围设备中设置所需的表示级别。
          </td>
        </tr>
        <tr>
          <td>LE_PSM</td>
          <td>读取</td>
          <td>
            要用于连接声道的 PSM。将从动态范围中挑选 [BT 第 3 卷，A 部分，第 4.22 节]
          </td>
        </tr>
      </tbody></table>

      <p>分配给服务和特征的 UUID：</p>

      <p><strong>服务 UUID</strong>：<code>{0xFDF0}</code></p>

      <table>
        <tbody><tr>
          <th>特征</th>
          <th>UUID</th>
        </tr>
        <tr>
          <td>ReadOnlyProperties</td>
          <td><code>{6333651e-c481-4a3e-9169-7c902aad37bb}</code></td>
        </tr>
        <tr>
          <td>AudioControlPoint</td>
          <td><code>{f0d4de7e-4a88-476c-9d9f-1937b0996cc0}</code></td>
        </tr>
        <tr>
          <td>AudioStatus</td>
          <td><code>{38663f1a-e711-4cac-b641-326b56404837}</code></td>
        </tr>
        <tr>
          <td>Volume</td>
          <td><code>{00e4ca9e-ab14-41e4-8823-f9e70c7e91df}</code></td>
        </tr>
        <tr>
          <td>LE_PSM</td>
          <td><code>{2d410339-82b6-42aa-b34e-e2e01df8cc1a}</code></td>
        </tr>
      </tbody></table>

      <p>
        除了 ASHA GATT 服务外，外围设备还应实现设备信息服务，以便中央设备检测外围设备的制造商名称和设备名称。
      </p>

      <h3 id="readonlyproperties">ReadOnlyProperties</h3>

        <p>ReadOnlyProperties 具有以下值：</p>

        <table>
          <tbody><tr>
            <th>字节</th>
            <th>说明</th>
          </tr>
          <tr>
            <td>0</td>
            <td>版本 - 必须为 0x01</td>
          </tr>
          <tr>
            <td>1</td>
            <td>请参见 <a href="#devicecapabilities">DeviceCapabilities</a>。</td>
          </tr>
          <tr>
            <td>2-9</td>
            <td>请参见 <a href="#hisyncid">HiSyncId</a>。</td>
          </tr>
          <tr>
            <td>10</td>
            <td>请参见 <a href="#featuremap">FeatureMap</a><strong>。</strong></td>
          </tr>
          <tr>
            <td>11-12</td>
            <td>
              RenderDelay。这是从外围设备接收音频帧到外围设备呈现输出所经过的时间（以毫秒为单位）。这些字节可用于延迟视频以便与音频同步。
            </td>
          </tr>
          <tr>
            <td>13-14</td>
            <td>
              PreparationDelay。这是在发出启动命令（例如加载编解码器）后，外围设备呈现音频所需的时间（以毫秒为单位）。中央设备可以使用 PreparationDelay 来延迟短消息的音频播放。
            </td>
          </tr>
          <tr>
            <td>15-16</td>
            <td>
              支持的<a href="#codec-ids">编解码器 ID</a>。这是受支持的编解码器 ID 的位掩码。位元位置中的 1 对应于支持的编解码器。所有其他位均应设置为 0。
            </td>
          </tr>
        </tbody></table>

          <h4 id="devicecapabilities">DeviceCapabilities</h4>

            <table>
              <tbody><tr>
                <th>位</th>
                <th>说明</th>
              </tr>
              <tr>
                <td>0</td>
                <td>设备端（左：0，右：1）。</td>
              </tr>
              <tr>
                <td>1</td>
                <td>
                  单声道 (0) / 双声道 (1)。指示设备是独立设备并接收单声道数据还是设备集的一部分。
                </td>
              </tr>
              <tr>
                <td>2-7</td>
                <td>已保留（设为 0）。</td>
              </tr>
            </tbody></table>

          <h4 id="hisyncid">HiSyncID</h4>

            <table>
              <tbody><tr>
                <th>字节</th>
                <th>说明</th>
              </tr>
              <tr>
                <td>0-1</td>
                <td>制造商的 ID。</td>
              </tr>
              <tr>
                <td>2-7</td>
                <td>
                  用于识别助听器组的唯一 ID。必须在左侧和右侧外围设备上将此 ID 设置为相同的值。
                </td>
              </tr>
            </tbody></table>

          <h4 id="featuremap">FeatureMap</h4>

            <table>
              <tbody><tr>
                <th>位</th>
                <th>说明</th>
              </tr>
              <tr>
                <td>0</td>
                <td>是否支持 LE CoC 音频流式传输（是/否）。</td>
              </tr>
              <tr>
                <td>1-7</td>
                <td>已保留（设为 0）。</td>
              </tr>
            </tbody></table>

          <h4 id="codec-ids">编解码器 ID</h4>

            <p>
              如果设置了位，则支持这个特定的编解码器。
            </p>

            <table>
              <tbody><tr>
                <th>位编号</th>
                <th>编解码器和采样率</th>
                <th>所需的比特率</th>
                <th>帧时间</th>
                <th>在中央设备 (C) 或外围设备 (P) 上必须提供</th>
              </tr>
              <tr>
                <td>0</td>
                <td>保留</td>
                <td>保留</td>
                <td>保留</td>
                <td>保留</td>
              </tr>
              <tr>
                <td>1</td>
                <td>G.722 @ 16 kHz</td>
                <td>64 kbit/s</td>
                <td>变量</td>
                <td>C 和 P</td>
              </tr>
              <tr>
                <td>2</td>
                <td>G.722 @ 24 kHz</td>
                <td>96 kbit/s</td>
                <td>变量</td>
                <td>C</td>
              </tr>
              <tr>
              </tr><tr>
                <td colspan="5">
                  3-15 已保留。<br />
                  0 也已保留。
                </td>
              </tr>
            </tbody></table>

        <h3 id="audiocontrolpoint">AudioControlPoint</h3>

          <p>
            当 LE CoC 关闭时，无法使用该控制点。有关过程说明，请参阅<a href="#starting-and-stopping-an-audio-stream">启动和停止音频流</a>。
          </p>

          <table>
            <tbody><tr>
              <th>运算码</th>
              <th>参数</th>
              <th>说明</th>
            </tr>
            <tr>
              <td>1 <code>«Start»</code></td>
              <td>
                <ul>
                  <li><code>uint8_t codec</code></li>
                  <li><code>uint8_t audiotype</code></li>
                  <li><code>int8_t volume</code></li>
                </ul>
              </td>
              <td>
                指示外围设备重置编解码器并开始播放第 0 帧。编解码器字段指示要用于这次播放的编解码器 ID 的位编号。<br /><br />
                音频类型位字段指示数据流中存在的音频类型：
                <ul>
                  <li><strong>0</strong> - 未知</li>
                  <li><strong>1</strong> - 铃声</li>
                  <li><strong>2</strong> - 通话</li>
                  <li><strong>3</strong> - 媒体</li>
                </ul>
                在收到 <code>«Stop»</code> 操作码之前，外围设备不得请求连接更新。
              </td>
            </tr>
            <tr>
              <td>2 <code>«Stop»</code></td>
              <td>无</td>
              <td>
                指示外围设备设备停止呈现音频。若要再次呈现音频，应在这次停止后启动新的音频设置序列。外围设备可以根据此命令请求连接更新。
              </td>
            </tr>
          </tbody></table>

    <h2 id="advertisements-for-asha-gatt-service">
      ASHA GATT 服务的广告
    </h2>

      <p>
        <a href="#asha-gatt-services">服务 UUID</a> 必须位于广告数据包中。外围设备必须在广告或扫描响应帧中包含服务数据：
      </p>

      <table>
        <tbody><tr>
          <th>字节偏移</th>
          <th>名称</th>
          <th>说明</th>
        </tr>
        <tr>
          <td>0</td>
          <td>广告长度</td>
          <td>&gt;= 0x09</td>
        </tr>
        <tr>
          <td>1</td>
          <td>广告类型</td>
          <td>0x16（服务数据 - 16 位 UUID）</td>
        </tr>
        <tr>
          <td>2-3</td>
          <td>服务 UUID</td>
          <td>
            0xFDF0（小端字节序）<br /><br />
            <strong>注意</strong>：这是一个临时 ID。
          </td>
        </tr>
        <tr>
          <td>4</td>
          <td>协议版本</td>
          <td>0x01</td>
        </tr>
        <tr>
          <td>5</td>
          <td>功能</td>
          <td>
            <ul>
              <li><strong>0</strong> - 左 (0) 或右 (1) 侧</li>
              <li><strong>1</strong> - 单 (0) 或双 (1) 设备。</li>
              <li>
                <strong>2-7</strong> - 保留。这些位必须为零。
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>6-9</td>
          <td>截断的 <a href="#hisyncid">HiSyncID</a></td>
          <td>
            <a href="#hisyncid">HiSyncId</a> 的四个最不重要的字节。
          </td>
        </tr>
      </tbody></table>

      <p>
        外围设备必须具有可指示助听器名称的<strong>完整本地名称</strong>数据类型。此名称将用在移动设备的界面中，以便用户可以选择正确的设备。此名称不应指示左/右声道，因为该信息在 <a href="#devicecapabilities">DeviceCapabilities</a> 中提供。
      </p>

      <p>
        如果外围设备将名称和 ASHA 服务数据类型放在相同的帧类型（ADV 或 SCAN RESP）中，则这两种数据类型应显示在同一个帧中。这可让移动设备扫描器在同一个扫描结果中获得这两项数据。
      </p>

      <p>
        在初始配对期间，外围设备必须以足够快的速率进行播发，才能让移动设备快速发现外围设备并与它们绑定。
      </p>

    <h2 id="synchronizing-left-and-right-peripheral-devices">同步左右外围设备</h2>

      <p>
        要在 Android 移动设备上使用蓝牙，外围设备应负责确保这些设备已同步。左右外围设备上的播放操作需要保持同步。两个外围设备必须同时播放相应来源的音频样本。
      </p>

      <p>
        外围设备可以使用附加到每个音频负载数据包前面的序列号来同步它们的时间。中央设备将保证打算在每个外围设备上同时播放的音频数据包具有相同的序列号。在每个音频数据包播放完毕之后，该序列号都会递增 1。每个序列号的长度都是 8 位，因此在播放完 256 个音频数据包之后，序列号将会重复。由于每次连接的每个音频数据包大小和采样率是固定的，因此两个外围设备可以推断出相对播放时间。要详细了解音频数据包，请参阅<a href="#audio-packet-format-and-timing">音频数据包格式和时间设置</a>。
      </p>

    <h2 id="audio-packet-format-and-timing">音频数据包格式和设置</h2>

      <p>
        将音频帧（样本块）打包成数据包可让助听器从链路层定时锚点推断时间。为了简化实施：
      </p>

      <ul>
        <li>
          音频帧应一律与连接时间间隔相匹配。
          例如，如果连接时间间隔为 15 毫秒且采样率为 1 kHz，则音频帧应包含 240 个样本。
        </li>
        <li>
          无论帧时间或连接时间间隔为何，系统中的采样率都限制为 8kHz 的倍数，以便帧中包含的样本个数始终为整数。
        </li>
        <li>
          序列字节应该附加到音频帧前面。字节序列应按环绕进行计数，并允许外围设备检测缓冲区不匹配或下溢的情况。
        </li>
        <li>
          音频帧始终都应适合单个 LE 数据包。音频帧应作为单独的 L2CAP 数据包发送。LE LL PDU 的大小应为：<br />
            <em>音频负载大小 + 1（序列计数器）+ 6（L2CAP 标头为 4，SDU 为 2）</em>
        </li>
        <li>
          连接事件始终都应足够大，以包含 2 个音频数据包和 2 个空数据包，供 ACK 预留用于重新传输的带宽。
        </li>
      </ul>

      <p>
        为了给中央设备提供一些灵活性，未指定 G.722 数据包长度。G.722 数据包长度可以根据中央设备设置的连接时间间隔而改变。
      </p>

      <p>
        对于外围设备支持的所有编解码器，外围设备都应支持下面的连接参数。这是中央设备可以实现的配置列表（此列表并未包含所有配置）。
      </p>

      <table>
        <tbody><tr>
          <th>编解码器</th>
          <th>比特率</th>
          <th>连接时间间隔</th>
          <th>CE 长度 (1/2 Mbit)</th>
          <th>音频负载大小</th>
        </tr>
        <tr>
          <td>G.722 @ 16 kHz</td>
          <td>64 kbit/s</td>
          <td>10 毫秒</td>
          <td>2500 / 2500 us</td>
          <td>80 个字节</td>
        </tr>
        <tr>
          <td>G.722 @ 16 kHz</td>
          <td>64 kbit/s</td>
          <td>20 毫秒</td>
          <td>5000/3750 us</td>
          <td>160 个字节</td>
        </tr>
        <tr>
          <td>G.722 @ 24 kHz</td>
          <td>96 kbit/s</td>
          <td>10 毫秒</td>
          <td>3750 / 2500 us</td>
          <td>120 个字节</td>
        </tr>
        <tr>
          <td>G.722 @ 24 kHz</td>
          <td>96 kbit/s</td>
          <td>20 毫秒</td>
          <td>5000 / 3750 us</td>
          <td>240 个字节</td>
        </tr>
      </tbody></table>

    <h2 id="starting-and-stopping-an-audio-stream">
      启动和停止音频流
    </h2>

      <aside class="note">
        <strong>注意</strong>：此部分基于音频帧缓冲区深度为 6 的模拟。在大多数丢包情景中，此深度足以用来防止外围设备下溢。设置此深度后，系统中的网络延迟将为连接时间间隔的六倍。为了减少延迟，建议首选较短的连接时间间隔。
      </aside>

      <p>
        在启动音频流之前，中央设备会查询外围设备，并确立最高质量标准的编解码器。接下来，音频流设置会按顺序进行以下操作：</p>

      <ol>
        <li>读取 PSM 和 PreparationDelay（可选）。</li>
        <li>
          打开 CoC L2CAP 通道 - 外围设备最初应授予 8 个 Credit。
        </li>
        <li>
          发出连接更新命令以将链接切换到所选编解码器需要的参数。
        </li>
        <li>
          中央设备和外围设备主机等待更新完成事件。
        </li>
        <li>
          重启音频编码器，并将数据包序列计数重置为 0。
          在 AudioControlPoint 上发出带有相关参数的 <code>«Start»</code> 命令。在音频流式传输期间，即使当前副本延迟可能为非零值，副本也应在每个连接事件中都可用。
        </li>
        <li>
          外围设备从其内部队列获取第一个音频数据包（序列号为 0），并播放该音频数据包。
        </li>
      </ol>

      <p>
        中央设备会发出 <strong>«Stop»</strong> 命令来关闭音频流。关闭音频流后，外围设备可以要求更宽泛的连接参数。再次浏览上面的序列即可重启音频流式传输。如果中央设备未在流式传输音频，那么它仍然应该为 GATT 服务保持 LE 连接。
      </p>

      <p>
        外围设备不得向中央设备发出连接更新命令。为了节省功耗，中央设备可以在未流式传输音频时向外围设备发出连接更新命令。
      </p>

</body></html>