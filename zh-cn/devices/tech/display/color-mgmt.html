<html devsite><head>
    <title>颜色管理</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p>Android 8.1 新增了对颜色管理功能的支持，此功能可用于在采用不同显示技术的设备间提供一致的体验。在 Android 8.1 上运行的应用可以访问广色域显示屏的所有功能，以便充分利用显示设备。</p>

<p>之前的 Android 版本不支持颜色管理功能，而是依赖于内容和显示屏之间的兼容性（这个目标通常需要借助电视行业的帮助才能得以实现）。不过，最新的显示技术可让不能按预期显示现有内容的显示屏具有更广的色域。在使用 Android 8.1 时，采用广色域显示屏（例如有机发光二极管有源阵列或 AMOLED 及某些 LCD）的设备将能够从应用中查看到广色域内容。</p>

<h2>确定设备支持</h2>
<p>运行 Android 8.1 且采用广色域显示屏的设备应支持颜色管理（广色域）。在启用这项功能之前，请确保设备符合以下要求：</p>
<ul>
<li>设备显示屏符合硬件要求，其中包括可支持 Display-P3 颜色空间的性能良好的显示屏。如果显示屏不符合此要求，请勿启用颜色管理。为了降低 CPU 和 GPU 影响，有必要在显示通道中支持扩展的 sRGB 和 HDR10。</li>
<li>设备支持出厂校准流程，该流程可生成校准数据（存储在设备上）来调整显示行为中的制造差异。校准数据至少应允许显示屏准确显示 sRGB 内容以及 D65 和 D73 白点（Android 的后续版本可能会针对每个设备支持完整 ICC 配置文件）。</li>
</ul>
<p>如果满足上述要求，您就可以为设备启用颜色管理功能。</p>

<h2>实现颜色管理</h2>
<p>要实现颜色管理，您必须先更新 <a href="/devices/graphics/implement-hwc">Hardware Composer 2 (HWC2)</a> 驱动程序，以便了解颜色模式并将这些模式应用到硬件。具体而言，HWC2 制作程序必须使用 <code>HWCDisplay::GetColorModes</code> 报告 Display-P3 和 sRGB 颜色模式。</p>

<aside class="key-term"><strong>关键术语：</strong>Display-P3 使用数字电影院线联盟基元和 sRGB 传输功能。</aside>

<p>接下来，启用必需的 OpenGL 扩展程序和库支持，以将 OpenGL 颜色空间转换为 HAL 数据空间。必需的 OpenGL 扩展程序包括：</p>
<ul>
<li>
<a href="https://www.khronos.org/registry/egl/extensions/EXT/EGL_EXT_pixel_format_float.txt" class="external">EGL_EXT_pixel_format_float</a>。允许应用使用 16 位浮点颜色组件创建可呈现的 EGLSurface。优先级：高（建议将这项作为宽色域感知应用的默认像素格式）。需要驱动程序支持。</li>
<li>
<a href="https://www.khronos.org/registry/egl/extensions/KHR/EGL_KHR_gl_colorspace.txt" class="external">EGL_KHR_gl_colorspace</a>。对于需要使用 sRGB 格式默认帧缓冲区来轻松实现显示设备的 sRGB 渲染的应用，此扩展程序允许创建将由支持该功能的 OpenGL 上下文采用 sRGB 格式进行渲染的 EGLSurface。需要驱动程序支持 sRGB 行为。</li>
</ul>
<p>Android 还提供以下可选的扩展程序：</p>
<ul>
<li>
<a href="https://www.khronos.org/registry/egl/extensions/EXT/EGL_EXT_gl_colorspace_scrgb_linear.txt" class="external">EGL_EXT_colorspace_scrgb_linear</a>。该扩展程序可提供一个新的颜色空间选项 (scRGB)，供应用在创建 EGLSurface 时进行选择。scRGB 颜色空间可定义与 sRGB 具有相同白点和颜色基准（以便与 sRGB 向后兼容）的线性显示引用空间。此扩展程序应该不需要驱动程序支持，而且可以在 Android EGL 封装容器中实现。为了正常发挥作用，此扩展程序需支持 16 位浮点 (FP16)。</li>
<li>
<a href="https://github.com/KhronosGroup/EGL-Registry/pull/10/files" class="external">EGL_KHR_gl_colorspace_display_p3
和 EGL_EXT_gl_colorspace_display_p3_linear</a>。对于需要使用 Display-P3 格式默认帧缓冲区来轻松实现显示设备的 sRGB 渲染的应用，此扩展程序允许创建将由支持该功能的 OpenGL 上下文采用 Display-P3 格式进行渲染的 EGLSurface。此扩展程序可以在 EGL 驱动程序封装容器中实现。</li>
<li>
<a href="https://www.khronos.org/registry/vulkan/specs/1.0-extensions/html/vkspec.html#VK_EXT_swapchain_colorspace" class="external">VK_EXT_swapchain_colorspace</a> (Vulkan)。可让应用通过它们正在使用的颜色空间来标记交换链。包括一些常见颜色空间，例如 DCI-P3、Display-P3、AdobeRGB、BT2020 等。</li>
</ul>

<h2>自定义</h2>
<p>您可以通过提供对各种颜色标准（例如 DCI-P3、AdobeRGB、Rec709、Rec2020）的支持来自定义颜色管理功能。其他自定义项包括：</p>
<ul>
<li><strong>针对显示通道中颜色转换的硬件支持</strong>。在硬件中支持多种颜色转换。
</li>
<li><strong>支持在多个图层上进行独立的颜色转换</strong>（例如，某些图层可以是 sRGB，其他图层是扩展的 sRGB，每个图层都具有自己的颜色管道）。如果有多个颜色空间可见时，需要将某些颜色空间转换为显示屏的颜色空间。理想情况下，此转换最好由显示引擎提供（否则 Android 必须执行 GPU 合成）。</li>
</ul>

<h2>测试</h2>
<p>要测试颜色管理，请使用 <code>opengl/tests</code> 中的以下资源：</p>
<ul>
<li><code>gl2_basic</code>。用于请求 Display-P3 颜色空间的简单 OpenGL 演示。</li>
<li><code>
<a href="https://android.googlesource.com/platform/frameworks/native/+/master/opengl/tests/EGLTest/EGL_test.cpp">EGL_test</a></code>。测试必要的扩展程序和配置支持（10:10:10:2 和 FP16）。</li>
<li><code>test_wide_color</code>。以与 SurfaceFlinger 创建 surface 的方式相同的方式创建 surface（例如配置、颜色空间、像素格式）。</li>
</ul>

<h2>参考实现</h2>
<p>有关参考实现，请参阅 <code>frameworks/native</code>。有关标题，请参阅：</p>
<ul>
<li>
<code><a href="https://android.googlesource.com/platform/system/core/+/master/libsystem/include/system/graphics.h">system/core/include/system/graphics.h</a></code></li>
<li>
<code><a href="https://android.googlesource.com/platform/system/core/+/master/libsystem/include/system/graphics-base.h">system/core/include/system/graphics-base.h</a></code><ul>
<li><code>HAL_DATASPACE_*</code></li>
<li><code>HAL_COLOR_MODE_*</code></li>
</ul>
</li>
</ul>

</body></html>