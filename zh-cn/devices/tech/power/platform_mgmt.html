<html devsite><head>
    <title>平台电源管理</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

  <p>
为了延长设备的电池续航时间，Android 可以通过监控设备使用情况和唤醒状况来影响设备状态。如果用户未使用设备，则平台会使设备进入休眠状态以便应用停止运行。
  </p>

  <h2 id="doze">低电耗模式</h2>

<p>当用户长时间未使用设备时，低电耗模式会延迟应用后台 CPU 和网络活动，从而延长电池续航时间。</p>

<p>处于低电耗模式的闲置设备会定期进入维护时段，在此期间，应用可以完成待进行的活动（同步、作业等）。然后，低电耗模式会使设备重新进入较长时间的休眠状态，接着进入下一个维护时段。在达到几个小时的休眠时间上限之前，平台会周而复始地重复低电耗模式休眠/维护的序列，且每一次都会延长闲置的时长。处于低电耗模式的设备始终可以感知到动作，且会在检测到动作时立即退出低电耗模式。</p>

<p>每当用户关闭设备屏幕时，即使仍处于移动状态，Android 7.0 及更高版本都会延长设备处于低电耗模式的时间，以触发一系列轻度优化，从而延长电池续航时间。</p>

<p>设备制造商通常将关键的系统服务设置为豁免应用，使其不进入低电耗模式。用户还可以在“设置”菜单中豁免特定的应用，使其不进入低电耗模式。不过，豁免的应用也可能导致设备过度耗电。低电耗模式在 AOSP 中默认处于<strong>停用</strong>状态；要详细了解如何启用低电耗模式，请参阅<a href="#integrate-doze">集成低电耗模式</a>。
</p>

<h3 id="doze-reqs">低电耗模式的要求</h3>
<p>要支持低电耗模式，设备必须具有云消息传递服务，例如，<a href="https://firebase.google.com/docs/cloud-messaging/" class="external">Firebase 云消息传递 (FCM)</a>。外部触发事件（例如云消息传递）可以暂时唤醒应用执行任务，即使设备处于低电耗模式。</p>

<p>此外，要全面支持低电耗模式，相应设备还必须具有<a href="/devices/sensors/sensor-types.html#significant_motion">大幅度动作检测器 (SMD)</a>；不过，Android 7.0 及更高版本中的轻度低电耗模式不要求具有 SMD。如果某台设备上启用了低电耗模式，且：</p>
<ul>
<li>该设备具有 SMD，则系统会进行全面的低电耗模式优化（包括轻度优化）。</li>
<li>该设备没有 SMD，则系统仅会进行轻度低电耗模式优化。</li>
</ul>

<h3 id="doze-life">低电耗模式生命周期</h3>

<p>当平台检测到设备处于闲置状态时，便会使其进入低电耗模式，并在一个或多个满足退出条件的活动发生时使设备退出低电耗模式。</p>

<table>
<tbody>
<tr>
<th width="20%">检测</th>
<th width="60%">低电耗模式期间</th>
<th width="20%">退出</th>
</tr>
<tr>
<td><p>当设备具有以下特点时，平台会检测到其处于闲置状态：</p>
<ul>
<li>设备是静止的（使用大幅度动作检测器检测）。</li>
<li>设备屏幕已关闭一段时间。</li>
</ul>
<p>当设备连接到充电器时，不会进入低电耗模式。
</p>
</td>
<td><p>平台会尝试使系统处于休眠状态，定期进入维护时段并恢复正常操作，然后使设备返回休眠状态并在更长的重复时间内处于休眠状态。在休眠期间，设备会受到以下限制：</p>
<ul>
<li>应用无法访问网络。</li>
<li>应用唤醒锁定被忽略。</li>
<li>闹钟被延迟。闹钟响铃以及使用 <code>setAndAllowWhileIdle()</code> 设置的闹钟（当设备处于低电耗模式时，限于每个应用每 15 分钟 1 次）除外。此豁免适用于必须显示活动提醒通知的应用（如日历）。</li>
<li>无法执行 WLAN 扫描。</li>
<li><code>SyncAdapter</code> 同步和 <code>JobScheduler</code> 作业被延迟，直到下一个维护时段才能恢复。</li>
<li>接收短信和彩信的应用被暂时列入白名单，以便它们可以完成处理任务。</li>
</ul>
</td>
<td><p>当平台检测到以下情况时，会使设备退出低电耗模式：</p>
<ul>
<li>用户与设备互动。</li>
<li>设备移动。</li>
<li>设备屏幕打开。</li>
<li>AlarmClock 即将响铃。</li>
</ul>
<p>通知不会使设备退出低电耗模式。</p>
</td>
</tr>
</tbody>
</table>

<p>在屏幕关闭期间、设备处于闲置状态之前，Android 7.0 及更高版本通过启用轻度休眠模式来延长设备处于低电耗模式的时间。</p>

<p><img src="/devices/tech/images/doze_lightweight.png"/></p>
<figcaption><strong>图 1.</strong> 非静止和静止设备的低电耗模式。</figcaption>

<table>
<tbody>
<tr>
<th>操作</th>
<th>低电耗模式</th>
<th>轻度低电耗模式</th>
</tr>
<tr>
<td>触发因素</td>
<td>屏幕关闭、电池供电、静止</td>
<td>屏幕关闭、电池供电（未插电）</td>
</tr>
<tr>
<td>时间</td>
<td>随维护时段依次增加</td>
<td>随维护时段反复持续 N 分钟</td>
</tr>
<tr>
<td>限制</td>
<td>无法进行网络访问、唤醒锁定和 GPS/WLAN 扫描。闹钟和作业/同步被延迟。</td>
<td>无法进行网络访问。作业/同步被延迟（维护时段除外）。
</td>
</tr>
<tr>
<td>行为</td>
<td>仅接收优先级较高的推送通知消息。</td>
<td>接收所有实时消息（即时消息、致电等）。优先级较高的推送通知消息可以暂时访问网络。</td>
</tr>
<tr>
<td>退出</td>
<td>动作、屏幕开启或闹钟响铃。</td>
<td>屏幕开启。</td>
</tr>
</tbody>
</table>

<h3 id="doze-interactions">与应用待机模式相集成</h3>
<ul>
<li>设备处于低电耗模式的时间不会计入应用待机模式。</li>
<li>当设备处于低电耗模式时，闲置应用一天至少可以执行一次正常操作。</li>
</ul>

<h3 id="integrate-doze">集成低电耗模式</h3>

<p>启用低电耗模式时，支持 <a href="/devices/sensors/sensor-types.html#significant_motion"><code>SENSOR_TYPE_SIGNIFICANT_MOTION</code>code&gt;</a> 的设备会执行全面的低电耗模式优化（包括轻度优化）；没有 SMD 的设备仅执行轻度低电耗模式优化。Android 会自动选择适当的低电耗模式优化，无需供应商配置。</p>

<p>要为设备启用低电耗模式，请执行以下操作：</p>

<ol>
<li>确认设备已安装云消息传递服务。</li>
<li>在设备叠加配置文件 <code>overlay/frameworks/base/core/res/res/values/config.xml</code> 中，将 <code>config_enableAutoPowerModes</code> 设置为 <strong>true</strong>：
<pre class="devsite-click-to-copy">
&lt;bool name="config_enableAutoPowerModes"&gt;true&lt;/bool&gt;
</pre>
在 AOSP 中，该参数默认设为 false（已停用低电耗模式）。<br />
</li>
<li>确认预加载的应用和服务满足以下要求：<ul>
<li>遵循<a href="https://developer.android.com/training/monitoring-device-state/doze-standby.html" class="external">节电优化指南</a>。有关详情，请参阅<a href="#test-apps">测试和优化应用</a>。
<p><strong>或</strong></p>
</li><li>获得豁免，不会进入低电耗模式和应用待机模式。有关详情，请参阅<a href="#exempt-apps">豁免应用</a>。</li>
</ul>
</li>
<li>确认必要的服务已获得豁免，不会进入低电耗模式。</li>
</ol>

<h4 id="doze-tips">技巧</h4>
<ul>
<li>如果可能，请使用 FCM 进行<a href="https://firebase.google.com/docs/cloud-messaging/http-server-ref#send-downstream" class="external">下游消息传递</a>。</li>
<li>如果您的用户必须立即看到通知，请使用 <a href="https://firebase.google.com/docs/cloud-messaging/concept-options#setting-the-priority-of-a-message" class="external">FCM 高优先级消息</a>。</li>
<li>在初始<a href="https://firebase.google.com/docs/cloud-messaging/concept-options#notifications_and_data_messages" class="external">消息有效负载</a>内提供充足的信息（以避免不必要的后续网络访问）。</li>
<li>使用 <a href="http://developer.android.com/reference/android/app/AlarmManager.html#setAndAllowWhileIdle(int,%20long,%20android.app.PendingIntent)" class="external"><code>setAndAllowWhileIdle()</code></a> 和 <a href="http://developer.android.com/reference/android/app/AlarmManager.html#setExactAndAllowWhileIdle(int,%20long,%20android.app.PendingIntent)" class="external"><code>setExactAndAllowWhileIdle()</code></a> 设置重要闹钟。
</li>
</ul>

<h4 id="test-apps">测试和优化应用</h4>
<p>在低电耗模式中测试所有应用（尤其是预加载的应用）。有关详情，请参阅<a href="https://developer.android.com/training/monitoring-device-state/doze-standby.html#testing_doze_and_app_standby">测试低电耗模式和应用待机模式</a>。</p>

<aside class="note"><strong>注意</strong>：彩信/短信/电话服务独立于低电耗模式发挥作用，即使设备处于低电耗模式，此类服务也一定会唤醒客户端应用。</aside>

</body></html>