<html devsite><head>
    <title>性能管理</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>对 Android 设备的电量和性能进行管理有助于确保应用能够在各类硬件上稳定、顺畅地运行。在 Android 7.0 及更高版本中，OEM 可支持提供持续性能提示以便应用能够维持稳定的设备性能，并可指定专属核心以便提高 CPU 密集型前台应用的性能。</p>

<h2 id="sustained_performance">持续性能</h2>
<p>对于长时间运行的应用（例如，游戏、相机、RenderScript 和音频处理应用）来说，当设备达到极限温度且系统芯片 (SoC) 引擎受到限制时，性能便可能会出现急剧变化。当设备开始升温时，底层平台的功能会变得飘忽不定，这会限制应用开发者打造可长时间运行的高性能应用。</p>

<p>为解决此类限制，Android 7.0 引入了对持续性能的支持，让 OEM 能够为长时间运行的应用提供设备性能提示。应用开发者可根据这些提示来调整应用，以使设备能在长时间内保持可预测且稳定的性能水平。</p>

<h3 id="architecture">架构</h3>
<p>Android 应用可以请求平台进入持续性能模式，以使 Android 设备能在长时间内保持稳定的性能水平。</p>

<p><img src="../images/power_sustained_perf.png"/></p>
<p class="img-caption"><strong>图 1.</strong> 持续性能模式架构</p>

<h3 id="implementation">实现</h3>
<p>为支持 Android 7.0 及更高版本实现持续性能，OEM 必须：</p>
<ul>
<li>针对具体设备更改 HAL 的电量文件，以锁定最大 CPU/GPU 频率<strong>或</strong>执行其他优化来防止出现过热保护。</li>
<li>在 HAL 的电量文件中实现新提示 <code>POWER_HINT_SUSTAINED_PERFORMANCE</code>。</li>
<li>通过使用 <code>isSustainedPerformanceModeSupported()</code> API 返回 TRUE 来声明支持。</li>
<li>实现 <code>Window.setSustainedPerformanceMode</code>。</li>
</ul>

<p>在 Nexus 参考实现中，电量提示将 CPU 和 GPU 的最大频率设定为最高可持续水平的频率。请注意，降低 CPU/GPU 频率的上限会降低帧速率，但由于此种较低的速率具有可持续性，因此在该模式下它依然是首选。例如，某部使用正常最大时钟频率的设备或许能以 60 FPS 的速度渲染几分钟，但当该设备持续升温，则可能会在渲染 30 分钟后调节到 30 FPS。而当使用持续模式时，该设备就可能会以 45 FPS 的速度持续渲染整整 30 分钟。这样做的目的是：通过使用该模式，获得一个与不使用该模式时相同（或更高）的帧速率，而且该速率能在长时间内维持稳定水平，使开发者不必费心追逐一个飘忽不定的目标。</p>
<p>我们强烈建议您采用持续模式，以使设备能够达到最高可持续性能，而非仅仅达到为通过测试而需采用的最小值（例如，选择不会导致设备长时间运行后进入过热保护的最高频率上限）。</p>

<p class="note"><strong>注意</strong>：实现持续模式不需要设定时钟频率的最大速率上限。</p>

<h3 id="validation">验证</h3>
<p>OEM 可以使用 CTS 测试（适用于 Android 7.0 及更高版本）来验证持续性能 API 的实现情况。这项测试会运行工作负载大约 30 分钟，并会分别在启用和不启用持续模式的情况下对性能进行基准测试：</p>
<ul>
<li>启用持续模式后，帧速率必须保持相对恒定（这项测试会测量帧速率在一段时间内的变化幅度百分比，且会要求该比例 &lt;5%）。</li>
<li>启用持续模式后，帧速率不得低于在不启用持续模式的情况下 30 分钟结束时的帧速率。</li>
</ul>
<p>此外，您可以对 CPU 和 GPU 进行多个高负荷工作来手动测试你的设备的实现情况，以确保设备在被使用 30 分钟后不会进入过热保护。在内部测试中，我们会使用游戏和基准测试应用（例如 <a href="https://gfxbench.com/result.jsp">gfxbench</a>）等示例工作负载。</p>

<h2 id="exclusive_core">专属核心</h2>
<p>对于 CPU 密集型、时间敏感型工作负载，被另一个线程抢占可能会影响到能否按各个帧的预定截止时间完成渲染。对于具有严格的延迟时间要求和帧速率要求的应用（如音频应用或虚拟实境应用），具有专属 CPU 核心能够保证实现可接受的性能水平。</p>
<p>运行 Android 7.0 或更高版本的设备现可为最主要前台应用明确保留一个核心，这不仅能提高所有前台应用的性能，还能让运行高强度工作负载的应用更好地控制其任务在各 CPU 核心上的分配方式。</p>
<p>要想支持在设备上使用专属核心，请执行以下操作：</p>
<ul>
<li>启用 <code>cpusets</code> 并配置仅包含最主要前台应用的 <code>cpuset</code>。</li>
<li>确保为来自 <code>cpuset</code> 的线程保留一个核心（即专属核心）。</li>
<li>实现 getExclusiveCores API 以返回这个专属核心的核心编号。</li>
</ul>
<p>要想确定哪些进程被安排在哪些核心上，请在运行任意工作负载时使用 <code>systrace</code>，并验证是否只有来自最主要前台应用的用户空间线程被安排在专属核心上。</p>
<p>要查看 Nexus 6P 的参考实现，请参阅 <code>android//device/huawei/angler/power/power.c</code>。</p>

</body></html>