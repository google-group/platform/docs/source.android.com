<html devsite><head>
    <title>实现 ART 即时 (JIT) 编译器</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>Android 运行时 (ART) 包含一个具备代码分析功能的即时 (JIT) 编译器，该编译器可以在 Android 应用运行时持续提高其性能。JIT 编译器补充了 ART 当前的预先 (AOT) 编译器的功能，有助于提高运行时性能，节省存储空间，以及加快应用及系统更新速度。相较于 AOT 编译器，JIT 编译器的优势也更为明显，因为它不会在应用自动更新期间或重新编译应用（在无线下载 (OTA) 更新期间）时拖慢系统速度。
</p>

<p>
尽管 JIT 和 AOT 使用相同的编译器，它们所进行的一系列优化也较为相似，但它们生成的代码可能会有所不同。JIT 会利用运行时类型信息，可以更高效地进行内联，并可让堆栈替换 (OSR) 编译成为可能，而这一切都会使其生成的代码略有不同。
</p>

<h2 id="architectural-overview">JIT 架构</h2>

<img src="./images/jit-arch.png" alt="JIT 架构"/>
<figcaption><strong>图 1.</strong> JIT 架构。</figcaption>

<h2 id="flow">JIT 编译</h2>

<p>JIT 编译涉及以下活动：</p>
<img src="./images/jit-profile-comp.png" alt="配置文件引导的编译"/>
<figcaption><strong>图 2.</strong> 配置文件引导的编译。</figcaption>

<ol>
<li>用户运行应用，而这随后就会触发 ART 加载 <code>.dex</code> 文件。
 <ul>
 <li>如果有 <code>.oat</code> 文件（即 <code>.dex</code> 文件的 AOT 二进制文件），则 ART 会直接使用该文件。虽然 <code>.oat</code> 文件会定期生成，但文件中不一定会包含经过编译的代码（即 AOT 二进制文件）。</li>
 <li>如果没有 <code>.oat</code> 文件，则 ART 会通过 JIT 或解释器执行 <code>.dex</code> 文件。</li>
如果有 <code>.oat</code> 文件，ART 将一律使用这类文件。否则，它将在内存中使用并解压 APK 文件，从而得到 <code>.dex</code> 文件，但是这会导致消耗大量内存（相当于 dex 文件的大小）。
</ul>
</li>
<li>针对任何未根据 <code>speed</code> 编译过滤器编译的应用启用 JIT（也就是说，要尽可能多地编译应用中的代码）。</li>
<li>将 JIT 配置文件数据转存到只限应用访问的系统目录内的文件中。</li>
<li>AOT 编译 (<code>dex2oat</code>) 守护进程通过解析该文件来推进其编译。
<br />
<br />
<img src="./images/jit-daemon.png" alt="JIT 守护进程"/>
<figcaption><strong>图 3.</strong> JIT 守护进程活动。</figcaption>
</li>
</ol>

<p>
举例来说，Google Play 服务就是一种由其他应用使用的类似于共享库的服务。
</p>

<h2 id="jit-workflow">JIT 工作流程</h2>

<img src="./images/jit-workflow.png" alt="JIT 架构"/>
<figcaption><strong>图 4.</strong> JIT 数据流。</figcaption>

<ul>
<li>分析信息会存储在代码缓存中，并会在内存紧张时作为垃圾被回收。
 <ul>
 <li>无法保证在应用处于后台运行状态时所捕获的快照能够包含完整的数据（即 JIT 编译的所有内容）。</li>
 <li>该过程不会尝试确保记录所有内容（因为这将影响运行时性能）。</li>
 </ul>
</li>
<li>方法可能有三种不同的状态：
 <ul>
 <li>已经过解释（dex 代码）</li>
 <li>已经过 JIT 编译</li>
 <li>已经过 AOT 编译</li>
 </ul>
如果同时存在 JIT 和 AOT 代码（例如，由于反复进行逆优化），经过 JIT 编译的代码将是首选代码。
</li>
<li>在不影响前台应用性能的情况下运行 JIT 所需的内存取决于相关应用。大型应用比小型应用需要更多内存。一般来说，大型应用所需的内存稳定维持在 4 MB 左右。</li>
</ul>

<h2 id="turn-on-jit-logging">开启 JIT 日志记录</h2>
<p>要开启 JIT 日志记录，请运行以下命令：</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">adb root</code>
<code class="devsite-terminal">adb shell stop</code>
<code class="devsite-terminal">adb shell setprop dalvik.vm.extra-opts -verbose:jit</code>
<code class="devsite-terminal">adb shell start</code>
</pre>

<h2 id="disable-jit-and-run-applications-in-interpreter">停用 JIT</h2>
<p>要停用 JIT，请运行以下命令：</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">adb root</code>
<code class="devsite-terminal">adb shell stop</code>
<code class="devsite-terminal">adb shell setprop dalvik.vm.usejit false</code>
<code class="devsite-terminal">adb shell start</code>
</pre>

<h2 id="force-compilation-of-a-specific-package">强制编译</h2>

<p>要强制编译，请运行以下命令：</p>
<pre class="devsite-terminal devsite-click-to-copy">
adb shell cmd package compile
</pre>

<p>强制编译特定软件包的常见用例：</p>
<ul>
<li>基于配置文件：<pre class="devsite-terminal devsite-click-to-copy">
adb shell cmd package compile -m speed-profile -f my-package
</pre>
</li>
<li>全面：
<pre class="devsite-terminal devsite-click-to-copy">
adb shell cmd package compile -m speed -f my-package
</pre>
</li>
</ul>

<p>强制编译所有软件包的常见用例：</p>
<ul>
<li>基于配置文件：<pre class="devsite-terminal devsite-click-to-copy">
adb shell cmd package compile -m speed-profile -f -a
</pre>
</li>
<li>全面：
<pre class="devsite-terminal devsite-click-to-copy">
adb shell cmd package compile -m speed -f -a
</pre>
</li>
</ul>

<h2 id="clear-profile-data-and-remove-compiled-code">清除配置文件数据</h2>
<p>要清除配置文件数据并移除经过编译的代码，请运行以下命令：</p>
<ul>
<li>针对一个软件包：<pre class="devsite-terminal devsite-click-to-copy">
adb shell cmd package compile --reset my-package
</pre>
</li>
<li>针对所有软件包：<pre class="devsite-terminal devsite-click-to-copy">
adb shell cmd package compile --reset -a
</pre>
</li>
</ul>

</body></html>