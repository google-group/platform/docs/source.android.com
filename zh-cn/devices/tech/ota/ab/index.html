<html devsite><head>
    <title>A/B（无缝）系统更新</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

    <p>A/B 系统更新（也称为无缝更新）的目标是确保在<a href="/devices/tech/ota/index.html">无线下载 (OTA) 更新</a>期间在磁盘上保留一个可正常启动和使用的系统。采用这种方式可以降低更新之后设备无法启动的可能性，这意味着用户需要将设备送到维修和保修中心进行更换和刷机的情况将会减少。其他某些商业级操作系统（例如 <a href="https://www.chromium.org/chromium-os">ChromeOS</a>）也成功使用了 A/B 更新机制。
    </p>

    <p>要详细了解 A/B 系统更新，请参见<a href="#slots">分区选择（槽位）</a>一节。
    </p>

    <p>A/B 系统更新可带来以下好处：</p>

    <ul>
      <li>
OTA 更新可以在系统运行期间进行，而不会打断用户。用户可以在 OTA 期间继续使用其设备。在更新期间，唯一的一次宕机发生在设备重新启动到更新后的磁盘分区时。
      </li>
      <li>
更新后，重新启动所用的时间不会超过常规重新启动所用的时间。
      </li>
      <li>
如果 OTA 无法应用（例如，因为刷机失败），用户将不会受到影响。用户将继续运行旧的操作系统，并且客户端可以重新尝试进行更新。
      </li>
      <li>
如果 OTA 更新已应用但无法启动，设备将重新启动回旧分区，并且仍然可以使用。客户端可以重新尝试进行更新。
      </li>
      <li>
任何错误（例如 I/O 错误）都只会影响<strong>未使用</strong>的分区组，并且用户可以进行重试。由于 I/O 负载被特意控制在较低水平，以免影响用户体验，因此发生此类错误的可能性也会降低。
      </li>
      <li>
更新包可以流式传输到 A/B 设备，因此在安装之前不需要先下载更新包。流式更新意味着用户没有必要在 <code>/data</code> 或 <code>/cache</code> 上留出足够的可用空间来存储更新包。
      </li>
      <li>
缓存分区不再用于存储 OTA 更新包，因此无需确保缓存分区的大小要足以应对日后的更新。
      </li>
      <li>
        <a href="/security/verifiedboot/dm-verity.html">dm-verity</a> 可保证设备将使用未损坏的启动映像。如果设备因 OTA 错误或 dm-verity 问题而无法启动，则可以重新启动到旧映像。（Android <a href="/security/verifiedboot/">验证启动</a>不需要 A/B 更新。）
      </li>
    </ul>

    <h2 id="overview">关于 A/B 系统更新</h2>

      <p>
进行 A/B 更新时，客户端和系统都需要进行更改。不过，OTA 更新包服务器应该不需要进行更改：更新包仍通过 HTTPS 提供。对于使用 Google OTA 基础架构的设备，系统更改全部是在 AOSP 中进行，并且客户端代码由 Google Play 服务提供。不使用 Google OTA 基础架构的原始设备制造商 (OEM) 将能够重复使用 AOSP 系统代码，但需要自行提供客户端。
      </p>

      <p>
如果 OEM 自行提供客户端，客户端需要：
      </p>

      <ul>
        <li>
确定何时进行更新。由于 A/B 更新是在后台进行，因此不再需要由用户启动。为了避免干扰用户，建议将更新安排在设备处于闲时维护模式（如夜间）并已连接到 WLAN 网络时进行。不过，客户端可以使用您希望使用的任何启发法。
        </li>
        <li>
向 OTA 更新包服务器进行核查，确定是否有可用的更新。这应与您现有的客户端代码大体相同，不过您需要表明相应设备支持 A/B 更新。（Google 的客户端还包含<strong>立即检查</strong>按钮，以便用户检查是否有最新更新。）
        </li>
        <li>
调用 <code>update_engine</code>（使用 HTTPS 网址），以获取更新包（假设有可用的更新包）。<code>update_engine</code> 将在流式传输更新包的同时，在当前未使用的分区上更新原始数据块。
        </li>
        <li>
根据 <code>update_engine</code> 结果代码向您的服务器报告安装是成功了还是失败了。如果更新已成功应用，<code>update_engine</code> 将会告知引导加载程序在下次重新启动时启动到新的操作系统。如果新的操作系统无法启动，引导加载程序将会回退到旧的操作系统，因此无需在客户端执行任何操作。如果更新失败，客户端将需要根据详细的错误代码确定何时（以及是否）重试。例如，优秀的客户端能够识别出是一部分（“diff”）OTA 更新包失败，并改为尝试完整的 OTA 更新包。
        </li>
      </ul>

      <p>客户端可能会：</p>

      <ul>
        <li>
显示通知，以提醒用户重新启动系统。如果您想要实施鼓励用户定期更新的政策，则可以将该通知添加到客户端。如果客户端不提示用户，用户将会在下次重新启动系统时收到更新。（Google 的客户端会有延迟，该延迟可按每次更新进行配置。）
        </li>
        <li>
显示通知，以告知用户他们是启动到了新的操作系统版本，还是应启动到新的操作系统版本，但却回退到了旧的操作系统版本。（Google 的客户端通常不会显示此类通知。）
        </li>
      </ul>

      <p>在系统方面，A/B 系统更新会影响以下各项：</p>

      <ul>
        <li>
分区选择（槽位）、<code>update_engine</code> 守护进程，以及引导加载程序交互（如下所述）
        </li>
        <li>
编译过程和 OTA 更新包生成（如<a href="/devices/tech/ota/ab/ab_implement.html">实现 A/B 更新</a>中所述）
        </li>
      </ul>

      <aside class="note">
        <strong>注意</strong>：只有对于新设备，才建议通过 OTA 实现 A/B 系统更新。
      </aside>

      <h3 id="slots">分区选择（槽位）</h3>

        <p>
A/B 系统更新使用两组称为槽位（通常是槽位 A 和槽位 B）的分区。<em></em>系统从“当前”槽位运行，但在正常操作期间，运行中的系统不会访问未使用的槽位中的分区。<em></em><em></em>这种方法通过将未使用的槽位保留为后备槽位，来防范更新出现问题：如果在更新期间或更新刚刚完成后出现错误，系统可以回滚到原来的槽位并继续正常运行。为了实现这一目标，当前槽位使用的任何分区（包括只有一个副本的分区）都不应在 OTA 更新期间进行更新。<em></em>
        </p>

        <p>
每个槽位都有一个“可启动”属性，该属性用于表明相应槽位存储的系统正确无误，设备可从相应槽位启动。<em></em>系统运行时，当前槽位处于可启动状态，但另一个槽位则可能包含旧版本（仍然正确）的系统、包含更新版本的系统，或包含无效的数据。无论当前槽位是哪一个，都有一个槽位是活动槽位（引导加载程序在下次启动时将使用的槽位，也称为首选槽位）。<em></em><em></em><em></em>
        </p>

        <p>
此外，每个槽位还都有一个由用户空间设置的“成功”属性，仅当相应槽位处于可启动状态时，该属性才具有相关性。<em></em>被标记为成功的槽位应该能够自行启动、运行和更新。未被标记为成功的可启动槽位（多次尝试使用它启动之后）应由引导加载程序标记为不可启动，其中包括将活动槽位更改为另一个可启动的槽位（通常是更改为在尝试启动到新的活动槽位之前正在运行的槽位）。关于相应接口的具体详细信息在 <code><a href="https://android.googlesource.com/platform/hardware/libhardware/+/master/include/hardware/boot_control.h" class="external-link">
          boot_control.h</a></code> 中进行了定义。
        </p>

      <h3 id="update-engine">更新引擎守护进程</h3>

        <p>
A/B 系统更新过程会使用名为 <code>update_engine</code> 的后台守护进程来使系统做好准备，以启动到更新后的新版本。该守护进程可以执行以下操作：
        </p>

        <ul>
          <li>
按照 OTA 更新包的指示，从当前槽位 A/B 分区读取数据，然后将所有数据写入到未使用槽位 A/B 分区。
          </li>
          <li>
在预定义的工作流程中调用 <code>boot_control</code> 接口。
          </li>
          <li>
按照 OTA 更新包的指示，在将数据写入到所有未使用槽位分区之后，从新分区运行安装后程序。<em></em><em></em>（有关详细信息，请参阅<a href="#post-installation">安装后</a>）。
          </li>
        </ul>

        <p>
由于 <code>update_engine</code> 守护进程本身不会参与到启动流程中，因此该守护进程在更新期间可执行的操作受限于当前槽位中的 <a href="/security/selinux/">SELinux</a> 政策和功能（在系统启动到新版本之前，此类政策和功能无法更新）。<em></em>为了维持一个稳定可靠的系统，更新流程<strong>不应</strong>修改分区表、当前槽位中各个分区的内容，以及无法通过恢复出厂设置擦除的非 A/B 分区的内容。
        </p>

        <h4 id="update_engine_source">更新引擎源代码</h4>

            <p>
<code>update_engine</code> 源代码位于 <code><a href="https://android.googlesource.com/platform/system/update_engine/" class="external">system/update_engine</a></code> 中。A/B OTA dexopt 文件分开放到了 <code>installd</code> 和一个程序包管理器中：
            </p>

            <ul>
              <li>
                <code><a href="https://android.googlesource.com/platform/frameworks/native/+/master/cmds/installd/" class="external-link">frameworks/native/cmds/installd/</a></code>ota* 包括安装后脚本、用于 chroot 的二进制文件、负责调用 dex2oat 的已安装克隆、OTA 后 move-artifacts 脚本，以及 move 脚本的 rc 文件。
              </li>
              <li>
                <code><a href="https://android.googlesource.com/platform/frameworks/base/+/master/services/core/java/com/android/server/pm/OtaDexoptService.java" class="external-link">frameworks/base/services/core/java/com/android/server/pm/OtaDexoptService.java</a></code>（加上 <code><a href="https://android.googlesource.com/platform/frameworks/base/+/master/services/core/java/com/android/server/pm/OtaDexoptShellCommand.java" class="external-link">OtaDexoptShellCommand</a></code>）是负责为应用准备 dex2oat 命令的程序包管理器。
              </li>
            </ul>

            <p>
如需实际示例，请参阅 <code><a href="https://android.googlesource.com/device/google/marlin/+/nougat-dr1-release/device-common.mk" class="external-link">/device/google/marlin/device-common.mk</a></code>。
            </p>

        <h4 id="update_engine_logs">更新引擎日志</h4>

          <p>
对于 Android 8.x 及更低版本，可在 <code>logcat</code> 及错误报告中找到 <code>update_engine</code> 日志。要使 <code>update_engine</code> 日志可在文件系统中使用，请将以下更改添加到您的细分版本中：
          </p>

          <ul>
            <li><a href="https://android-review.googlesource.com/c/platform/system/update_engine/+/486618">
更改 486618</a></li>
            <li><a href="https://android-review.googlesource.com/c/platform/system/core/+/529080">
更改 529080</a></li>
            <li><a href="https://android-review.googlesource.com/c/platform/system/update_engine/+/529081">
更改 529081</a></li>
            <li><a href="https://android-review.googlesource.com/c/platform/system/sepolicy/+/534660">
更改 534660</a></li>
            <li><a href="https://android-review.googlesource.com/c/platform/system/update_engine/+/594637">
更改 594637</a></li>
          </ul>

          <p>这些更改会将最新的 <code>update_engine</code> 日志的副本保存到 <code>/data/misc/update_engine_log/update_engine.<var>YEAR</var>-<var>TIME</var></code>。除当前日志以外，最近的五个日志也会保存在 <code>/data/misc/update_engine_log/</code> 下。拥有<strong>日志</strong>组 ID 的用户将能够访问相应的文件系统日志。</p>

      <h3 id="bootloader-interactions">引导加载程序交互</h3>

        <p>
<code>boot_control</code> HAL 供 <code>update_engine</code>（可能还有其他守护进程）用于指示引导加载程序从何处启动。常见的示例情况及其相关状态包括：
        </p>

        <ul>
          <li>
            <strong>正常情况</strong>：系统正在从其当前槽位（槽位 A 或槽位 B）运行。到目前为止尚未应用任何更新。系统的当前槽位是可启动且被标记为成功的活动槽位。
          </li>
          <li>
            <strong>正在更新</strong>：系统正在从槽位 B 运行，因此，槽位 B 是可启动且被标记为成功的活动槽位。由于槽位 A 中的内容正在更新，但是尚未完成，因此槽位 A 被标记为不可启动。在此状态下，应继续从槽位 B 重新启动。
          </li>
          <li>
            <strong>已应用更新，正在等待重新启动</strong>：系统正在从槽位 B 运行，槽位 B 可启动且被标记为成功，但槽位 A 之前被标记为活动槽位（因此现在被标记为可启动）。槽位 A 尚未被标记为成功，引导加载程序应尝试从槽位 A 启动若干次。
          </li>
          <li>
            <strong>系统已重新启动到新的更新</strong>：系统正在首次从槽位 A 运行，槽位 B 仍可启动且被标记为成功，而槽位 A 仅可启动，且仍是活动槽位，但未被标记为成功。在进行一些检查之后，用户空间守护进程 <code>update_verifier</code> 应将槽位 A 标记为成功。
          </li>
        </ul>

      <h3 id="streaming-updates">流式更新支持</h3>

        <p>
用户设备并非在 <code>/data</code> 上总是有足够的空间来下载更新包。由于 OEM 和用户都不想浪费 <code>/cache</code> 分区上的空间，因此有些用户会因为设备上没有空间来存储更新包而不进行更新。为了解决这个问题，Android 8.0 中添加了对流式 A/B 更新（下载数据块后直接将数据块写入 B 分区，而无需将数据块存储在 <code>/data</code> 上）的支持。流式 A/B 更新几乎不需要临时存储空间，并且只需要能够存储大约 100KiB 元数据的存储空间即可。
        </p>

        <p>要在 Android 7.1 中实现流式更新，请选择以下补丁程序：</p>

        <ul>
          <li>
            <a href="https://android-review.googlesource.com/333624" class="external">
允许取消代理解析请求</a>
          </li>
          <li>
            <a href="https://android-review.googlesource.com/333625" class="external">
解决在解析代理时会终止传输的问题</a>
          </li>
          <li>
            <a href="https://android-review.googlesource.com/333626" class="external">
针对范围之间的 TerminateTransfer 添加单元测试</a>
          </li>
          <li>
            <a href="https://android-review.googlesource.com/333627" class="external">
清理 RetryTimeoutCallback()</a>
          </li>
        </ul>

        <p>
无论是使用 <a href="https://www.android.com/gms/">Google 移动服务 (GMS)</a>，还是使用任何其他更新客户端，都需要安装这些补丁程序，才能在 Android 7.1 中支持流式传输 A/B 更新包。
        </p>

    <h2 id="life-of-an-a-b-update">A/B 更新过程</h2>

      <p>
当有 OTA 更新包（在代码中称为有效负载）可供下载时，更新流程便开始了。<em></em>设备中的政策可以根据电池电量、用户活动、充电状态或其他政策来延迟下载和应用有效负载。此外，由于更新是在后台运行，因此用户可能并不知道正在进行更新。所有这些都意味着，更新流程可能随时会由于政策、意外重新启动或用户操作而中断。
      </p>

      <p>
OTA 更新包本身所含的元数据可能会指示可进行流式更新，在这种情况下，相应更新包也可采用非流式安装方式。服务器可以利用这些元数据告诉客户端正在进行流式更新，以便客户端正确地将 OTA 移交给 <code>update_engine</code>。如果设备制造商具有自己的服务器和客户端，便可以通过确保以下两项来实现流式更新：确保服务器能够识别出更新是流式更新（或假定所有更新都是流式更新），并确保客户端能够正确调用 <code>update_engine</code> 来进行流式更新。制造商可以根据更新包是流式更新变体这一事实向客户端发送一个标记，以便在进行流式更新时触发向框架端的移交工作。
      </p>

      <p>有可用的有效负载后，更新流程将遵循如下步骤：</p>

      <table>
        <tbody><tr>
          <th>步骤</th>
          <th>操作</th>
        </tr>
        <tr>
          <td>1</td>
          <td>通过 <code>markBootSuccessful()</code> 将当前槽位（或“源槽位”）标记为成功（如果尚未标记）。</td>
        </tr>
        <tr>
          <td>2</td>
          <td>
调用函数 <code>setSlotAsUnbootable()</code>，将未使用的槽位（或“目标槽位”）标记为不可启动。当前槽位始终会在更新开始时被标记为成功，以防止引导加载程序回退到未使用的槽位（该槽位中很快将会有无效数据）。如果系统已做好准备，可以开始应用更新，那么即使其他主要组件出现损坏（例如界面陷入崩溃循环），当前槽位也会被标记为成功，因为可以通过推送新软件来解决这些问题。
            <br /><br />
更新有效负载是不透明的 Blob，其中包含更新到新版本的指示。更新有效负载由以下部分组成：
            <ul>
              <li>
                元数据。<em></em>元数据在更新有效负载中所占的比重相对较小，其中包含一系列用于在目标槽位上生成和验证新版本的操作。例如，某项操作可能会解压缩特定 Blob 并将其写入到目标分区中的特定块，或者从源分区读取数据、应用二进制补丁程序，然后写入到目标分区中的特定块。
              </li>
              <li>
                额外数据。<em></em>与操作相关的额外数据在更新有效负载中占据了大部分比重，其中包含这些示例中的已压缩 Blob 或二进制补丁程序。
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>3</td>
          <td>下载有效负载元数据。</td>
        </tr>
        <tr>
          <td>4</td>
          <td>
对于元数据中定义的每项操作，都将按顺序发生以下行为：将相关数据（如果有）下载到内存中、应用操作，然后释放关联的内存。
          </td>
        </tr>
        <tr>
          <td>5</td>
          <td>
对照预期的哈希重新读取并验证所有分区。
          </td>
        </tr>
        <tr>
          <td>6</td>
          <td>
运行安装后步骤（如果有）。如果在执行任何步骤期间出现错误，则更新失败，系统可能会通过其他有效负载重新尝试更新。如果上述所有步骤均已成功完成，则更新成功，系统会执行最后一个步骤。
          </td>
        </tr>
        <tr>
          <td>7</td>
          <td>
<em></em>调用 <code>setActiveBootSlot()</code>，将未使用的槽位标记为活动槽位。将未使用的槽位标记为活动槽位并不意味着它将完成启动。如果引导加载程序（或系统本身）未读取到“成功”状态，则可以将活动槽位切换回来。
          </td>
        </tr>
        <tr>
          <td>8</td>
          <td>
安装后步骤（如下所述）包括从“新更新”版本中运行仍在旧版本中运行的程序。如果此步骤已在 OTA 更新包中定义，则为<strong>强制性</strong>步骤，且程序必须返回并显示退出代码 <code>0</code>，否则更新会失败。
          </td>
        </tr>
          <tr><td>9</td>
          <td>
在系统足够深入地成功启动到新槽位并完成重新启动后检查之后，系统会调用 <code>markBootSuccessful()</code>，将现在的当前槽位（原“目标槽位”）标记为成功。
          </td>
        </tr><tr>
      </tr></tbody></table>

      <aside class="note">
        <strong>注意</strong>：第 3 步和第 4 步占用了大部分更新时间，因为这两个步骤涉及写入和下载大量数据，并且可能会因政策或重新启动等原因而中断。
      </aside>

      <h3 id="post-installation">安装后</h3>

        <p>
对于定义了安装后步骤的每个分区，<code>update_engine</code> 都会将新分区装载到特定位置，并执行与装载的分区相关的 OTA 中指定的程序。例如，如果安装后程序被定义为相应系统分区中的 <code>usr/bin/postinstall</code>，则系统会将未使用槽位中的这个分区装载到一个固定位置（例如 <code>/postinstall_mount</code>），然后执行 <code>/postinstall_mount/usr/bin/postinstall</code> 命令。
        </p>

        <p>
为确保成功执行安装后步骤，旧内核必须能够：
        </p>

        <ul>
          <li>
            <strong>装载新的文件系统格式</strong>。文件系统类型不能更改（除非旧内核中支持这么做），包括使用的压缩算法（如果使用 SquashFS 等经过压缩的文件系统）等详细信息。
          </li>
          <li>
            <strong>理解新分区的安装后程序格式</strong>。如果使用可执行且可链接格式 (ELF) 的二进制文件，则该文件应该与旧内核兼容（例如，如果架构从 32 位细分版本改为使用 64 位细分版本，则 64 位的新程序应该可以在旧的 32 位内核上运行）。除非加载程序 (<code>ld</code>) 收到使用其他路径或编译静态二进制文件的指令，否则将会从旧系统映像而非新系统映像加载各种库。
          </li>
        </ul>

        <p>
例如，您可以使用 shell 脚本作为安装后程序（由旧系统中顶部包含 <code>#!</code> 标记的 shell 二进制文件解析），然后从新环境设置库路径，以便执行更复杂的二进制安装后程序。或者，您可以从专用的较小分区执行安装后步骤，以便主系统分区中的文件系统格式可以得到更新，同时不会产生向后兼容问题或引发 stepping-stone 更新；这样一来，用户便可以从出厂映像直接更新到最新版本。
        </p>

        <p>
新的安装后程序将受旧系统中定义的 SELinux 政策限制。因此，安装后步骤适用于在指定设备上执行设计所要求的任务或其他需要尽可能完成的任务（例如，更新支持 A/B 更新的固件或引导加载程序、为新版本准备数据库副本，等等）。安装后步骤<strong>不适用于</strong>重新启动之前的一次性错误修复（此类修复需要无法预见的权限）。
        </p>

        <p>
所选的安装后程序在 <code>postinstall</code> SELinux 环境中运行。新装载的分区中的所有文件都将带有 <code>postinstall_file</code> 标记，无论在重新启动到新系统后它们的属性如何，都是如此。在新系统中对 SELinux 属性进行的更改不会影响安装后步骤。如果安装后程序需要额外的权限，则必须将这些权限添加到安装后环境中。
        </p>

      <h3 id="after_reboot">重新启动后</h3>

        <p>
重新启动后，<code>update_verifier</code> 会触发利用 dm-verity 进行完整性检查。系统会先启动该检查，然后再启动 zygote，以避免 Java 服务进行任何无法撤消且会导致无法进行安全回滚的更改。在此过程中，如果验证启动功能或 dm-verity 检测到任何损坏，引导加载程序和内核还可能会触发重新启动。检查完成后，<code>update_verifier</code> 会将启动标记为成功。
        </p>

        <p>
          <code>update_verifier</code> 只会读取 <code>/data/ota_package/care_map.txt</code>（在使用 AOSP 代码时，该文件会包含在 A/B OTA 更新包中）中列出的数据块。Java 系统更新客户端（例如 GmsCore）会在重新启动设备前提取 <code>care_map.txt</code> 并设置访问权限，在系统成功启动到新版本后会删除所提取的文件。
        </p>

</body></html>