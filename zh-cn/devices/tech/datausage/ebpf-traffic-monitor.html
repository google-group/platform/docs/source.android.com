<html devsite><head>

  <meta name="book_path" value="/_book.yaml"/>

  <meta name="project_path" value="/_project.yaml"/>
</head>
<body>
<!--
    Copyright 2018 The Android Open Source Project
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->

<h1 id="ebpf_traffic_monitoring" class="page-title">eBPF 流量监控</h1>

<h2 id="introduction">简介</h2>

<p>eBPF 网络流量工具可结合使用内核与用户空间实现来监控设备自上次启动以来的网络使用情况，而且还提供了一些额外的功能，例如套接字标记、分离前台/后台流量，以及可根据手机状态阻止应用访问网络的 per-UID 防火墙。从该工具收集的统计数据存储在称为 <code>eBPF maps</code> 的内核数据结构中，并且相应结果可供 <code>NetworkStatsService</code> 等服务用于提供自设备上次启动以来的持久流量统计数据。</p>

<h2 id="examples-and-source">示例和源代码</h2>

<p>对用户空间进行的更改主要在 <code>system/netd</code> 和 <code>framework/base</code> 项目中。开发工作在 AOSP 中完成，因此 AOSP 代码将始终保持最新状态。源代码主要位于 <a href="https://android.googlesource.com/platform/system/netd/+/master/server/TrafficController.cpp" class="external"><code>system/netd/server/TrafficController*</code></a>、<a href="https://android.googlesource.com/platform/system/netd/+/master/bpfloader/" class="external"><code>system/netd/bpfloader</code></a> 和 <a href="https://android.googlesource.com/platform/system/netd/+/master/libbpf/" class="external"><code>system/netd/libbpf/</code></a> 中。另外，对框架进行的一些必要更改也在 <code>framework/base/</code> 和 <code>system/core</code> 中。</p>

<h2 id="implementation">实现</h2>

<p>从 Android 9 开始，内核版本为 4.9 或更高且最初搭载了 Android P 版本的 Android 设备必须使用基于 eBPF 的网络流量监控记帐模块，而不是 <code>xt_qtaguid</code>。新的基础架构更灵活且更易于维护，并且不需要任何外部内核代码。</p>

<p>旧版流量监控和 eBPF 流量监控之间的主要设计差异如图 1 所示。</p>

<p><img src="/devices/images/ebpf-net-monitor.png" alt="旧版流量监控和 eBPF 流量监控的设计差异"/></p>

<p><strong>图 1.</strong> 旧版流量监控（左）和 eBPF 流量监控（右）的设计差异</p>

<p>新的 <code>trafficController</code> 设计基于 per-<code>cgroup</code> eBPF 过滤器以及内核中的 <code>xt_bpf</code> netfilter 模块。当数据包 tx/rx 通过 eBPF 过滤器时，系统会对其应用相应过滤器。<code>cgroup</code> eBPF 过滤器位于传输层中，负责根据套接字 UID 和用户空间设置对正确的 UID 计算流量。<code>xt_bpf</code> netfilter 连接在 <code>bw_raw_PREROUTING</code> 和 <code>bw_mangle_POSTROUTING</code> 链上，负责对正确的接口计算流量。</p>

<p>在设备启动时，用户空间进程 <code>trafficController</code> 会创建用于收集数据的 eBPF 映射，并将所有映射作为虚拟文件固定在 <code>sys/fs/bpf</code>。然后，特权进程 <code>bpfloader</code> 将预编译的 eBPF 程序加载到内核中，并将其附加到正确的 <code>cgroup</code>。所有流量都对应于同一个根 <code>cgroup</code>，因此默认情况下，所有进程都应包含在该 <code>cgroup</code> 中。</p>

<p>在系统运行时，<code>trafficController</code> 可以通过写入到 <code>traffic_cookie_tag_map</code> 和 <code>traffic_uid_counterSet_map</code>，对套接字进行标记/取消标记。<code>NetworkStatsService</code> 可以从 <code>traffic_tag_stats_map</code>、<code>traffic_uid_stats_map</code> 和 <code>traffic_iface_stats_map</code> 中读取流量统计数据。除了流量统计数据收集功能外，<code>trafficController</code> 和 <code>cgroup</code> eBPF 过滤器还负责根据手机设置屏蔽来自特定 UID 的流量。基于 UID 的网络流量屏蔽功能取代了内核中的 <code>xt_owner</code> 模块，详细模式可以通过写入到 <code>traffic_powersave_uid_map</code>、<code>traffic_standby_uid_map</code> 和 <code>traffic_dozable_uid_map</code> 来进行配置。</p>

<p>新实现遵循旧版 <code>xt_qtaguid</code> 模块实现，因此 <code>TrafficController</code> 和 <code>NetworkStatsService</code> 将使用旧版实现或新实现运行。如果应用使用公共 API，则无论后台使用 <code>xt_qtaguid</code> 还是 eBPF 工具，都不应受到任何影响。</p>

<p>如果设备内核基于 Android 通用内核 4.9（SHA39c856663dcc81739e52b02b77d6af259eb838f6 或以上版本），则实现新的 eBPF 工具时无需修改 HAL、驱动程序或内核代码。</p>

<h2 id="requirements">要求</h2>

<ol>
<li><p>内核配置必须开启以下配置：</p>

<ol>
<li><code>CONFIG_CGROUP_BPF=y</code></li>
<li><code>CONFIG_BPF=y</code></li>
<li><code>CONFIG_BPF_SYSCALL=y</code></li>
<li><code>CONFIG_NETFILTER_XT_MATCH_BPF=y</code></li>
</ol>

<p>验证是否已开启正确配置时，<a href="https://android.googlesource.com/platform/test/vts-testcase/kernel/+/master/config/VtsKernelConfigTest.py" class="external">VTS 内核配置测试</a>非常有用。</p></li>
<li><p>设备 <code>MEM_LOCK</code> 资源限制必须设为 8 MB 或更多。</p></li>
</ol>

<h2 id="legacy-xt_qtaguid-deprecation-process">旧版 xt_qtaguid 弃用过程</h2>

<p>新的 eBPF 工具将取代 <code>xt_qtaguid</code> 模块以及它所基于的 <code>xt_owner</code> 模块。我们将开始从 Android 内核中移除 <code>xt_qtaguid</code> 模块，并停用不必要的配置。</p>

<p>在 Android 9 版本中，<code>xt_qtaguid</code> 模块在所有设备上都处于开启状态，不过，直接读取 <code>xt_qtaguid</code> 模块 proc 文件的所有公共 API 都将移至 <code>NetworkManagement</code> 服务中。根据设备内核版本和首个 API 级别，<code>NetworkManagement</code> 服务能够知道 eBPF 工具是否处于开启状态，并选择正确的模块来获取每个应用的网络使用情况统计数据。sepolicy 会阻止 SDK 级别为 28 及以上的应用访问 <code>xt_qtaguid</code> proc 文件。</p>

<p>从 Android 9 之后的下一个版本起，我们将完全阻止应用访问这些 <code>xt_qtaguid</code> proc 文件，并开始从新的 Android 通用内核中移除 <code>xt_qtaguid</code> 模块。移除该模块后，我们将更新相应内核版本的 Android 基础配置，以明确关闭 <code>xt_qtaguid</code> 模块。当 Android 版本的最低内核版本要求为 4.9 或更高时，我们将彻底弃用 <code>xt_qtaguid</code> 模块。</p>

<p>在 Android 9 版本中，只有搭载 Android 9 版本的设备才需要具备新的 eBPF 功能。如果设备搭载的内核可以支持 eBPF 工具，我们建议在升级到 Android 9 版本时，将设备更新到采用新的 eBPF 功能。没有旨在强制执行该更新的 CTS 测试。</p>

<h2 id="validation">验证</h2>

<p>您应该定期从 Android 通用内核和 Android AOSP 主代码库获取补丁程序。请确保您的实现通过适用的 VTS 和 CTS 测试、<code>netd_unit_test</code> 以及 <code>libbpf_test</code>。</p>

<h3 id="testing">测试</h3>

<p><a href="https://android.googlesource.com/kernel/tests/+/master/net/test/bpf_test.py" class="external">内核 net_tests</a> 旨在确保您已开启必要的功能，并向后移植了必要的内核补丁程序。这些测试已集成到 Android 9 版本 VTS 测试中。<code>system/netd/</code> 中有一些单元测试（<a href="https://android.googlesource.com/platform/system/netd/+/master/server/TrafficControllerTest.cpp" class="external"><code>netd_unit_test</code></a> 和 <a href="https://android.googlesource.com/platform/system/netd/+/master/libbpf/BpfNetworkStatsTest.cpp" class="external"><code>libbpf_test</code></a>）。<code>netd_integration_test</code> 中有一些旨在验证新工具整体行为的测试。</p>

<h4 id="cts-and-cts-verifier">CTS 和 CTS 验证程序</h4>

<p>由于 Android 9 版本支持这两种流量监控模块，因此没有旨在强制在所有设备上实现新模块的 CTS 测试。不过，对于内核版本高于 4.9 且最初搭载了 Android 9 版本（即首个 API 级别 &gt;= 28）的设备，GSI 上提供了一些旨在验证新模块是否已正确配置的 CTS 测试。旧的 CTS 测试（如 <code>TrafficStatsTest</code>、<code>NetworkUsageStatsTest</code> 和 <code>CtsNativeNetTestCases</code>）可用于验证新模块的行为是否与旧的 UID 模块一致。</p>

<h4 id="manual-testing">手动测试</h4>

<p><code>system/netd/</code> 中有一些单元测试（<a href="https://android.googlesource.com/platform/system/netd/+/master/server/TrafficControllerTest.cpp" class="external"><code>netd_unit_test</code></a>、<a href="https://android.googlesource.com/platform/system/netd/+/master/tests/bpf_base_test.cpp" class="external"><code>netd_integration_test</code></a> 和 <a href="https://android.googlesource.com/platform/system/netd/+/master/libbpf/BpfNetworkStatsTest.cpp" class="external"><code>libbpf_test</code></a>）。系统支持 dumpsys，以便手动检查状态。命令 <strong><code>dumpsys netd</code></strong> 可显示 <code>trafficController</code> 模块的基本状态以及 eBPF 是否已正确开启。如果 eBPF 处于开启状态，命令 <strong><code>dumpsys netd trafficcontroller</code></strong> 会显示每个 eBPF 映射的详细内容，包括带标记套接字信息、每个标记的统计数据、UID 和 iface，以及所有者 UID 匹配项。</p>

<h3 id="test-locations">测试所在位置</h3>

<p>CTS 测试位于以下位置：</p>

<ul>
<li class="external"><a href="https://android.googlesource.com/platform/cts/+/master/tests/tests/net/src/android/net/cts/TrafficStatsTest.java">https://android.googlesource.com/platform/cts/+/master/tests/tests/net/src/android/net/cts/TrafficStatsTest.java</a></li>
<li class="external"><a href="https://android.googlesource.com/platform/cts/+/master/tests/tests/app.usage/src/android/app/usage/cts/NetworkUsageStatsTest.java">https://android.googlesource.com/platform/cts/+/master/tests/tests/app.usage/src/android/app/usage/cts/NetworkUsageStatsTest.java</a></li>
<li class="external"><a href="https://android.googlesource.com/platform/system/netd/+/master/tests/bpf_base_test.cpp">https://android.googlesource.com/platform/system/netd/+/master/tests/bpf_base_test.cpp</a></li>
</ul>

<p>VTS 测试位于以下位置：<a href="https://android.googlesource.com/kernel/tests/+/master/net/test/bpf_test.py" class="external">https://android.googlesource.com/kernel/tests/+/master/net/test/bpf_test.py</a>。</p>

<p>单元测试位于以下位置：</p>

<ul>
<li class="external"><a href="https://android.googlesource.com/platform/system/netd/+/master/libbpf/BpfNetworkStatsTest.cpp">https://android.googlesource.com/platform/system/netd/+/master/libbpf/BpfNetworkStatsTest.cpp</a></li>
<li class="external"><a href="https://android.googlesource.com/platform/system/netd/+/master/server/TrafficControllerTest.cpp">https://android.googlesource.com/platform/system/netd/+/master/server/TrafficControllerTest.cpp</a></li>
</ul>

</body></html>