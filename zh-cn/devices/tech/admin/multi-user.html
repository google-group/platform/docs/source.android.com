<html devsite><head>
    <title>支持多用户</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>通过将用户帐号和应用数据进行分离，Android 支持在一台 Android 设备上添加多名用户。例如，父母可能会允许他们的孩子使用家用平板电脑，或应急响应团队可能会共用一台移动设备用于呼叫值班。</p>

<h2 id="definitions">术语</h2>
<p>Android 在描述 Android 用户和帐号时会使用以下术语。</p>

<h3 id="general_defs">通用</h3>
<p>Android 设备管理会使用以下通用术语。</p>

<ul>
  <li>用户：<em></em>每个用户分别由一个自然人使用。每个用户都有不同的应用数据和一些独特的设置，以及可在多个用户之间明确切换的用户界面。当某个用户处于活动状态时，另一个用户可以在后台运行；系统会在适当的时候尝试关闭用户来节约资源。次要用户可直接通过主要用户界面或<a href="https://developer.android.com/guide/topics/admin/device-admin.html">设备管理</a>应用进行创建。</li>
  <li>帐号：<em></em>帐号包含在用户中，但并非由某个用户定义；用户既不由任何特定帐号进行定义，也不会关联到任何特定帐号。用户和个人资料包含其各自的唯一帐号；不过，即使没有用户和个人资料，帐号也可以正常发挥作用。帐号列表因用户而异。有关详细信息，请参阅 <a href="https://developer.android.com/reference/android/accounts/Account.html">Account 类</a>定义。</li>
  <li>个人资料：<em></em>个人资料具有独立的应用数据，但会共享一些系统范围内的设置（例如 WLAN 和蓝牙）。个人资料会与已存在用户绑定在一起，并是已存在用户的子集。一个用户可以有多份个人资料。个人资料通过<a href="https://developer.android.com/guide/topics/admin/device-admin.html">设备管理</a>应用进行创建。个人资料由创建个人资料的用户进行定义，它与父用户之间总是存在不可变的关联。个人资料的存在时间不会超出相应创建用户的生命周期。</li>
  <li>应用：应用的数据存在于各关联用户中。<em></em>同一用户的其他应用的应用数据均已沙盒化。同一用户的应用可以通过 IPC 相互影响。如需了解详情，请参阅<a href="https://developer.android.com/training/enterprise/index.html">编译 Apps for Work</a>。</li>
</ul>

<h3 id="user_types">用户类型</h3>
<p>Android 设备管理使用以下用户类型。</p>

<ul>
  <li>主要用户。<em></em>添加到设备的第一个用户。除非恢复出厂设置，否则无法移除主要用户；此外，即使其他用户在前台运行，主要用户也会始终处于运行状态。该用户还拥有只有自己可以设置的特殊权限和设置。</li>
  <li>次要用户。<em></em>除主要用户之外添加到设备的任何用户。次要用户可以移除（由用户自行移除或由主要用户移除），且不会影响设备上的其他用户。这种用户可以在后台运行且可以继续连接到网络。</li>
  <li>访客：临时的次要用户。<em></em>访客用户设置中有明确的删除选项，以便在访客用户帐号过期时快速将其删除。一次只能创建一个访客用户。</li>
</ul>

<h3 id="profile_types">个人资料类型</h3>
<p>Android 设备管理使用以下个人资料类型。</p>

<ul>
  <li>受管理。<em></em>由应用创建，包含工作数据和应用。受管理个人资料专门由个人资料所有者（创建企业资料的应用）管理。启动器、通知和最近的任务均由主要用户和企业资料共享。</li>
  <li>受限。<em></em>由主要用户（控制受限个人资料上哪些应用可用）控制的帐号。仅适用于平板电脑和电视设备。</li>
</ul>

<h2 id="applying_the_overlay">支持多用户</h2>

<p>从 Android 5.0 开始，多用户功能默认处于停用状态。要启用这项功能，设备制造商必须定义资源叠加层，以取代 <code>frameworks/base/core/res/res/values/config.xml</code> 中的以下值：</p>

<pre class="devsite-click-to-copy">
&lt;!--  Maximum number of supported users --&gt;
&lt;integer name="config_multiuserMaximumUsers"&gt;1&lt;/integer&gt;
&lt;!--  Whether Multiuser UI should be shown --&gt;
&lt;bool name="config_enableMultiUserUI"&gt;false&lt;/bool&gt;
</pre>

<p>要应用此叠加层并在设备上启用访客和次要用户，请使用 Android 编译系统的 <code>DEVICE_PACKAGE_OVERLAYS</code> 功能执行以下操作：</p>

<ul>
  <li>将 <code>config_multiuserMaximumUsers</code> 的值替换为大于 1 的数</li>
  <li>将 <code>config_enableMultiUserUI</code> 的值替换为 <code>true</code></li>
</ul>

<p>设备制造商可以决定用户数上限。如果设备制造商或其他人修改了设置，他们必须确保短信和电话通讯遵循 <a href="/compatibility/android-cdd.pdf">Android 兼容性定义文档</a> (CDD) 中定义的说明。</p>

<h2 id="managing_users">管理多个用户</h2>

<p>用户和个人资料（受限个人资料除外）由以编程方式在 <code>DevicePolicyManager</code> 类中调用 API 的应用进行管理以限制使用。</p>

<p>学校和企业可以将上述类型与 <a href="http://developer.android.com/reference/android/os/UserManager.html">UserManager API</a> 结合使用，以针对其使用情况构建独特的解决方案，从而借助用户和个人资料来管理设备上应用和数据的生命周期和适用范围。</p>

<h2 id="effects">多用户系统行为</h2>

<p>将用户添加到设备后，当其他用户在前台活动时，一些功能会受限制。由于应用数据会按用户分开，因此，这些应用的状态也会因用户而异。例如，在设备上激活某个用户和帐号之前，发往该用户帐号（当前处于未激活状态）的电子邮件将无法查看。</p>

<p>默认情况下，只有主要用户拥有电话和短信的完全访问权限。次要用户可以接听呼入电话，但是不能发送或接收短信。主要用户必须为其他人启用这些功能，他们才能使用这些功能。</p>

<p class="note"><strong>注意</strong>：要为次要用户启用或停用电话和短信功能，请依次转到“设置”&gt;“用户”，选择相应用户，然后将“允许接打电话和收发短信”设置切换到关闭状态。<em></em><em></em></p>

<p>次要用户在后台活动时会受到一些限制。例如，后台次要用户无法显示界面或开启蓝牙服务。此外，如果设备需要更多内存才能满足前台用户的操作需求，系统进程将暂停后台次要用户的活动。</p>

<p>在 Android 设备上使用多用户功能时，请注意以下几点：</p>

<ul>
  <li>通知会同时出现在一位用户的所有帐号中。</li>
  <li>其他用户的通知在激活用户后才会显示。</li>
  <li>每位用户都会获得用于安装和放置应用的工作区。</li>
  <li>任何用户都无权访问其他用户的应用数据。</li>
  <li>任何用户都可以影响为所有用户安装的应用。</li>
  <li>主要用户可以移除次要用户所创建的应用甚至整个工作区。</li>
</ul>

<p>Android 7.0 具备一些增强功能，例如：</p>

<ul>
  <li>切换工作资料。<em></em>用户可以停用其受管理的个人资料（例如不使用时）。这项功能可以通过停止用户来实现；UserManagerService 会调用 <code>ActivityManagerNative#stopUser()</code>。
  </li>
  <li>始终开启的 VPN。VPN 应用现已可以由用户、设备 DPC 或 Managed Profile DPC（仅适用于受管理个人资料应用）设置为始终开启。<em></em>启用后，应用将无法访问公共网络（在连接 VPN 且连接可以通过它正常进行路由前，无法访问网络资源）。报告 <code>device_admin</code> 的设备必须实现始终开启的 VPN。</li>
</ul>

<p>要详细了解 Android 7.0 设备管理功能，请参阅 <a href="https://developer.android.com/about/versions/nougat/android-7.0.html#android_for_work">Android for Work</a>。</p>

</body></html>