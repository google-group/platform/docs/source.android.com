<html devsite><head>
    <title>实现 AudioControl HAL</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>

  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
Android 9 弃用了之前各版车载 HAL 中的 <code>AUDIO_*</code> 属性，并改为使用包含显式函数调用和已输入参数列表的专用音频控制 HAL。
</p>

<p>
这个新的 HAL 提供了 <code>IAudioControl</code> 作为主接口对象，该对象负责提供出于配置和音量控制目的与车辆的音频引擎进行交互所需的入口点。系统可以包含该对象的一个实例，即 <code>CarAudioService</code> 启动时创建的实例。该对象是传统 Android 音频 HAL 的汽车扩展；在大多数实现中，发布音频 HAL 接口的进程还应发布 <code>IAudioControl interfaces</code>。
</p>

<h2 id="supported-interfaces">支持的接口</h2>

<p>
<code>AudioControl</code> HAL 支持以下接口：
</p>

<ul>
  <li><code><strong>getBusforContext</strong></code>：在启动时针对每个上下文调用一次，以获取从 <code>ContextNumber</code> 到 <code>busAddress</code> 的映射。用法示例：

<pre class="prettyprint">
getBusForContext(ContextNumber contextNumber)
    generates (uint32_t busNumber);
</pre>

使车辆能够针对每个上下文告诉框架将物理输出音频流路由到哪里。对于每个上下文，必须返回有效的总线编号（0 - 总线数-1）。如果遇到无法识别的 <code>contextNumber</code>，则应返回 -1。对于任何上下文，如果返回无效的 <code>busNumber</code>，都会路由到总线 0。
<br /><br />
任何通过此机制与同一个 <code>busNumber</code> 相关联的并发声音都会先通过 Android <code>AudioFlinger</code> 进行混音，然后才会作为单个音频流提供给音频 HAL。这会取代车载 HAL 属性 <code>AUDIO_HW_VARIANT</code> 和 <code>AUDIO_ROUTING_POLICY</code>。
  </li>

  <li><code><strong>setBalanceTowardRight</strong></code>：用于控制车载音响设备的右/左平衡设置。用法示例：

<pre class="prettyprint">
setBalanceTowardRight(float value);
</pre>

将音响设备音量调向汽车右侧 (+) 或左侧 (-)。0.0 表示在中间，+1.0 表示完全调到右侧，-1.0 表示完全调到左侧，如果值未在 -1 到 1 之间，则是错误的。
  </li>

  <li><code><strong>setFadeTowardFront</strong></code>：用于控制车载音响设备的前/后淡化设置。用法示例：

<pre class="prettyprint">
setFadeTowardFront(float value);
</pre>

将音响设备音量调向汽车前面 (+) 或后面 (-)。0.0 表示在中间，+1.0 表示完全调到前面，-1.0 表示完全调到后面，如果值未在 -1 到 1 之间，则是错误的。
  </li>
</ul>

<h2 id="configure-volume">配置音量</h2>

<p>
Android Automotive 实现应使用硬件放大器（而非软件混音器）来控制音量。为避免产生副作用，请在 <code>device/generic/car/emulator/audio/overlay/frameworks/base/core/res/res/values/config.xml</code> 中将 <code>config_useFixedVolume</code> 标记设为 <code>true</code>（根据需要叠加）：
</p>

<pre class="prettyprint">
&lt;resources&gt;
    &lt;!-- Car uses hardware amplifier for volume. --&gt;
    &lt;bool name="config_useFixedVolume"&gt;true&lt;/bool&gt;
&lt;/resources&gt;
</pre>

<p>
<code>config_useFixedVolume</code> 标记未设置或设为 <code>false</code> 时，应用可以调用 <code>AudioManager.setStreamVolume()</code>，并在软件混音器中按音频流类型更改音量。用户可能不希望出现这种情况，因为这会对其他应用带来潜在影响，而且使用硬件放大器接收信号时，软件混音器中的音量衰减会导致信号中的可用有效位减少。
</p>

<h2 id="configure-volume-groups">配置音量组</h2>

<p>
  <code>CarAudioService</code> 使用 <code>packages/services/Car/service/res/xml/car_volume_group.xml</code> 中定义的音量组。您可以替换此文件，以便根据需要重新定义音量组。系统在运行时会按 XML 文件中的定义顺序来识别音量组。ID 介于 0 到 N-1 之间，其中 N 是音量组的数量。示例：
</p>

<pre class="prettyprint">
&lt;volumeGroups xmlns:car="http://schemas.android.com/apk/res-auto"&gt;
    &lt;group&gt;
        &lt;context car:context="music"/&gt;
        &lt;context car:context="call_ring"/&gt;
        &lt;context car:context="notification"/&gt;
        &lt;context car:context="system_sound"/&gt;
    &lt;/group&gt;
    &lt;group&gt;
        &lt;context car:context="navigation"/&gt;
        &lt;context car:context="voice_command"/&gt;
    &lt;/group&gt;
    &lt;group&gt;
        &lt;context car:context="call"/&gt;
    &lt;/group&gt;
    &lt;group&gt;
        &lt;context car:context="alarm"/&gt;
    &lt;/group&gt;
&lt;/volumeGroups&gt;
</pre>

<p>
此配置中使用的属性在 <code>packages/services/Car/service/res/values/attrs.xml</code> 中定义。
</p>

<h2 id="handle-volumn-key-events">处理音量键事件</h2>

<p>
Android 定义了一些用于控制音量的键码，包括 <code>KEYCODE_VOLUME_UP</code>、<code>KEYCODE_VOLUME_DOWN</code> 和 <code>KEYCODE_VOLUME_MUTE</code>。默认情况下，Android 会将音量键事件路由到应用。Automotive 实现应强制将这些键事件路由到 <code>CarAudioService</code>，以便该服务可以根据情况适当地调用 <code>setGroupVolume</code> 或 <code>setMasterMute</code>。
</p>

<p>
要强制实现此行为，请在 <code>device/generic/car/emulator/car/overlay/frameworks/base/core/res/res/values/config.xml</code> 中将 <code>config_handleVolumeKeysInWindowManager</code> 标记设为 <code>true</code>：
</p>

<pre class="prettyprint">
&lt;resources&gt;
    &lt;bool name="config_handleVolumeKeysInWindowManager"&gt;true&lt;/bool&gt;
&lt;/resources&gt;
</pre>

<h2 id="caraudiomanager-api">CarAudioManager API</h2>

<p>
<code>CarAudioManager</code> 使用 <code>CarAudioService</code> 来配置和控制车载音频系统。该管理器对于系统中的大多数应用来说都是不可见的，但车辆专用组件（如音量控制器）可以使用 <code>CarAudioManager</code> API 与系统交互。
</p>

<p>
下文介绍了 Android 9 对 <code>CarAudioManager API</code> 所做的更改。
</p>

<h3 id="deprecated-apis">弃用的 API</h3>

<p>
Android 9 通过现有的 <code>AudioManager</code> <code>getDeviceList</code> API 处理设备枚举，因此弃用并移除了以下车辆专用函数：
</p>

<ul>
  <li><code>String[] getSupportedExternalSourceTypes()</code></li>
  <li><code>String[] getSupportedRadioTypes()</code></li>
</ul>

<p>
Android 9 使用 <code>AudioAttributes.AttributeUsage</code> 或基于音量组的入口点处理音量，因此移除了以下依赖于 <code>streamType</code> 的 API：
</p>

<ul>
  <li><code>void setStreamVolume(int streamType, int index, int flags)</code>
  </li>
  <li><code>int getStreamMaxVolume(int streamType)</code></li>
  <li><code>int getStreamMinVolume(int streamType)</code></li>
  <li><code>void setVolumeController(IVolumeController controller)</code></li>
</ul>

<h3 id="new-apis">新增的 API</h3>

<p>
Android 9 添加了以下用于控制放大器硬件的新 API（明确基于音量组）：
</p>

<ul>
  <li><code>int getVolumeGroupIdForUsage(@AudioAttributes.AttributeUsage int
  usage)</code></li>
  <li><code>int getVolumeGroupCount()</code></li>
  <li><code>int getGroupVolume(int groupId)</code></li>
  <li><code>int getGroupMaxVolume(int groupId)</code></li>
  <li><code>int getGroupMinVolume(int groupId)</code></li>
</ul>

<p>
Android 9 还提供了以下全新的系统 API 供系统 GUI 使用：
</p>

<ul>
  <li><code>void setGroupVolume(int groupId, int index, int flags)</code></li>
  <li><code>void registerVolumeChangeObserver(@NonNull ContentObserver
  observer)</code></li>
  <li><code>void unregisterVolumeChangeObserver(@NonNull ContentObserver
  observer)</code></li>
  <li><code>void registerVolumeCallback(@NonNull IBinder binder)</code></li>
  <li><code>void unregisterVolumeCallback(@NonNull IBinder binder)</code></li>
  <li><code>void setFadeToFront(float value)</code></li>
  <li><code>Void setBalanceToRight(float value)</code></li>
</ul>

<p>
此外，Android 9 还添加了用于管理外部来源的新 API。这些 API 主要用于支持根据媒体类型将音频从外部来源路由到输出总线。借助这些 API，第三方应用还能够访问外部设备。
</p>

<ul>
  <li><code>String[] getExternalSources()</code>：返回一个地址数组，这些地址用于识别系统中 <code>AUX_LINE</code>、<code>FM_TUNER</code>、<code>TV_TUNER</code> 和 <code>BUS_INPUT</code> 类型的可用音频端口。</li>
  <li><code>CarPatchHandle createAudioPatch(String sourceAddress, int
  carUsage)</code>：将来源地址路由到与所提供的 <code>carUsage</code> 关联的输出 <code>BUS</code>。</li>
  <li><code>int releaseAudioPatch(CarPatchHandle patch)</code>：移除所提供的音频通路。如果 <code>CarPatchHandle</code> 的创建者意外终止，则由 <code>AudioPolicyService::removeNotificationClient()</code> 自动处理。</li>
</ul>

<h2 id="create-audio-patches">创建音频通路</h2>

<p>
您可以在两个音频端口（混音端口或设备端口）之间创建音频通路。通常，从混音端口到设备端口的音频通路用于播放音频，而从设备端口到混音端口的音频通路则用于捕获音频。</p>

<p>
例如，将音频样本直接从 <code>FM_TUNER</code> 来源路由到媒体接收器的音频通路会绕过软件混音器。因此，您必须使用硬件混音器为接收器将来自 Android 和 <code>FM_TUNER</code> 的音频样本进行混音。创建直接从 <code>FM_TUNER</code> 来源到媒体接收器的音频通路时：
</p>

<ul>
  <li>音量控制会应用于媒体接收器，并且应该会影响 Android 和 <code>FM_TUNER</code> 音频。</li>
  <li>用户应该能够通过简单的应用切换在 Android 和 <code>FM_TUNER</code> 音频之间进行切换（应该不需要必须选择显式媒体来源）。</li>
</ul>

<p>
Automotive 实现可能还需要在两个设备端口之间创建音频通路。为此，您必须先在 <code>audio_policy_configuration.xml</code> 中声明设备端口和可能的路由，并将混音端口与这些设备端口相关联。
</p>

<h3 id="example-config">配置示例</h3>

<p>
另请参阅 <code>device/generic/car/emulator/audio/audio_policy_configuration.xml</code>。
</p>

<pre class="prettyprint">
&lt;audioPolicyConfiguration&gt;
    &lt;modules&gt;
        &lt;module name="primary" halVersion="3.0"&gt;
            &lt;attachedDevices&gt;
                &lt;item&gt;bus0_media_out&lt;/item&gt;
                &lt;item&gt;bus1_audio_patch_test_in&lt;/item&gt;
            &lt;/attachedDevices&gt;
            &lt;mixPorts&gt;
                &lt;mixPort name="mixport_bus0_media_out" role="source"
                        flags="AUDIO_OUTPUT_FLAG_PRIMARY"&gt;
                    &lt;profile name="" format="AUDIO_FORMAT_PCM_16_BIT"
                            samplingRates="48000"
                            channelMasks="AUDIO_CHANNEL_OUT_STEREO"/&gt;
                &lt;/mixPort&gt;
                &lt;mixPort name="mixport_audio_patch_in" role="sink"&gt;
                    &lt;profile name="" format="AUDIO_FORMAT_PCM_16_BIT"
                           samplingRates="48000"
                           channelMasks="AUDIO_CHANNEL_IN_STEREO"/&gt;
                &lt;/mixPort&gt;
            &lt;/mixPorts&gt;
            &lt;devicePorts&gt;
                &lt;devicePort tagName="bus0_media_out" role="sink" type="AUDIO_DEVICE_OUT_BUS"
                        address="bus0_media_out"&gt;
                    &lt;profile balance="" format="AUDIO_FORMAT_PCM_16_BIT"
                            samplingRates="48000" channelMasks="AUDIO_CHANNEL_OUT_STEREO"/&gt;
                    &lt;gains&gt;
                        &lt;gain name="" mode="AUDIO_GAIN_MODE_JOINT"
                                minValueMB="-8400" maxValueMB="4000" defaultValueMB="0" stepValueMB="100"/&gt;
                    &lt;/gains&gt;
                &lt;/devicePort&gt;
                &lt;devicePort tagName="bus1_audio_patch_test_in" type="AUDIO_DEVICE_IN_BUS" role="source"
                        address="bus1_audio_patch_test_in"&gt;
                    &lt;profile name="" format="AUDIO_FORMAT_PCM_16_BIT"
                            samplingRates="48000" channelMasks="AUDIO_CHANNEL_IN_STEREO"/&gt;
                    &lt;gains&gt;
                        &lt;gain name="" mode="AUDIO_GAIN_MODE_JOINT"
                                minValueMB="-8400" maxValueMB="4000" defaultValueMB="0" stepValueMB="100"/&gt;
                    &lt;/gains&gt;
                &lt;/devicePort&gt;
            &lt;/devicePorts&gt;
            &lt;routes&gt;
                &lt;route type="mix" sink="bus0_media_out" sources="mixport_bus0_media_out,bus1_audio_patch_test_in"/&gt;
                &lt;route type="mix" sink="mixport_audio_patch_in" sources="bus1_audio_patch_test_in"/&gt;
            &lt;/routes&gt;
        &lt;/module&gt;
    &lt;/modules&gt;
&lt;/audioPolicyConfiguration&gt;
</pre>

<h3 id="audio-driver-api">音频驱动程序 API</h3>

<p>
您可以使用 <code>getExternalSources()</code> 获取可用来源列表（按地址进行标识），然后按音频用法在这些来源和接收器端口之间创建音频通路。音频 HAL 上相应的入口点显示在 <code>IDevice.hal</code> 中：
</p>

<pre class="prettyprint">
Interface IDevice {
...
/**
* Creates an audio patch between several source and sink ports.  The handle
* is allocated by the HAL and must be unique for this audio HAL module.
*
* @param sources patch sources.
* @param sinks patch sinks.
* @return retval operation completion status.
* @return patch created patch handle.
*/
createAudioPatch(vec&lt;AudioPortConfig&gt; sources, vec&lt;AudioPortConfig&gt; sinks)
       generates (Result retval, AudioPatchHandle patch);

/**
* Release an audio patch.
*
* @param patch patch handle.
* @return retval operation completion status.
*/
releaseAudioPatch(AudioPatchHandle patch) generates (Result retval);
...
}
</pre>

<aside class="note"><strong>注意</strong>：这些 API 钩子从 AUDIO_DEVICE_API_VERSION_3_0 便已开始提供。如需了解更多详情，请参阅 <code>device/generic/car/emulator/audio/driver/audio_hw.c</code>。</aside>

<h2 id="configure-volume-settings-ui">配置音量设置界面</h2>

<p>
Android 9 将音量设置界面从音量组配置（该配置可叠加，如“配置音量组”中所述）中分离出来。这种分离可确保将来音量组配置发生更改时，无需对音量设置界面进行任何更改。
</p>

<p>
在汽车设置界面中，<code>packages/apps/Car/Settings/res/xml/car_volume_items.xml</code> 文件包含与每个定义的 <code>AudioAttributes.USAGE</code> 相关联的界面元素（标题和图标资源）。此文件通过使用与每个 VolumeGroup 中包含的首个识别出的用法相关联的资源，合理呈现已定义的 VolumeGroups。
</p>

<p>
例如，以下示例将 VolumeGroup 定义为同时包含 <code>voice_communication</code> 和 <code>voice_communication_signalling</code>。汽车设置界面的默认实现使用与 <code>voice_communication</code> 相关联的资源呈现 VolumeGroup，因为它是文件中的首个匹配项。
</p>

<pre class="prettyprint">
&lt;carVolumeItems xmlns:car="http://schemas.android.com/apk/res-auto"&gt;
    &lt;item car:usage="voice_communication"
          car:title="@*android:string/volume_call"
          car:icon="@*android:drawable/ic_audio_ring_notif"/&gt;
    &lt;item car:usage="voice_communication_signalling"
          car:title="@*android:string/volume_call"
          car:icon="@*android:drawable/ic_audio_ring_notif"/&gt;
    &lt;item car:usage="media"
          car:title="@*android:string/volume_music"
          car:icon="@*android:drawable/ic_audio_media"/&gt;
    &lt;item car:usage="game"
          car:title="@*android:string/volume_music"
          car:icon="@*android:drawable/ic_audio_media"/&gt;
    &lt;item car:usage="alarm"
          car:title="@*android:string/volume_alarm"
          car:icon="@*android:drawable/ic_audio_alarm"/&gt;
    &lt;item car:usage="assistance_navigation_guidance"
          car:title="@string/navi_volume_title"
          car:icon="@drawable/ic_audio_navi"/&gt;
    &lt;item car:usage="notification_ringtone"
          car:title="@*android:string/volume_ringtone"
          car:icon="@*android:drawable/ic_audio_ring_notif"/&gt;
    &lt;item car:usage="assistant"
          car:title="@*android:string/volume_unknown"
          car:icon="@*android:drawable/ic_audio_vol"/&gt;
    &lt;item car:usage="notification"
          car:title="@*android:string/volume_notification"
          car:icon="@*android:drawable/ic_audio_ring_notif"/&gt;
    &lt;item car:usage="notification_communication_request"
          car:title="@*android:string/volume_notification"
          car:icon="@*android:drawable/ic_audio_ring_notif"/&gt;
    &lt;item car:usage="notification_communication_instant"
          car:title="@*android:string/volume_notification"
          car:icon="@*android:drawable/ic_audio_ring_notif"/&gt;
    &lt;item car:usage="notification_communication_delayed"
          car:title="@*android:string/volume_notification"
          car:icon="@*android:drawable/ic_audio_ring_notif"/&gt;
    &lt;item car:usage="notification_event"
          car:title="@*android:string/volume_notification"
          car:icon="@*android:drawable/ic_audio_ring_notif"/&gt;
    &lt;item car:usage="assistance_accessibility"
          car:title="@*android:string/volume_notification"
          car:icon="@*android:drawable/ic_audio_ring_notif"/&gt;
    &lt;item car:usage="assistance_sonification"
          car:title="@*android:string/volume_unknown"
          car:icon="@*android:drawable/ic_audio_vol"/&gt;
    &lt;item car:usage="unknown"
          car:title="@*android:string/volume_unknown"
          car:icon="@*android:drawable/ic_audio_vol"/&gt;
&lt;/carVolumeItems&gt;
</pre>

<p>
上述配置中使用的属性和值在 <code>packages/apps/Car/Settings/res/values/attrs.xml</code> 中声明。音量设置界面使用以下基于 <code>VolumeGroup</code> 的 <code>CarAudioManager</code> API：
</p>

<ul>
  <li><code>getVolumeGroupCount()</code>：用于了解应绘制多少个控件。</li>
  <li><code>getGroupMinVolume()</code> 和 <code>getGroupMaxVolume()</code>：用于获取音量上限和下限。</li>
  <li><code>getGroupVolume()</code>：用于获取当前音量。</li>
  <li><code>registerVolumeChangeObserver()</code>：用于获取音量更改通知。</li>
</ul>

</body></html>