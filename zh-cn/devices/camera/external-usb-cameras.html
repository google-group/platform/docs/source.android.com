<html devsite><head>

  <meta name="book_path" value="/_book.yaml"/>

  <meta name="project_path" value="/_project.yaml"/>
</head>
<body>
<!--
  Copyright 2018 The Android Open Source Project

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<h1 id="external_usb_cameras" class="page-title">外接 USB 摄像头</h1>

<p>Android 平台支持使用即插即用的 USB 摄像头（例如网络摄像头），但前提是这些摄像头采用标准的 <a href="https://developer.android.com/reference/android/hardware/camera2/package-summary.html">Android Camera2 API</a> 和摄像头 <a href="/reference/hidl/android/hardware/camera/provider/2.4/ICameraProvider">HIDL</a> 接口。网络摄像头通常支持 <a href="https://en.wikipedia.org/wiki/USB_video_device_class">USB 视频类 (UVC)</a> 驱动程序，并且在 Linux 上，系统采用标准的 <a href="https://en.wikipedia.org/wiki/Video4Linux">Video4Linux (V4L)</a> 驱动程序控制 UVC 摄像头。</p>

<p>如果系统支持网络摄像头，设备便可用于视频聊天和照片冲印机等轻量级用例。此功能并不是为了替代 Android 手机上典型的内部摄像头 HAL，也不是为了协助执行涉及高分辨率和高速流式传输、AR 以及手动 ISP/传感器/智能镜头控制的性能密集型复杂任务。</p>

<p>全新的 USB 摄像头 HAL 进程是外接摄像头提供程序的一部分，该提供程序会监听 USB 设备可用性，并相应地枚举外接摄像头设备。该进程具有与内置摄像头 HAL 进程类似的权限和 SE 策略。直接与 USB 设备通信的第三方网络摄像头应用访问 UVC 设备时所需的摄像头权限与所有常规摄像头应用所需的权限相同。</p>

<h2 id="examples_and_sources">示例和源代码</h2>

<p>要详细了解如何实现 USB 摄像头，请参阅 <a href="https://android.googlesource.com/platform/hardware/interfaces/+/master/camera/provider/2.4/default/"><code>ExternalCameraProvider</code></a> 中的外接摄像头提供程序参考实现。外接摄像头设备和会话实现包含在 <a href="https://android.googlesource.com/platform/hardware/interfaces/+/master/camera/device/"><code>ExternalCameraDevice</code></a> 和 <a href="https://android.googlesource.com/platform/hardware/interfaces/+/master/camera/device/"><code>ExternalCameraDeviceSession</code></a> 中。Java 客户端 API 包含一个新的 <a href="https://developer.android.com/reference/android/hardware/camera2/CameraMetadata?authuser=3#INFO_SUPPORTED_HARDWARE_LEVEL_EXTERNAL"><code>EXTERNAL</code></a> 硬件级别。</p>

<h2 id="implementation">实现</h2>

<p>实现必须支持 <a href="https://developer.android.com/guide/topics/connectivity/usb/host"><code>android.hardware.usb.host</code></a> 系统功能。</p>

<p>此外，还必须启用对 UVC 设备的内核支持。您可以通过将以下内容添加到相应的内核 <code>deconfig</code> 文件来启用该支持。</p>
<pre class="prettyprint"><code>+CONFIG_USB_VIDEO_CLASS=y
+CONFIG_MEDIA_USB_SUPPORT=y
</code></pre><aside class="note"><strong>注意</strong>：<span>请确保您还针对 uvcvideo 安装了这个<a href="https://patchwork.kernel.org/patch/6874491/">补丁程序</a>。</span></aside>
<p>要在相应的设备细分版本中启用外接摄像头提供程序，以便添加必要的 SELinux 权限、外接摄像头配置以及外接摄像头提供程序依赖项，请完成以下步骤：</p>

<ul>
<li><p>将外接摄像头配置文件和外接摄像头库添加到 <code>device.mk</code></p>
<pre class="prettyprint"><code>+PRODUCT_PACKAGES += android.hardware.camera.provider@2.4-impl
+PRODUCT_PACKAGES += android.hardware.camera.provider@2.4-external-service

+PRODUCT_COPY_FILES += \
+device/manufacturerX/productY/external_camera_config.xml:$(TARGET_COPY_OUT_VENDOR)/etc/external_camera_config.xml
</code></pre></li>
<li><p>将外接摄像头提供程序名称添加到设备 Treble HAL 清单</p>
<pre class="prettyprint"><code>&lt;hal format="hidl"&gt;
   &lt;name&gt;android.hardware.camera.provider&lt;/name&gt;
   &lt;transport arch="32+64"&gt;passthrough&lt;/transport&gt;
   &lt;impl level="generic"&gt;&lt;/impl&gt;
   &lt;version&gt;2.4&lt;/version&gt;
   &lt;interface&gt;
       &lt;name&gt;ICameraProvider&lt;/name&gt;
       &lt;instance&gt;legacy/0&lt;/instance&gt;
+       &lt;instance&gt;external/0&lt;/instance&gt;
   &lt;/interface&gt;
&lt;/hal&gt;
</code></pre></li>
<li><p>（选做）如果设备在 Treble 直通模式下运行，请更新 <code>sepolicy</code>，以便 <code>cameraserver</code> 可以访问 UVC 摄像头</p>
<pre class="prettyprint"><code>+# for external camera
+allow cameraserver device:dir r_dir_perms;
+allow cameraserver video_device:dir r_dir_perms;
+allow cameraserver video_device:chr_file rw_file_perms;
</code></pre></li>
</ul>

<p>以下是 <code>external_camera_config.xml</code> 的示例（省略了版权行）</p>
<pre class="prettyprint"><code>&lt;ExternalCamera&gt;
    &lt;Provider&gt;
         &lt;!-- Internal video devices to be ignored by external camera HAL --&gt;
            &lt;id&gt;0&lt;/id&gt; &lt;!-- No leading/trailing spaces --&gt;
            &lt;id&gt;1&lt;/id&gt;

    &lt;/Provider&gt;
    &lt;!-- See ExternalCameraUtils.cpp for default values of Device configurations below --&gt;
    &lt;Device&gt;
        &lt;!-- Max JPEG buffer size in bytes--&gt;
        &lt;MaxJpegBufferSize bytes="3145728"/&gt; &lt;!-- 3MB (~= 1080p YUV420) --&gt;
        &lt;!-- Size of v4l2 buffer queue when streaming &gt;= 30fps --&gt;
        &lt;!-- Larger value: more request can be cached pipeline (less janky)  --&gt;
        &lt;!-- Smaller value: use less memory --&gt;
        &lt;NumVideoBuffers count="4"/&gt;
        &lt;!-- Size of v4l2 buffer queue when streaming &lt; 30fps --&gt;
        &lt;NumStillBuffers count="2"/&gt;

        &lt;!-- List of maximum fps for various output sizes --&gt;
        &lt;!-- Any image size smaller than the size listed in Limit row will report
            fps (as minimum frame duration) up to the fpsBound value. --&gt;
        &lt;FpsList&gt;
            &lt;!-- width/height must be increasing, fpsBound must be decreasing--&gt;
            &lt;Limit width="640" height="480" fpsBound="30.0"/&gt;
            &lt;Limit width="1280" height="720" fpsBound="15.0"/&gt;
            &lt;Limit width="1920" height="1080" fpsBound="10.0"/&gt;
            &lt;!-- image size larger than the last entry will not be supported--&gt;
        &lt;/FpsList&gt;
    &lt;/Device&gt;
&lt;/ExternalCamera&gt;
</code></pre>
<h2 id="customization">自定义</h2>

<p>您可以通过常规自定义选项或设备专用的优化来提升 Android 摄像头的性能。</p>

<h3 id="general_customizations">常规自定义</h3>

<p>您可以通过修改 <code>external_camera_config.xml</code> 文件来自定义外接摄像头提供程序。具体而言，客户可以自定义以下参数：</p>

<ul>
<li>内部摄像头的排除视频节点</li>
<li>支持的图片大小和帧速率上限</li>
<li>Inflight 缓冲区数量（在卡顿与内存之间进行权衡）</li>
</ul>

<p>除了这些参数之外，您还可以添加自己的参数或开发自己的配置。</p>

<h3 id="device_specific_optimizations">设备专用的优化</h3>

<p>您还可以通过添加设备专用的优化来提升性能。</p>

<h4 id="buffer_copyscaling_and_jpeg_decodeencode">缓冲区复制/调整大小和 JPEG 解码/编码</h4>

<p>通用实现使用的是 CPU (libyuv/libjpeg)，但您可以将其替换为设备专用的优化。</p>

<h4 id="hal_output_format">HAL 输出格式</h4>

<p>通用实现采用以下输出格式：</p>

<ul>
<li>YUV_420_888：适用于视频 IMPLEMENTATION_DEFINED 缓冲区。</li>
<li>YV12：适用于所有其他 IMPLEMENTATION_DEFINED 缓冲区。</li>
</ul>

<p>为了提升性能，您可以将输出格式替换为设备专用的高效格式。此外，您还可以在自定义的实现中支持其他格式</p>

<h2 id="validation">验证</h2>

<p>支持外接摄像头的设备必须通过摄像头 CTS 测试。在整个测试运行期间，外接 USB 网络摄像头必须始终插入到特定设备，否则某些测试用例会失败。</p>
<aside class="note"><strong>注意</strong>：<span><code>media_profiles</code> 条目不适用于外接 USB 网络摄像头，因此没有 <a href="https://developer.android.com/reference/android/media/CamcorderProfile">camcorder 配置文件</a>。</span></aside>

</body></html>