<html devsite><head>
    <title>Keymaster 函数</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
<body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p>本页提供了一些对 Keymaster 硬件抽象层 (HAL) 实现人员很有帮助的详细信息。其中介绍了 API 中的每个函数、提供函数的 Keymaster 版本以及函数的默认实现方法。如需了解标记，请参阅 <a href="/security/keystore/tags">Keymaster 标记</a>页面。</p>

<p class="note">为了支持 Keymaster 3 从旧式 C 结构 HAL 转换到从采用新的硬件接口定义语言 (HIDL) 的定义生成的 C++ HAL 接口，Android 8.0 中的函数名称已更改。为此，目前函数都采用驼峰式大小写形式。例如，<code>get_key_characteristics</code> 现在是 <code>getKeyCharacteristics</code>。
</p>

<h2 id="general_implementation_guidelines">常规实现准则</h2>

<p>以下准则适用于 API 中的所有函数。</p>

<h3 id="input_pointer_parameters">输入指针参数</h3>

<p><strong>版本</strong>：1、2</p>

<p>进行指定调用时不使用的输入指针参数可以是 <code>NULL</code>。调用程序无需提供占位符。例如，某些密钥类型和模式可能不会使用 <code>inParams</code> 参数中的任何值来调用 <a href="#begin">begin</a>，因此调用程序可以将 <code>inParams</code> 设为 <code>NULL</code> 或提供一个空的参数集。调用程序也可以提供不使用的参数，而 Keymaster 方法不得发出错误。</p>

<p>如果某必需的输入参数为 NULL，Keymaster 方法应返回 <code>ErrorCode::UNEXPECTED_NULL_POINTER</code>。</p>

<p>从 Keymaster 3 开始，便已没有指针参数。所有参数均通过值或常量引用传递。</p>

<h3 id="output_pointer_parameters">输出指针参数</h3>

<p><strong>版本</strong>：1、2</p>

<p>与输入指针参数类似，不使用的输出指针参数可以是 <code>NULL</code>。如果某个方法需要在某个输出参数中返回数据，但发现该参数为 <code>NULL</code>，则应返回 <code>ErrorCode::OUTPUT_PARAMETER_NULL</code>。</p>
<p>从 Keymaster 3 开始，便已没有指针参数。所有参数均通过值或常量引用传递。</p>

<h3 id="api_misuse">API 滥用</h3>

<p><strong>版本</strong>：1、2、3</p>

<p>调用程序可以通过多种方式提出虽然不合理或很荒谬但技术上并没有错误的请求。在这种情况下，Keymaster 实现无需失败或发出诊断。实现不应诊断以下情况：使用过小的密钥、指定不相关的输入参数、重复使用 IV 或随机数、生成密钥时未指定目的（因此生成的密钥没有用处），以及类似情况。但必须诊断以下情况：缺少必需的参数、指定无效的必需参数，以及类似错误。</p>

<p>应用、框架和 Android Keystore 需负责确保对 Keymaster 模块的调用是合理的，而且是有用的。</p>

<p class="caution">Keymaster 实现应该尝试诊断严重错误，如缺少必需的参数、指定无效的必需参数，以及会破坏 Keymaster 实现完整性的类似错误。</p>

<h2 id="functions">函数</h2>

<h3 id="get_hardware_features">getHardwareFeatures</h3>
<p><strong>版本</strong>：3</p>

<p>
全新的 <code>getHardwareFeatures</code> 方法向客户端披露了底层安全硬件的一些重要特征。该方法不需要任何参数，且返回四个值（都是布尔值）：</p>
<ul>
  <li>如果密钥始终存储在安全硬件（TEE 等）中，则 <code>isSecure</code> 为 <code>true</code>。</li>
  <li>如果硬件支持采用 NIST 曲线（P-224、P-256、P-384 和 P-521）的椭圆曲线加密，则 <code>supportsEllipticCurve</code> 为 <code>true</code>。</li>
  <li>如果硬件支持对称加密（包括 AES 和 HMAC），则 <code>supportsSymmetricCryptography</code> 为 <code>true</code>。</li>
  <li>如果硬件支持生成使用注入安全环境中的密钥进行签名的 Keymaster 公钥认证证书，则 <code>supportsAttestation</code> 为 <code>true</code>。</li>
</ul>
<p>
该方法仅可能返回以下错误代码：<code>ErrorCode:OK</code>、<code>ErrorCode::KEYMASTER_NOT_CONFIGURED</code> 或指示无法与安全硬件通信的错误代码之一。
</p>
<pre class="devsite-click-to-copy">
getHardwareFeatures()
    generates(bool isSecure, bool supportsEllipticCurve, bool supportsSymmetricCryptography,
              bool supportsAttestation, bool supportsAllDigests, string keymasterName,
              string keymasterAuthorName);</pre>

<h3 id="configure">configure</h3>
<p><strong>版本</strong>：2</p>
<p>该函数在 Keymaster 2 中引入，并在 Keymaster 3 中被弃用，因为这些信息已在系统属性文件中提供，制造商实现会在启动期间读取这些文件。</p>

<p>配置 Keymaster。该方法在打开设备之后、使用设备之前被调用一次。它用于向 Keymaster 提供 <a href="/security/keystore/tags#os_version">KM_TAG_OS_VERSION</a> 和 <a href="/security/keystore/tags#os_patchlevel">KM_TAG_OS_PATCHLEVEL</a>。
在该方法被调用之前，所有其他方法会返回 <code>KM_ERROR_KEYMASTER_NOT_CONFIGURED</code>。该方法提供的值仅在每次启动时被 Keymaster 接受一次。之后的调用都会返回 <code>KM_ERROR_OK</code>，但不执行任何操作。</p>
<p>
如果 Keymaster 实现在安全的硬件中进行，且提供的操作系统版本和补丁程序级别的值与引导加载程序向安全硬件提供的值不匹配（或者引导加载程序未提供任何值），则该方法会返回 <code>KM_ERROR_INVALID_ARGUMENT</code>，所有其他方法仍然返回 <code>KM_ERROR_KEYMASTER_NOT_CONFIGURED</code>。</p>
<pre class="devsite-click-to-copy">
keymaster_error_t (*configure)(const struct keymaster2_device* dev,
                               const keymaster_key_param_set_t* params);</pre>

<h3 id="add_rng_entropy">addRngEntropy</h3>

<p><strong>版本</strong>：1、2、3</p>
<p>该函数在 Keymaster 1 中引入（名为 <code>add_rng_entropy</code>），并在 Keymaster 3 中进行了重命名。</p>

<p>该函数将调用程序提供的熵添加到 Keymaster 1 实现生成随机数（用作密钥、IV 等）所用的池中。</p>

<p>Keymaster 实现需要将收到的熵安全地混合到所使用的池中，该池中还必须包含由硬件随机数生成器在内部生成的熵。
对混合操作的处理应该实现：即使攻击者能够完全控制 <code>addRngEntropy</code> 提供的数位或硬件生成的数位（但不能同时控制这两者），他们在预测从熵池生成的数位方面也不具有明显的优势。</p>

<p>尝试估算内部池中的熵的 Keymaster 实现假定 <code>addRngEntropy</code> 提供的数据不包含熵。如果在单次调用中向 Keymaster 实现提供的数据超过 2KiB，则这些实现可能会返回 <code>ErrorCode::INVALID_INPUT_LENGTH</code>。</p>

<h3 id="generate_key">generateKey</h3>

<p><strong>版本</strong>：1、2、3</p>
<p>该函数在 Keymaster 1 中引入（名为 <code>generate_key</code>），并在 Keymaster 3 中进行了重命名。</p>

<p>该函数会生成一个新的加密密钥，同时指定关联的授权，这些授权会永久绑定到该密钥。Keymaster 实现确保无法通过任何与生成密钥时指定的授权不一致的方式使用密钥。对于安全硬件无法强制执行的授权，安全硬件的义务仅限于确保与密钥关联的无法强制执行的授权不能被修改，以便每次调用 <a href="#get_key_characteristics">getKeyCharacteristics</a> 时都会返回原始值。此外，由 <code>generateKey</code> 返回的特性将授权正确地分配到由硬件强制执行的列表和由软件强制执行的列表。如需了解详情，请参阅 <a href="#get_key_characteristics">getKeyCharacteristics</a>。</p>

<p>向 <code>generateKey</code> 提供的参数取决于要生成的密钥的类型。本部分总结了每种类型密钥的必需和可选标记。<a href="/security/keystore/tags#algorithm">Tag::ALGORITHM</a> 始终为必需的标记，用于指定类型。</p>

<h4 id="generate_key_rsa_keys">RSA 密钥</h4>

<p>以下参数是生成 RSA 密钥所必需的参数。</p>

<ul>
  <li><a href="/security/keystore/tags#key_size">Tag::KEY_SIZE</a> 用于指定公开模数的大小（以位计）。如果缺少此参数，该方法会返回 <code>ErrorCode::UNSUPPORTED_KEY_SIZE</code>。支持的值包括 1024、2048、3072 和 4096。最好支持所有为 8 的倍数的密钥大小。</li>
  <li><a href="/security/keystore/tags#rsa_public_exponent">Tag::RSA_PUBLIC_EXPONENT</a> 用于指定 RSA 公开指数的值。如果缺少此参数，该方法会返回 <code>ErrorCode::INVALID_ARGUMENT</code>。支持的值包括 3 和 65537。最好支持不超过 2^64 的所有质数值。</li>
</ul>

<p>以下参数不是生成 RSA 密钥所必需的参数，但如果在缺少这些参数的情况下生成 RSA 密钥，生成的密钥将无法使用。不过，如果缺少这些参数，<code>generateKey</code> 函数不会返回错误。</p>

<ul>
  <li><a href="/security/keystore/tags#purpose">Tag::PURPOSE</a> 用于指定允许的目的。
  对于 RSA 密钥，需要支持采用任意组合的所有目的。</li>
  <li><a href="/security/keystore/tags#digest">Tag::DIGEST</a> 用于指定可与新密钥配合使用的摘要算法。不支持任何摘要算法的实现需要接受包含不受支持的摘要的密钥生成请求。不受支持的摘要应被放入“由软件强制执行”的列表内的返回的密钥特性中。这是因为相应密钥能够与其他摘要配合使用，但添加摘要会在软件中进行。然后，将调用硬件按 <code>Digest::NONE</code> 摘要算法执行相应操作。</li>
  <li><a href="/security/keystore/tags#padding">Tag::PADDING</a> 用于指定可与新密钥配合使用的填充模式。如果指定了任何不受支持的摘要算法，则不支持所有摘要算法的实现需要将 <code>PaddingMode::RSA_PSS</code> 和 <code>PaddingMode::RSA_OAEP</code> 放入由软件强制执行的密钥特性列表中。</li>
</ul>

<h4 id="generate_key_ecdsa_keys">ECDSA 密钥</h4>

<p>只有 <a href="/security/keystore/tags#key_size">Tag::KEY_SIZE</a> 是生成 ECDSA 密钥所必需的参数。此参数用于选择 EC 组。
支持的值包括 224、256、384 和 521，这些值分别表示 NIST p-224、p-256、p-384 和 p521 曲线。</p>

<p>为了使生成的 ECDSA 密钥可以使用，还需要 <a href="/security/keystore/tags#digest">Tag::DIGEST</a>，但此参数不是生成 ECDSA 密钥所必需的参数。</p>

<h4 id="generate_key_aes_keys">AES 密钥</h4>

<p>只有 <a href="/security/keystore/tags#key_size">Tag::KEY_SIZE</a> 是生成 AES 密钥所必需的参数。如果缺少此参数，该方法会返回 <code>ErrorCode::UNSUPPORTED_KEY_SIZE</code>。支持的值包括 128 和 256，可以选择支持 192 位 AES 密钥。</p>

<p>以下参数仅与 AES 密钥相关，但它们并不是生成 AES 密钥所必需的参数：</p>

<ul>
  <li><code>Tag::BLOCK_MODE</code> 用于指定可与新密钥配合使用的分块模式。</li>
  <li><code>Tag::PADDING</code> 用于指定可以使用的填充模式。此参数仅与 ECB 和 CBC 模式相关。</li>
</ul>

<p>如果指定的是 GCM 分块模式，则提供 <a href="/security/keystore/tags#min_mac_length">Tag::MIN_MAC_LENGTH</a>。
如果缺少此参数，该方法会返回 <code>ErrorCode::MISSING_MIN_MAC_LENGTH</code>。标记的值为 8 的倍数且介于 96 和 128 之间。</p>

<h4 id="generate_key_hmac_keys">HMAC 密钥</h4>

<p>以下参数是生成 HMAC 密钥所必需的参数：</p>

<ul>
  <li><a href="/security/keystore/tags#key_size">Tag::KEY_SIZE</a> 用于指定密钥大小（以位计）。不支持小于 64 以及不是 8 的倍数的值。支持介于 64 和 512 之间并且是 8 的倍数的所有值。可以支持更大的值。</li>
  <li><a href="/security/keystore/tags#min_mac_length">Tag::MIN_MAC_LENGTH</a> 用于指定可通过相应密钥生成或验证的 MAC 的最小长度。此参数的值是 8 的倍数，并且不小于 64。</li>
  <li><a href="/security/keystore/tags#digest">Tag::DIGEST</a> 用于指定相应密钥的摘要算法。只能指定一种摘要，否则返回 <code>ErrorCode::UNSUPPORTED_DIGEST</code>。
  如果 Trustlet 不支持指定的摘要，则返回 <code>ErrorCode::UNSUPPORTED_DIGEST</code>。</li>
</ul>

<h4 id="key_characteristics">密钥特性</h4>

<p>如果特性参数为非 NULL 值，<code>generateKey</code> 会返回新生成密钥的特性（相应划分到由硬件强制执行的列表和由软件强制执行的列表中）。
要了解哪些特性会划分到哪个列表中，请参阅 <a href="#get_key_characteristics">getKeyCharacteristics</a>。返回的特性包含为生成密钥而指定的所有参数，<a href="/security/keystore/tags#application_id">Tag::APPLICATION_ID</a> 和 <a href="/security/keystore/tags#application_data">Tag::APPLICATION_DATA</a> 除外。如果这两个标记包含在密钥参数中，则将其从返回的特性中移除，以确保无法通过查看返回的密钥 Blob 找出这两个标记的值。不过，这两个标记以加密形式绑定到密钥 Blob，以便在使用相应密钥时，如果未提供正确的值，使用会失败。<a href="/security/keystore/tags#root_of_trust">Tag::ROOT_OF_TRUST</a> 同样会以加密形式绑定到相应密钥，但在生成或导入密钥期间可以不指定此标记，并且在任何情况下都不会返回此标记。</p>

<p>除了收到的标记外，Trustlet 还会添加 <a href="/security/keystore/tags#origin">Tag::ORIGIN</a>（值为 <code>KeyOrigin::GENERATED</code>）；如果相应密钥可抗回滚，还要添加 <a href="/security/keystore/tags#rollback_resistant">Tag::ROLLBACK_RESISTANT</a>。</p>

<h4 id="rollback_resistance">抗回滚</h4>

<p>抗回滚意味着，密钥在通过 <a href="#delete_key">deleteKey</a> 或 <a href="#delete_all_keys">deleteAllKeys</a> 删除后，安全硬件会保证它绝对无法再使用。不具抗回滚功能的实现通常会将生成或导入的密钥材料作为密钥 Blob（一种经过加密和身份验证的形式）返回给调用程序。当 Keystore 删除密钥 Blob 后，相应密钥将会消失，但之前已设法获取密钥材料的攻击者可能能够将相应密钥材料恢复到设备上。</p>

<p>如果安全硬件保证被删除的密钥以后无法被恢复，那么相应密钥便可抗回滚。安全硬件通常是通过将额外的密钥元数据存储在攻击者无法操控的可信位置来做到这一点。在移动设备上，用于实现这一点的机制通常为 Replay Protected Memory Block (RPMB)。由于可创建的密钥数量基本上没有限制，而用于抗回滚的可信存储空间的大小则可能有限制，因此，该方法需要在即使无法为新密钥提供抗回滚功能的情况下同样取得成功。在这种情况下，不得将 <a href="/security/keystore/tags#rollback_resistant">Tag::ROLLBACK_RESISTANT</a> 添加到密钥特性中。</p>

<h3 id="get_key_characteristics">getKeyCharacteristics</h3>

<p><strong>版本</strong>：1、2、3</p>
<p>该函数在 Keymaster 1 中引入（名为 <code>get_key_characteristics</code>），并在 Keymaster 3 中进行了重命名。</p>

<p>用于返回与收到的密钥关联的参数和授权，并且返回的参数和授权会划分为两组：一组由硬件强制执行，一组由软件强制执行。此处的说明同样适用于通过 <a href="#generate_key">generateKey</a> 和 <a href="#import_key">importKey</a> 返回的密钥特性列表。</p>

<p>如果在生成或导入密钥期间提供了 <code>Tag::APPLICATION_ID</code>，则在 <code>clientId</code> 参数中为此方法提供相同的值。否则，此方法会返回 <code>ErrorCode::INVALID_KEY_BLOB</code>。同样，如果在生成或导入密钥期间提供了 <code>Tag::APPLICATION_DATA</code>，则在 <code>appData</code> 参数中为此方法提供相同的值。</p>

<p>此方法返回的特性完整地说明了指定密钥的类型和用法。</p>

<p>要确定某个指定标记是属于由硬件强制执行的列表，还是属于由软件强制执行的列表，一般规则是：如果该标记的含义完全由安全硬件来保证，则属于由硬件强制执行的列表，否则属于由软件强制执行的列表。下面列出了可能无法明确确定到底属于哪个列表的具体标记：</p>

<ul>
  <li><a href="/security/keystore/tags#algorithm">Tag::ALGORITHM</a>、<a href="/security/keystore/tags#key_size">Tag::KEY_SIZE</a> 和 <a href="/security/keystore/tags#rsa_public_exponent">Tag::RSA_PUBLIC_EXPONENT</a> 是密钥的固有属性。对于任何由硬件来保障安全的密钥，这些标记都将位于由硬件强制执行的列表中。</li>
  <li>由安全硬件支持的 <a href="/security/keystore/tags#digest">Tag::DIGEST</a> 值位于由硬件支持的列表中。不受支持的摘要则位于由软件支持的列表中。</li>
  <li><a href="/security/keystore/tags#padding">Tag::PADDING</a> 的值通常位于由硬件支持的列表中，除非存在这样一种可能性，某种特定的填充模式必须要由软件来执行，在这种情况下，这些值将位于由软件强制执行的列表中。如果 RSA 密钥允许使用不是由安全硬件支持的摘要算法进行 PSS 或 OAEP 填充，则存在这种可能性。</li>
  <li><a href="/security/keystore/tags#user_secure_id">Tag::USER_SECURE_ID</a> 和 <a href="/security/keystore/tags#mac_length">Tag::USER_AUTH_TYPE</a> 仅在用户身份验证由硬件强制执行时，才由硬件强制执行。要让用户身份验证由硬件强制执行，Keymaster Trustlet 和相关的身份验证 Trustlet 都必须是安全的，且共用一个用于签署和验证身份验证令牌的 HMAC 密钥。要了解详情，请参阅<a href="/security/authentication/">身份验证</a>页面。</li>
  <li><a href="/security/keystore/tags#active_datetime">Tag::ACTIVE_DATETIME</a>、<a href="/security/keystore/tags#origination_expire_datetime">Tag::ORIGINATION_EXPIRE_DATETIME</a> 和 <a href="/security/keystore/tags#usage_expire_datetime">Tag::USAGE_EXPIRE_DATETIME</a> 标记要求能够访问可验证的正确时钟。大多数安全硬件只能访问由非安全操作系统提供的时间信息，这意味着这些标记由软件强制执行。</li>
  <li>对于绑定到硬件的密钥，<a href="/security/keystore/tags#origin">Tag::ORIGIN</a> 始终位于硬件列表中。如果此标记出现在硬件列表中，更高的层级便可据此确定相应密钥是由硬件支持的。</li>
</ul>

<h3 id="import_key">importKey</h3>

<p><strong>版本</strong>：1、2、3</p>
<p>该函数在 Keymaster 1 中引入（名为 <code>import_key</code>），并在 Keymaster 3 中进行了重命名。</p>

<p>用于将密钥材料导入到 Keymaster 硬件中。密钥定义参数和输出特性的处理方式与 <code>generateKey</code> 相同，但存在以下例外情况：</p>

<ul>
  <li><a href="/security/keystore/tags#key_size">Tag::KEY_SIZE</a> 和 <a href="/security/keystore/tags#rsa_public_exponent">Tag::RSA_PUBLIC_EXPONENT</a>（仅限 RSA 密钥）不是输入参数中必需的标记。如果未收到这两个标记，Trustlet 会根据收到的密钥材料推导出这两个标记的值，并将适当的标记和值添加到密钥特性中。如果收到了这两个参数，Trustlet 会根据密钥材料对其进行验证。如果收到的值与密钥材料中的值不一致，该方法会返回 <code>ErrorCode::IMPORT_PARAMETER_MISMATCH</code>。</li>
  <li>返回的 <a href="/security/keystore/tags#origin">Tag::ORIGIN</a> 与 <code>KeyOrigin::IMPORTED</code> 的值相同。</li>
</ul>

<h3 id="export_key">exportKey</h3>

<p><strong>版本</strong>：1、2、3</p>
<p>该函数在 Keymaster 1 中引入（名为 <code>export_key</code>），并在 Keymaster 3 中进行了重命名。</p>

<p>用于从 Keymaster RSA 密钥对或 EC 密钥对中导出公钥。</p>

<p>如果在生成或导入密钥期间提供了 <code>Tag::APPLICATION_ID</code>，则在 <code>clientId</code> 参数中为此方法提供相同的值。否则，此方法会返回 <code>ErrorCode::INVALID_KEY_BLOB</code>。同样，如果在生成或导入密钥期间提供了 <code>Tag::APPLICATION_DATA</code>，则在 <code>appData</code> 参数中为此方法提供相同的值。</p>

<h3 id="delete_key">deleteKey</h3>

<p><strong>版本</strong>：1、2、3</p>
<p>该函数在 Keymaster 1 中引入（名为 <code>delete_key</code>），并在 Keymaster 3 中进行了重命名。</p>

<p>用于删除收到的密钥。此方法为可选方法，只能由提供抗回滚功能的 Keymaster 模块来实现。</p>

<h3 id="delete_all_keys">deleteAllKeys</h3>

<p><strong>版本</strong>：1、2、3</p>
<p>该函数在 Keymaster 1 中引入（名为 <code>delete_all_keys</code>），并在 Keymaster 3 中进行了重命名。</p>

<p>用于删除所有密钥。此方法为可选方法，只能由提供抗回滚功能的 Keymaster 模块来实现。</p>

<h3 id="destroy_attestation_ids">destroyAttestationIds</h3>
<p><strong>版本</strong>：3</p>
<p>
<code>destroyAttestationIds()</code> 方法用于永久停用新的<a href="/security/keystore/attestation#id-attestation">ID 认证</a>功能（该功能是可选功能，但我们强烈推荐）。如果 TEE 无法确保在调用此方法后永久停用相应的 ID 认证，则一定不得实现 ID 认证，在这种情况下，此方法不执行任何操作并返回 <code>ErrorCode::UNIMPLEMENTED</code>。如果支持 ID 认证，则需要实现此方法且必须永久停用未来尝试进行 ID 认证的所有操作。此方法的调用次数不受限制。如果已永久停用 ID 认证，则此方法不会执行任何操作并返回 <code>ErrorCode::OK</code>。</p>
<p>
此方法只可能返回以下错误代码：<code>ErrorCode::UNIMPLEMENTED</code>（如果不支持 ID 认证）、<code>ErrorCode:OK</code>、<code>ErrorCode::KEYMASTER_NOT_CONFIGURED</code> 或指示无法与安全硬件通信的错误代码之一。
</p>

<h3 id="begin">begin</h3>

<p><strong>版本</strong>：1、2、3</p>

<p>使用指定的密钥和参数（视情况而定）针对指定的目的开始执行加密操作，并返回用于与 <a href="#update">update</a> 和 <a href="#finish">finish</a> 配合使用以完成操作的操作句柄。该操作句柄还会在经过身份验证的操作中用作“质询”令牌，并且对于此类操作，该操作句柄包含在身份验证令牌的 <code>challenge</code> 字段中。</p>

<p>Keymaster 实现支持至少 16 个并行操作。Keystore 最多使用 15 个，留一个给 vold 用于对密码进行加密。当 Keystore 有 15 个操作正在进行（已调用 <code>begin</code>，但尚未调用 <code>finish</code> 或 <code>abort</code>）时，如果 Keystore 收到开始第 16 个操作的请求，它会对最近使用最少的操作调用 <code>abort</code>，以便将进行中的操作减少到 14 个，然后再调用 <code>begin</code> 来开始执行新请求的操作。</p>

<p>如果在生成或导入密钥期间指定了 <a href="/security/keystore/tags#application_id">Tag::APPLICATION_ID</a> 或 <a href="#application_data">Tag::APPLICATION_DATA</a>，那么调用 <code>begin</code> 时会包含这两个标记，标记的值为最初在 <code>inParams</code> 参数中为此方法指定的值。</p>

<h4 id="begin_authorization_enforcement">密钥授权强制执行</h4>

<p>在执行此方法期间，如果实现将以下密钥授权放入到了“由硬件强制执行的”特性中，并且相应操作不是公钥操作，那么这些密钥授权将由 Trustlet 来强制执行。即使不符合授权要求，也会允许公钥操作（即使用 RSA 或 EC 密钥进行的 <code>KeyPurpose::ENCRYPT</code> 和 <code>KeyPurpose::VERIFY</code>）成功完成。</p>

<ul>
  <li><a href="/security/keystore/tags#purpose">Tag::PURPOSE</a>：除非请求的操作是公钥操作，否则 <code>begin()</code> 调用中指定的目的必须与密钥授权中的某个目的一致。如果指定的目的不一致且操作并非公钥操作，则 <code>begin</code> 会返回 <code>ErrorCode::UNSUPPORTED_PURPOSE</code>。公钥操作是不对称加密或验证操作。</li>
  <li>只有可信 UTC 时间源可用时才能强制执行 <a href="/security/keystore/tags#active_datetime">Tag::ACTIVE_DATETIME</a>。如果当前日期和时间早于此标记的值，该方法会返回 <code>ErrorCode::KEY_NOT_YET_VALID</code>。</li>
  <li>只有可信 UTC 时间源可用时才能强制执行 <a href="/security/keystore/tags#origination_expire_datetime">Tag::ORIGINATION_EXPIRE_DATETIME</a>。如果当前日期和时间晚于此标记的值，并且目的是 <code>KeyPurpose::ENCRYPT</code> 或 <code>KeyPurpose::SIGN</code>，该方法会返回 <code>ErrorCode::KEY_EXPIRED</code>。</li>
  <li>只有可信 UTC 时间源可用时才能强制执行 <a href="/security/keystore/tags#usage_expire_datetime">Tag::USAGE_EXPIRE_DATETIME</a>。如果当前日期和时间晚于此标记的值，并且目的是 <code>KeyPurpose::DECRYPT</code> 或 <code>KeyPurpose::VERIFY</code>，该方法会返回 <code>ErrorCode::KEY_EXPIRED</code>。</li>
  <li><a href="/security/keystore/tags#min_seconds_between_ops">Tag::MIN_SECONDS_BETWEEN_OPS</a> 会与指明相应密钥上次使用时间的可信相对计时器进行比较。如果上次使用时间加上此标记的值后小于当前时间，该方法会返回 <code>ErrorCode::KEY_RATE_LIMIT_EXCEEDED</code>。要查看重要的实现详情，请参阅<a href="/security/keystore/tags#min_seconds_between_ops">标记说明</a>。</li>
  <li><a href="/security/keystore/tags#max_uses_per_boot">Tag::MAX_USES_PER_BOOT</a> 会与用于跟踪自系统启动以来相应密钥使用次数的安全计数器进行比较。如果之前的使用次数已超过此标记的值，该方法会返回 <code>ErrorCode::KEY_MAX_OPS_EXCEEDED</code>。</li>
  <li>仅当相应密钥还有 <a href="/security/keystore/tags#auth_timeout">Tag::AUTH_TIMEOUT</a> 时，<a href="/security/keystore/tags#user_secure_id">Tag::USER_SECURE_ID</a>才会由此方法强制执行。如果相应密钥同时具有这两个标记，此方法必须在 <code>inParams</code> 中收到有效的 <a href="/security/keystore/tags#auth_token">Tag::AUTH_TOKEN</a>。要使身份验证令牌有效，必须满足以下所有条件：
   <ul>
     <li>HMAC 字段可正确验证。</li>
     <li>相应密钥中有至少一个 <a href="/security/keystore/tags#user_secure_id">Tag::USER_SECURE_ID</a> 值与令牌中至少一个安全 ID 值一致。</li>
     <li>相应密钥具有与令牌中的身份验证类型一致的 <a href="/security/keystore/tags#mac_length">Tag::USER_AUTH_TYPE</a>。</li>
   </ul>
  <p>如果上述条件中有任何一个不满足，该方法就会返回 <code>ErrorCode::KEY_USER_NOT_AUTHENTICATED</code>。</p></li>
  <li><a href="/security/keystore/tags#caller_nonce">Tag::CALLER_NONCE</a> 允许调用程序指定随机数或初始化矢量 (IV)。如果相应密钥没有此标记，但调用程序为此方法提供了 <a href="/security/keystore/tags#nonce">Tag::NONCE</a>，则返回 <code>ErrorCode::CALLER_NONCE_PROHIBITED</code>。</li>
  <li><a href="/security/keystore/tags#bootloader_only">Tag::BOOTLOADER_ONLY</a> 用于指定相应密钥只能由引导加载程序使用。如果在引导加载程序执行完毕后调用此方法，并且提供的是仅限引导加载程序使用的密钥，则返回 <code>ErrorCode::INVALID_KEY_BLOB</code>。</li>
</ul>

<h4 id="begin_rsa_keys">RSA 密钥</h4>

<p>执行任何 RSA 密钥操作时，都会在 <code>inParams</code> 中指定一种且只能指定一种填充模式。
如果未指定或指定了多次，该方法会返回 <code>ErrorCode::UNSUPPORTED_PADDING_MODE</code>。</p>

<p>RSA 签名和验证操作需要摘要，就像使用 OAEP 填充模式进行 RSA 加密和解密操作一样。在这类情况下，调用程序会在 <code>inParams</code> 中指定一种且只能指定一种摘要。如果未指定或指定了多次，该方法会返回 <code>ErrorCode::UNSUPPORTED_DIGEST</code>。</p>

<p>私钥操作（<code>KeyPurpose::DECYPT</code> 和 <code>KeyPurpose::SIGN</code>）要求摘要和填充获得授权，也就是说，密钥授权需要包含指定的值。否则，该方法会根据具体情况返回 <code>ErrorCode::INCOMPATIBLE_DIGEST</code> 或 <code>ErrorCode::INCOMPATIBLE_PADDING</code>。公钥操作（<code>KeyPurpose::ENCRYPT</code> 和 <code>KeyPurpose::VERIFY</code>）可以使用未经授权的摘要或填充。</p>

<p>除了 <code>PaddingMode::NONE</code> 之外，所有 RSA 填充模式都仅适用于特定目的。具体来说就是，<code>PaddingMode::RSA_PKCS1_1_5_SIGN</code> 和 <code>PaddingMode::RSA_PSS</code> 仅支持签名和验证，而 <code>PaddingMode::RSA_PKCS1_1_1_5_ENCRYPT</code> 和 <code>PaddingMode::RSA_OAEP</code> 仅支持加密和解密。
如果指定的模式不支持指定的目的，该方法会返回 <code>ErrorCode::UNSUPPORTED_PADDING_MODE</code>。</p>

<p>填充模式与摘要之间存在以下非常重要的相互关系：</p>

<ul>

  <li><code>PaddingMode::NONE</code> 表示执行“原始”RSA 操作。如果是进行签名或验证，则指定 <code>Digest::NONE</code> 这种摘要。如果是进行非填充式加密或解密，则不需要摘要。</li>
  <li><code>PaddingMode::RSA_PKCS1_1_5_SIGN</code> 填充需要摘要。摘要可以是 <code>Digest::NONE</code>，在这种情况下，Keymaster 实现无法构建适当的 PKCS#1 v1.5 签名结构，因为它无法添加 DigestInfo 结构。不过，实现会构建 <code>0x00 || 0x01 || PS || 0x00 || M</code>，其中 M 是收到的消息，PS 是填充字符串。RSA 密钥的大小要比消息至少多 11 个字节，否则该方法会返回 <code>ErrorCode::INVALID_INPUT_LENGTH</code>。</li>
  <li><code>PaddingMode::RSA_PKCS1_1_1_5_ENCRYPT</code> 填充不需要摘要。</li>
  <li><code>PaddingMode::RSA_PSS</code> 填充需要摘要，并且摘要不能是 <code>Digest::NONE</code>。如果指定了 <code>Digest::NONE</code>，该方法会返回 <code>ErrorCode::INCOMPATIBLE_DIGEST</code>。此外，RSA 密钥的大小必须至少比摘要的输出大小大 2 + D 字节，其中 D 表示摘要的大小（以字节计）。否则，该方法会返回 <code>ErrorCode::INCOMPATIBLE_DIGEST</code>。盐的大小为 D。</li>
  <li><code>PaddingMode::RSA_OAEP</code> 填充需要摘要，并且摘要不能是 <code>Digest::NONE</code>。如果指定了 <code>Digest::NONE</code>，该方法会返回 <code>ErrorCode::INCOMPATIBLE_DIGEST</code>。</li>
</ul>

<h4 id="begin_ec_keys">EC 密钥</h4>

<p>执行 EC 密钥操作时，会在 <code>inParams</code> 中指定一种且只能指定一种填充模式。
如果未指定或指定了多次，该方法会返回 <code>ErrorCode::UNSUPPORTED_PADDING_MODE</code>。</p>

<p>私钥操作 (<code>KeyPurpose::SIGN</code>) 要求摘要和填充获得授权，也就是说，密钥授权需要包含指定的值。否则返回 <code>ErrorCode::INCOMPATIBLE_DIGEST</code>。公钥操作 (<code>KeyPurpose::VERIFY</code>) 可以使用未经授权的摘要或填充。</p>

<h4 id="begin_aes_keys">AES 密钥</h4>

<p>执行 AES 密钥操作时，会在 <code>inParams</code> 中指定一种且只能指定一种分块模式和填充模式。如果有任何一项未指定或指定了多次，则返回 <code>ErrorCode::UNSUPPORTED_BLOCK_MODE</code> 或 <code>ErrorCode::UNSUPPORTED_PADDING_MODE</code>。指定的模式必须要通过相应密钥授权，否则，该方法会返回 <code>ErrorCode::INCOMPATIBLE_BLOCK_MODE</code> 或 <code>ErrorCode::INCOMPATIBLE_PADDING_MODE</code>。</p>

<p>如果分块模式是 <code>BlockMode::GCM</code>，则在 <code>inParams</code> 中指定 <code>Tag::MAC_LENGTH</code>。指定的值是 8 的倍数，并且不大于 128，也不小于密钥授权中 <code>Tag::MIN_MAC_LENGTH</code> 的值。如果 MAC 长度大于 128 或不是 8 的倍数，则返回 <code>ErrorCode::UNSUPPORTED_MAC_LENGTH</code>。如果 MAC 长度小于密钥最小长度，则返回 <code>ErrorCode::INVALID_MAC_LENGTH</code>。</p>

<p>如果分块模式是 <code>BlockMode::GCM</code> 或 <code>BlockMode::CTR</code>，那么指定的填充模式必须是 <code>PaddingMode::NONE</code>。如果分块模式是 <code>BlockMode::ECB</code> 或 <code>BlockMode::CBC</code>，那么指定的填充模式可以是 <code>PaddingMode::NONE</code> 或 <code>PaddingMode::PKCS7</code>。如果填充模式不符合这些条件，则返回 <code>ErrorCode::INCOMPATIBLE_PADDING_MODE</code>。</p>

<p>如果分块模式是 <code>BlockMode::CBC</code>、<code>BlockMode::CTR</code> 或 <code>BlockMode::GCM</code>，则需要初始化矢量或随机数。
在大多数情况下，调用程序都不应提供 IV 或随机数。在这种情况下，Keymaster 实现会生成一个随机 IV 或随机数，并通过 <code>outParams</code> 中的 <a href="/security/keystore/tags#nonce">Tag::NONCE</a> 将其返回。
CBC 和 CTR IV 均为 16 个字节。GCM 随机数为 12 个字节。如果密钥授权包含 <a href="/security/keystore/tags#caller_nonce">Tag::CALLER_NONCE</a>，那么调用程序可以通过 <code>inParams</code> 中的 <a href="/security/keystore/tags#nonce">Tag::NONCE</a> 提供 IV/随机数。如果在 <a href="/security/keystore/tags#caller_nonce">Tag::CALLER_NONCE</a> 未获得授权时提供了随机数，则返回 <code>ErrorCode::CALLER_NONCE_PROHIBITED</code>。如果在 <a href="/security/keystore/tags#caller_nonce">Tag::CALLER_NONCE</a> 获得了授权的情况下未提供随机数，则生成一个随机 IV/随机数。</p>

<h4 id="begin_hmac_keys">HMAC 密钥</h4>

<p>执行 HMAC 密钥操作时，会在 <code>inParams</code> 中指定 <code>Tag::MAC_LENGTH</code>。
指定的值必须是 8 的倍数，并且不大于摘要长度，也不小于密钥授权中 <code>Tag::MIN_MAC_LENGTH</code> 的值。如果 MAC 长度大于摘要长度或不是 8 的倍数，则返回 <code>ErrorCode::UNSUPPORTED_MAC_LENGTH</code>。如果 MAC 长度小于密钥最小长度，则返回 <code>ErrorCode::INVALID_MAC_LENGTH</code>。</p>

<h3 id="update">更新</h3>

<p><strong>版本</strong>：1、2、3</p>

<p>用于提供要在通过 <a href="#begin">begin</a> 开始且正在进行的操作中处理的数据。
操作是通过 <code>operationHandle</code> 参数指定的。</p>

<p>为了更灵活地处理缓冲区，此方法的实现可以选择不消耗完收到的数据。调用程序负责执行循环操作，以便在后续调用中馈送其余数据。在 <code>inputConsumed</code> 参数中返回所消耗的输入数据量。
实现始终消耗至少一个字节，除非相应操作无法再接受更多字节；如果收到了零个以上的字节，但消耗了零字节，调用程序会将此视为错误并中止相应操作。</p>

<p>实现还可以选择返回多少数据（作为 update 的结果）。这仅与加密和解密操作有关，因为在调用 <a href="#finish">finish</a> 之前，签名和验证操作不会返回任何数据。
尽早返回数据，而不是缓冲数据。</p>

<h4 id="error_handling">错误处理</h4>

<p>如果此方法返回除 <code>ErrorCode::OK</code> 之外的错误代码，那么相应操作会被中止，操作句柄也会变为无效。如果以后再将该句柄与此方法、<a href="#finish">finish</a> 或 <a href="#abort">abort</a> 配合使用，都会返回 <code>ErrorCode::INVALID_OPERATION_HANDLE</code>。</p>

<h4 id="update_authorization_enforcement">密钥授权强制执行</h4>

<p>密钥授权强制执行主要在 <a href="#begin">begin</a> 中进行。
不过，密钥存在以下情况时例外：</p>

<ul>
  <li>一个或多个 <a href="/security/keystore/tags#user_secure_id">Tag::USER_SECURE_IDs</a>，且</li>
  <li>没有 <a href="/security/keystore/tags#auth_timeout">Tag::AUTH_TIMEOUT</a></li>
</ul>

<p>在这种情况下，密钥需要针对各项操作的授权，并且 update 方法会在 <code>inParams</code> 参数中收到 <a href="/security/keystore/tags#auth_token">Tag::AUTH_TOKEN</a>。HMAC 会验证该令牌有效且包含匹配的安全用户 ID，与密钥的 <a href="/security/keystore/tags#mac_length">Tag::USER_AUTH_TYPE</a> 匹配，并且包含质询字段中当前操作的操作句柄。如果不符合这些条件，则返回 <code>ErrorCode::KEY_USER_NOT_AUTHENTICATED</code>。</p>

<p>调用程序会在每次调用 <a href="#update">update</a> 和 <a href="#finish">finish</a> 时提供身份验证令牌。实现只需对该令牌验证一次（如果它倾向于这么做）。</p>

<h4 id="update_rsa_keys">RSA 密钥</h4>

<p>对于使用 <code>Digest::NONE</code> 的签名和验证操作，此方法会在单次 update 中接受要签署或验证的整个分块。此方法不会只消耗分块的一部分。不过，如果调用程序选择在多次 update 中提供数据，此方法会接受相应数据。
如果调用程序提供的要签署的数据多于可以消耗的数据（数据长度超出 RSA 密钥大小），则返回 <code>ErrorCode::INVALID_INPUT_LENGTH</code>。</p>

<h4 id="update_ecdsa_keys">ECDSA 密钥</h4>

<p>对于使用 <code>Digest::NONE</code> 的签名和验证操作，此方法会在单次 update 中接受要签署或验证的整个分块。此方法不会只消耗分块的一部分。</p>

<p>不过，如果调用程序选择在多次 update 中提供数据，此方法会接受相应数据。如果调用程序提供的要签署的数据多于可以消耗的数据，则以静默方式截断这些数据。（这与处理在类似 RSA 操作中提供的超量数据不同，因为此方法与旧版客户端兼容。）</p>

<h4 id="update_aes_keys">AES 密钥</h4>

<p>AES GCM 模式支持通过 <code>inParams</code> 参数中的 <a href="/security/keystore/tags#associated_data">Tag::ASSOCIATED_DATA</a> 标记提供的“相关身份验证数据”。
可以在重复调用（如果数据太大而无法在单个分块中发送，那么重复调用非常重要）中提供相关数据，但始终先于要加密或解密的数据提供。update 调用可以同时接收相关数据以及要加密/解密的数据，但后续 update 中不会包含相关数据。如果调用程序已在某次调用 update 时提供了要加密/解密的数据，若再次向 update 调用提供相关数据，则返回 <code>ErrorCode::INVALID_TAG</code>。</p>

<p>对于 GCM 加密，此标记会通过 <a href="#finish">finish</a> 附加到密文中。在解密期间，向上一次 update 调用提供的数据的最后 <code>Tag::MAC_LENGTH</code> 个字节就是此标记。由于 <a href="#update">update</a> 的指定调用无法得知自己是否为最后一次调用，因此它会处理除标记长度之外的所有数据，并缓冲调用 <a href="#finish">finish</a> 期间可能用到的标记数据。</p>

<h3 id="finish">finish</h3>

<p><strong>版本</strong>：1、2、3</p>

<p>用于完成通过 <a href="#begin">begin</a> 开始且正在进行的操作，负责处理通过 <a href="#update">update</a> 提供的所有尚未处理的数据。</p>

<p>此方法是操作期间调用的最后一个方法，因此会返回所有处理后的数据。</p>

<p>无论是成功完成还是返回错误，此方法都会结束相应操作，从而使收到的操作句柄无效。如果以后再将该句柄与此方法、<a href="#update">update</a> 或 <a href="#abort">abort</a> 配合使用，都会返回 <code>ErrorCode::INVALID_OPERATION_HANDLE</code>。</p>

<p>签名操作将返回签名作为输出。验证操作将接受 <code>signature</code> 参数中的签名，并且不会返回任何输出。</p>

<h4 id="finish_authorization_enforcement">密钥授权强制执行</h4>

<p>密钥授权强制执行主要在 <a href="#begin">begin</a> 中进行。不过，密钥存在以下情况时例外：</p>

<ul>
  <li>一个或多个 <a href="/security/keystore/tags#user_secure_id">Tag::USER_SECURE_IDs</a>，且</li>
  <li>没有 <a href="/security/keystore/tags#auth_timeout">Tag::AUTH_TIMEOUT</a></li>
</ul>

<p>在这种情况下，密钥需要针对各项操作的授权，并且 update 方法会在 <code>inParams</code> 参数中收到 <a href="/security/keystore/tags#auth_token">Tag::AUTH_TOKEN</a>。HMAC 会验证该令牌有效且包含匹配的安全用户 ID，与密钥的 <a href="/security/keystore/tags#mac_length">Tag::USER_AUTH_TYPE</a> 匹配，并且包含质询字段中当前操作的操作句柄。如果不符合这些条件，则返回 <code>ErrorCode::KEY_USER_NOT_AUTHENTICATED</code>。</p>

<p>调用程序会在每次调用 <a href="#update">update</a> 和 <a href="#finish">finish</a> 时提供身份验证令牌。实现只需对该令牌验证一次（如果它倾向于这么做）。</p>

<h4 id="finish_rsa_keys">RSA 密钥</h4>

<p>有一些附加要求，具体取决于填充模式：</p>

<ul>
  <li><code>PaddingMode::NONE</code>：对于非填充式签名和加密操作，如果收到的数据比密钥短，那么在签名/加密之前，在数据左侧填充零来补齐。如果数据与密钥一样长度，但数值较大，则返回 <code>ErrorCode::INVALID_ARGUMENT</code>。对于验证和解密操作，数据必须与密钥一样长。否则返回 <code>ErrorCode::INVALID_INPUT_LENGTH.</code></li>
  <li><code>PaddingMode::RSA_PSS</code>：对于 PSS 填充式签名操作，PSS 盐不得短于 20 个字节，并且是随机生成的。
  盐可以更长；参考实现使用的是长度最大的盐。
  调用 <a href="#begin">begin</a> 时在 <code>inputParams</code> 中使用 <a href="/security/keystore/tags#digest">Tag::DIGEST</a> 指定的摘要会用作 PSS 摘要算法，而 SHA1 会用作 MGF1 摘要算法。</li>
  <li><code>PaddingMode::RSA_OAEP</code>：调用 <a href="#begin">begin</a> 时在 <code>inputParams</code> 中使用 <a href="/security/keystore/tags#digest">Tag::DIGEST</a> 指定的摘要会用作 OAEP 摘要算法，而 SHA1 会用作 MGF1 摘要算法。</li>
</ul>

<h4 id="finish_ecdsa_keys">ECDSA 密钥</h4>

<p>如果为非填充式签名或验证操作提供的数据太长，则要将其截断。</p>

<h4 id="finish_aes_keys">AES 密钥</h4>

<p>有一些附加条件，具体取决于分块模式：</p>

<ul>
  <li><code>BlockMode::ECB</code> 或 <code>BlockMode::CBC</code>：如果填充模式是 <code>PaddingMode::NONE</code>，并且数据长度不是 AES 分块大小的倍数，则返回 <code>ErrorCode::INVALID_INPUT_LENGTH</code>。如果填充模式是 <code>PaddingMode::PKCS7</code>，则按照 PKCS#7 规范填充数据。
  请注意，PKCS#7 建议，如果数据长度是分块长度的倍数，则添加一个额外的填充分块。</li>
  <li><code>BlockMode::GCM</code>：在加密期间，处理所有明文之后，会计算此标记（<a href="/security/keystore/tags#mac_length">Tag::MAC_LENGTH</a> 个字节）并将其附加到返回的密文。在解密期间，会将最后 <a href="/security/keystore/tags#mac_length">Tag::MAC_LENGTH</a> 个字节作为标记处理。如果标记验证失败，则返回 <code>ErrorCode::VERIFICATION_FAILED</code>。</li>
</ul>

<h3 id="abort">abort</h3>

<p><strong>版本</strong>：1、2、3</p>

<p>用于中止正在进行的操作。在调用 abort 之后，如果后续再将收到的操作句柄与 <a href="#update">update</a>、<a href="#finish">finish</a> 或 <a href="#abort">abort</a> 配合使用，则返回 <code>ErrorCode::INVALID_OPERATION_HANDLE</code>。</p>

<h3 id="get_supported_algorithms">get_supported_algorithms</h3>

<p><strong>版本</strong>：1</p>

<p>用于返回一个列表，其中包含 Keymaster 硬件实现支持的算法。如果是软件实现，则返回一个空列表；如果是混合实现，则返回一个仅包含硬件支持的算法的列表。</p>

<p>Keymaster 1 实现支持 RSA、EC、AES 和 HMAC。</p>

<h3 id="get_supported_block_modes">get_supported_block_modes</h3>

<p><strong>版本</strong>：1</p>

<p>用于返回一个列表，其中包含对于指定的算法和目的，Keymaster 硬件实现支持的 AES 分块模式。</p>

<p>对于不是分块加密算法的 RSA、EC 和 HMAC，无论是任何有效目的，此方法都会返回一个空列表。如果目的无效，则应导致此方法返回 <code>ErrorCode::INVALID_PURPOSE</code>。</p>

<p>Keymaster 1 实现支持使用 ECB、CBC、CTR 和 GCM 进行 AES 加密和解密。</p>

<h3 id="get_supported_padding_modes">get_supported_padding_modes</h3>

<p><strong>版本</strong>：1</p>

<p>用于返回一个列表，其中包含对于指定的算法和目的，Keymaster 硬件实现支持的填充模式。</p>

<p>HMAC 和 EC 并没有填充这一概念，因此针对所有有效目的，此方法都会返回一个空列表。如果目的无效，则应导致此方法返回 <code>ErrorCode::INVALID_PURPOSE</code>。</p>

<p>对于 RSA，Keymaster 1 实现支持：</p>

<ul>
  <li>非填充式加密、解密、签名和验证。对于非填充式加密和签名，如果消息比公开模数短，实现必须要在消息左侧填充零来补齐。对于非填充式解密和验证，输入长度必须与公开模数的大小一致。</li>
  <li>PKCS#1 v1.5 加密和签名填充模式</li>
  <li>盐最小长度为 20 的 PSS</li>
  <li>OAEP</li>
</ul>

<p>对于采用 ECB 和 CBC 模式的 AES 算法，Keymaster 1 实现支持无填充和 PKCS#7 填充。CTR 和 GCM 模式仅支持无填充。</p>

<h3 id="get_supported_digests">get_supported_digests</h3>

<p><strong>版本</strong>：1</p>

<p>用于返回一个列表，其中包含对于指定的算法和目的，Keymaster 硬件实现支持的摘要模式。</p>

<p>任何 AES 模式都不支持摘要，也不需要摘要，因此无论是任何有效目的，此方法都会返回一个空列表。</p>

<p>Keymaster 1 实现可以只实现一部分已定义的摘要。实现会提供 SHA-256，且可以提供 MD5、SHA1、SHA-224、SHA-256、SHA384 和 SHA512（完整的已定义摘要集）。</p>

<h3 id="get_supported_import_formats">get_supported_import_formats</h3>

<p><strong>版本</strong>：1</p>

<p>用于返回一个列表，其中包含指定算法的 Keymaster 硬件实现支持的导入格式。</p>

<p>Keymaster 1 实现支持 PKCS#8 格式（无密码保护），以便导入 RSA 密钥对和 EC 密钥对，并且支持以原始格式导入 AES 密钥材料和 HMAC 密钥材料。</p>

<h3 id="get_supported_export_formats">get_supported_export_formats</h3>

<p><strong>版本</strong>：1</p>

<p>用于返回一个列表，其中包含指定算法的 Keymaster 硬件实现支持的导出格式。</p>

<p>Keymaster1 实现支持 X.509 格式，以便导出 RSA 公钥和 EC 公钥。不支持导出私钥或非对称密钥。</p>

<h2 id="historical-functions">历史函数</h2>

<h3 id="km0">Keymaster 0</h3>
<p>
以下函数属于最初的 Keymaster 0 定义。它们出现在 Keymaster 1 结构 keymaster1_device_t 中。不过，Keymaster 1.0 中并没有实现这些函数，且它们的函数指针设为了 NULL。
</p>
<ul>
<li><code>generate_keypair</code></li>
<li><code>import_keypair</code></li>
<li><code>get_keypair_public</code></li>
<li><code>delete_keypair</code></li>
<li><code>delete_all</code></li>
<li><code>sign_data</code></li>
<li><code>Verify_data</code></li>
</ul>

<h3 id="km1">Keymaster 1</h3>
<p>以下函数属于 Keymaster 1 定义，但在 Keymaster 2 中已被移除，一同被移除的还有上述 Keymaster 0 函数。
</p>
<ul>
  <li><code>get_supported_algorithms</code></li>
  <li><code>get_supported_block_modes</code></li>
  <li><code>get_supported_padding_modes</code></li>
  <li><code>get_supported_digests</code></li>
  <li><code> get_supported_import_formats</code></li>
  <li><code>get_supported_export_formats</code></li>
</ul>

<h3 id="km2">Keymaster 2</h3>
<p>以下函数属于 Keymaster 2 定义，但在 Keymaster 3 中已被移除，一同被移除的还有上述 Keymaster 1 函数。
</p>
<ul>
  <li><code>configure</code></li>
</ul>

</body></html>