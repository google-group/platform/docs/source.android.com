toc:
- title: Overview
  path: /devices/tech/
- title: ART and Dalvik
  section:
  - title: Overview
    path: /devices/tech/dalvik
  - title: Improvements
    path: /devices/tech/dalvik/improvements
  - title: Bytecode Format
    path: /devices/tech/dalvik/dalvik-bytecode
  - title: Dex Format
    path: /devices/tech/dalvik/dex-format
  - title: Instruction Formats
    path: /devices/tech/dalvik/instruction-formats
  - title: Constraints
    path: /devices/tech/dalvik/constraints
  - title: Configuration
    path: /devices/tech/dalvik/configure
  - title: Garbage Collection
    path: /devices/tech/dalvik/gc-debug
  - title: JIT Compilation
    path: /devices/tech/dalvik/jit-compiler
- title: Configuration
  section:
  - title: Overview
    path: /devices/tech/config/
  - title: Ambient Capabilities
    path: /devices/tech/config/ambient
  - title: Carrier Customization
    section:
    - title: Carrier Configuration
      path: /devices/tech/config/carrier
    - title: APN and CarrierConfig
      path: /devices/tech/config/update
    - title: UICC
      path: /devices/tech/config/uicc
  - title: File DAC Configuration
    path: /devices/tech/config/filesystem
  - title: Namespaces for Libraries
    path: /devices/tech/config/namespaces_libraries
  - title: Privileged Permission Whitelist
    path: /devices/tech/config/perms-whitelist
  - title: Runtime Permissions
    path: /devices/tech/config/runtime_perms
  - title: Time Zone Rules
    path: /devices/tech/config/timezone-rules
  - title: USB HAL
    path: /devices/tech/config/usb-hal
  - title: Visual Voicemail
    path: /devices/tech/config/voicemail
- title: Connectivity
  section:
  - title: Overview
    path: /devices/tech/connect/
  - title: Block Phone Numbers
    path: /devices/tech/connect/block-numbers
  - title: Call Notifications
    path: /devices/tech/connect/call-notification
  - title: Data Saver Mode
    path: /devices/tech/connect/data-saver
  - title: Emergency Affordance
    path: /devices/tech/connect/emergency-affordance
  - title: Host Card Emulation of FeliCa
    path: /devices/tech/connect/felica
  - title: Out-of-Balance Users
    path: /devices/tech/connect/oob-users
  - title: Network Connectivity Tests
    path: /devices/tech/connect/connect_tests
  - title: Radio Interface Layer (RIL)
    path: /devices/tech/connect/ril
  - title: Wi-Fi Aware
    path: /devices/tech/connect/wifi-aware
- title: Data Usage
  section:
  - title: Overview
    path: /devices/tech/datausage/
  - title: Network Interface Statistics Overview
    path: /devices/tech/datausage/iface-overview
  - title: Excluding Network Types from Data Usage
    path: /devices/tech/datausage/excluding-network-types
  - title: Tethering Data
    path: /devices/tech/datausage/tethering-data
  - title: Usage Cycle Reset Dates
    path: /devices/tech/datausage/usage-cycle-resets-dates
  - title: Kernel Overview
    path: /devices/tech/datausage/kernel-overview
  - title: Data Usage Tags Explained
    path: /devices/tech/datausage/tags-explained
  - title: Kernel Changes
    path: /devices/tech/datausage/kernel-changes
- title: Debugging
  section:
  - title: Overview
    path: /devices/tech/debug/
  - title: Diagnosing Native Crashes
    path: /devices/tech/debug/native-crash
  - title: Evaluating Performance
    section:
    - title: Overview
      path: /devices/tech/debug/eval_perf
    - title: Understanding systrace
      path: /devices/tech/debug/systrace
    - title: Using ftrace
      path: /devices/tech/debug/ftrace
    - title: Identifying Capacity Jank
      path: /devices/tech/debug/jank_capacity
    - title: Identifying Jitter Jank
      path: /devices/tech/debug/jank_jitter
  - title: Fuzzing and Sanitizing
    section:
    - title: Overview
      path: /devices/tech/debug/fuzz-sanitize
    - title: AddressSanitizer
      path: /devices/tech/debug/asan
    - title: LLVM Sanitizers
      path: /devices/tech/debug/sanitizers
    - title: Build kernel with KASAN+KCOV
      path: /devices/tech/debug/kasan-kcov
    - title: Fuzzing with libFuzzer
      path: /devices/tech/debug/libfuzzer
  - title: Using GDB
    path: /devices/tech/debug/gdb
  - title: Native Memory Use
    path: /devices/tech/debug/native-memory
  - title: Rescue Party
    path: /devices/tech/debug/rescue-party
  - title: Storaged
    path: /devices/tech/debug/storaged
  - title: Strace
    path: /devices/tech/debug/strace
  - title: Valgrind
    path: /devices/tech/debug/valgrind
- title: Device Administration
  section:
  - title: Overview
    path: /devices/tech/admin/
  - title: Implementation
    path: /devices/tech/admin/implement
  - title: Multiple Users
    path: /devices/tech/admin/multi-user
  - title: Managed Profiles
    path: /devices/tech/admin/managed-profiles
  - title: Provisioning
    path: /devices/tech/admin/provision
  - title: Multiuser Apps
    path: /devices/tech/admin/multiuser-apps
  - title: Enterprise Telephony
    path: /devices/tech/admin/enterprise-telephony
  - title: Testing Device Provisioning
    path: /devices/tech/admin/testing-provision
  - title: Testing Device Administration
    path: /devices/tech/admin/testing-setup
- title: Display
  section:
  - title: Overview
    path: /devices/tech/display/
  - title: Adaptive Icons
    path: /devices/tech/display/adaptive-icons
  - title: App Shortcuts
    path: /devices/tech/display/app-shortcuts
  - title: Circular Icons
    path: /devices/tech/display/circular-icons
  - title: Color Management
    path: /devices/tech/display/color-mgmt
  - title: Do Not Disturb
    path: /devices/tech/display/dnd
  - title: HDR Video
    path: /devices/tech/display/hdr
  - title: Multi-Window
    path: /devices/tech/display/multi-window
  - title: Night Light
    path: /devices/tech/display/night-light
  - title: Picture-in-picture
    path: /devices/tech/display/pip
  - title: Retail Demo Mode
    path: /devices/tech/display/retail-mode
  - title: Split-Screen Interactions
    path: /devices/tech/display/split-screen
  - title: TEXTCLASSIFIER
    path: /devices/tech/display/textclassifier
  - title: Widgets & Shortcuts
    path: /devices/tech/display/widgets-shortcuts
- title: OTA Updates
  section:
  - title: Overview
    path: /devices/tech/ota/
  - title: OTA Tools
    path: /devices/tech/ota/tools
  - title: Signing Builds for Release
    path: /devices/tech/ota/sign_builds
  - title: Reducing OTA Size
    path: /devices/tech/ota/reduce_size
  - title: A/B System Updates
    section:
    - title: Overview
      path: /devices/tech/ota/ab/
    - title: Implementing A/B Updates
      path: /devices/tech/ota/ab/ab_implement
    - title: Frequently Asked Questions
      path: /devices/tech/ota/ab/ab_faqs
  - title: Non-A/B System Updates
    section:
    - title: Overview
      path: /devices/tech/ota/nonab/
    - title: Block-Based OTA
      path: /devices/tech/ota/nonab/block
    - title: Inside OTA Packages
      path: /devices/tech/ota/nonab/inside_packages
    - title: Device-Specific Code
      path: /devices/tech/ota/nonab/device_code
- title: Performance
  section:
  - title: Overview
    path: /devices/tech/perf/
  - title: Boot Times
    path: /devices/tech/perf/boot-times
  - title: Flash Wear Management
    path: /devices/tech/perf/flash-wear
  - title: Low RAM
    path: /devices/tech/perf/low-ram
  - title: Task Snapshots
    path: /devices/tech/perf/task-snapshots
- title: Power
  section:
  - title: Overview
    path: /devices/tech/power/
  - title: Power Management
    path: /devices/tech/power/mgmt
  - title: Performance Management
    path: /devices/tech/power/performance
  - title: Component Power
    path: /devices/tech/power/component
  - title: Device Power
    path: /devices/tech/power/device
  - title: Power Values
    path: /devices/tech/power/values
- title: Settings Menu
  section:
  - title: Overview
    path: /devices/tech/settings/
  - title: Patterns and Components
    path: /devices/tech/settings/patterns-components
  - title: Information Architecture
    path: /devices/tech/settings/info-architecture
  - title: Personalized Settings
    path: /devices/tech/settings/personalized
  - title: Universal Search
    path: /devices/tech/settings/universal-search
  - title: Design Guidelines
    path: /devices/tech/settings/settings-guidelines
- title: Testing Infrastructure
  section:
  - title: Overview
    path: /devices/tech/test_infra/tradefed/
  - title: Start Here
    path: /devices/tech/test_infra/tradefed/fundamentals
  - title: Machine Setup
    path: /devices/tech/test_infra/tradefed/fundamentals/machine_setup
  - title: Working with Devices
    path: /devices/tech/test_infra/tradefed/fundamentals/devices
  - title: Test Lifecycle
    path: /devices/tech/test_infra/tradefed/fundamentals/lifecycle
  - title: Option Handling
    path: /devices/tech/test_infra/tradefed/fundamentals/options
  - title: An End-to-End Example
    path: /devices/tech/test_infra/tradefed/full_example
  - title: Package Index
    path: /reference/tradefed/
