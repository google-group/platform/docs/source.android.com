<html devsite>
  <head>
    <title>Employing Managed Profiles</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

  {% include "_versions.html" %}

<p>A <em>managed profile</em> or <em>work profile</em> is an Android <a
href="/devices/tech/admin/multi-user.html">user</a> with additional special
properties around management and visual aesthetic.</p>

<p>The primary goal of a managed profile is to create a segregated and secure
space for managed data (such as corporate data) to reside. The administrator of
the profile has full control over scope, ingress, and egress of data as well as
its lifetime. These policies offer great powers and therefore fall upon the
managed profile instead of the device administrator.</p>

<ul>
  <li><strong>Creation</strong>. Managed profiles can be created by any
  application in the primary user. The user is notified of managed profile
  behaviors and policy enforcement before creation.</li>
  <li><strong>Management</strong>. Management is performed by applications that
  programmatically invoke APIs in the
  <a href="http://developer.android.com/reference/android/app/admin/DevicePolicyManager.html"
  class="external">DevicePolicyManager</a>
  class to restrict use. Such applications are referred to as <em>profile
  owners</em> and are defined at initial profile setup. Policies unique to
  managed profile involve app restrictions, updatability, and intent behaviors.
  </li>
  <li><strong>Visual treatment</strong>. Applications, notifications, and
  widgets from the managed profile are always badged and typically made
  available inline with user interface (UI) elements from the primary user.</li>
</ul>

<h2 id="data_segregation">Data segregation</h2>
<p>Managed profiles use the following data segregation rules.</p>

<h3 id="applications">Applications</h3>

<p>Applications are scoped with their own segregated data when the same app
exists in the primary user and managed profile. Generally, applications act
independently of one another and cannot communicate directly with one another
across the profile-user boundary.</p>

<h3 id="accounts">Accounts</h3>

<p>
Accounts in the managed profile are distinctly unique from the primary user.
There is no way to access credentials across the profile-user boundary. Only
apps in their respective context are able to access their respective accounts.
</p>

<h3 id="intents">Intents</h3>

<p>The administrator controls whether intents are resolved in/out of managed
profile or not. Applications from the managed profile are default scoped to
stay within the managed profile exception of the Device Policy API.</p>

<h3 id="settings">Settings</h3>

<p>Enforcement of settings is generally scoped to the managed profile, with
exceptions for lockscreen and encryption settings that are still scoped
to the device and shared between the primary user and managed profile.
Otherwise, a profile owner does not have any device administrator privileges
outside the managed profile.</p>

<p>
Managed profiles are implemented as a new kind of secondary user, such that:
</p>

<pre class="devsite-click-to-copy">
uid = 100000 * userid + appid
</pre>

<p>They have separate app data like regular users:</p>

<pre class="devsite-click-to-copy">
/data/user/&lt;userid&gt;
</pre>

<p>The UserId is calculated for all system requests using
<code>Binder.getCallingUid()</code>, and all system state and responses are
separated by userId. You may consider instead using
<code>Binder.getCallingUserHandle</code> rather than <code>getCallingUid</code>
to avoid confusion between uid and userId.</p>

<p>The AccountManagerService maintains a separate list of accounts for each
user. The main differences between a managed profile and a regular secondary
user are as follows:</p>

<ul>
  <li>The managed profile is associated with its parent user and started
  alongside the primary user at boot time.</li>
  <li>Notifications for managed profiles are enabled by ActivityManagerService
  allowing the managed profile to share the activity stack with the primary
  user.</li>
  <li>Other shared system services include IME, A11Y services, Wi-Fi, and NFC.
  </li>
  <li>New Launcher APIs allow launchers to display badged apps and whitelisted
  widgets from the managed profile alongside apps in the primary profile without
  switching users.</li>
</ul>

<h2 id="device_administration">Device administration</h2>

<p>Android device administration includes the following types of device
administrators for enterprises:</p>

<ul>
  <li><em>Profile owner</em>. Designed for bring your own device (BYOD)
  environments</li>
  <li><em>Device Owner</em>. Designed for corp-liable environments</li>
</ul>

<p>The majority of the new device administrator APIs added for Android 5.0 are
available only to profile or device owners. Traditional device administrators
remain but are applicable to the simpler consumer-only case (e.g., find my
device).</p>

<h3 id="profile_owners">Profile owners</h3>

<p>A Device Policy Client (DPC) app typically functions as the profile owner.
The DPC app is typically provided by an enterprise mobility management (EMM)
partner, such as Google Apps Device Policy.</p>

<p>The profile owner app creates a managed profile on the device by sending the
<code>ACTION_PROVISION_MANAGED_PROFILE</code> intent. This profile is
distinguished by the appearance of badged instances of
apps, as well as personal instances. That badge, or Android device
administration icon, identifies which apps are work apps.</p>

<p>The EMM has control only over the managed profile (not personal space) with
some exceptions, such as enforcing the lock screen.</p>

<h3 id="device_owners">Device owners</h3>

<p>The device owner can be set only in an unprovisioned device:</p>

<ul>
  <li>Can be provisioned only at initial device setup</li>
  <li>Enforced disclosure always displayed in quick-settings</li>
</ul>

<p>Device owners can conduct some tasks profile owners cannot, such as:</p>

<ul>
  <li>Wipe device data</li>
  <li>Disable Wi-Fi/Bluetooth</li>
  <li>Control <code>setGlobalSetting</code></li>
  <li><code>setLockTaskPackages</code> (the ability to whitelist packages that
  can pin themselves to the foreground)</li>
  <li>Set <code>DISALLOW_MOUNT_PHYSICAL_MEDIA</code> (<code>FALSE</code> by
  default). When <code>TRUE</code>, physical media, both portable and adoptable,
  cannot be mounted.</li>
</ul>

<h3 id="dpm_api">DevicePolicyManager APIs</h3>

<p>Android 5.0 and higher offers a greatly improved DevicePolicyManager with
dozens of new APIs to support both corporate-owned and bring your own device
(BYOD) administration use cases. Examples include app restrictions, silent
installation of certificates, and cross-profile sharing intent access control.
Use the sample Device Policy Client (DPC) app
<a href="https://developer.android.com/samples/BasicManagedProfile/index.html"
class="external">BasicManagedProfile.apk</a>
as a starting point. For details, refer to
<a href="https://developer.android.com/training/enterprise/work-policy-ctrl.html"
class="external">Building
a Work Policy Controller</a>.</p>

<h2 id="user-experience">Managed profile user experience</h2>

  <p>
  Android {{ androidPVersionNumber }} creates a tighter integration between
  managed profiles and the platform, making it easier for users to keep their
  work and personal information separate on their devices. These managed
  profile user experience changes appear in the Launcher. Implementing the UX
  managed profile changes creates a consistent user experience across managed
  devices.
  </p>

  <h3 id="ux-changes-app-tray">UX changes for devices with an app tray</h3>

    <p>
      The managed profile UX changes for Launcher 3 in Android
      {{ androidPVersionNumber }} help users maintain separate personal and
      managed profiles. The apps drawer provides a tabbed view to distinguish
      between personal profile apps. When users first view the managed profile
      tab, they are presented with an educational view to help them navigate
      the managed profile. Users can also turn the managed profile on and off
      by using a toggle in the Launcher's work tab.
    </p>

    <h4 id="tabbed-profile-views">Tabbed profile views</h4>

      <p>
        In Android {{ androidPVersionNumber }}, the managed profile lets users
        switch between personal and managed app lists in the apps drawer. When
        the managed profile is enabled, the app views are separated into two
        distinct
        <a
          href="https://developer.android.com/reference/android/support/v7/widget/RecyclerView.html"
          class="external"><code>RecyclerViews</code></a>, managed by a
        <a
          href="https://developer.android.com/reference/android/support/v4/view/ViewPager.html"
          class="external"><code>ViewPager</code></a>. Users can switch between
        the different profiles' views by using profile tabs at the top of the
        app drawer. The <code>PersonalWorkSlidingTabStrip</code> class
        provides a reference implementation of the tabbed profile indicator.
        The tabbed view is implemented as part of the Launcher3 class
        <a
          href="https://android.googlesource.com/platform/packages/apps/Launcher3/+/master/src/com/android/launcher3/allapps/AllAppsContainerView.java"
          class="external"><code>AllAppsContainerView</code></a>.
      </p>

      <table>
        <tr>
          <td width="50%">
            <img src="/devices/tech/admin/images/Work-profile0.png" width=""
            alt="Personal tab view" title="image_tooltip">
          </td>
          <td width="50%">
            <img src="/devices/tech/admin/images/Work-profile1.png" width=""
            alt="Managed profile toggle" title="image_tooltip">
          </td>
        </tr>
        <tr>
          <td><strong>Figure 1.</strong> Personal tab view</td>
          <td><strong>Figure 2.</strong> Work tab view with the managed profile
            toggle at the bottom of the screen
          </td>
        </tr>
      </table>

    <h4 id="educational-view">Educational view</h4>

      <p>
      Launcher3 also has the option of presenting an educational view at the
      bottom of the screen when users first open the work tab, as seen in
      <strong>Figure 3</strong>. Use the educational view to inform users of
      the purpose of the work tab and how to make work apps easier to access.
      </p>

      <p>
      The educational view is defined in Android {{ androidPVersionNumber }}
      and higher by the class
      <code>BottomUserEducationView</code> with the layout controlled by
      <code>work_tab_tottom_user_education_view.xml</code>. Within
      <code>BottomUserEducationView</code>, the
      <code>KEY_SHOWED_BOTTOM_USER_EDUCATION</code> boolean is set to
      <code>false</code> by default. When the user dismisses the educational
      view, the boolean is set to <code>true</code>.
      </p>

      <p>
        <img src="/devices/tech/admin/images/Work-profile2.png" width="50%"
        alt="Educational view" title="image_tooltip">
        <p class="img-caption">
          <strong>Figure 3.</strong> Educational view in work tab
        </p>
      </p>

    <h4 id="toggle-to-enable-disable">
      Toggle to enable/disable managed profiles
    </h4>

      <p>
      Within the work tab, managed device administrators can present a toggle
      in the footer view for users to enable or disable the managed profile as
      seen in <strong>Figure 2</strong> above. The source for the toggle can be
      found in <code>WorkFooterContainer</code>, starting in Android
      {{ androidPVersionNumber }}. Enabling and disabling the managed profile
      is done asynchronously and applied to all valid user profiles. This
      process is controlled by the<code>WorkModeSwitch</code> class in Android
      {{ androidPVersionNumber }}.
      </p>

  <h3 id="ux-changes-without-an-app-tray">
    UX changes for devices without an app tray
  </h3>

    <p>
    For launchers without an app tray, it is recommended to continue placing
    shortcuts to the managed profile apps in the work folder.
    </p>

    <p>
    If the work folder fails to populate correctly, and newly installed apps
    are not added to the folder, apply the following change in the
    <code>onAllAppsLoaded</code> method in the
    <a
      href="https://android.googlesource.com/platform/packages/apps/Launcher3/+/master/src/com/android/launcher3/util/ManagedProfileHeuristic.java"
      class="external"><code>ManagedProfileHeuristic</code></a> class:
    </p>

<pre class="prettyprint">for (LauncherActivityInfo app : apps) {
        // Queue all items which should go in the work folder.
        if (app.getFirstInstallTime() &lt; Long.MAX_VALUE) {
                InstallShortcutReceiver.queueActivityInfo(app, context);
        }
}</pre>


  <h3 id="validating-ux-changes">Validating UX changes</h3>

    <p>
    Test the managed profile UX implementation using the TestDPC app.
    </p>

    <ol>
      <li>Install the
        <a
          href="https://play.google.com/store/apps/details?id=com.afwsamples.testdpc"
          class="external">TestDPC</a> app from the Google Play Store.
      </li>
      <li>
        Open the launcher or app drawer and select the <strong>Set up
          TestDPC</strong> icon.
      </li>
      <li>Follow the on-screen instructions to set up a managed profile.</li>
      <li>
        Open the launcher or app drawer and verify that there is a work tab
        there.
      </li>
      <li>
        Verify that there is a managed profile footer under the work tab.
      </li>
      <li>
        Verify that you can toggle the managed profile switch on and off. The
        managed profile should be enabled and disabled accordingly.
      </li>
    </ol>

    <table>
      <tr>
        <td width="33%">
          <img src="/devices/tech/admin/images/Work-profile3.png" width=""
          alt="TestDPC profile setup" title="image_tooltip">
        </td>
        <td width="33%">
          <img src="/devices/tech/admin/images/Work-profile4.png" width=""
          alt="TestDPC add accounts" title="image_tooltip">
        </td>
        <td width="33%">
          <img src="/devices/tech/admin/images/Work-profile5.png" width=""
          alt="TestDPC setup complete" title="image_tooltip">
        </td>
      </tr>
      <tr>
        <td>
          <strong>Figure 4.</strong> Setting up a managed profile in <strong>Set up TestDPC</strong>
        </td>
        <td>
          <strong>Figure 5.</strong> Add accounts in <strong>Set up TestDPC</strong>
        </td>
        <td>
          <strong>Figure 6.</strong> Set up complete
        </td>
      </tr>
    </table>

    <table>
      <tr>
        <td width="50%">
          <img src="/devices/tech/admin/images/Work-profile6.png" width=""
          alt="App drawer work tab toggle on" title="image_tooltip">
        </td>
        <td width="50%">
          <img src="/devices/tech/admin/images/Work-profile7.png" width=""
          alt="App drawer work tab toggle off" title="image_tooltip">
        </td>
      </tr>
      <tr>
        <td>
          <strong>Figure 7.</strong> App drawer with a work tab. The managed
          profile footer switch is <strong>ON</strong>, and the managed profile
          is enabled.
        </td>
        <td>
          <strong>Figure 8.</strong> App drawer with a work tab. The managed
          profile footer switch is <strong>OFF</strong>, and the managed
          profile is disabled.
        </td>
      </tr>
    </table>

  <h3 id="managed-profile-app-badge">Managed profile app badge</h3>

  <p>
  For accessibility reasons, the color of the work badge changes from
  orange to blue (#1A73E8) in Android {{ androidPVersionNumber}}.
  </p>

  </body>
</html>
