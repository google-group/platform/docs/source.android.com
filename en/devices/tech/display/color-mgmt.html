<html devsite>
  <head>
    <title>Color Management</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p>Android 8.1 adds support for color management that can be used to provide a
consistent experience across display technologies. Applications running on
Android 8.1 can access the full capabilities of a wide gamut display to get the
most out of a display device.</p>

<p>Previous Android releases did not include color management support and
instead relied on content and displays being compatible (a goal often aided by
the TV industry). However, recent display technologies allow for much larger
gamut displays that do not display existing content as expected. With Android
8.1, devices that use a wide-gamut display (e.g. active-matrix organic
light-emitting diode or AMOLED, some LCDs) will be able to see wide-gamut
content from applications.</p>

<h2>Determining device support</h2>
<p>Devices with wide-color displays running Android 8.1 should support color
management (wide-color). Before enabling this feature, ensure the device meets
the following requirements:</p>
<ul>
<li>Device display meets the hardware requirements, which include a
well-characterized display that supports the Display-P3 colorspace. If the
display does not meet this requirement, do not enable color management. To
reduce CPU and GPU impact, support for extended sRGB and HDR10 in the display
pipeline is desirable.</li>
<li>Device supports a factory calibration process that generates calibration
data (stored on the device) to adjust for manufacturing variance in display
behavior. At a minimum, calibration data should allow the display to accurately
display sRGB content and D65 and D73 white points (future versions of Android
may support full ICC profiles for each device).</li>
</ul>
<p>If the above requirements are met, you can enable the color management
feature for the device.</p>

<h2>Implementing color management</h2>
<p>To implement color management, you must first update the
<a href="/devices/graphics/implement-hwc">Hardware Composer 2 (HWC2)</a> driver
to understand color modes and to apply those modes to the hardware.
Specifically, the HWC2 composer must report the Display-P3 and sRGB
color modes using <code>HWCDisplay::GetColorModes</code>.</p>

<aside class="key-term"><strong>Key Term:</strong> Display-P3 uses Digital
Cinema Initiatives primaries and sRGB transfer function.</aside>

<p>Next, enable the necessary OpenGL extensions and library support to
translate OpenGL color spaces to HAL data spaces. Required OpenGL extensions
include:</p>
<ul>
<li>
<a href="https://www.khronos.org/registry/egl/extensions/EXT/EGL_EXT_pixel_format_float.txt" class="external">EGL_EXT_pixel_format_float</a>.
Allows applications to create presentable EGLSurface(s) with 16-bit float color
components. Priority: high (expect this is the default pixel format for
wide-color aware applications). Requires driver support.</li>
<li>
<a href="https://www.khronos.org/registry/egl/extensions/KHR/EGL_KHR_gl_colorspace.txt" class="external">EGL_KHR_gl_colorspace</a>.
For applications that want to use sRGB format default framebuffers to more
easily achieve sRGB rendering to display devices, this extension allows creating
EGLSurfaces that will be rendered to in sRGB by OpenGL contexts supporting that
capability. Requires driver support for sRGB behavior.</li>
</ul>
<p>Android also provides the following optional extensions:</p>
<ul>
<li>
<a href="https://www.khronos.org/registry/egl/extensions/EXT/EGL_EXT_gl_colorspace_scrgb_linear.txt" class="external">EGL_EXT_colorspace_scrgb_linear</a>.
This extension provides a new color space option, scRGB, for applications to
choose when creating an EGLSurface. The scRGB color space defines a linear
display referred space with the same white point and color primaries as sRGB
(and thus is backward-compatible with sRGB). This should not require driver
support and can be implemented in the Android EGL wrapper. To be useful, this
extension requires support for 16-bit floating point (FP16).</li>
<li>
<a href="https://github.com/KhronosGroup/EGL-Registry/pull/10/files" class="external">EGL_KHR_gl_colorspace_display_p3
and EGL_EXT_gl_colorspace_display_p3_linear</a>. For applications that want
to use Display-P3 format default framebuffers to more easily achieve sRGB
rendering to display devices, this extension allows creating EGLSurfaces that
will be rendered to in Display-P3 by OpenGL contexts supporting that capability.
This can be implemented in EGL driver wrapper.</li>
<li>
<a href="https://www.khronos.org/registry/vulkan/specs/1.0-extensions/html/vkspec.html#VK_EXT_swapchain_colorspace" class="external">VK_EXT_swapchain_colorspace</a>
(Vulkan). Enables applications to tag swap chains with the color space they are
using. Includes a number of common color spaces such as DCI-P3, Display-P3,
AdobeRGB, BT2020, etc.</li>
</ul>

<h2>Customizing</h2>
<p>You can customize the color management feature by including support for a
variety of color standards such as DCI-P3, AdobeRGB, Rec709, Rec2020. Other
customizations include:</p>
<ul>
<li><strong>Hardware support for color conversion in the display
pipeline</strong>. Enables support for multiple color transforms in hardware.
</li>
<li><strong>Support for independent color transform on multiple layers</strong>
(e.g. some layers could be sRGB and others extended-sRGB, each with their own
color pipeline). When there is more than one color space visible, some color
spaces will need to be converted to the color space of the display. Ideally,
this transform is best provided by the display engine (otherwise Android must
perform GPU composition).</li>
</ul>

<h2>Testing</h2>
<p>To test color management, use the following resources in
<code>opengl/tests</code>:</p>
<ul>
<li><code>gl2_basic</code>. Simple OpenGL demo that requests a Display-P3
colorspace.</li>
<li><code>
<a href="https://android.googlesource.com/platform/frameworks/native/+/master/opengl/tests/EGLTest/EGL_test.cpp">EGL_test</code></a>.
Tests for necessary extension and config support (10:10:10:2 and FP16).</li>
<li><code>test_wide_color</code>. Creates surface in same manner as
SurfaceFlinger (e.g. config, color space, pixel format).</li>
</ul>

<h2>Reference implementation</h2>
<p>For a reference implementation, refer to <code>frameworks/native</code>. For
headers, refer to:</p>
<ul>
<li>
<code><a href="https://android.googlesource.com/platform/system/core/+/master/libsystem/include/system/graphics.h">system/core/include/system/graphics.h</code></a></li>
<li>
<code><a href="https://android.googlesource.com/platform/system/core/+/master/libsystem/include/system/graphics-base.h">system/core/include/system/graphics-base.h</code></a><ul>
<li><code>HAL_DATASPACE_*</code></li>
<li><code>HAL_COLOR_MODE_*</code></li>
</ul>
</li>
</ul>

</body>
</html>
