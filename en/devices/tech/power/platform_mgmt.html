<html devsite>
  <head>
    <title>Platform Power Management</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

  <p>
    To improve device battery life, Android can affect the device state by
    monitoring device use and wakefulness. The platform can enter a state of
    sleep to pause activities from running while the device is unused.
  </p>

  <h2 id="doze">Doze</h2>

<p>Doze extends battery life by deferring application background CPU and
network activity when a device is unused for long periods.</p>

<p>Idle devices in Doze periodically enter a maintenance window, during which
apps can complete pending work (syncs, jobs, etc.). Doze then resumes
sleep for a longer period of time, followed by another maintenance window. The
platform continues the Doze sleep/maintenance sequence, increasing the length of
idle each time, until a maximum of a few hours of sleep time is reached. At all
times, a device in Doze remains aware of motion and immediately leaves Doze
if motion is detected.</p>

<p>Android 7.0 and higher extends Doze to trigger a lighter set of optimizations
every time a user turns off the device screen, even when the user continues to
move around, enabling longer lasting battery life.</p>

<p>Critical system services are genereally set up by device manufacturers to be
  exempt from Doze. Users can also exempt specific apps from Doze via the
Settings menu. However, exempting apps may cause battery drain on the device.
  By default, Doze is <strong>disabled</strong> in AOSP; for
details on enabling Doze, see <a href="#integrate-doze">Integrating Doze</a>.
</p>

<h3 id="doze-reqs">Doze requirements</h3>
<p>Doze support requires the device has a cloud messaging service, such as
<a href="https://firebase.google.com/docs/cloud-messaging/" class="external">Firebase
Cloud Messaging (FCM)</a>. External triggers events, such as cloud messages,
  can temporarily wake apps to do work while the device remains in Doze mode.</p>

<p>Full Doze support also requires a
<a href="/devices/sensors/sensor-types.html#significant_motion">Significant
Motion Detector (SMD)</a> on the device; however, the lightweight Doze mode in
Android 7.0 and higher does not require an SMD. If Doze is enabled on a device
that:</p>
<ul>
<li>Has an SMD, full Doze optimizations occur (includes lightweight
optimizations).</li>
<li>Does not have an SMD, only the lightweight Doze optimizations occur.</li>
</ul>

<h3 id="doze-life">Doze lifecycle</h3>

<p>Doze begins when the platform detects the device is idle and
ends when one or more exit criteria activities occur.</p>

<table>
<tbody>
<tr>
<th width=20%>Detection</th>
<th width=60%>During Doze</th>
<th width=20%>Exit</th>
</tr>
<tr>
<td><p>The platform detects a device is idle when:</p>
<ul>
<li>Device is stationary (using significant motion detector).</li>
<li>Device screen is off for some amount of time.</li>
</ul>
<p>Doze mode does not engage when the device is plugged into a power charger.
</p>
</td>
<td><p>The platform attempts to keep the system in a sleep state, periodically
resuming normal operations during a maintenance window then returning the device
to sleep for longer repeating periods. During sleep, the following
restrictions are active:</p>
<ul>
<li>Apps not allowed network access.</li>
<li>App wakelocks ignored.</li>
<li>Alarms deferred. Excludes alarm clock alarms and alarms set using
<code>setAndAllowWhileIdle()</code> (limited to 1 per 15 minutes per app while
in Doze). This exemption is intended for apps (such as Calendar) that must show
event reminder notifications.</li>
<li>Wi-Fi scans not performed.</li>
<li><code>SyncAdapter</code> syncs and <code>JobScheduler</code> jobs deferred
until the next maintenance window.</li>
<li>Apps receiving SMS and MMS messages are put on a temporary whitelist so
they can complete their processing.</li>
</ul>
</td>
<td><p>The platform exits the device from Doze when it detects:</p>
<ul>
<li>User interaction with device.</li>
<li>Device movement.</li>
<li>Device screen turns on.</li>
<li>Imminent AlarmClock alarm.</li>
</ul>
<p>Notifications do not cause the device to exit from Doze.</p>
</td>
</tr>
</tbody>
</table>

<p>Android 7.0 and higher extends Doze by enabling a lightweight sleep mode
during screen off, before the device is idle.</p>

<p><img src="/devices/tech/images/doze_lightweight.png"></p>
<figcaption><strong>Figure 1.</strong> Doze modes for non-stationary and
stationary devices.</figcaption>

<table>
<tbody>
<tr>
<th>Action</th>
<th>Doze</th>
<th>Lightweight Doze</th>
</tr>
<tr>
<td>Trigger</td>
<td>Screen off, on battery, stationary</td>
<td>Screen off, on battery (unplugged)</td>
</tr>
<tr>
<td>Timing</td>
<td>Successively increasing periods with maintenance</td>
<td>Repeated N-minute periods with maintenance windows</td>
</tr>
<tr>
<td>Restrictions</td>
<td>No network access, wake lock, or GPS/Wi-Fi scan. Alarms and jobs/syncs
deferred.</td>
<td>No network access. Jobs/syncs deferred except during maintenance windows.
</td>
</tr>
<tr>
<td>Behavior</td>
<td>Only high-priority push notification messages received.</td>
<td>All real-time messages (instant messages, calls, etc.) received.
High-priority push notification message enables temporary network access.</td>
</tr>
<tr>
<td>Exit</td>
<td>Motion, screen on, or alarm clock alarm.</td>
<td>Screen on.</td>
</tr>
</tbody>
</table>

<h3 id="doze-interactions">Interaction with App Standby</h3>
<ul>
<li>Time spent in Doze does not count towards App Standby.</li>
<li>While the device is in Doze, idle applications are allowed to perform normal
operations at least once a day.</li>
</ul>

<h3 id="integrate-doze">Integrating Doze</h3>

<p>When Doze is enabled, devices that support
<a href="/devices/sensors/sensor-types.html#significant_motion"><code>SENSOR_TYPE_SIGNIFICANT_MOTION</code>code></a>
perform full Doze optimizations (including lightweight optimizations);
devices without an SMD perform only lightweight Doze optimizations. Android
automatically selects the appropriate Doze optimizations and no vendor
configuration is necessary.</p>

<p>To enable Doze for a device:</p>

<ol>
<li>Confirm the device has a cloud messaging service installed.</li>
<li>In the device overlay config file
<code>overlay/frameworks/base/core/res/res/values/config.xml</code>, set
<code>config_enableAutoPowerModes</code> to <strong>true</strong>:
<pre class="devsite-click-to-copy">
&lt;bool name="config_enableAutoPowerModes"&gt;true&lt;/bool&gt;
</pre>
In AOSP, this parameter is set to false (Doze disabled) by default.<br>
</li>
<li>Confirm that preloaded apps and services:
<ul>
<li>Use the
<a href="https://developer.android.com/training/monitoring-device-state/doze-standby.html" class="external">power-saving
optimization guidelines</a>. For details, see <a href="#test-apps">Testing and
optimizing applications</a>.
<p><strong>OR</strong></p>
<li>Are exempted from Doze and App Standby. For details, see
<a href="/devices/tech/power/mgmt#exempt-apps">Exempting applications</a>.</li>
</ul>
</li>
<li>Confirm the necessary services are exempted from Doze.</li>
</ol>

<h4 id="doze-tips">Tips</h4>
<ul>
<li>If possible, use FCM for
<a href="https://firebase.google.com/docs/cloud-messaging/http-server-ref#send-downstream" class="external">downstream
messaging</a>.</li>
<li>If your users must see a notification right away, use a
<a href="https://firebase.google.com/docs/cloud-messaging/concept-options#setting-the-priority-of-a-message" class="external">FCM
high priority message</a>.</li>
<li>Provide sufficient information within the initial
<a href="https://firebase.google.com/docs/cloud-messaging/concept-options#notifications_and_data_messages" class="external">message
payload</a> (to avoid unnecessary subsequent network access).</li>
<li>Set critical alarms with
<a href="http://developer.android.com/reference/android/app/AlarmManager.html#setAndAllowWhileIdle(int,%20long,%20android.app.PendingIntent)" class="external"><code>setAndAllowWhileIdle()</code></a>
and
<a href="http://developer.android.com/reference/android/app/AlarmManager.html#setExactAndAllowWhileIdle(int,%20long,%20android.app.PendingIntent)" class="external"><code>setExactAndAllowWhileIdle()</code></a>.
</li>
</ul>

<h4 id="test-apps">Testing and optimizing applications</h4>
<p>Test all applications (especially preloaded applications) in Doze mode. For
details, refer to
<a href="https://developer.android.com/training/monitoring-device-state/doze-standby.html#testing_doze_and_app_standby">Testing
Doze and App Standby</a>.</p>

<aside class="note"><strong>Note:</strong> MMS/SMS/Telephony services function
independently of Doze and will always wake client apps even while the device
remains in Doze mode.</aside>

  </body>
</html>
