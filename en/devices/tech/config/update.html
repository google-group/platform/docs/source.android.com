<html devsite>
  <head>
    <title>APN and CarrierConfig</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>Carriers can update their Access Point Name (APN) information and their
carrier-specific configuration settings
(<a href="/devices/tech/config/carrier">CarrierConfig</a>)
in the Android Open Source Project (AOSP).</p>

<h2 id="create-account">Google Account with your corporate email address</h2>

<p>To update APN information or your CarrierConfig, you need
to submit the request using a Google Account with an active corporate email
address (e.g., An APN update request from Acme Company should come from an
email address such as <em>foobar@acme.com</em>).</p>
<p>If you do not have a Google Account that links to your corporate email
address, sign out of all Gmail accounts from your browser (we recommend you use
a private browsing feature such as an incognito window to avoid confusion with
your other accounts) and then
<a href="https://accounts.google.com/SignUpWithoutGmail?hl=en"> create a Google
account with your corporate email address</a>.</p>
<!--<ol>-->
  <!--<li>Sign out of all gmail accounts from your browser. (We recommend you use a private browsing
  feature such as an incognito window to avoid confusion with your other accounts.)</li>-->
  <!--<li>Create a Google account with your corporate email address using:
  <a href="https://accounts.google.com/SignUpWithoutGmail?hl=en">
  https://accounts.google.com/SignUpWithoutGmail?hl=en</a></li>-->
<!--</ol>-->
<aside class="note"><strong>Note:</strong> Do NOT associate any Gmail accounts with this
newly created account.</aside>

<h2 id="local-environment">Prepare a local development environment</h2>

<p>If you've never submitted code to AOSP before, you will
need to initialize your build environment, become familiar with the tools, and
understand how to submit patches:</p>
<ul>
  <li><a href="/setup/build/initializing">Establishing a build environment</a></li>
  <li><a href="/setup/develop/">Getting familiar with Git and Repo</a></li>
  <li><a href="/setup/build/downloading">Downloading the source</a></li>
  <li><a href="/setup/contribute/submit-patches">Submitting patches</a></li>
</ul>

<p>In addition, we strongly recommend that you use the
<a class="external" href="https://developers.google.com/issue-tracker/">Google Issue Tracker</a>
to track changes.</p>

<h2 id="file-a-bug">File a bug</h2>

<ol>
  <li>Go to <a class="external" href="https://issuetracker.google.com">issuetracker.google.com</a>
  and sign in using your corporate email address (see <a href="#create-account">
  Google Account with your corporate email address</a>).</li>
  <aside class="note"><strong>Note:</strong> If this is your first time logging in, you
  need to accept a license.</aside>
  <li>After logging in, click <strong>Create issue</strong> on the left side of the
  screen.</li>
  <li>In <strong>Component</strong>, select <a class="external"
  href="https://issuetracker.google.com/issues/new?component=192705&template=845803"
  class="external"><strong>Android Public Tracker > Framework</strong></a></li>
  <li>Continue to <a href="#apn-update1">APN update</a> or
  <a href="#carrierconfig-update1">CarrierConfig update</a>, as appropriate,
  to enter the Title and Description.</li>
</ol>

<h3 id="apn-update1">APN bug</h3>

<p>Set the bug attributes as follows:</p>
<p><strong>Title:</strong> <em>Add</em>/<em>Modify</em>/<em>Remove</em> APNs for CarrierXYZ</p>
<p><strong>Description:</strong> Add a detailed description of the changes you're
requesting, including the APN settings themselves.</p>

<h3 id="carrierconfig-update1">CarrierConfig bug</h3>

<p>Set the bug attributes as follows:</p>
<p><strong>Title:</strong> Config changes for CarrierXYZ</p>
<p><strong>Description:</strong> Add a detailed description of the changes you're
requesting.</p>

<h2 id="submit-changes">Submit changes</h2>

<p>When you're ready to make changes, follow these steps:</p>
<ol>
  <li>Identify which file to change.</li>
  <li>Make changes to the file.</li>
  <li>Commit the changes to your local repository by following the instructions
  for <a href="/setup/contribute/submit-patches#make-your-change">
  submitting patches</a>, paying attention to the commit message guidelines,
  including recommended format.</li>
  <li>After you commit your change to your personal history, upload it to Gerrit
  using the <code>repo upload</code> command.</li>
  <li>Following a successful upload, repo provides the URL of a new page on
  Gerrit. Use this URL to:
   <ul>
     <li>View your patch on the review server</li>
     <li>Add comments</li>
     <li>Request specific reviewers for your patch</li>
   </ul>
  </li>
</ol>

<aside class="note"><b>Note:</b> Submission of APN and CarrierConfig changes
to AOSP doesn't guarantee that OEMs will pick up the change. Carriers should
work with OEMs so that the required changes are made on their devices.</aside>

<h3 id="apn-update2">APN syntax</h3>

<p><strong>Android project name</strong> - device/sample</p>
<p><strong>File name(s)</strong> - etc/apns-full-conf.xml
(<a class="external"
href="https://android.googlesource.com/device/sample/+/master/etc/apns-full-conf.xml">Google
Git master link</a>)</p>
<p>The file contains APN settings in XML format and serves as a sample file
so there is no change in the  behavior of Android devices.</p>
<p>A typical APN config looks like this:</p>

<pre class="prettyprint">&lt;apn carrier="CarrierXYZ"
      mcc="123"
      mnc="123"
      apn="carrierxyz"
      type="default,supl,mms,ims,cbs"
      mmsc="http://mms.carrierxyz.com"
      mmsproxy="0.0.0.0"
      mmsport="80"
      bearer_bitmask="4|5|6|7|8|12"
/&gt;
</pre>

<h4 id="testing">Testing</h4>
<ol>
  <li>Connect any Android device for which you have root access.</li>
  <li>Add settings to /etc/apns-conf.xml.</li>
  <li>Reset APN settings.</li>
  <li>In APN settings, confirm that profiles are loaded correctly.</li>
  <li>For each profile, test data connectivity, Wi-Fi hotspot, and MMS
  to make sure they work correctly.</li>
</ol>

<h4 id="commit-message">Commit message</h4>

<pre class="devsite-click-to-copy">[Example - "Add CarrierXYZ apns to sample apns"]
Bug: [Issue ID from Google Issue Tracker]
Test: No change to behavior as this is only a sample file
</pre>

<h4 id="sample">Sample CL</h4>
<p> See <a class="external" href="https://android-review.googlesource.com/c/device/sample/+/532609">
Sample BICS APNs</a> for an example CL.</p>

<h3 id="carrierconfig-update2">CarrierConfig syntax</h3>

<p><b>Project name</b> - platform/packages/apps/CarrierConfig</p>
<p><b>File name(s)</b> - assets/carrier_config_<mccmnc>.xml
(<a class="external"
href="https://android.googlesource.com/platform/packages/apps/CarrierConfig/+/master/assets">Google
Git master link</a>)</p>
<p>Identify the relevant XML file(s) in the assets folder by the relevant MCC/MNC
tuple(s). The file contains the carrier config object in XML format. The
attribute names are defined as keys under the
<a class="external" href="https://developer.android.com/reference/android/telephony/CarrierConfigManager.html">
CarrierConfigManager</a>, and the type of value (int/string/bool) is indicated
by the suffixes.</p>
<p>Typical int/string/bool attributes look like this:</p>

<pre class="prettyprint">&lt;int name="vvm_port_number_int" value="5499" /&gt;
&lt;string name="vvm_type_string">vvm_type_omtp&lt;/string &gt;
&lt;boolean name="vvm_cellular_data_required_bool" value="true" /&gt;
</pre>

<h4 id="commit-message">Commit message</h4>

<pre class="devsite-click-to-copy">[Example - "Add VVM settings for CarrierXYZ"]

[Example - "Updated &lt;mccmnc&gt; carrier config file to include VVM settings
as defined by CarrierXYZ."]

Bug: [Issue ID from Google Issue Tracker]
Test: [Testing notes]
</pre>

<h4 id="sample">Sample CL</h4>
<p> See an <a class="external"
href="https://android-review.googlesource.com/c/platform/packages/apps/CarrierConfig/+/625835">
updated carrier config file</a> for an example CL. </p>

<h2 id="request-a-review">Request a review</h2>

<p>To request a review:</p>
<ul>
  <li>Go to the Gerrit URL of your change and add
   <em>android-carrier-config-review@google.com</em> as a reviewer.</li>
  <li>You can also add your Google contact to the CC list on Gerrit.</li>
  <li>For tracking purposes, you can change the assignee of the issue to
  <em>android-carrier-config-review@google.com</em>.</li>
</ul>

<p>After a submission makes it through the review and verification process,
Gerrit automatically merges the change into the public repository. Other users
can run <code> repo sync </code> to pull the update into their local client.</p>
<aside class="note"><strong>Note:</strong> Changes to the APN file don't automatically
take effect because the changes are in device/sample folder. If you are working
with key OEMs, make sure they take the latest information from AOSP. Typically
OEMs have a regular schedule where they pick the latest information, but if you
are in a hurry, reach out to OEMs to encourage them to update their list sooner
rather than later.</aside>

  </body>
</html>
