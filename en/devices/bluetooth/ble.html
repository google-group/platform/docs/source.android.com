<html devsite>
  <head>
    <title>Bluetooth Low Energy</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
    <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License.  You may obtain a
      copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
      License for the specific language governing permissions and limitations
      under the License.
    -->

    <p>
      Bluetooth Low Energy (BLE), available in Android 4.3 and later, creates
      short connections between devices to transfer bursts of data. BLE
      remains in sleep mode when not connected. This lets BLE provide lower
      bandwidth and reduced power consumption compared to Classic Bluetooth.
      It is ideal for applications such as a heart-rate monitor or a wireless
      keyboard. To use BLE, devices need to have a chipset that supports BLE.
    </p>

    <h2 id="implementation">Implementation</h2>

      <p>
        All current BLE application profiles are based on the
        <a href="https://www.bluetooth.com/specifications/gatt">Generic
        Attribute Profile</a> (GATT). When an Android device interacts with a
        BLE device, the device that sends information is the server and the
        device that receives information is the client. Android includes
        <a href="https://developer.android.com/guide/topics/connectivity/bluetooth-le.html">
        developer APIs for BLE</a>, including APIs for interactions between
        GATT servers and GATT clients. To fully leverage the BLE APIs,
        implement the
        <a href="/devices/bluetooth/hci_requirements.html">
        Android Bluetooth HCI Requirements</a>.
      </p>

    <h2 id="device-modes">Device modes</h2>

      <p>
        When using BLE, an Android device can act as a peripheral device, a
        central device, or both. Peripheral mode lets devices send
        advertisement packets. Central mode lets devices scan for
        advertisements. An Android device acting as both a peripheral and
        central device can communicate with other BLE peripheral devices while
        sending advertisements in peripheral mode. Devices supporting
        Bluetooth 4.1 and earlier can only use BLE in central mode. Older
        device chipsets may not support BLE peripheral mode.
      </p>

    <h2 id="ble-scanning">BLE scanning</h2>

      <p>
        An Android device can target and scan for specific Bluetooth devices
        more efficiently when using BLE. BLE APIs let app developers create
        filters for finding devices with less involvement from the host
        controller.
      </p>

      <aside class="caution">
        <strong>Caution:</strong> BLE scanning needs location permissions as BLE
        scanning identifies objects that could be used for geolocation. Turning
        off Location services will turn off Bluetooth scanning.
      </aside>

      <h3 id="location-scanning">Location scanning</h3>

        <p>
          A device's location services can use Bluetooth to detect Bluetooth
          beacons and provide a more accurate location. This is how users
          enable the feature:
        </p>

        <ol>
          <li>Go to <strong>Settings</strong> &gt; <strong> Security &amp;
          Location</strong> &gt; <strong>Location</strong>.</li>
          <li>Check that the Location <strong>On</strong>/<strong>Off</strong>
            toggle is in the <strong>On</strong> position.</li>
          <li>Go to <strong>Mode</strong>.</li>
          <li>Select either <strong>High accuracy</strong> or <strong>Battery
          saving</strong> mode.</li>
        </ol>

        <p>
          Individual apps need location permissions to use BLE scanning, even
          if they are scanning only to find devices to connect to. If the user
          disables location scanning, or does not give an app location
          permissions, then the app will not receive any BLE scanning results.
        </p>

        <p>
          Users can disable system-level Bluetooth background scanning by
          going to <strong>Settings</strong> &gt; <strong>Security &amp;
          Location</strong> &gt; <strong>Location</strong> &gt; <strong>
          Scanning</strong> and disabling the toggle for <strong>Bluetooth
          scanning</strong>. This does not affect BLE scanning for location or
          local devices.
        </p>

      <h3 id="filtering-scan-results">Filtering scan results</h3>

        <p>
          Android 6.0 and later includes BLE scanning and filter-matching on
          the Bluetooth controller. A device can filter scan results and
          report <strong>found</strong> and <strong>lost</strong> events
          related to BLE devices to the application processor (AP). Filtering
          also works for batch scans, which helps save power as BLE scanning is
          offloaded to the firmware. Batch scans reduce how often the AP wakes
          up due to BLE scanning for devices or beacons.
        </p>

        <p>
          The <code>OnFound</code>/<code>OnLost</code> feature is implemented
          in the Bluetooth controller and then tested to confirm that BLE
          devices are not missed in scans. This provides power savings, and it
          can also provide the following benefits:
        </p>

        <ul>
          <li>For an <code>OnFound</code> event, the main AP wakes up upon the
          discovery of a specific device.</li>
          <li>For an <code>OnLost</code> event, the AP wakes up when a
            specific device cannot be found.</li>
          <li>Framework apps get fewer unwanted notifications when a nearby device is in range.</li>
          <li>Continuous scans enable framework apps to be notified when a device is out of range.</li>
        </ul>

        <p>
          A scan filter can be based on the discovery of a device's
          advertisements (the <code>OnFound</code> event). The Java layer can
          specify parameters such as first discovery (or a specified number of
          advertisements since first discovery). Implement BLE filtering using
          the <a href="/devices/bluetooth/hci_requirements#advertisement-packet-content-filter">
          Advertising Packet Content Filter HCI command</a>.
        </p>
</body>
</html>