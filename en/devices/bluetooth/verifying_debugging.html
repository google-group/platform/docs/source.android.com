<html devsite>
  <head>
    <title>Verifying and Debugging</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

    <p>To verify and debug the Bluetooth stack, use the tools provided in
    AOSP and the Bluetooth Special Interest Group's (SIG) tests.</p>

    <h2 id="testing-and-verifying">Testing and verifying</h2>
      <p>To test the Bluetooth stack, AOSP provides a mix of unit tests,
      CTS tests, and tools for the Bluetooth Profile Tuning Suite.</p>

      <h3 id="unit-tests-in-aosp">Unit tests in AOSP</h3>
        <p>AOSP includes functional and unit tests for the default
        Bluetooth stack. These tests are located in <code><a
        href="https://android.googlesource.com/platform/system/bt/+/master/test/">
          system/bt/test</a></code>.
        To run the AOSP tests, do the following:</p>

        <ol>
          <li>Stop the Android runtime:
          <pre class="devsite-terminal devsite-click-to-copy">adb shell stop</pre></li>
          <li>From the test directory, run the shell executable file and
          include options if you want to run a specific test or test suite:
          <pre class="devsite-terminal devsite-click-to-copy">./run_unit_tests.sh <var>TEST_GROUP_NAME</var> <var>TEST_NAME</var> <var>OPTIONS</var></pre></li>
          <li>When the tests finish, re-enable the Android runtime:
          <pre class="devsite-terminal devsite-click-to-copy">adb shell start</pre></li>
        </ol>

        <p>The list of test names can be found in the file <code><a
        href="https://android.googlesource.com/platform/system/bt/+/master/test/run_unit_tests.sh">
        system/bt/test/run_unit_tests.sh</a></code>.</p>

      <h3 id="android-comms-test-suite">Android Comms Test Suite</h3>
        <p>The Android Comms Test Suite (ACTS) performs automated testing of
        connectivity stacks, such as Wi-Fi, Bluetooth, and cellular services.
        The testing tool requires adb and python, and it can be found in
        <code><a
        href="https://android.googlesource.com/platform/tools/test/connectivity/+/master/acts">
          tools/test/connectivity/acts</a></code>.</p>

        <p>The ACTS tests for Bluetooth and Bluetooth Low Energy are found in
        <code><a
        href="https://android.googlesource.com/platform/tools/test/connectivity/+/master/acts/tests/google/bt/">
          tools/test/connectivity/acts/tests/google/bt</a></code>
        and <code><a
        href="https://android.googlesource.com/platform/tools/test/connectivity/+/master/acts/tests/google/ble">
          tools/test/connectivity/acts/tests/google/ble</a></code>
        respectively.</p>

      <h3 id="profile-tuning-suite">Profile Tuning Suite</h3>
        <p>The Bluetooth SIG provides the Bluetooth Profile Tuning Suite (PTS),
        a testing tool for protocol and profile interoperability.
        For more information, see the <a
        href="https://www.bluetooth.com/develop-with-bluetooth/test-tools/profile-tuning-suite">
          Bluetooth Profile Tuning Suite</a> site.</p>

        <p>AOSP provides additional tools to complement the Bluetooth PTS.
        These tools are located in <code><a
        href="https://android.googlesource.com/platform/tools/test/connectivity/+/master/acts/tests/google/bt/pts/">
          tools/test/connectivity/acts/tests/google/bt/pts</a></code>.</p>

      <h3 id="cts-tests">CTS Tests</h3>
      <p>
        The <a href="/compatibility/cts/">Compatibility Test Suite</a> (CTS)
        includes tests for the Bluetooth stack. These are located in <code><a
        href="https://android.googlesource.com/platform/cts/+/master/apps/CtsVerifier/src/com/android/cts/verifier/bluetooth/">
        cts/apps/CtsVerifier/src/com/android/cts/verifier/bluetooth</a></code>.
      </p>

    <h2 id="debugging-options">Debugging options</h2>
      <p>AOSP provides different methods of debugging a device's
      Bluetooth stack, including logs and bug reports. These methods may
      not work for issues that cannot be reproduced or for audio issues,
      which can be affected by multiple parts of the platform and device.
      </p>

      <h3 id="debugging-with-bug-reports">Debugging with bug reports</h3>
        <p>To check the Bluetooth service status using <code>dumpsys</code>,
        use the following command:</p>

        <pre class="devsite-terminal devsite-click-to-copy">adb shell dumpsys bluetooth_manager</pre>

        <p>By default, all log messages are trace level 2.
        To find out more about the logging levels and change the logging levels
        for different profiles, look in <code><a
        href="https://android.googlesource.com/platform/system/bt/+/master/conf/bt_stack.conf">
          system/bt/conf/bt_stack.conf</a></code>.</p>

        <p>To extract snoop logs from the bug report, use the
        <code>btsnooz</code> script.</p>

        <ol>
          <li>Get <code><a
          href="https://android.googlesource.com/platform/system/bt/+/master/tools/scripts/btsnooz.py">
            btsnooz.py</a></code>.</li>
          <li>Extract the text version of the bug report.</li>
          <li>Run <code>btsnooz.py</code> on the text version of the bug report:
          <pre class="devsite-terminal devsite-click-to-copy">btsnooz.py <em>BUG_REPORT</em>.txt &gt; <em>BTSNOOP</em>.log</pre>
          </li>
        </ol>

      <h3 id="debugging-with-logs">Debugging with logs</h3>
        <p>In Android 4.4 and later, you can manually collect BTSnoop logs,
        which resemble the snoop format in RFC 1761. These logs capture the
        Host Controller Interface (HCI) packets.
        For most Android devices, the logs are stored in
        <code>data/misc/bluetooth/logs</code>.</p>

        <p>For privacy reasons, always-on, "in-memory" BTSnoop only logs
        non-personal information and events. To log all data, the user needs
        to enable Bluetooth HCI snoop by doing the following:</p>

        <ol>
          <li>Enable <strong>Developer options</strong> on the device.</li>
          <li>In the <strong>Developer options</strong> menu, activate the
          <strong>Enable Bluetooth HCI snoop log</strong> toggle.</li>
          <li>Restart Bluetooth for logging to take effect.</li>
        </ol>

  </body>

</html>
