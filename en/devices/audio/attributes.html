<html devsite>
  <head>
    <title>Audio Attributes</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  {% include "_versions.html" %}
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<p>
  Audio players support attributes that define how the audio system handles
  routing, volume, and focus decisions for the specified source. Applications
  can attach attributes to an audio playback (such as music played by a
  streaming service or a notification for a new email) then pass the audio
  source attributes to the framework, where the audio system uses the attributes
  to make mixing decisions and to notify applications about the state of the
  system.
</p>

<aside class="note"><strong>Note:</strong> Applications can also attach
  attributes to an audio recording (such as audio captured in a video
  recording), but this functionality is not exposed in the public API.
</aside>

<p>
  In Android 4.4 and earlier, the framework made mixing decisions using only
  the audio stream type. However, basing such decisions on stream type was too
  limiting to produce quality output across multiple applications and devices.
  For example, on a mobile device, some applications (i.e. Google Maps) played
  driving directions on the STREAM_MUSIC stream type; however, on mobile devices
  in projection mode (i.e. Android Auto), applications cannot mix driving
  directions with other media streams.
</p>

<p>
  Using the
  <a href="http://developer.android.com/reference/android/media/AudioAttributes.html" class="external">Audio
  Attribute API</a>, applications provide the audio system with detailed
  information about a specific audio source, including usage (why the source is
  playing), content type (what the source is playing), flags (how the source
  should be played), and contexts (new in Android {{ androidPVersionNumber }}).
  Syntax:
</p>

<pre class="prettyprint">
AudioAttributes {
    mUsage
    mContentType
    mSource
    mFlags
    mTags / mFormattedTags / mBundle    (key value pairs)
}
</pre>


<ul>
  <li><strong>Usage</strong>. Specifies why the source is playing and controls
  routing, focus, and volume decisions.</li>
  <li><strong>Content type</strong>. Specifies what the source is playing
  (music, movie, speech, sonification, unknown).</li>
  <li><strong>Context</strong>. Usage values abstracted to the Audio HAL.
  <li><strong>Flags</strong>. Specifies how the source should be played.
  Includes support for audibility enforcement (camera shutter sounds required in
  some countries) and hardware audio/video synchronization.</li>
</ul>

<p>
  For dynamics processing, applications must distinguish between movie, music,
  and speech content. Information about the data itself may also matter, such as
  loudness and peak sample value.
</p>

<h2 id="using">Using attributes</h2>

<p>
  Usage specifies the context in which the stream is used, providing
  information about why the sound is playing and what the sound is used for.
  Usage information is more expressive than a stream type and allows platforms
  or routing policies to refine volume or routing decisions.
</p>

<p>
  Supply one of the following usage values for any instance:
</p>

<ul>
<li>USAGE_UNKNOWN</li>
<li>USAGE_MEDIA</li>
<li>USAGE_VOICE_COMMUNICATION</li>
<li>USAGE_VOICE_COMMUNICATION_SIGNALLING</li>
<li>USAGE_ALARM</li>
<li>USAGE_NOTIFICATION</li>
<li>USAGE_NOTIFICATION_TELEPHONY_RINGTONE</li>
<li>USAGE_NOTIFICATION_COMMUNICATION_REQUEST</li>
<li>USAGE_NOTIFICATION_COMMUNICATION_INSTANT</li>
<li>USAGE_NOTIFICATION_COMMUNICATION_DELAYED</li>
<li>USAGE_NOTIFICATION_EVENT</li>
<li>USAGE_ASSISTANCE_ACCESSIBILITY</li>
<li>USAGE_ASSISTANCE_NAVIGATION_GUIDANCE</li>
<li>USAGE_ASSISTANCE_SONIFICATION</li>
<li>USAGE_GAME</li>
<li>USAGE_VIRTUAL_SOURCE</li>
<li>USAGE_ASSISTANT</li>
</ul>

<p>
  Audio attribute usage values are mutually exclusive. For examples, refer to
  <code><a href="http://developer.android.com/reference/android/media/AudioAttributes.html#USAGE_MEDIA" class="external">USAGE_MEDIA</a></code>
  and
  <code><a href="http://developer.android.com/reference/android/media/AudioAttributes.html#USAGE_ALARM" class="external">USAGE_ALARM</a></code>
  definitions; for exceptions, refer to the
  <code><a href="http://developer.android.com/reference/android/media/AudioAttributes.Builder.html" class="external">AudioAttributes.Builder</a></code>
  definition.
</p>

<h2 id="content-type">Content type</h2>

<p>
  Content type defines what the sound is and expresses the general category of
  the content such as movie, speech, or beep/ringtone. The audio framework uses
  content type information to selectively configure audio post-processing
  blocks. While supplying the content type is optional, you should include type
  information whenever the content type is known, such as using
  <code>CONTENT_TYPE_MOVIE</code> for a movie streaming service or
  <code>CONTENT_TYPE_MUSIC</code> for a music playback application.
</p>

<p>
  Supply one of the following content type values for any instance:
</p>

<ul>
<li><code>CONTENT_TYPE_UNKNOWN</code> (default)</li>
<li><code>CONTENT_TYPE_MOVIE</code></li>
<li><code>CONTENT_TYPE_MUSIC</code></li>
<li><code>CONTENT_TYPE_SONIFICATION</code></li>
<li><code>CONTENT_TYPE_SPEECH</code></li>
</ul>

<p>
  Audio attribute content type values are mutually exclusive. For details on content types,
  refer to the
  <a href="http://developer.android.com/reference/android/media/AudioAttributes.html" class="external">audio
  attribute API</a>.
</p>

<h2 id="contexts">Contexts</h2>

<p>
  Each sound in Android is identified by the responsible application and reason
  for generating the sound; and Android device uses this information to
  determine how to present the sound. In Android 8.x and lower, applications can
  report the sound generation reason using legacy stream types (e.g.
  <code>AudioSystem.STREAM_MUSIC</code>) or <code>AudioAttributes</code>. In
  Android {{ androidPVersionNumber }}, <code>AudioAttributes.usage</code> values
  are abstracted at the HAL level as <em>Contexts</em>.
</p>

<table>
<thead>
<tr>
<th>HAL audio contexts</th>
<th>AudioAttributes usage</th>
</tr>
</thead>
<tbody>
<tr>
<td>MUSIC</td>
<td>MEDIA</td>
</tr>
<tr>
<td>VOICE_COMMAND</td>
<td>USAGE_ASSISTANT</td>
</tr>
<tr>
<td>NAVIGATION</td>
<td>ASSISTANCE_NAVIGATION_GUIDANCE</td>
</tr>
<tr>
<td>CALL</td>
<td>VOICE_COMMUNICATION</td>
</tr>
<tr>
<td>RINGTONE</td>
<td>NOTIFICATION_RINGTONE</td>
</tr>
<tr>
<td>NOTIFICATION</td>
<td>NOTIFICATION</td>
</tr>
<tr>
<td>ALARM</td>
<td>ALARM</td>
</tr>
<tr>
<td>SYSTEM_SOUND</td>
<td>ASSISTANCE_SONIFICATION</td>
</tr>
<tr>
<td>UNKNOWN</td>
<td>UNKNOWN</td>
</tr>
</tbody>
</table>

<p>
  You can supply one of the following <code>CONTEXT_NUMBER</code> values for any
  instance:
</p>

<ul>
<li>MUSIC_CONTEXT           // Music playback</li>
<li>NAVIGATION_CONTEXT      // Navigation directions</li>
<li>VOICE_COMMAND_CONTEXT   // Voice command session</li>
<li>CALL_RING_CONTEXT       // Voice call ringing</li>
<li>CALL_CONTEXT            // Voice call</li>
<li>ALARM_CONTEXT           // Alarm sound from Android</li>
<li>NOTIFICATION_CONTEXT    // Notifications</li>
<li>SYSTEM_SOUND_CONTEXT    // User interaction sounds (button clicks, etc)</li>
</ul>


<h2 id="flags">Flags</h2>

<p>
  Flags specify how the audio framework applies effects to the audio playback.
  Supply one or more of the following flags for an instance:
</p>

<ul>
  <li><code>FLAG_AUDIBILITY_ENFORCED</code>. Requests the system to ensure the
  audibility of the sound. Use to address the needs of legacy
  <code>STREAM_SYSTEM_ENFORCED</code> (such as forcing camera shutter sounds).
  </li>
  <li><code>HW_AV_SYNC</code>. Requests the system to select an output stream
  that supports hardware A/V synchronization.</li>
</ul>

<p>
  Audio attribute flags are non-exclusive and can be combined. For details on
  these flags, refer to the
  <a href="http://developer.android.com/reference/android/media/AudioAttributes.html" class="external">audio
  attribute API</a>.
</p>

<h2 id="example">Example</h2>

<p>
  In this example, <code>AudioAttributes.Builder</code> defines the
  <code>AudioAttributes</code> to be used by a new <code>AudioTrack</code>
  instance:
</p>

<pre class="prettyprint">
AudioTrack myTrack = new AudioTrack(
  new AudioAttributes.Builder()
 .setUsage(AudioAttributes.USAGE_MEDIA)
    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
    .build(),
  myFormat, myBuffSize, AudioTrack.MODE_STREAM, mySession);
</pre>

<h2 id="compatibility">Compatibility</h2>

<p>
  Application developers should use audio attributes when creating or updating
  applications for Android 5.0 and higher. However, applications are not
  required to take advantage of attributes; they can handle legacy stream types
  only or remain unaware of attributes (i.e. a generic media player that doesn't
  know anything about the content it's playing).
</p>

<p>
  In such cases, the framework maintains backwards compatibility with older
  devices and Android releases by automatically translating legacy audio stream
  types to audio attributes. However, the framework does not enforce or
  guarantee this mapping across devices, manufacturers, or Android releases.
</p>

<p>
  Compatibility mappings:
</p>

<table>
<tr>
  <th>Android 5.0 and higher</th>
  <th>Android 4.4 and earlier</th>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SPEECH</code><br />
  <code>USAGE_VOICE_COMMUNICATION</code>
  </td>
  <td>
  <code>STREAM_VOICE_CALL</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_ASSISTANCE_SONIFICATION</code>
  </td>
  <td>
  <code>STREAM_SYSTEM</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_NOTIFICATION_RINGTONE</code>
  </td>
  <td>
  <code>STREAM_RING</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_MUSIC</code><br />
  <code>USAGE_UNKNOWN</code><br />
  <code>USAGE_MEDIA</code><br />
  <code>USAGE_GAME</code><br />
  <code>USAGE_ASSISTANCE_ACCESSIBILITY</code><br />
  <code>USAGE_ASSISTANCE_NAVIGATION_GUIDANCE</code>
  </td>
  <td>
  <code>STREAM_MUSIC</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_ALARM</code>
  </td>
  <td>
  <code>STREAM_ALARM</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_NOTIFICATION</code><br />
  <code>USAGE_NOTIFICATION_COMMUNICATION_REQUEST</code><br />
  <code>USAGE_NOTIFICATION_COMMUNICATION_INSTANT</code><br />
  <code>USAGE_NOTIFICATION_COMMUNICATION_DELAYED</code><br />
  <code>USAGE_NOTIFICATION_EVENT</code>
  </td>
  <td>
  <code>STREAM_NOTIFICATION</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SPEECH</code>
  </td>
  <td>
  (@hide)<code> STREAM_BLUETOOTH_SCO</code>
  </td>
</tr>
<tr>
  <td>
  <code>FLAG_AUDIBILITY_ENFORCED</code>
  </td>
  <td>
  (@hide)<code> STREAM_SYSTEM_ENFORCED</code>
  </td>
</tr>
<tr>
  <td>
  <code>CONTENT_TYPE_SONIFICATION</code><br />
  <code>USAGE_VOICE_COMMUNICATION_SIGNALLING</code>
  </td>
  <td>
  (@hide)<code> STREAM_DTMF</code>
  </td>
</tr>
</table>

<aside class="note"><strong>Note:</strong> @hide streams are used internally by
  the framework but are not part of the public API.
</aside>

<h2 id="deprecated">Deprecated stream types</h2>

<p>
  Android {{ androidPVersionNumber }} deprecates the following stream types for
  automotive use:
</p>

<ul>
<li>STREAM_DEFAULT</li>
<li>STREAM_VOICE_CALL</li>
<li>STREAM_SYSTEM</li>
<li>STREAM_RING</li>
<li>STREAM_MUSIC</li>
<li>STREAM_ALARM</li>
<li>STREAM_NOTIFICATION</li>
<li>STREAM_BLUETOOTH_SCO</li>
<li>STREAM_SYSTEM_ENFORCED</li>
<li>STREAM_DTMF</li>
<li>STREAM_TTS</li>
<li>STREAM_ACCESSIBILITY</li>
</ul>

<p>For more details, see
  <a href="/devices/automotive/audio">Automotive Audio</a>.
</p>

  </body>
</html>
