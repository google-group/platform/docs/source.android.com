<html devsite>
  <head>
    <title>Vehicle Properties</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<p>The vehicle HAL interface defines the properties OEMs can implement and
contains property metadata (for example, whether the property is an int and
which change modes are allowed). The vehicle HAL interface is based on accessing
(read, write, subscribe) a property, which is an abstraction for a specific
function.</p>

<h2 id=interfaces>HAL interfaces</h2>
<p>The vehicle HAL uses the following interfaces:</p>
<ul>
<li><code>vehicle_prop_config_t const *(*list_properties)(..., int*
num_properties)</code>
<br>List configuration of all properties supported by the vehicle HAL. Only
supported properties are used by vehicle network service.
</li>
<li><code>(*get)(..., vehicle_prop_value_t *data)</code>
<br>Read the current value of the property. For zoned property, each zone may
have different value.</li>
<li><code>(*set)(..., const vehicle_prop_value_t *data)</code>
<br>Write a value to property. Result of write is defined per property.</li>
<li><code>(*subscribe)(..., int32_t prop, float sample_rate, int32_t
zones)</code>
<ul>
<li>Start monitoring a property value change. For zoned property, subscription
applies to requested zones. Zones = 0 is used to request all zones supported.
</li>
<li>Vehicle HAL should call separate callback when the property's value changes
(=on change) or in const interval (=continuous type).</li>
</ul></li>
<li><code>(*release_memory_from_get)(struct vehicle_hw_device* device,
vehicle_prop_value_t *data)</code>
<br>Release memory allocated from get call.</li>
</ul>

<p>The vehicle HAL uses the following callback interfaces:</p>
<ul>
<li><code>(*vehicle_event_callback_fn)(const vehicle_prop_value_t
*event_data)</code>
<br>Notifies vehicle property's value change. Should be done only for
subscribed properties.</li>
<li><code>(*vehicle_error_callback_fn)(int32_t error_code, int32_t property,
int32_t operation)</code>
<br>Return global vehicle HAL level error or error per property. Global error
causes HAL restart, which can lead to restarting other components (including
applications).</li>
</ul>

<h2 id=properties>Vehicle properties</h2>
<p>Properties can be read-only, write-only (used to pass information to vehicle
HAL level), or read and write (support of most properties is optional). Each
property is uniquely identified by an int32 key and has a predefined type
(<code>value_type</code>):</p>

<ul>
<li><code>BYTES</code></li>
<li><code>BOOLEAN</code></li>
<li><code>FLOAT</code></li>
<li><code>FLOAT[]</code></li>
<li><code>INT32</code></li>
<li><code>INT32[]</code></li>
<li><code>INT64</code></li>
<li><code>INT64[]</code></li>
<li><code>STRING</code></li>
</ul>
<p>A zoned property may have more than one value, based on the number of zones
supported by the property.</p>

<h2 id=area_type>Area types</h2>
<p>The vehicle HAL defines multiple area types:</p>
<ul>
<li><code>GLOBAL</code>
<br>This property is a singleton and does not have multiple areas.</li>
<li><code>WINDOW</code>
<br>Area based on windows, uses <code>VehicleAreaWindow</code> enum.</li>
<li><code>MIRROR</code>
<br>Area based on mirrors, uses <code>VehicleAreaMirror</code> enum.</li>
<li><code>SEAT</code>
<br>Area based on seats, uses <code>VehicleAreaSeat</code> enum.</li>
<li><code>DOOR</code>
<br>Area based on doors, uses <code>VehicleAreaDoor</code> enum.</li>
<li><code>WHEEL</code>
<br>Area based on wheels, uses <code>VehicleAreaWheel</code> enum.</li>
</ul>
<p>Each zoned property must use pre-defined area type. Each area type has a
set of bit flags defined in an enum for the area type. For example, the SEAT
area defines VehicleAreaSeat enums:</p>
<ul>
<li><code>ROW_1_LEFT = 0x0001</code></li>
<li><code>ROW_1_CENTER = 0x0002</code></li>
<li><code>ROW_1_RIGHT = 0x0004</code></li>
<li><code>ROW_2_LEFT = 0x0010</code></li>
<li><code>ROW_2_CENTER = 0x0020</code></li>
<li><code>ROW_2_RIGHT = 0x0040</code></li>
<li><code>ROW_3_LEFT = 0x0100</code></li>
<li>...</li>
</ul>

<h2 id=area_id>Area IDs</h2>
<p>Zoned properties are addressed via Area IDs. Each zoned property may
support one or more Area IDs. An Area ID is composed of one or more flags
from its respective enum. For example, a property using
<code>VehicleAreaSeat</code> might use the following Area IDs:</p>
<ul>
<li><code>ROW_1_LEFT | ROW_1_RIGHT</code>
<br>The Area ID applies to both front seats.</li>
<li><code>ROW_2_LEFT</code>
<br>Only applies to rear left seat.</li>
<li><code>ROW_2_RIGHT</code>
<br>Only applies to rear right seat.</li>
</ul>

<h2 id=status>Property Status</h2>
<p>Every property value comes with a <code>VehiclePropertyStatus</code> value.
This indicates the current status for the property:
<ul>
<li><code>AVAILABLE</code>
<br>Property is available and the value is valid.</li>
<li><code>UNAVAILABLE</code>
<br>Property value is currently unavailable. This is used for transiently
disabled features for a supported property.</li>
<li><code>ERROR</code>
<br>Something is wrong with this property.</li>
</ul>
<aside class="note"><strong>Note:</strong> If a property is not supported by the
vehicle, it should not be included in the VHAL. It is not acceptable to set the
property status to <code>UNAVAILABLE</code> permanently to denote an unsupported
property.</aside>

<h2 id=prop_config>Configuring a property</h2>
<p>Use <code>vehicle_prop_config_t</code> to provide configuration information
for each property. Information includes:</p>
<ul>
<li><code>access</code> (r, w, rw)</li>
<li><code>changeMode</code> (represents how property is monitored: on change vs
continuous)</li>
<li><code>areaConfigs</code> (areaId, min, and max values)</li>
<li><code>configArray</code> (additional configuration parameters)</li>
<li><code>configString</code> (additional information passed as a string)</li>
<li><code>minSampleRate</code>, <code>max_sample_rate</code></li>
<li><code>prop</code> (Property ID, int)</li>
</ul>

<h2 id=zone_prop>Handling zone properties</h2>
<p>A zoned property is equivalent to a collection of multiple properties where
each sub property is accessible by specified Area ID value.</p>
<ul>
<li><code>get</code> call for zoned property always includes the Area ID in
the request, so only the current value for the requested Area ID is returned.
If the property is a global, then Area ID is 0.</li>
<li><code>set</code> call for zoned property always includes the Area ID in the
request, so only the requested Area ID is changed.</li>
<li><code>subscribe</code> call will generate events for all Area IDs for the
property.</li>
</ul>

<h3 id=get>Get calls</h3>
<p>During initialization, the value for the property may not be available yet as
the matching vehicle network message has not yet been received. In such cases,
the <code>get</code> call should return <code>-EAGAIN</code>. Some properties
(such as HVAC) have separate on/off power property. Calling <code>get</code> for
such a property (when powered off) should return a <code>UNAVAILABLE</code>
status rather than returning an error.</p>

<p>Example: get HVAC Temperature</p>
<img src="../images/vehicle_hvac_get.png" alt="Vehicle HAL get HVAC example">
<p class="img-caption"><strong>Figure 1</strong>. Get HVAC temperature (CS =
CarService, VNS = VehicleNetworkService, VHAL = Vehicle HAL)</p>

<h3 id=set>Set calls</h3>
<p>A <code>set</code> call is an asynchronous operation involving event
notification after a requested change is made. In a typical operation, a
<code>set</code> call leads to making a change request across vehicle network.
When the change is performed by the electronic control unit (ECU) owning the
property, the updated value is returned through vehicle network and the vehicle
HAL sends an updated value as an event to vehicle network service (VNS).</p>
<p>Some <code>set</code> calls may require initial data to be ready but during
initialization, such data may not be available yet. In such cases, the
<code>set</code> call should return <code>-EAGAIN</code>. Some properties with
separate power on /off should return <code>-ESHUTDOWN</code> when the property
is powered off and set cannot be done.</p>
<p>Until <code>set</code> is made effective, <code>get</code> does not
necessarily return the same value as what is set.</p>

<p>Example: set HVAC Temperature</p>
<img src="../images/vehicle_hvac_set.png" alt="Vehicle HAL set HVAC example">
<p class="img-caption"><strong>Figure 2</strong>. Set HVAC temperature (CD =
CarService, VNS = VehicleNetworkService, VHAL = Vehicle HAL)</p>

<h2 id=prop_custom>Handling custom properties</h2>
<p>To support partner-specific needs, the vehicle HAL allows custom properties
that are restricted to system apps. Use the following guidelines when working
with custom properties:</p>
<ul>
<li>Property ID should be generated using the following fields:
  <ul>
    <li><code>VehiclePropertyGroup:VENDOR</code>
      <br>The <code>VENDOR</code> group is used only for custom properties.</li>
    <li><code>VehicleArea</code>
      <br>Select an appropriate Area Type.</li>
    <li><code>VehiclePropertyType</code>
      <br>Select the proper data type. BYTES type allows passing raw
        data, so this is enough in most cases. Sending big data frequently
        through custom properties can slow down the whole vehicle network
        access, so be careful when you add a big payload.</li>
    <li>Property ID
      <br>Choose a four nibble ID for the custom property.</li>
  </ul></li>
<li>Access via <code>CarPropertyManager</code> (for Java components) or
via Vehicle Network Service API (for native). Do not modify other car APIs as it
can lead to compatibility issues in the future.</li>
</ul>

<h2 id=prop_hvac>Handling HVAC properties</h2>
<p>
  You can use the vehicle HAL to control HVAC by setting HVAC-related
  properties. Most HVAC properties are zoned properties, but a few are non-zoned
  (global) properties. Example properties defined include:</p>

<ul>
  <li><code>VEHICLE_PROPERTY_HVAC_TEMPERATURE_SET</code>
  <br>Set temperature per zone.</li>
  <li><code>VEHICLE_PROPERTY_HVAC_RECIRC_ON</code>
  <br>Control recirculation per zone).</li>
</ul>

<p>
  For a full list of HVAC properties, search for
  <code>VEHICLE_PROPERTY_HVAC_*</code> in <code>types.hal</code>.
</p>

<p>
  There are additional rules for mapping a zoned HVAC property to Area IDs when
  the HVAC property uses <code>VehicleAreaSeat</code>. Every available seat in
  the car must be part of an Area ID in the Area ID array.
</p>

<p>
  Example 1: A car has two front seats <code>(ROW_1_LEFT, ROW_1_RIGHT)</code>
  and three back seats <code>(ROW_2_LEFT, ROW_2_CENTER, ROW_2_RIGHT)</code>.
  There are two temperature control units: driver side and passenger side.
</p>

<ul>
  <li>A valid mapping set of Area IDs for <code>HVAC_TEMPERATURE SET</code> is:
  <ul>
    <li><code>ROW_1_LEFT | ROW_2_LEFT</code></li>
    <li><code>ROW_1_RIGHT | ROW_2_CENTER | ROW_2_RIGHT</code></li>
    </ul>
  </li>
  <li>An alternative mapping for the same hardware configuration is:
  <ul>
    <li><code>ROW_1_LEFT | ROW_2_LEFT | ROW_2_CENTER</code></li>
    <li><code>ROW_1_RIGHT | ROW_2_RIGHT</code></li>
  </ul>
  </li>
</ul>

<p>
  Example 2: A car has three seat rows with two seats in the front row
  <code>(ROW_1_LEFT, ROW_1_RIGHT)</code> and three seats in the second
  <code>(ROW_2_LEFT, ROW_2_CENTER, ROW_2_RIGHT)</code> and third rows
  <code>(ROW_3_LEFT, ROW_3_CENTER, ROW_3_RIGHT)</code>. There are three
  temperature control units: driver side, passenger side, and rear. A
  reasonable way to map <code>HVAC_TEMPERATURE_SET</code> to Area IDs is a
  three element array:
</p>

<ul>
  <li><code>ROW_1_LEFT</code></li>
  <li><code>ROW_1_RIGHT</code></li>
  <li><code>ROW_2_LEFT | ROW_2_CENTER | ROW_2_RIGHT | ROW_3_LEFT | ROW_3_CENTER
  | ROW_3_RIGHT</code></li>
</ul>

<h2 id=prop_sensor>Handling sensor properties</h2>

<p>
  Vehicle HAL sensor properties represent real sensor data or policy information
  such as driving status. Some sensor information (such as driving status and
  day/night mode) is accessible by any app without restriction as the data is
  mandatory to build a safe vehicle application. Other sensor information (such
  as vehicle speed) is more sensitive and requires specific permissions that
  users can manage.
</p>

<p>
  Supported sensor properties include:
</p>

<ul>
  <li><code>NIGHT_MODE</code>
  <br>Should support. Determines day/night mode of display.</li>
  <li><code>GEAR_SELECTION/CURRENT_GEAR</code>
  <br>Gear selected by driver vs. actual gear.</li>
  <li><code>VEHICLE_SPEED</code>
  <br>Vehicle speed. Protected with permission.</li>
  <li><code>ODOMETER</code>
  <br>Current odometer reading. Protected with permission.
  </li>
  <li><code>FUEL_LEVEL</code>
  <br>Current fuel level in %.</li>
  <li><code>FUEL_LEVEL_LOW</code>
  <br>Fuel level is low or not (boolean).</li>
</ul>

<h2 id=vms>Vehicle Mapping Service (VMS)</h2>

<p>
  The Vehicle Map Service (VMS) provides a mechanism to exchange map data
  between clients through a pub/sub interface to support common vehicle features
  such as advanced driver assistance
  (<a href="https://en.wikipedia.org/wiki/Advanced_driver-assistance_systems" class="external">ADAS</a>).
  Clients may include vehicle systems interfacing through the VMS property in
  the Vehicle HAL or privileged Android applications. Data shared on VMS are
  intended to be limited to map data for use by vehicle systems and supporting
  apps.
</p>

<p>VMS is intended for use only in Android Automotive implementations; AOSP does
  not contain default clients that publish or subscribe to VMS.
</p>

<p>
  For the VMS property in the Vehicle HAL, the message types and data structures
  are described in
  <a href="https://android.googlesource.com/platform/hardware/interfaces/+/master/automotive/vehicle/2.0/types.hal" class="external">Vehicle
  HAL 2.0</a> in the
  <a href="https://android.googlesource.com/platform/hardware/interfaces/+/master/automotive/vehicle/2.0/types.hal#3216" class="external">VmsMessageType</a>
  enum, which lists the types of supported VMS messages. This enum is used as
  the first integer in the vehicle property integers array and determines how
  the rest of the message is decoded.
</p>

<aside class="note">
  <strong>Note:</strong> For details on compatibility requirements for
  Automotive, see
  <a href="/compatibility/android-cdd#2_5_automotive_requirements">Automotive
  Requirements</a>.
</aside>

  </body>
</html>
