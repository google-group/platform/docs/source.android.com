<html devsite>
  <head>
    <title>Automotive Audio</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  {% include "_versions.html" %}
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<p>
  This section details the audio architecture for automotive-related Android
  implementations. OEMs and other Android developers implementing an automotive
  audio system should review all content in this section thoroughly in addition
  to content in the main <a href="/devices/audio/">Audio</a> section.
</p>

<h2 id="key-concepts">Key concepts</h2>

<p>
  Android is responsible for infotainment sounds (i.e. media, navigation, and
  communications) but is not directly responsible for chimes and warnings that
  have strict availability and timing requirements. External sources are
  represented by applications, which are responsible for audio focus. However,
  you cannot rely on focus for sound selection and mixing.
</p>

<p>
  Android {{ androidPVersionNumber }} includes the following changes to
  automotive-related audio support:
</p>

<ul>
  <li>The Audio HAL Context maps to <code>AudioAttributes.usage</code> to
  identify sounds; the Audio HAL implementation is responsible for
  Context-specific mixing/routing.</li>
  <li>Vehicles define a generic output device
  (<code>AUDIO_DEVICE_OUT_BUS</code>) for use in vehicle audio systems; Android
  supports one <code>AUDIO_DEVICE_OUT_BUS</code> per Context.</li>
  <li><code>IAudioControl HAL</code> provides vehicle-specific extensions to the
  Audio HAL; for an example implementation, refer to
  <code>device/generic/car/emulator/audio</code>. Android
  {{ androidPVersionNumber }} does not include <code>AUDIO_* VHAL</code>
  properties.</li>
</ul>

<h2 id="android-sounds-streams">Android sounds and streams</h2>

<p>
  Automotive audio systems handle the following sounds and streams:
</p>

<img src="/devices/automotive/images/audio_streams_all.png">
<figcaption><strong>Figure 1.</strong> Stream-centric architecture diagram
</figcaption>

<p>
  Android is responsible for sounds coming from Android applications,
  controlling those applications and routing their sounds to individual streams
  at the HAL based on the type of sound:
</p>

<ul>
  <li><strong>Logical</strong> streams, known as <em>sources</em> in core audio
  nomenclature, are tagged with <a href="/devices/audio/attributes">Audio
  Attributes</a>.</li>
  <li><strong>Physical</strong> streams, known as <em>devices</em> in core audio
  nomenclature, have no context information after mixing.</li>
</ul>

<p>
  For reliability, external sounds (coming from independent sources such as seat
  belt warning chimes) are managed outside Android, below the HAL or even in
  separate hardware. System implementers must provide a mixer that accepts one
  or more streams of sound input from Android and then combines those streams in
  a suitable way with the external sound sources required by the vehicle.
  External streams can be always on, or controlled via
  <code>createAudioPatch</code> entry points in the HAL.
</p>

<p>
  The HAL implementation and external mixer are responsible for ensuring the
  safety-critical external sounds are heard and for mixing in the
  Android-provided streams and routing them to suitable speakers.
</p>

<h3 id="android-sounds">Android sounds</h3>

<p>
  Applications may have one or more players that interact through the standard
  Android APIs (e.g.
  <a href="https://developer.android.com/reference/android/media/AudioManager.html" class="external">AudioManager</a>
  for focus control or
  <a href="https://developer.android.com/reference/android/media/MediaPlayer.html" class="external">MediaPlayer</a>
  for streaming) to emit one or more logical streams of audio data. This data
  could be single channel mono or 7.1 surround, but is routed and treated as a
  single source. The application stream is associated with
  <a href="/devices/audio/attributes">AudioAttributes</a> that give the system
  hints about how the audio should be expressed.
</p>

<p>
  The logical streams are sent through the <code>AudioService</code> and routed
  to one (and only one) of the available physical output streams, each of which
  is the output of a mixer within <code>AudioFlinger</code>. After
  <code>AudioAttributes</code> have been mixed down to a physical stream, they
  are no longer available.
</p>

<p>
  Each physical stream is then delivered to the Audio HAL for rendering on the
  hardware. In automotive applications, rendering hardware can be local codecs
  (similar to mobile devices) or a remote processor across the vehicle's
  physical network. Either way, it is the job of the Audio HAL implementation to
  deliver the actual sample data and cause it to become audible.
</p>

<h3 id="external-streams">External streams</h3>

<p>
  Sound streams that should not be routed through Android (for certification or
  timing reasons) may be sent directly to the external mixer. In many cases,
  Android doesn't need to know these sounds exist as the external mixer can mix
  them over Android sounds. If a sound needs to be ducked or routed to different
  speakers, the external mixer can do that invisibly to Android.
</p>

<p>
  If external streams are media sources that should interact with the sound
  environment Android is generating (e.g. stop MP3 playback when an external
  tuner is turned on), those external streams should be represented by an
  Android app. Such an app would request audio focus and respond to focus
  notifications by starting/stopping the external source as necessary to fit
  into the Android focus policy. One suggested mechanism to control such
  external devices is <code>AudioManager.createAudioPatch()</code>.
</p>

<h3 id="audio-focus">Audio focus</h3>

<p>
  Before starting a logical stream, an application should request audio focus
  using the same <code>AudioAttributes</code> as it will use for its logical
  stream. While sending such a focus request is recommended, it is not enforced
  by the system. Some applications may explicitly skip sending the request to
  achieve specific behaviors (e.g. to intentionally play sound during a phone
  call).
</p>

<p>
  For this reason, you should consider focus as a way to indirectly control
  and deconflict media playback and not as a primary audio control
  mechanism—the vehicle should not depend on the focus system for operation of
  the audio subsystem. Focus awareness is <strong>not part of the HAL</strong>
  and should <strong>not be used to influence audio routing</strong>.
</p>

<h3 id="output-bus">Output BUS</h3>

<p>
  At the Audio HAL level, the device type <code>AUDIO_DEVICE_OUT_BUS</code>
  provides a generic output device for use in vehicle audio systems. The BUS
  device supports addressable ports (where each port is the end point for a
  physical stream) and is expected to be the only supported output device type
  in a vehicle.
</p>

<p>
  A system implementation can use one BUS port for all Android sounds, in which
  case Android mixes everything together and delivers it as one stream.
  Alternatively, the HAL can provide one BUS port for each Context to allow
  concurrent delivery of any sound type. This makes it possible for the HAL
  implementation to mix or duck the different sounds as desired.</p>

<p>
  The assignment of Contexts to BUS ports is done through the Audio Control
  HAL and creates a many:one relationship between Contexts and BUS ports.
</p>

<h2 id="mic-input">Microphone input</h2>

<p>
  When capturing audio, the Audio HAL receives an <code>openInputStream</code>
  call that includes an <code>AudioSource</code> argument indicating how the
  microphone input should be processed.</p>

<p>
  <code>VOICE_RECOGNITION</code> (specifically the Google Assistant) expects a
  stereo microphone stream that has an echo cancellation effect (if available)
  but no other processing applied to it. Beamforming is expected to be done by
  the Assistant itself.
</p>

<h3 id="multi-channel-mic-input">Multi-channel microphone input</h3>

<p>
  To capture audio from a device with more than two channels (stereo), use a
  channel index mask instead of positional index mask (such as
  <code>CHANNEL_IN_LEFT</code>). Example:
</p>

<pre class="prettyprint">
final AudioFormat audioFormat = new AudioFormat.Builder()
    .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
    .setSampleRate(44100)
    .setChannelIndexMask(0xf /* 4 channels, 0..3 */)
    .build();
final AudioRecord audioRecord = new AudioRecord.Builder()
    .setAudioFormat(audioFormat)
    .build();
audioRecord.setPreferredDevice(someAudioDeviceInfo);
</pre>

<p>
  When both <code>setChannelMask</code> and <code>setChannelIndexMask</code>
  are set, <code>AudioRecord</code> uses only the value set by
  <code>setChannelMask</code> (maximum of two channels).
</p>

<h3 id="concurrent-capture">Concurrent capture</h3>

<p>
  The Android framework does not allow concurrent capture for most input audio
  device types but makes exceptions for <code>AUDIO_DEVICE_IN_BUS</code> and
  <code>AUDIO_DEVICE_IN_FM_TUNER</code> by handling them as virtual devices.
  Doing so means the framework assumes no competition for resources exists
  between/among these devices and thus any/all of them are allowed to be
  captured concurrently along with one regular input device (such as the
  microphone). If hardware constraints on concurrent capture do exist
  between/among these devices, such constraints must be handled by custom
  application logic in the first party applications designed to use these input
  devices.
</p>

<p>
  Applications designed to work with <code>AUDIO_DEVICE_IN_BUS</code> devices or
  with secondary <code>AUDIO_DEVICE_IN_FM_TUNER</code> devices must rely on
  explicitly identifying those devices and using
  <code>AudioRecord.setPreferredDevice()</code> to bypass the Android default
  source selection logic.
</p>

<h2 id="volume-and-groups">Volume and volume groups</h2>

<p>
  Android 8.x and lower supports three volume groups (ring, media, and alarm)
  along with a hidden group for phone in-call. Each group can be set to a
  different volume level based on the output device, such as higher volumes for
  speakers and lower volumes for headsets).
</p>

<p>
  Android {{ androidPVersionNumber }} adds a <em>speech</em> volume group and
  the automotive-related contexts as shown below:
</p>

<table>
<thead>
<tr>
<th>Volume group</th>
<th>Audio contexts</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>Ring</td>
<td>CALL_RING_CONTEXT</td>
<td>Voice call ringing</td>
</tr>
<tr>
<td></td>
<td>NOTIFICATION_CONTEXT</td>
<td>Notifications</td>
</tr>
<tr>
<td></td>
<td>ALARM_CONTEXT</td>
<td>Alarm sound from Android</td>
</tr>
<tr>
<td></td>
<td>SYSTEM_SOUND_CONTEXT</td>
<td>System sound from Android</td>
</tr>
<tr>
<td>Media</td>
<td>MUSIC_CONTEXT</td>
<td>Music playback</td>
</tr>
<tr>
<td>Phone</td>
<td>CALL_CONTEXT</td>
<td>Voice call</td>
</tr>
<tr>
<td>Speech</td>
<td>NAVIGATION_CONTEXT</td>
<td>Navigation directions</td>
</tr>
<tr>
<td></td>
<td>VOICE_COMMAND_CONTEXT</td>
<td>Voice command session</td>
</tr>
</tbody>
</table>

<p>
  When the value for a volume group is updated, the framework's
  <code>CarAudioService</code> handles setting the affected physical stream
  gains. Physical stream volume in a vehicle is based on volume group (rather
  than stream_type) and each volume group consists of one or more Audio
  Contexts. Each <code>AudioAttributes.USAGE</code> maps to an Audio Context in
  a <code>CarAudioService</code> and can be configured to be routed to an output
  bus (see
  <a href="/devices/automotive/audio/audio-control.html#configure-volume">Configuring
  volume</a> and
  <a href="/devices/automotive/audio/audio-control.html#configure-volume-groups">Configuring
  volume groups</a>).
</p>

<p>
  Android {{ androidPVersionNumber }} simplifies controlling the hardware volume
in the amplifier:
</p>

<ul>
  <li>Each volume group is routed to one or more output buses. The volume for a
  specific group can be changed using the Car Settings UI or via an
  externally-generated
  <code>KEYCODE_VOLUME_DOWN</code> or <code>KEYCODE_VOLUME_UP</code> key event.
  </li>
  <li>In response, <code>CarAudioService</code> calls
  <code>AudioManager.setAudioPortGain()</code> with the audio device port(s)
  bound to targeted volume group. At the HAL, this appears as a series of one or
  more calls to <code>IDevice.setAudioPortConfig()</code> with the volume gain
  value for each physical output stream associated with the targeted volume
  group.</li>
</ul>

<p>
  You can configure the maximum, minimum, and step gain value for each audio
  device port in <code>audio_policy_configuration.xml</code>. For a sample
  configuration and details on overriding the default set of volume groups, see
  <a href="/devices/automotive/audio/audio-hal.html#configure-audio-devices">Configuring
  audio devices.
</p>

</body>
</html>