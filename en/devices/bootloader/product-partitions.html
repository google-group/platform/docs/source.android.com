<html devsite>
<head>
  <title>Building Product Partitions</title>
  <meta name="project_path" value="/_project.yaml">
  <meta name="book_path" value="/_book.yaml">
</head>
{% include "_versions.html" %}
<body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  Android {{ androidPVersionNumber }}​ includes support for building
  <code>/product</code> partitions using the Android build system. Previously,
  Android 8.x enforced the separation of System-on-Chip (SoC)-specific
  components from the <code>/system</code> partition to the <code>/vendor</code>
  partition without dedicating space for OEM-specific components built from
  Android build system.
</p>

<h2 id="product-partitions">About product partitions</h2>

<p>
  Many OEMs customize the AOSP system image to implement their own features
  and requirements from carriers. However, such customizations make it
  impossible to use a single system image for multiple software SKUs as each
  image must be different to support a different locale, carrier, etc. Using a
  separate <code>/product</code> partition to contain customizations makes it
  possible to use a single system image for multiple software SKUs (the
  <code>/system</code> partition hosts generic code that can be shared among
  many software SKUs). The <code>/vendor</code> partition continues to host
  SoC-specific Board-Specific (BSP) code that can be shared among multiple
  devices based on the given SoC.
</p>

<p>
  Using separate partitions has some disadvantages, such as difficulties in
  managing disk space (a limited amount of space should be reserved for future
  growth) and in <a href="/devices/architecture/vndk/abi-stability">maintaining
  stable Application Binary Interface (ABI)</a> between partitions. Before
  deciding to use <code>/product</code> partitions, take time to consider your
  unique AOSP implementation and possible mitigation tactics (such as
  repartitioning a device during an <a href="/devices/tech/ota/">Over-the-Air
  (OTA) update</a>, which is not done by Google but is done by some OEMs).
</p>

<h3 id="legacy-oem">Legacy /oem vs /product</h3>

<p>
  The new <code>/product</code> partition is incompatible with the legacy
  <code>/oem</code> partition:
</p>

<table>
  <tr>
  <th>Partition</th>
  <th>Attributes</th>
  </tr>
  <tr>
  <td><code>/oem</code></td>
  <td>
  <ul>
    <li>Not updateable; usually flashed once at factory.</li>
    <li>Built per small variations such as branding and color. Different
    <code>/oem</code> partition contents do not make them different product
    software.</li>
    <li>System partition does not depend on <code>/oem</code> (it uses the
    partition only when a specific file is found there).</li>
    <li>Does not use API on system other than public API.</li>
  </ul>
  </td>
  </tr>
  <tr>
  <td><code>/product</code></td>
  <td>
   <ul>
    <li>Updateable</li>
    <li>Coupled with the system image (they update together)</li>
    <li>Built per product or product families.</li>
    <li>System partition can depend on <code>/product</code> partition.</li>
    <li>Can use non-public APIs since they are updated simultaneously.</li>
  </ul>
  </td>
  </tr>
  </table>

<p>
  For these reasons, Android {{ androidPVersionNumber }} supports the new
  <code>/product</code> partition and also retains support for the legacy
  <code>/oem</code> partition for devices that depend on it.
</p>

<h3 id="product-components">/product components</h3>

<p>
  The <code>/product</code> partition contains the following components:
</p>

<ul>
  <li>Product-specific system properties (<code>/product/build.prop</code>)</li>
  <li>Product-specific RROs (<code>/product/overlay/*.apk</code>)</li>
  <li>Product-specific apps (<code>/product/app/*.apk</code>)</li>
  <li>Product-specific priv-apps (<code>/product/priv-app/*.apk</code>)</li>
  <li>Product-specific libraries (<code>/product/lib/*</code>)</li>
  <li>Product-specific java libraries (<code>/product/framework/*.jar</code>)
  </li>
  <li>Product-specific Android Framework system configs
  (<code>/product/etc/sysconfig/*</code> and
  <code>/product/etc/permissions/*</code>)</li>
  <li>Product-specific media files (<code>/product/media/audio/*</code>)</li>
  <li>Product-specific <code>bootanimation</code> files</li>
</ul>

<h3 id="custom-images">No custom_images</h3>

<p>
  You cannot use <code>custom_images</code> as they lack support for the
  following:
</p>

<ul>
  <li><strong>Installing modules into a specific target</strong>.
  <code>custom_images</code> support copying artifacts into an image but
  cannot install a module into a specific partition by specifying its target
  partition as a part of a build rule.</li>
  <li><strong>Soong support</strong>. <code>custom_images</code> cannot be
  built using the Soong build system.</li>
  <li>OTA update support</a>. <code>custom_images</code> are used as factory ROM
  images that cannot be OTA-ed.</li>
</ul>

<h3 id="maintaining-abis">Maintaining ABIs between partitions</h3>

<p>
  The <code>/product</code> partition in Android {{ androidPVersionNumber }}
  is an extension of the <code>/system</code> partition. As there is weak ABI
  between <code>/product</code> and <code>/system</code> partitions, both must
  be upgraded at the same time and the ABI should be System SDK-based. If the
  System SDK doesn't cover all API surfaces between <code>/product</code> and
  <code>/system</code>, it is up to the OEM to maintain their own ABI between
  the two partitions.
</p>

<p>The <code>/product</code> and <code>/system</code> partitions can have
  dependency on each other. However, tests with the
  <a href="/setup/build/gsi">Generic System Image (GSI)</a> must work properly
  without the <code>/product</code> partition.
</p>

<p>The <code>/product</code> partition must not have any dependency on
  <code>/vendor</code> partition and no direct interaction between the
  <code>/product</code> partition and the <code>/vendor</code> is permitted
  (enforced by SEpolicy).
</p>

<h2 id="implementing-product-partitions">Implementing product partitions</h2>

<p>
  Before implementing a new product partition, review the
  <a href="https://android-review.googlesource.com/q/topic:product_partition+(status:open+OR+status:merged)" class="external">related
  product partition changes in AOSP</a>. Then, to set up <code>/product</code>,
  include the following board or product build flags.
</p>

<ul>
  <li><code>BOARD_USES_PRODUCTIMAGE</code></li>
  <li><code>BOARD_PRODUCTIMAGE_PARTITION_SIZE</code></li>
  <li><code>BOARD_PRODUCTIMAGE_FILE_SYSTEM_TYPE</code></li>
  <li><code>PRODUCT_PRODUCT_PROPERTIES for /product/build.prop</code>.
  Should be within a <code>$(call inherit-product path/to/device.mk)</code>,
  e.g., <code>PRODUCT_PRODUCT_PROPERTIES += product.abc=ok</code>.
   </li>
 </ul>

<h3 id="enabling-verified-boot">Enabling verified boot</h3>

<p>
  To prevent the <code>/product</code> partition from being tampered with by
  malicious software, you should enable
  <a href="https://android.googlesource.com/platform/external/avb/" class="external">Android
  Verified Boot (AVB)</a> for that partition (just as you do for
  <code>/vendor</code> and <code>/system</code> partitions). To enable AVB, 
  include the following build flags:
  <code>BOARD_AVB_PRODUCT_ADD_HASHTREE_FOOTER_ARGS</code>
</p>

</body>
</html>
