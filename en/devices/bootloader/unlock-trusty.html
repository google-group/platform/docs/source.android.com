<html devsite>
  <head>
    <title>Using the Bootloader</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<h2 id="unlock">Unlocking and Trusty</h2>
<h3 id="recommendations">Recommendations</h3>
<p>
All Google-branded devices should be made unlockable so that all partitions
listed above can be reflashed.  This unlocked mode is set with <code>fastboot
flashing unlock</code>, and once set this mode should persist across reboots.
</p>
<p>
Devices should deny the <code>fastboot flashing unlock</code> command
unless<code> fastboot flashing get_unlock_ability </code>is: "1" If
<code>get_unlock_ability</code> is "0" the user needs to boot to the home
screen, go into the <em>Settings > System > <a
href="https://developer.android.com/studio/debug/dev-options.html">Developer
options</a></em> menu and enable the <strong>OEM unlocking</strong> option to
set <code>unlock_ability</code> to: "1" That flag should be persistent across
reboots and across factory data resets.
</p>
<p>
When the <code>fastboot flashing unlock</code> command is sent, the device should
prompt users to warn them that they may encounter problems with unofficial
images. After acknowledging, a factory data reset should be done to prevent
unauthorized data access. The bootloader should reset the device even if it is not
able to reformat it properly. Only after reset can the persistent flag be set so
that the device can be reflashed.
</p>
<p>
The <code>fastboot flashing lock</code> command relocks and resets the device so
that future flash/unlock attempts require another data reset.
</p>
<p>
All RAM not already overwritten should be reset during the <code>fastboot flashing
unlock</code> process. This measure prevents attacks that read leftover RAM
contents from the previous boot. Similarly, unlocked devices should clear RAM at
every boot if this does not create an unacceptable delay, but should leave the
region used for the kernel's <a
href="https://www.kernel.org/doc/html/v4.12/admin-guide/ramoops.html"
class="external"><code>ramoops</code></a>.
</p>
<p>
Devices intended for retail should be shipped in the locked state (and with
<code>get_unlock_ability</code> returning "0"). This is to ensure an attacker
cannot compromise the device by installing their own system or boot image.
</p>
<h3 id="properties">Properties</h3>
<p>
The<code> ro.oem_unlock_supported</code> property should be set at build time
based on whether the device supports flashing unlock.
<code>ro.oem_unlock_supported</code> should be set to "0" if flashing unlock is
not supported on the device or "1" if flashing unlock is supported.
</p>
<p>
If the device supports flashing unlock (i.e. <code>ro.oem_unlock_supported =
1</code>), then the bootloader should indicate the lock status by setting the
kernel command line variable <code>androidboot.flash.locked</code> (or the
<code>/firmware/android/flash.locked</code> DT property) to "1" if locked or "0"
if unlocked.
</p>
<p>
<strong>Note:</strong> For devices that support <a
href="/security/verifiedboot/dm-verity">dm-verity</a>,
you can instead use <code>ro.boot.verifiedbootstate</code> to set the value of
<code>ro.boot.flash.locked</code> where the value is "0" i.e. unlocked if the
verified boot state is orange.
</p>
<h3 id="flashing-lock-unlock_critical">Flashing lock/unlock_critical</h3>
<p>
The device should support locking and unlocking of critical sections. Those
critical sections are defined as whatever is needed to boot the device into the
bootloader. This might include fuses, virtual partitions for a sensor hub,
first-stage bootloader, and more.
</p>
<p>
Locking of critical sections is defined as preventing any code (kernel, recovery
image, OTA code, etc.) running on the device from deliberately modifying any
critical section. This implies that OTAs should fail to update critical sections
if the device is in lock critical state. Transitioning from locked to unlocked
state should require a physical interaction with the device.
</p>
<p>
The physical interaction is similar to what <code>fastboot flashing
unlock</code> would cause: the user would have to press some physical buttons on the
device. The design should not allow programmatically transitioning from
<code>lock critical</code> to <code>unlock critical </code>without physical
interaction. Devices should ship in the <code>unlock critical</code> state.
</p>
<h2 id="designation-of-critical-partitions-data">Designation of critical
partitions/data</h2>
<p>
Any partitions or data needed for the device to run, need to be either:
</p><ul>
<li>Re-flashable - either re-buildable, provided, or extractable via some
<code>fastboot oem</code> command
<li>fully-protected (i.e. considered critical per the previous
section)</li></ul>
<p>
This includes per-device factory-specific settings, serial numbers, calibration
data, etc.
</p>
<h2 id="off-mode-charging">Off-mode charging</h2>
<p>
If a device supports "off-mode charging" or otherwise autoboots into a
special mode when power is applied, <code>fastboot oem off-mode-charge 0</code>
should bypass these special modes and boot as if the user had pressed the power
button.
</p>
<h2 id="bootloader-for-trusty">Bootloader for Trusty</h2>
<p>
<a href="/security/trusty/">Trusty</a> is Google's
implementation of a Trusted Execution Environment (TEE) OS that runs alongside
Android. This is the specification for devices using ARM Trustzone<sup>TM
</sup>technology to provide a TEE.
</p>
<p>
If Trusty is used as the secure OS solution on your ARM device, the bootloader
should be implemented as described within the following sections.
</p>
<h3 id="initialization">Initialization</h3>
<p>
In order to load and initialize the Trusty OS (TOS), a bootloader should:
</p><ul>
<li>Set up and configure all available RAM
<li>Initialize at least one serial port
<li>Verify signature of TOS image
<li>Load TOS into RAM (execution from flash or TCM is not supported)
<li>Jump to the first instruction in the TOS image after setting up the state
and registers as described in the next section</li></ul>
<h3 id="calling-into-tos-image">Calling into TOS image</h3>
<p>
The following state should be configured at entry:
</p><ul>
<li>MMU turned off
<li>Data cache flushed and turned off (instruction cache can be on or off)
<li>All interrupts (IRQs and FIQs) disabled
<li>CPU in SVC mode on ARM v7 and EL3 on ARM v8
<li>Registers in the following state: <ul>
 <li>r0/x0: size of memory allocated to TOS.
 <li>r1/x1: physical address of a contiguous block of memory that contains
platform-specific boot parameters. The layout of this block is platform
specific.
 <li>r2/x2: size of the above block of memory.
 <li>r14/x30: return address to jump to (in non-secure mode) after TOS
initializes.</li> </ul>
</li> </ul>
<p>
<strong>Note: </strong>r0-r3/x0-x3 also serve as scratch registers to TOS. Do
not expect their values to be preserved upon return.
</p>
<p>
On a 64-bit platform:
</p><ul>
<li>Only w0-w2 are used for parameters, so x0-x2 should contain only 32-bit
values.
<li>x30 can contain a 64-bit value.
<li>The value in x0 when added to the base address of TOS entry-point should
result in a 32-bit value. The same applies to the size in register x2 when added
to the address of boot parameter block in x1.</li></ul>
<h3 id="return-from-tos">Return from TOS</h3>
<p>
TOS will return to the bootloader in non-secure mode (SCR.NS set to "1") when it
is done initializing so that the bootloader may continue loading the primary
operating system (e.g., Android).
</p>
</body>
</html>
