<html devsite>
  <head>
    <title>Android Common Kernels</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<p>The
<a href="https://android.googlesource.com/kernel/common/" class="external">AOSP
common kernels</a> are downstream of Long Term Supported (LTS) kernels and
include patches of interest to the Android community that have not been merged
into LTS. These patches can include:</p>

<ul>
<li>Features tailored for Android needs (e.g. interactive <code>cpufreq</code>
governor).</li>
<li>Features rejected by upstream due to implementation concerns (e.g. MTP/PTP,
paranoid networking).</li>
<li>Features ready for Android devices but still under development upstream
(e.g. Energy Aware Scheduling/EAS).</li>
<li>Vendor/OEM features that are useful for others (e.g. <code>sdcardfs</code>).
</li>
</ul>

<h2 id="list-of-kernels">List of common kernels</h2>
<p>To view a list of Android common kernels, refer to
<a href="https://android.googlesource.com/kernel/common/" class=external>https://android.googlesource.com/kernel/common/</a>
(shown below).</p>
<p><img src="../images/android-diffs.png"></p>
<p class="img-caption"><strong>Figure 1.</strong> List of Android common
kernels</p>

<h3 id="differences-lts">Differences from LTS</h3>
<p>When compared to LTS (4.14.0), the Android common kernel has 355 changes,
32266 insertions, and 1546 deletions (as of February 2018).</p>

<p><img src="../images/kernel_lts_diff.png"></p>
<p class="img-caption"><strong>Figure 2.</strong> Android-specific code over
time</p>

<p>The largest features include:</p>
<ul>
<li>19.8% Energy Aware Scheduling (kernel/sched)</li>
<li>13.8% Networking (net/netfilter)</li>
<li>13.5% Sdcardfs (fs/sdcardfs)</li>
<li>9.4% USB (drivers/usb)</li>
<li>7.2% SoC (arch/arm64, arch/x86)</li>
<li>6.2% f2fs (fs/f2fs -- backports from upstream)</li>
<li>6.1% Input (drivers/input/misc)</li>
<li>5.4% FIQ Debugger (drivers/staging/android/fiq_debugger)</li>
<li>3.6% Goldfish Emulator (drivers/platform/goldfish)</li>
<li>3.4% Verity (drivers/md)</li>
<li>11.6% Other</li>
</ul>

<h2 id="requirements">Requirements</h2>
<p>All AOSP common kernels must provide the following:</p>
<ul>
<li>Method for downstream partners to get timely updates that include all
LTS patches.</li>
<li>Mechanism to guarantee that new feature development does not interfere with
merging from AOSP common (even for previous Android releases).</li>
<li>Method for downstream partners to easily identify security patches that are
part of an <a href="/security/bulletin/">Android Security Bulletin (ASB)</a>.
This satisfies carriers who require a full requalification if OEMs attempt to
include patches beyond those listed in the bulletin.</li>
</ul>
<p>In addition, regular testing must be performed on AOSP common kernels and
branches must be tagged when passing.</p>

<h3 id="lts-merges">LTS merges</h3>
<p>To ensure downstream partners can get timely updates that include all LTS
patches, android-<var>X</var>.<var>Y</var> gets regular merges from LTS and is
validated via automated VTS, CTS, and build/boot tests.</p>

<h3 id="android-dessert-branches">Android-dessert branches</h3>
<p>To guarantee that new feature development does not interfere with merging
from the AOSP common kernel (even for previous Android releases),
android-<var>X</var>.<var>Y</var>-<var>androidRel</var> is cloned from
android-<var>X</var>.<var>Y</var> prior to the initial dessert release, gets regular
merges from LTS, and is tested against the associated Android release. For
example, the android-4.4-n branch gets merges from the LTS 4.4.y branch. </p>

<h3 id="android-release-branches">Android-release branches</h3>
<p>To ensure downstream partners can easily identify security patches that are
part of an ASB,
android-<var>X</var>.<var>Y</var>-<var>androidRel</var>-<var>type</var> is
cloned from android-<var>X</var>.<var>Y</var>-<var>androidRel</var> at the time
of the Android release and gets only the patches listed in the bulletin.</p>

<p>After the patches associated with a bulletin are confirmed to be merged
into a release branch, the branch is tagged with the ASB level. For example, the
tag <strong>ASB-2017-10-05</strong> indicates the release branch contains
patches from the Android Security Bulletin for October 5th, 2017. Parent
branches contain those security patches, so if the android-4.4-o-release branch
is tagged with <strong>ASB-2017-10-01</strong>, android-4.4-o and android-4.4
are also up-to-date with that bulletin. Example:</p>
<ul>
<li>Before releasing Android N MR1, <strong>android-4.4-n-mr1</strong> is cloned
from <strong>android-4.4-n</strong>.</li>
<li>Only patches listed in ASBs are merged, allowing OEMs (who have strict
requirements from carriers to avoid full qualification on security updates) to
find the patches listed in the bulletin.</li>
<li><strong>android-4.4-n-mr2</strong> will be
<strong>android-4.4-n-mr1</strong> plus LTS patches that were merged between the
releases.</li>
<li>Each month when the ASB is released publicly, the release branches are
updated with any patches cited in the bulletin that are upstream
(device-specific patches cited in the bulletin are not applied to the common
kernels).</li>
</ul>

<h3 id="regular-testing">Regular testing</h3>
<p>Regular testing is performed on all on AOSP common kernels and test results
are available to the public. Specifically:</p>
<ul>
<li>After LTS updates or other patches are merged, VTS and a subset of CTS
is run and results are made available at
<a href="https://qa-reports.linaro.org/lkft" class="external">https://qa-reports.linaro.org/lkft</a>.
</li>
<li>To continually test for build/boot breaks in a variety of architectures and
builds, <code>kernelci</code> is run and results are made available at
<a href="https://kernelci.org/job/android/" class="external">https://kernelci.org/job/android</a>.
</li>
</ul>

<h3 id="branch-hierarchy">Branch hierarchy (android-4.4)</h3>
<p>The branch hierarchy for the android-4.4 kernel uses the following structure:
</p>

<p><img src="../images/kernel_branch_hierarchy_44.png"></p>
<p class="img-caption"><strong>Figure 3.</strong> Branch hierarchy for the
android-4.4 kernel.</p>

<h2 id="guidelines">Guidelines</h2>
<p>Android implementations should use the following kernel guidelines:</p>
<ul>
<li>Use the new AOSP common kernels as upstream merge sources.<ul>
<li>To get patches from LTS, merge from android-<var>X</var>.<var>Y</var>.<ul>
<li>Merge regularly during development phase.</li>
<li>When updating device to a new Android release, merge either from the
android-<var>X</var>.<var>Y</var> branch or the release branch for the target
release (e.g. for an update to Nougat MR2, merge from the android-4.4-n-mr2
branch).</li>
</ul>
<li>When constrained by the carrier for a security release, merge from release
branches for security updates.</li>
</ul>
<li>Send fixes upstream to mainline, LTS, or AOSP common.</li>
</ul>

  </body>
</html>
