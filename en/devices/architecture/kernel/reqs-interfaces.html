<html devsite>
  <head>
    <title>Interface Requirements</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  {% include "_versions.html" %}
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>This page describes a subset of the Linux kernel interfaces on which Android
relies to function properly. The presence and correctness of these interfaces is
tested as part of the <a href="/devices/tech/vts/index.html">Vendor Test Suite
(VTS)</a>. This subset will grow over time to contain a larger portion of
Android kernel interfaces.</p>

<h2 id="system-calls">System calls</h2>
<p>All system calls are expected to provide the same signatures and semantics as
in the upstream Linux kernel of the same version.</p>

<h3 id="prctl">prctl</h3>
<p>In addition to the upstream <code>prctl</code> operations for supported
kernel versions, Android relies on additional <code>prctl</code> operations, the
implementation of which can be found in the android-common kernel.</p>

<pre class="prettyprint">
PR_SET_TIMERSLACK_PID
PR_SET_VMA
</pre>

<h2 id="filesystems">Filesystems</h2>
<p>The Linux kernel exports interfaces via several filesystems. Android expects
these interfaces to communicate the same information, in the same format, and
provide the same semantics as in the upstream Linux kernel. For interfaces that
do not exist upstream, the appropriate behavior is dictated by the corresponding
branch of the Android common kernel.</p>

<h3 id="procfs">procfs</h3>
<table>
  <tr>
   <th>Interface</th>
   <th>Description</th>
  </tr>
  <tr>
  <td>/proc/asound/</td>
  <td>Read-only file showing the list of currently configured ALSA drivers.</td>
  </tr>
  <tr>
   <td>/proc/cmdline</td>
   <td>Read-only file containing command line arguments passed to the kernel.
   </td>
  </tr>
  <tr>
   <td>/proc/config.gz</td>
   <td>Read-only file containing kernel build configuration.</td>
  </tr>
  <tr>
   <td>/proc/cpuinfo</td>
   <td>Read-only file containing architecture-specific CPU details.</td>
  </tr>
  <tr>
    <td>/proc/diskstats</td>
    <td>Read-only file showing I/O statistics of block devices.</td>
  </tr>
  <tr>
    <td>/proc/filesystems</td>
    <td>Read-only file listing filesystems currently supported by the
    kernel.</td>
  </tr>
  <tr>
  <tr>
   <td>/proc/kmsg</td>
   <td>Read-only file showing kernel messages in real time.</td>
  </tr>
  <tr>
    <td>/proc/loadavg</td>
    <td>Read-only file showing CPU and IO load average over time.</td>
  </tr>
  <tr>
   <td>/proc/meminfo</td>
   <td>Read-only file showing memory subsystem details.</td>
  </tr>
  <tr>
    <td>/proc/misc</td>
    <td>Read-only file listing miscellaneous drivers registered on the
    miscellaneous major device.</td>
  </tr>
  <tr>
   <td>/proc/modules</td>
   <td>Read-only file containing information about loaded kernel modules.</td>
  </tr>
  <tr>
   <td>/proc/mounts</td>
 <td>Symlink to <code>/proc/self/mounts</code>, which is a read-only file
 listing information about the mounted filesystems.</td>
  </tr>
  <tr>
    <td>/proc/net</td>
    <td>Directory containing a variety of network stack parameters.</td>
  </tr>
  <tr>
   <td>/proc/net/xt_qtaguid/</td>
   <td>Read-write file providing information about tagged sockets.</td>
  </tr>
  <tr>
    <td>/proc/pagetypeinfo</td>
    <td>Read-only file containing page allocator information.</td>
  </tr>
  <tr>
   <td>/proc/stat</td>
   <td>Read-only file containing various kernel and system statistics.</td>
  </tr>
  <tr>
    <td>/proc/swaps</td>
    <td>Read-only file showing swap space utilization. <strong>This file is
    optional; its contents and permissions will be verified in VTS only if the
    file is present</strong>.</td>
  </tr>
  <tr>
    <td>/proc/sys/abi/swp</td>
    <td>Read-write file which determines the behavior of the obsoleted ARM
    instruction SWP.</td>
  </tr>
  <tr>
    <td>/proc/sys/fs/pipe-max-size</td>
    <td>Read-write file that reports the maximum size, in bytes, of an
    individual pipe buffer.</td>
  </tr>
  <tr>
    <td>/proc/sys/fs/protected_hardlinks</td>
    <td>Read-write file that controls the behavior of creation of hard links.
    </td>
  </tr>
  <tr>
    <td>/proc/sys/fs/protected_symlinks</td>
    <td>Read-write file that controls the behavior of creation of symbolic
    links.</td>
  </tr>
  <tr>
    <td>/proc/sys/fs/suid_dumpable</td>
    <td>Read-write file that controls the core dump mode for setuid or otherwise
    protected/tainted binaries.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/core_pattern</td>
    <td>Read-write file that specifies the core dump filename pattern.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/core_pipe_limit</td>
    <td>Read-write file that defines how many concurrent crashing processes may
    be piped to user applications in parallel.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/dmesg_restrict</td>
    <td>Read-write file that controls whether unprivileged users may access
    dmesg.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/domainname</td>
    <td>Read-write file that contains the YP/NIS domain name of the system.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/hostname</td>
    <td>Read-write file that determines the host name of the system.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/hung_task_timeout_secs</td>
    <td>Read-write file that controls the timeout used to determine when a task
    has become non-responsive and should be considered hung. <strong>This file
    is optional; its contents and permissions will be verified in VTS only if
    the file is present</strong>.</td>
  </tr>
  <tr>
   <td>/proc/sys/kernel/kptr_restrict</td>
   <td>Read-write file that determines whether kernel pointers are printed in
   <code>proc</code> files and other interfaces.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/modules_disabled</td>
    <td>Read-write file that controls whether kernel modules may be loaded.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/panic_on_oops</td>
    <td>Read-write file that controls the kernel's behavior on oops.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/perf_event_max_sample_rate</td>
    <td>Read-write file that controls the maximum sample rate of performance
    events.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/perf_event_paranoid</td>
    <td>Read-write file that controls the usage of the performance events system
    by unprivileged users.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/pid_max</td>
    <td>Read-write file that contains the PID allocation wrap value.</td>
  </tr>
  <tr>
    <td>/proc/sys/kernel/random/boot_id</td>
    <td>Read-only file that contains a new random ID on each boot.</td>
  </tr>
  <tr>
   <td>/proc/sys/kernel/randomize_va_space</td>
   <td>Read-write file that determines the address layout randomization policy
   for the system.</td>
  </tr>
  <tr>
  <td>/proc/sys/kernel/sched_child_runs_first</td>
  <td>Read-write file that controls whether newly forked tasks are favored in
  scheduling over their parent tasks.</td>
  </tr>
  <tr>
  <td>/proc/sys/kernel/sched_latency_ns</td>
  <td>Read-write file that contains the maximum latency, in nanoseconds, a task
  may incur prior to being scheduled.</td>
  </tr>
  <tr>
  <td>/proc/sys/kernel/sched_rt_period_us</td>
  <td>Read-write file that contains the period length used by the system-wide RT
  execution limit in microseconds.</td>
  </tr>
  <tr>
  <td>/proc/sys/kernel/sched_rt_runtime_us</td>
  <td>Read-write file that contains the amount of time, relative to
  sched_rt_period_us, that the system may execute RT tasks.</td>
  </tr>
  <tr>
  <td>/proc/sys/kernel/sched_tunable_scaling</td>
  <td>Read-write file that controls whether sched_latency_ns should be
  automatically adjusted by the scheduler based on the number of CPUs.</td>
  </tr>
  <tr>
  <td>/proc/sys/kernel/sched_wakeup_granularity_ns</td>
  <td>Read-write file that contains how much more virtual runtime task A must
  have than task B in nanoseconds for task B to preempt it.</td>
  </tr>
  <tr>
  <td>/proc/sys/kernel/sysrq</td>
  <td>Read-write file that controls the functions allowed to be invoked via the
  sysrq key.</td>
  </tr>
  <tr>
  <td>/proc/sys/net/*</td>
  <td>Directory containing a variety of network stack parameters.</td>
  </tr>
  <tr>
  <td>/proc/sysrq-trigger</td>
  <td>Write-only file that can be used to initiate sysrq functions.</td>
  </tr>
  <tr>
  <td>/proc/sys/vm/dirty_background_ratio</td>
  <td>Read-write file that contains, as a percentage of total available memory
  (free pages and reclaimable pages), the number of pages at which the
  background kernel flusher threads will start writing out dirty data.</td>
  </tr>
  <tr>
  <td>/proc/sys/vm/dirty_expire_centisecs</td>
  <td>Read-write file that defines when dirty data is old enough to be eligible
  for write out by the kernel flusher threads.</td>
  </tr>
  <tr>
  <td>/proc/sys/vm/drop_caches</td>
  <td>Read-write file that can be used to force the kernel to drop clean
  caches.</td>
  </tr>
  <tr>
  <td>/proc/sys/vm/extra_free_kbytes</td>
  <td>Read-write file that can be used to keep extra free memory between the
  threshold where background reclaim (kswapd) kicks in, and the threshold
  where direct reclaim (by allocating processes) kicks in. <strong>This file
  is optional; its contents and permissions will be verified in VTS only if
  the file is present</strong>.</td>
  </tr>
  <tr>
  <td>/proc/sys/vm/max_map_count</td>
  <td>Read-write file that contains the maximum number of memory map areas a
  process may have.</td>
  </tr>

  <tr>
   <td>/proc/sys/vm/mmap_min_addr</td>
   <td>Read-write file that determines the minimum address than can be
   <code>mmap</code>'d.</td>
  </tr>
  <tr>
   <td>/proc/sys/vm/mmap_rnd_bits</td>
   <td>Read-write file that specifies the amount of randomness in
   <code>mmap</code>'d addresses.</td>
  </tr>
  <tr>
   <td>/proc/sys/vm/mmap_rnd_compat_bits</td>
   <td>Read-write file that specifies the amount of randomness in
   <code>mmap</code>'d addresses.</td>
  </tr>
  <tr>
   <td>/proc/sys/vm/overcommit_memory</td>
   <td>Read-write file that determines the kernel virtual memory accounting
   mode.</td>
  </tr>
  <tr>
    <td>/proc/sys/vm/page-cluster</td>
    <td>Read-write file that controls the number of pages up to which
    consecutive pages are read in from swap in a single attempt.</td>
  </tr>
  <tr>
   <td>/proc/uid_cputime/remove_uid_range</td>
   <td>Write-only file that, when written, removes UIDs from being shown in
   <code>/proc/uid_cputime/show_uid_stat</code>.</td>
  </tr>
  <tr>
   <td>/proc/uid_cputime/show_uid_stat</td>
   <td>Read-only file containing the time a UID's processes spent in user and
   kernel space.</td>
  </tr>
  <tr>
  <td>/proc/uid_io/stats</td>
  <td>Read-only file containing a list of I/O stats for each UID in the
  system</td>
  </tr>
  <tr>
  <td>/proc/uid_procstat/set</td>
  <td>Write-only file used to configure a UID as foreground or background.</td>
  </tr>
  <tr>
  <td>/proc/uid_time_in_state</td>
  <td>Read-only file containing the time each UID's processes spend executing at
  each available frequency. <strong>This file is optional; its contents and
  permissions will be verified in VTS only if the file is present</strong>.</td>
  </tr>
  <tr>
  <td>/proc/uptime</td>
  <td>Read-only file that shows how long the system has been running.</td>
  </tr>
  <tr>
   <td>/proc/version</td>
   <td>Read-only file containing a string describing the kernel version.</td>
  </tr>
  <tr>
   <td>/proc/vmallocinfo</td>
   <td>Read-only file containing <code>vmalloc</code>'d ranges.</td>
  </tr>
  <tr>
    <td>/proc/vmstat</td>
    <td>Read-only file containing virtual memory statistics from the kernel.
    </td>
  </tr>
  <tr>
   <td>/proc/zoneinfo</td>
   <td>Read-only file containing information about memory zones.</td>
  </tr>
</table>

<h3 id="dev">dev</h3>

<table>
  <tr>
   <th>Interface</th>
   <th>Description</th>
  </tr>
  <tr>
   <td>/dev/ashmem</td>
   <td>Anonymous shared memory device file.</td>
  </tr>
  <tr>
   <td>/dev/binder</td>
   <td>Binder device file.</td>
  </tr>
  <tr>
   <td>/dev/hwbinder</td>
   <td>Hardware binder device file.</td>
  </tr>
  <tr>
   <td>/dev/tun</td>
   <td>Universal TUN/TAP device file.</td>
  </tr>
  <tr>
   <td>/dev/xt_qtaguid</td>
   <td>QTAGUID netfilter device file.</td>
  </tr>
</table>

<h3 id="sysfs">sysfs</h3>
<table>
  <tr>
   <th>Interface</th>
   <th>Description</th>
  </tr>
  <tr>
  <td>/sys/class/net/*/mtu</td>
  <td>Read-write file containing the maximum transmission unit for each
  interface.</td>
  </tr>
  <tr>
  <td>/sys/class/rtc/*/hctosys</td>
  <td>Read-only file showing whether a particular rtc supplies the system time
    on boot and resume.</td>
  </tr>
  <tr>
  <td>/sys/devices/system/cpu/</td>
  <td>Directory containing information about CPU configuration and
  frequency.</td>
  </tr>
  <tr>
  <td>/sys/kernel/ipv4</td>
  <td>Directory of read-write files to configure TCP socket buffer sizes.</td>
  </tr>
  <tr>
  <td>/sys/kernel/wakeup_reasons</td>
  <td>Directory of read-only files containing the last suspend time and resume
  reason.</td>
  </tr>
  <tr>
  <td>/sys/power/state</td>
  <td>Read-write file that controls the system sleep states.</td>
  </tr>
  <tr>
  <td>/sys/power/wake_lock</td>
  <td>Read-write file that contains the active wake locks.</td>
  </tr>
  <tr>
  <td>/sys/power/wake_unlock</td>
  <td>Read-write file that contains non-active wake locks.</td>
  </tr>
  <tr>
  <td>/sys/power/wakeup_count</td>
  <td>Read-write file that can be used to put the system into a sleep state
  while taking into account the concurrent arrival of wakeup events.</td>
  </tr>
</table>

<h3 id="selinuxfs">selinuxfs</h3>
<p>The framework mounts <code>selinuxfs</code> at <code>/sys/fs/selinux</code>.
</p>

<table>
  <tr>
   <th>Path</th>
   <th>Description</th>
  </tr>
  <tr>
   <td>/sys/fs/selinux/checkreqprot</td>
   <td>Read/write file containing a binary flag that determines how selinux
   protections are checked on <code>mmap</code> and <code>mprotect</code> calls.
   </td>
  </tr>
  <tr>
   <td>/sys/fs/selinux/null</td>
   <td>Read/write null device for use by selinux.</td>
  </tr>
  <tr>
   <td>/sys/fs/selinux/policy</td>
   <td>Read-only file containing the selinux policy in binary form.</td>
  </tr>
</table>

<aside class="note"><strong>Note:</strong> For details on SELinux in Android
8.0, see <a href="/security/selinux/images/SELinux_Treble.pdf">SELinux for
Android 8.0</a>.</aside>

  </body>
</html>
