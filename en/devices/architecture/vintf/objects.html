<html devsite>
  <head>
    <title>Manifests</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  {% include "_versions.html" %}
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>A VINTF object aggregates data from <a href="#device-manifest-file">device
manifest</a> and <a href="#framework-manifest-file">framework manifest</a> files
(XML) and from the device itself at <a href="#runtime-data">runtime</a>. Both
manifests share a format, although not all elements apply to both (for details
on the schema, see <a href="#manifest-file-schema">Manifest file schema</a>).
</p>

<h2 id="device-manifest-file">Device manifest</h2>
<p>The Device manifest (provided by the device) consists of the vendor manifest
and the ODM manifest.</p>

<ul>
<li>The vendor manifest specifies HALs, VNDK versions, etc. common to an SoC. It
is recommended to be placed in the Android source tree at
<code>device/<var>VENDOR</var>/<var>DEVICE</var>/manifest.xml</code>, but multiple fragment
files can be used. For details, see
<a href="/devices/architecture/vintf/resources.html#manifest-fragments">Generate
DM from fragments</a>.
</li>
<li>The ODM manifest lists HALs specific to the product. VINTF Object loads the ODM manifest as
  follows:
  <ol>
    <li>If <code><var>SKU</var></code> is defined (where <code><var>SKU</var></code> is the value of
      the property <code>ro.boot.product.hardware.sku</code>),
      <code>/odm/etc/vintf/manifest_<var>SKU</var>.xml</code>
    </li>
    <li><code>/odm/etc/vintf/manifest.xml</code></li>
    <li>If <code><var>SKU</var></code> is defined,
      <code>/odm/etc/manifest_<var>SKU</var>.xml</code></li>
    <li><code>/odm/etc/manifest.xml</code></li>
  </ol>
</li>
<li>VINTF Object loads the device manifest as follows:
  <ol>
    <li>If <code>/vendor/etc/vintf/manifest.xml</code> exists, combine the following:
      <ol>
        <li><code>/vendor/etc/vintf/manifest.xml</code></li>
        <li>optional vendor manifest fragments</li>
        <li>optional ODM manifest
        <li>optional ODM manifest fragments</li>
      </ol>
    <li>If ODM manifest exists, combine ODM manifest with optional ODM manifest fragments.</li>
    <li><code>/vendor/manfiest.xml</code> (legacy, no fragments)</li>
  </ol>
Note that:
  <ul>
    <li>On legacy devices, the legacy vendor manifest and ODM manifest are used. The
      ODM manifest may completely override the legacy vendor manifest.</li>
    <li>On devices launched with Android {{ androidPVersionNumber }}, the ODM manifest is combined
      with vendor manifest.</li>
    <li>When combining a list of manifests, manifests that appear later in the list may override
      tags in manifests that appear earlier in the list, provided that the tags in the later
      manifest has attribute <code>override="true"</code>. For example, ODM manifest may override
      some <code>&lt;hal></code> tags from vendor manifest. See documentation for attribute
      <code>override</code> below.</li>
  </ul>
</li>
</ul>

<p>This setup enables multiple products with the same board to share the same
vendor image (which provides common HALs) yet have different ODM images (which
specify product-specific HALs).</p>

<p>Example vendor manifest:</p>

<pre class="prettyprint">
&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;!-- Comments, Legal notices, etc. here -->
&lt;manifest version="1.0" type="device" target-level="1">
    &lt;hal>
        &lt;name>android.hardware.camera&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
        &lt;version>3.4&lt;/version>
        &lt;interface>
            &lt;name>ICameraProvider&lt;/name>
            &lt;instance>legacy/0&lt;/instance>
            &lt;instance>proprietary/0&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.hardware.nfc&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
        &lt;version>1.0&lt;/version>
        &lt;version>2.0&lt;/version>
        &lt;interface>
            &lt;name>INfc&lt;/name>
            &lt;instance>nfc_nci&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.hardware.nfc&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
        &lt;version>2.0&lt;/version>
        &lt;interface>
            &lt;name>INfc&lt;/name>
            &lt;instance>default&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.hardware.drm&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>ICryptoFactory&lt;/name>
            &lt;instance>default&lt;/instance>
        &lt;/interface>
        &lt;interface>
            &lt;name>IDrmFactory&lt;/name>
            &lt;instance>default&lt;/instance>
        &lt;/interface>
        &lt;fqname>@1.1::ICryptoFactory/clearkey&lt;/fqname>
        &lt;fqname>@1.1::IDrmFactory/clearkey&lt;/fqname>
    &lt;/hal>
    &lt;hal format="native">
        &lt;name>EGL&lt;/name>
        &lt;version>1.1&lt;/version>
    &lt;/hal>
    &lt;hal format="native">
        &lt;name>GLES&lt;/name>
        &lt;version>1.1&lt;/version>
        &lt;version>2.0&lt;/version>
        &lt;version>3.0&lt;/version>
    &lt;/hal>
    &lt;sepolicy>
        &lt;version>25.0&lt;/version>
    &lt;/sepolicy>
&lt;/manifest>
</pre>

<p>Example ODM manifest:</p>

<pre class="prettyprint">
&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;!-- Comments, Legal notices, etc. here -->
&lt;manifest version="1.0" type="device">
    &lt;!-- camera 3.4 in vendor manifest is ignored -->
    &lt;hal override="true">
        &lt;name>android.hardware.camera&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
        &lt;version>3.5&lt;/version>
        &lt;interface>
            &lt;name>ICameraProvider&lt;/name>
            &lt;instance>legacy/0&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;!-- NFC is disabled -->
    &lt;hal override="true">
        &lt;name>android.hardware.nfc&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.hardware.power&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
        &lt;version>1.1&lt;/version>
        &lt;interface>
            &lt;name>IPower&lt;/name>
            &lt;instance>default&lt;/instance>
        &lt;/interface>
    &lt;/hal>
&lt;/manifest>
</pre>

For more details, see <a href="/devices/architecture/vintf/dm">DM
Development</a>.

<h2 id="framework-manifest-file">Framework manifest</h2>
<p>The Framework manifest file (provided by Google) is manually generated and
lives in the Android source tree at
<code><a href="https://android.googlesource.com/platform/system/libhidl/+/master/manifest.xml" class="external">/system/libhidl/manifest.xml</a></code>.
</p>

<p>Example Framework manifest:</p>

<pre class="prettyprint">
&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;!-- Comments, Legal notices, etc. here -->
&lt;manifest version="1.0" type="framework">
    &lt;hal>
        &lt;name>android.hidl.allocator&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>IAllocator&lt;/name>
            &lt;instance>ashmem&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.hidl.memory&lt;/name>
        &lt;transport arch="32+64">passthrough&lt;/transport>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>IMapper&lt;/name>
            &lt;instance>ashmem&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.hidl.manager&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>IServiceManager&lt;/name>
            &lt;instance>default&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.frameworks.sensorservice&lt;/name>
        &lt;transport>hwbinder&lt;/transport>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>ISensorManager&lt;/name>
            &lt;instance>default&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;vendor-ndk>
        &lt;version>27&lt;/version>
    &lt;/vendor-ndk>
    &lt;system-sdk>
        &lt;version>27&lt;/version>
    &lt;/system-sdk>
&lt;/manifest>
</pre>

<h2 id="manifest-file-schema">Manifest file schema</h2>
<p>This section describes the meaning of these XML tags. Some "required" tags
can be missing from the source file in Android source tree and written by
<code><a href="/devices/architecture/vintf/resources.html#assemble_vintf">assemble_vintf</a></code>
at build time. "Required" tags must be present in the corresponding files on the
device.</p>

<dl>
<dt><code>?xml</code></dt>
<dd>Optional. Only provides information to the XML parser.</dd>
<dt><code>manifest.version</code></dt>
<dd>Required. Meta-version of <strong>this</strong> manifest. Describes the
elements expected in the manifest. Unrelated to XML version.</dd>
<dt><code>manifest.type</code></dt>
<dd>Required. Type of this manifest. It has value <code>device</code> for
device manifest file and <code>framework</code> for framework manifest
file.</dd>
<dt><code>manifest.target-level</code></dt>
<dd>Required for device manifest. Specifies the Framework Compatibility Matrix
Version (FCM Version) that this device manifest is targeted to be compatible
with. This is also called the Shipping FCM Version of the device.</dd>
<dt><code>manifest.hal</code></dt>
<dd>Optional, can repeat. A single HAL (HIDL or native, such as GL),
depending on the <code>format</code> attribute.</dd>
<dt><code>manifest.hal.format</code></dt>
<dd>Optional. Value can be one of:
 <ul>
 <li><code>hidl</code>: HIDL HALs. This is the default.
 <li><code>native</code>: native HALs.</li>
 </ul>
</dd>
<dt><code>manifest.hal.override</code></dt>
<dd>Optional. Value can be one of:
 <ul>
 <li><code>true</code>: override other <code>&lt;hal></code> elements with
  the same <code>&lt;name></code> and major version. If no
  <code>&lt;version></code> or <code>&lt;fqname></code> are in this
  <code>&lt;hal></code> element, then this HAL is disabled.</li>
 <li><code>false</code>: do not override other <code>&lt;hal></code> elements
  with the same <code>&lt;name></code> and major version.</li>
 </ul>
</dd>
<dt><code>manifest.hal.name</code></dt>
<dd>Required. Fully-qualified package name of HAL. Multiple HAL entries can use
the same name. Examples:
 <ul>
 <li><code>android.hardware.camera</code> (HIDL HAL)</li>
 <li><code>GLES</code> (native HAL, requires name only)</li>
 </ul>
 </dd>
<dt><code>manifest.hal.transport</code></dt>
<dd>Required when <code>manifest.hal.format == "hidl"</code>. Must NOT be
present otherwise. States what transport will be used when an interface from
this package is queried from service manager. Value can be one of:
 <ul>
 <li><code>hwbinder</code>: binderized mode</li>
 <li><code>passthrough</code>: passthrough mode</li>
 </ul>
</dd>
<dt><code>manifest.hal.transport.arch</code></dt>
<dd>Required for <code>passthrough</code> and must not be present for
<code>hwbinder</code>. Describes the bitness of the passthrough service being
provided. Value can be one of:
 <ul>
 <li><code>32</code>: 32-bit mode</li>
 <li><code>64</code>: 64-bit mode</li>
 <li><code>32+64</code>: both</li>
 </ul>
</dd>
<dt><code>manifest.hal.version</code></dt>
<dd>Optional, can repeat. A version for the <code>hal</code> tags in a
manifest. Format is <code><var>MAJOR</var>.<var>MINOR</var></code>. For
examples, refer to <code>hardware/interfaces</code>,
<code>vendor/${VENDOR}/interfaces</code>,
<code>framework/hardware/interfaces</code>, or<code>
system/hardware/interfaces</code>.
<br><br>
HIDL and native HALs may use multiple version fields as long as they represent
<strong>distinct major versions</strong>, with only one minor version per major
version provided. For example, 3.1 and 3.2 cannot coexist, but 1.0 and 3.4 can.
This applies for all <code>hal</code> elements with the same name, unless
<code>override="true"</code>.</dd>
<dt><code>manifest.hal.interface</code></dt>
<dd>Required, can repeat without duplicates. State an interface in the
package that has an instance name. There can be multiple
<code>&lt;interface&gt;</code> elements in a <code>&lt;hal&gt;</code>; names
must be distinct.</dd>
<dt><code>manifest.hal.interface.name</code></dt>
<dd>Required. Name of the interface.</dd>
<dt><code>manifest.hal.interface.instance</code></dt>
<dd>Required, can repeat. Instance name of the interface. Can have multiple
instances for an interface but no duplicated <code>&lt;instance&gt;</code>
elements.</dd>
<dt><code>manifest.hal.fqname</code></dt>
<dd>Optional, can repeat. An alternative way to specify an instance for the HAL
with name <code>manifest.hal.name</code>. Format is
<code>@<var>MAJOR</var>.<var>MINOR</var>::<var>INTERFACE</var>/<var>INSTANCE</var></code>.
For devices upgrading from Android 8.0, this cannot be used to declare
instances required by the compatibility matrix.</dd>
<dt><code>manifest.sepolicy</code></dt>
<dd>Required. Contains all sepolicy-related entries.</dd>
<dt><code>manifest.sepolicy.version</code></dt>
<dd>Required for device manifest. Declares SELinux version. It has the
format <code><var>SDK_INT</var>.<var>PLAT_INT</var></code>.</dd>
<dt><code>manifest.vendor-ndk</code></dt>
<dd>Required, can repeat; required for framework manifest. Must not be present
in the device manifest. Multiple <code>&lt;vendor-ndk></code> entries must have
different <code>&lt;version></code>’s. Describes a set of VNDK snapshots
provided by the framework.</dd>
<dt><code>manifest.vendor-ndk.version</code></dt>
<dd>Required. It is a positive integer representing the version of the VNDK
snapshot.</dd>
<dt><code>manifest.vendor-ndk.library</code></dt>
<dd>Optional, can repeat, without duplicates. Describes a set of VNDK libraries
provided by the framework for this VNDK vendor snapshot. The value is the
filename of a library, e.g. <code>libjpeg.so</code>, including the prefix
<code>lib</code> and the suffix <code>.so</code>. No path components are
allowed.</dd>
<dt><code>manifest.system-sdk.version</code></dt>
<dd>Optional, can repeat, without duplicates; used only by the framework
manifest. Describes a set of System SDK versions provided by the framework to
vendor apps.</dd>
</dl>

  </body>
</html>

