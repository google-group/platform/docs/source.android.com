<html devsite>
  <head>
    <title>Matching Rules</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  {% include "_versions.html" %}
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>The two pairs of compatibility matrices and manifests are meant to be
reconciled at <a href="/devices/tech/ota/index.html">OTA</a> time to verify the
framework and vendor implementation can work with each other. This verification
is successful upon a match between the framework compatibility matrix and the
device manifest, as well as between the framework manifest and the device
compatibility matrix. The following sections detail matching rules used by
various components.</p>

<h2 id="fcm-version">Framework compatibility matrix version matches</h2>
<p>To match a device manifest with a framework compatibility matrix,
the Shipping FCM version specified by <code>manifest.target-level</code>
must exactly equal to the FCM version specified by
<code>compatibility-matrix.level</code>. Otherwise there is no match.</p>

<p>If the framework compatibility matrix is requested with
<code>libvintf</code>, this match is always successful because
<code>libvintf</code> opens the device manifest, retrieves the Shipping FCM
Version, and returns the framework compatibility matrix at that Shipping FCM
Version (plus some optional HALs from compatibility matrices at higher FCM
Versions).</p>

<h2 id="hals">HAL matches</h2>
<p>The HAL-match rule identifies the versions of <code>hal</code> elements in a
manifest file that are considered supported by the owner of the corresponding
compatibility matrix.</p>
<ul>
<li>Multiple <code>&lt;hal&gt;</code> elements have <strong>AND</strong>
relationship.</li>
<li>Multiple <code>&lt;version&gt;</code> elements within the same
<code>&lt;hal&gt;</code> have
<strong>OR</strong> relationship. If two or more are specified, only
one of the version needs to be implemented (see DRM example below).</li>
<li>Multiple <code>&lt;instance&gt;</code> and
<code>&lt;regex-instance&gt;</code> elements within the same
<code>&lt;hal&gt;</code> have
<strong>AND</strong> relationship (see DRM example below).</li>
</ul>

<h4><strong>Example</strong>: Successful HAL match for Camera module</h4>
<p>For a HAL at version 2.5, the match rule is as follows:</p>

<table>
  <tr>
   <th>Matrix</th>
   <th>Matching Manifest</th>
  </tr>
  <tr>
   <td><code>2.5</code></td>
   <td>2.5-2.∞. Shorthand for 2.5-5.</td>
  </tr>
  <tr>
   <td><code>2.5-7</code></td>
   <td>2.5-2.∞. Indicates the following:
   <br>
   <ul>
   <li>2.5 is the minimum required version, meaning a manifest providing HAL
   2.0-2.4 is not compatible.</li>
   <li>2.7 is the maximum version that could be requested, meaning the owner of
   the compatibility matrix (framework or device) will not request versions
   beyond 2.7. The owner of the matching manifest can still serve version 2.10
   (as an example) when 2.7 is requested. The compatibility-matrix owner knows
   only that the requested service is compatible with API version 2.7.</li>
   <li>-7 is informational only and does not affect the OTA update process.</li>
   </ul>
   Thus, a device with a HAL at version 2.10 in its manifest file remains
   compatible with a framework that states <code>camera:</code>
   <code>2.5-7</code> in its compatibility matrix.</td>
  </tr>
</table>

<h4><strong>Example:</strong> Successful HAL match for DRM module</h4>
<p>The framework compatibility matrix states the following version information
for DRM HAL:</p>

<pre class="prettyprint">&lt;hal>
    &lt;name>android.hardware.drm
    &lt;version>1.0&lt;/version>
    &lt;version>3.1-2&lt;/version>
    &lt;interface>
        &lt;name>IDrmFactory&lt;/name>
        &lt;instance>default&lt;/instance>
        &lt;instance>specific&lt;/instance>
    &lt;/interface>
&lt;/hal>
&lt;hal>
    &lt;name>android.hardware.drm
    &lt;version>2.0&lt;/version>
    &lt;interface>
        &lt;name>ICryptoFactory&lt;/name>
        &lt;instance>default&lt;/instance>
        &lt;regex-instance>[a-z]+/[0-9]+&lt;/regex-instance>
    &lt;/interface>
&lt;/hal>
</pre>

<p>A vendor must implement ONE of the following instances:</p>

<pre>
android.hardware.drm@1.x::IDrmFactory/default          // where x >= 0
android.hardware.drm@1.x::IDrmFactory/specific         // where x >= 0
</pre>

OR

<pre>
android.hardware.drm@3.y::IDrmFactory/default          // where y >= 1
android.hardware.drm@3.y::IDrmFactory/specific         // where y >= 1
</pre>

<p>... AND must also implement all of these instances:</p>

<pre>
android.hardware.drm@2.z::ICryptoFactory/default       // where z >= 0
android.hardware.drm@2.z::ICryptoFactory/${INSTANCE}
            // where z >= 0 and ${INSTANCE} matches [a-z]+/[0-9]+
            // e.g. legacy/0
</pre>

<h2 id="kernel">Kernel matches</h2>
<p>The <code>&lt;kernel&gt;</code> section of the framework compatibility matrix
describes the framework's requirements of the Linux kernel on the device. This
information is meant to be matched at OTA time against the
<a href="/devices/architecture/vintf/objects.html#runtime-collectible-information">information</a>
about the kernel that gets reported by the device's VINTF object.</p>

<p>A matrix can include multiple <code>&lt;kernel&gt;</code> sections, each with
a different <code>version</code> attribute using the format:</p>

<pre class="prettyprint">${ver}.${major_rev}.${kernel_minor_rev}</pre>

<p>The OTA considers only the <code>&lt;kernel&gt;</code> section with the same
<code>${ver}</code> and <code>${major_rev}</code> as the device kernel (i.e.,
<code>version="${ver}.${major_rev}.${matrix_minor_rev}")</code>; other sections
are ignored. In addition, the minor revision from the kernel must be a value
from the compatibility matrix (<code>${kernel_minor_rev} >=
${matrix_minor_rev}</code>;). If no <code>&lt;kernel&gt;</code> section meets
these requirements, it is a non-match.</p>

<p>If the <code>&lt;kernel&gt;</code> section does match, the process continues
by attempting to match <code>config</code> elements against
<code>/proc/config.gz</code>. For each config element in the compatibility
matrix, it looks up <code>/proc/config.gz</code> to see if the config is
present. When a config item is set to <code>n</code> in the compatibility
matrix for the matching <code>&lt;kernel&gt;</code> section, it must be absent
from <code>/proc/config.gz</code>. Finally, a config item not in the
compatibility matrix may or may not be present in <code>/proc/config.gz</code>.
</p>

<p>Examples of matches:</p>
<ul>
<li><code>&lt;value type="string"&gt;bar&lt;/value&gt;</code> matches
<code>"bar"</code>. Quotes are omitted in the compatibility matrix but present
in <code>/proc/config.gz</code>.</li>
<li><code>&lt;value type="int"&gt;4096&lt;/value&gt;</code> matches
<code>4096</code> or <code>0x1000</code> or <code>0X1000</code>.</li>
<li><code>&lt;value type="int"&gt;0x1000&lt;/value&gt;</code> matches
<code>4096</code> or <code>0x1000</code> or <code>0X1000</code>.</li>
<li><code>&lt;value type="int"&gt;0X1000&lt;/value&gt;</code> matches
<code>4096</code> or <code>0x1000</code> or <code>0X1000</code>.</li>
<li><code>&lt;value type="tristate"&gt;y&lt;/value&gt;</code> matches
<code>y</code>.</li>
<li><code>&lt;value type="tristate"&gt;m&lt;/value&gt;</code> matches
<code>m</code>.</li>
<li><code>&lt;value type="tristate"&gt;n&lt;/value&gt;</code> means the config
item must NOT exist in <code>/proc/config.gz</code>.</li>
<li><code>&lt;value type="range"&gt;1-0x3&lt;/value&gt;</code> matches
<code>1</code>, <code>2</code>, or <code>3</code>, or hexadecimal equivalent.
</li>
</ul>

<h4><strong>Example:</strong> Successful kernel match</h4>
<p>A framework compatibility matrix has the following kernel information:</p>

<pre class="prettyprint">
&lt;kernel version=&quot;3.18.51&quot;&gt;
   &lt;config&gt;
      &lt;key&gt;CONFIG_TRI&lt;/key&gt;
      &lt;value type=&quot;tristate&quot;&gt;y&lt;/value&gt;
   &lt;/config&gt;
   &lt;config&gt;
      &lt;key&gt;CONFIG_NOEXIST&lt;/key&gt;
      &lt;value type=&quot;tristate&quot;&gt;n&lt;/value&gt;
   &lt;/config&gt;
   &lt;config&gt;
      &lt;key&gt;CONFIG_DEC&lt;/key&gt;
      &lt;value type=&quot;int&quot;&gt;4096&lt;/value&gt;
   &lt;/config&gt;
   &lt;config&gt;
      &lt;key&gt;CONFIG_HEX&lt;/key&gt;
      &lt;value type=&quot;int&quot;&gt;0XDEAD&lt;/value&gt;
   &lt;/config&gt;
   &lt;config&gt;
      &lt;key&gt;CONFIG_STR&lt;/key&gt;
      &lt;value type=&quot;string&quot;&gt;str&lt;/value&gt;
   &lt;/config&gt;
   &lt;config&gt;
      &lt;key&gt;CONFIG_EMPTY&lt;/key&gt;
      &lt;value type=&quot;string&quot;&gt;&lt;/value&gt;
   &lt;/config&gt;
&lt;/kernel&gt;
</pre>

<p>The kernel version is matched first. If a device in <code>uname()</code>
reports:</p>
<ul>
<li>3.10.73 (no match to matrix unless there is a separate kernel section
with <code>&lt;kernel version="3.10.x"&gt;</code> where <code>x <= 73</code>)
</li>
<li>3.18.50 (no match to matrix, smaller than <code>version</code>)</li>
<li>3.18.51 (match to matrix)</li>
<li>3.18.52 (match to matrix)</li>
<li>4.1.22 (no match to matrix unless there is a separate kernel section
with <code>&lt;kernel version="4.1.x"&gt;</code> where <code>x <= 22</code>)
</li>
</ul>

<p>After the appropriate <code>&lt;kernel&gt;</code> section is selected, for
each <code>&lt;config&gt;</code> item with value other than <code>n</code>, we
expect the corresponding entry to be present in <code>/proc/config.gz</code>;
for each <code>&lt;config&gt;</code> item with value <code>n</code>, we expect
the corresponding entry to not be present in <code>/proc/config.gz</code>. We
expect the content of <code>&lt;value&gt;</code> to exactly match the text after
the equal sign (including quotes), up to the newline character or
<code>#</code>, with leading and trailing whitespace truncated.</p>

<p>The following kernel configuration is an example of a successful match:</p>

<pre class="prettyprint">
# comments don't matter
CONFIG_TRI=y
# CONFIG_NOEXIST should not exist
CONFIG_DEC = 4096 # trailing comments and whitespaces are fine
CONFIG_HEX=57005  # 0XDEAD == 57005
CONFIG_STR="str"
CONFIG_EMPTY=""   # empty string must have quotes
CONFIG_EXTRA="extra config items are fine too"
</pre>

<p>The following kernel configuration is an example of an unsuccessful match:</p>

<pre class="prettyprint">
CONFIG_TRI="y"   # mismatch: quotes
CONFIG_NOEXIST=y # mismatch: CONFIG_NOEXIST exists
CONFIG_HEX=0x0   # mismatch; value doesn't match
CONFIG_DEC=""    # mismatch; type mismatch (expect int)
CONFIG_EMPTY=1   # mismatch; expects ""
# mismatch: CONFIG_STR is missing
</pre>

<h2 id="se-policy">SE policy matches</h2>
<p>SE policy requires the following matches:</p>
<ul>
<li><code>&lt;sepolicy-version&gt;</code> defines a closed range of minor
versions for every major version. The sepolicy version reported by the device
must fall within one of these ranges to be compatible with the framework. Match
rules are similar to HAL versions; it is a match if the sepolicy version is
higher or equal to the minimum version for the range. The maximum version is
purely informational.</li>
<li><code>&lt;kernel-sepolicy-version&gt;</code> i.e. policydb version. Must
be less than the <code>security_policyvers()</code> reported by the device.
</li>
</ul>

<h4><strong>Example:</strong> Successful SE policy match</h4>
<p>The framework compatibility matrix states the following sepolicy information:
</p>

<pre class="prettyprint">
&lt;sepolicy>
    &lt;kernel-sepolicy-version>30&lt;/kernel-sepolicy-version>
    &lt;sepolicy-version>25.0&lt;/sepolicy-version>
    &lt;sepolicy-version>26.0-3&lt;/sepolicy-version>
&lt;/sepolicy>
</pre>

<p>On the device:</p>
<ul>
<li>The value returned by <code>security_policyvers()</code> must be greater
than or equal to 30. Otherwise it is not a match. For example:
<ul>
<li>If a device returns 29, it is not a match.</li>
<li>If a device returns 31, it is a match.</li>
</ul>
</li>
<li>SE Policy version must be one of 25.0-∞ or 26.0-∞. Otherwise it is not a
match. (The "<code>-3</code>" after "<code>26.0</code>" is purely
informational.)</li>
</ul>

<h2 id="avb-version">AVB version matches</h2>
<p>The AVB version contains a MAJOR version and MINOR version, with the format
as MAJOR.MINOR (e.g., 1.0, 2.1). For details, refer to
<a href="https://android.googlesource.com/platform/external/avb/#Versioning-and-compatibility" class="external">Versioning
and Compatibility</a>. AVB version has the following system properties:</p>
<ul>
<li><code>ro.boot.vbmeta.avb_version</code> is the <code>libavb</code> version
in bootloader</li>
<li><code>ro.boot.avb_version</code> is the <code>libavb</code> version in
Android OS (<code>init/fs_mgr</code>)</li>
</ul>

<p>The system property appears only when the corresponding libavb has been used
to verify AVB metadata (and returns OK). It is absent if a verification failure
occurred (or no verification occurred at all).</p>

<p>A compatibility match compares the following:</p>
<ul>
<li>sysprop <code>ro.boot.vbmeta.avb_version</code> with
<code>avb.vbmeta-version</code> from framework compatibility matrix;
 <ul>
 <li><code>ro.boot.vbmeta.avb_version.MAJOR == avb.vbmeta-version.MAJOR</code></li>
 <li><code>ro.boot.vbmeta.avb_version.MINOR >= avb.vbmeta-version.MINOR</code></li>
 </ul>
</li>
<li>sysprop <code>ro.boot.avb_version</code> with
<code>avb.vbmeta-version</code> from framework compatibility matrix.
 <ul>
 <li><code>ro.boot.avb_version.MAJOR == avb.vbmeta-version.MAJOR</code></li>
 <li><code>ro.boot.avb_version.MINOR >= avb.vbmeta-version.MINOR</code></li>
 </ul>
</li>
</ul>

<p>The bootloader or Android OS might contain two copies of <code>libavb</code>
libraries, each with a different MAJOR version for upgrade devices and launch
devices. In this case, the same <em>unsigned</em> system image can be shared but
the final <em>signed</em> system images are different (with different
<code>avb.vbmeta-version</code>):</p>

<img src="../images/treble_vintf_avb_o_p.png">
<figcaption><strong>Figure 1. </strong>AVB version matches (<code>/system</code>
is P, all other partitions are O).</figcaption>
<br>
<br>
<img src="../images/treble_vintf_avb_p.png">
<figcaption><strong>Figure 2.</strong> AVB version matches (all partitions are
P).</figcaption>

<h4><strong>Example:</strong> Successful AVB version match</h4>
<p>The framework compatibility matrix states the following AVB information:</p>

<pre class="prettyprint">
&lt;avb>
    &lt;vbmeta-version>2.1&lt;/vbmeta-version>
&lt;/avb>
</pre>

<p>On the device:</p>

<pre class="prettyprint">
ro.boot.avb_version              == 1.0 &amp;&amp;
ro.boot.vbmeta.avb_version       == 2.1 <font style="font-family: Roboto, Arial, Helvetica, sans-serif; background-color: red; color: white">&nbsp;mismatch&nbsp;</font>
</pre>

<pre class="prettyprint">
ro.boot.avb_version              == 2.1 &amp;&amp;
ro.boot.vbmeta.avb_version       == 3.0 <font style="font-family: Roboto, Arial, Helvetica, sans-serif; background-color: red; color: white">&nbsp;mismatch&nbsp;</font>
</pre>

<pre class="prettyprint">
ro.boot.avb_version              == 2.1 &amp;&amp;
ro.boot.vbmeta.avb_version       == 2.3 <font style="font-family: Roboto, Arial, Helvetica, sans-serif; background-color: green; color: white">&nbsp;match&nbsp;</font>
</pre>

<pre class="prettyprint">
ro.boot.avb_version              == 2.3 &amp;&amp;
ro.boot.vbmeta.avb_version       == 2.1 <font style="font-family: Roboto, Arial, Helvetica, sans-serif; background-color: green; color: white">&nbsp;match&nbsp;</font>
</pre>

<h3 id="avb-version-ota">Matching AVB version during OTA</h3>
<p>For devices launched with Android {{ androidPVersionNumber }} or lower, during OTA, the AVB
version requirements in framework compatibility matrix are matched against the current AVB version
on the device. If the AVB version has an major version upgrade during an OTA (for example, from
0.0 to 1.0), the check in OTA does not reflect the compatibility after the OTA.</p>
<p>To mitigate the issue, an OEM can place a fake AVB version in the OTA package
(<code>compatibility.zip</code>) to pass the check. To do so:</p>
<ol>
<li>Cherry-pick the following CLs to the Android {{ androidPVersionNumber }} source tree:
<ul>
  <li><a href="https://android-review.googlesource.com/732261" class="external">CL 732261</a></li>
  <li><a href="https://android-review.googlesource.com/732262" class="external">CL 732262</a></li>
</ul>
</li>
<li>Define <code>BOARD_OTA_FRAMEWORK_VBMETA_VERSION_OVERRIDE</code> for the device. Its value
should equal the AVB version before the OTA, i.e. the AVB version of the device when it was
launched.</li>
<li>Rebuild the OTA package.</li>
</ol>
<p>These changes automatically place
<code>BOARD_OTA_FRAMEWORK_VBMETA_VERSION_OVERRIDE</code> as
<code>compatibility-matrix.avb.vbmeta-version</code> in the following files:
<ul>
  <li><code>/system/compatibility_matrix.xml</code>
  (which is not used in Android {{ androidPVersionNumber }}) on the device</li>
  <li><code>system_matrix.xml</code> in <code>compatibility.zip</code> in the OTA package</li>
</ul>
These changes do not affect other framework compatibility matrices, including
<code>/system/etc/vintf/compatibility_matrix.xml</code>. After the OTA, the new value in
<code>/system/etc/vintf/compatibility_matrix.xml</code> is used for compatibility checks instead.
</p>

<h2 id="vndk">VNDK version matches</h2>
<p>The device compatibility matrix declares the required VNDK version in
<code>compatibility-matrix.vendor-ndk.version</code>. If the device
compatibility matrix does not have a <code>&lt;vendor-ndk></code> tag, no
requirements are imposed, and hence it is always considered a match.</p>
<p>If the device compatibility matrix does have a <code>&lt;vendor-ndk></code>
tag, an <code>&lt;vendor-ndk></code> entry with a matching
<code>&lt;version></code> is looked up from the set of VNDK vendor snapshots
provided by the framework in the framework manifest. If such an entry does not
exist, there is no match.</p>
<p>If such entry does exist, the set of libraries enumerated in the device
compatibility matrix must be a subset of the set of libraries stated in the
framework manifest; otherwise, the entry is not considered a match.</p>
<ul>
  <li>As a special case, if no libraries are enumerated in the device
  compatibility matrix, the entry is always considered a match, because empty
  set is a subset of any set.</li>
</ul>

<h4><strong>Example:</strong> Successful VNDK version match</h4>
<p>If the device compatibility matrix states the following requirement on VNDK:
</p>

<pre class="prettyprint">
&lt;!-- Example Device Compatibility Matrix -->
&lt;vendor-ndk>
    &lt;version>27&lt;/version>
    &lt;library>libjpeg.so&lt;/library>
    &lt;library>libbase.so&lt;/library>
&lt;/vendor-ndk>
</pre>

<p>In the framework manifest, only the entry with version 27 is considered.</p>

<pre class="prettyprint">
&lt;!-- Framework Manifest Example A -->
&lt;vendor-ndk>
    &lt;version>27&lt;/version>
    &lt;library>libjpeg.so&lt;/library>
    &lt;library>libbase.so&lt;/library>
    &lt;library>libfoo.so&lt;/library>
&lt;/vendor-ndk>
</pre>

<p>Example A is a match, because VNDK version 27 is in the framework manifest,
and <code>{libjpeg.so, libbase.so, libfoo.so} ⊇ {libjpeg.so, libbase.so}</code>.
</p>

<pre class="prettyprint">
&lt;!-- Framework Manifest Example B -->
&lt;vendor-ndk>
    &lt;version>26&lt;/version>
    &lt;library>libjpeg.so&lt;/library>
    &lt;library>libbase.so&lt;/library>
&lt;/vendor-ndk>
&lt;vendor-ndk>
    &lt;version>27&lt;/version>
    &lt;library>libbase.so&lt;/library>
&lt;/vendor-ndk>
</pre>

<p>Example B is not a match. Even though VNDK version 27 is in the framework
manifest, <code>libjpeg.so</code> is not supported by the framework in that
snapshot. VNDK version 26 is ignored.</p>

<h2 id="vsdk">System SDK version matches</h2>
<p>The device compatibility matrix declares a set of required System SDK
version in <code>compatibility-matrix.system-sdk.version</code>. There is a
match only if the set is a subset of provided System SDK versions as declared
in <code>manifest.system-sdk.version</code> in the framework manifest.</p>
<ul>
  <li>As a special case, if no System SDK versions are enumerated in the device
  compatibility matrix, it is always considered a match, because empty
  set is a subset of any set.</li>
</ul>

<h4><strong>Example:</strong> Successful System SDK version match</h4>
<p>If the device compatibility matrix states the following requirement on System
SDK:
</p>

<pre class="prettyprint">
&lt;!-- Example Device Compatibility Matrix -->
&lt;system-sdk>
    &lt;version>26&lt;/version>
    &lt;version>27&lt;/version>
&lt;/system-sdk>
</pre>

<p>Then, the framework must provide System SDK version 26 and 27 to match.</p>

<pre class="prettyprint">
&lt;!-- Framework Manifest Example A -->
&lt;system-sdk>
    &lt;version>26&lt;/version>
    &lt;version>27&lt;/version>
&lt;/system-sdk>
</pre>

<p>Example A is a match.</p>

<pre class="prettyprint">
&lt;!-- Framework Manifest Example B -->
&lt;system-sdk>
    &lt;version>26&lt;/version>
    &lt;version>27&lt;/version>
    &lt;version>28&lt;/version>
&lt;/system-sdk>
</pre>

<p>Example B is a match.</p>

<pre class="prettyprint">
&lt;!-- Framework Manifest Example C -->
&lt;system-sdk>
    &lt;version>26&lt;/version>
&lt;/system-sdk>
</pre>

<p>Example C is not a match, because System SDK version 27 is not provided.</p>

  </body>
</html>
