<html devsite="">
<head>
  <title>Compiling & Verifying</title>
  <meta name="project_path" value="/_project.yaml">
  <meta name="book_path" value="/_book.yaml">
</head>

<body>
   {% include "_versions.html" %}
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


  <p>You can use Device Tree Compiler (DTC) to compile the Device Tree Source
  files. However, before applying the overlay DT on the target main DT, you
  should also verify the result by simulating the behavior of DTO.</p>


  <h2 id="compile">Compiling with DTC</h2>


  <p>When using <code>dtc</code> to compile <code>.dts</code>, you must add
  option <code>-@</code> to add a <code>__symbols__</code> node in the
  resulting <code>.dtbo</code>. The <code>__symbols__</code> node contains a
  list of all nodes that are marked with a label, which the DTO library can use
  for references.</p>


  <p>Sample command to build main DT <code>.dts</code>:</p>

  <pre class="devsite-terminal">
dtc -@ -O dtb -o my_main_dt.dtb my_main_dt.dts
</pre>

  <p>Sample command to build the overlay DT <code>.dts</code>:</p>

  <pre class="devsite-terminal">
dtc -@ -O dtb -o my_overlay_dt.dtbo my_overlay_dt.dts
</pre>

  <aside class="note"><strong>Note:</strong> If you encounter the DTC build error:
  <code>invalid option --'@'</code>, you might need to update your DTC version.
  Upstream of AOSP, the official DTC supports DTO as of <a href=
  "https://github.com/dgibson/dtc/tree/v1.4.4" class="external">version
  1.4.4</a> and most patches are merged after December 2016. For DTO support,
  we recommend using the <code><a href=
  "https://android.googlesource.com/platform/external/dtc/" class=
  "external">external/dtc</a></code> in AOSP, which is synced with the latest DTC
  (with DTO patches merged as needed).</aside>


  <h2 id="verify">Verify DTO results on the host</h2>


  <p>Verification can help you identify errors that might occur when placing
  the overlay DT on the main DT. Before updating the target, you can verify the
  result of overlaying DT on the host by simulating the behavior of DTO using
  <code>/include/</code> in <code>.dts</code>.</p>


  <aside class="note"><strong>Note:</strong> <code>/include/</code> does NOT
  support the use of <code>__overlay__</code> in overlay DT sources.</aside>


  <p><img src="../images/treble_dto_simulate.png">
  </p>


  <figcaption><strong>Figure 1.</strong> Use syntax <code>/include/</code> to simulate
  DTO on the host</figcaption>


  <ol>
    <li>Create a copy of the overlay <code>.dts</code>. In the copy, remove the
    first line header. Example:

      <pre>
/dts-v1/;
/plugin/;
</pre>Save the file as <code>my_overlay_dt_wo_header.dts</code> (or any
filename you want).
    </li>


    <li>Create a copy of the main <code>.dts</code>. In the copy, after the
    last line, append the include syntax for the file you created in step 1.
    For example:

      <pre>
/include/ "my_overlay_dt_wo_header.dts"
</pre>Save the file as <code>my_main_dt_with_include.dts</code> (or any
filename you want).
    </li>


    <li>Use <code>dtc</code> to compile
    <code>my_main_dt_with_include.dts</code> to get the merged DT, which should
    be the same result as DTO. For example:

      <pre class="devsite-terminal">
dtc -@ -O dtb -o my_merged_dt.dtb my_main_dt_with_include.dts
</pre>
    </li>


    <li>Use <code>dtc</code> to dump <code>my_merged_dt.dto</code>.

      <pre class="devsite-terminal">
dtc -O dts -o my_merged_dt.dts my_merged_dt.dtb
</pre>
    </li>
  </ol>


  <h2 id="verifying-DTO-in-p">Verifying DTO in Android {{ androidPVersionNumber }}</h2>


  <p>Android {{ androidPVersionNumber}} requires a Device Tree Blob Overlay
  (DTBO) partition. To add nodes or make changes to the properties in the SoC
  DT, the bootloader must dynamically overlay a device specific DT over
  the SoC DT.</p>


  <h3 id="indicating-applied-overlays">Indicating applied overlays</h3>


  <p>To enable the <a href="/compatibility/vts/">
  Vendor Test Suite (VTS)</a> to assess the correctness of overlay
  application, vendors must add a new kernel command line parameter
  <code>androidboot.dtbo_idx</code> that indicates the overlays selected from
  the DTBO partition. For example, the parameter <code>androidboot.
  dtbo_idx=x,y,z</code> reports <code>x</code>, <code>y</code> and
  <code>z</code> as the zero-based indices of the Device Tree Overlays (DTOs)
  from the DTBO partition applied (in that order) by the bootloader to the base
  Device Tree (DT).</p>


  <p>Overlays can apply to nodes from the main device tree or add new nodes,
  but <strong>cannot</strong> refer to a node added in a previous overlay. This
  restriction is necessary because the overlay application does not merge the
  overlay symbol table with the main DT symbol table (not merging avoids
  conflicts in symbol names and complication of dependencies between
  overlays).</p>


  <h4 id="example-invalid-overlays">Example: Invalid overlays</h4>


  <p>In this example, <code>overlay_2.dts</code> refers to node
  <strong><code>e</code></strong> , which was added by
  <code>overlay_1.dts</code>. After <code>overlay_1</code> is applied to the
  main DT, if an attempt is made to apply <code>overlay_2</code> to the
  resultant DT, the overlay application will fail with an error that the symbol
  <strong><code>e</code></strong> is not present in the symbol table for the
  base DT.</p>


  <table>
    <tr>
      <th width="33%">main.dts</th>

      <th>overlay_1.dts</th>

      <th>overlay_2.dts</th>

    </tr>
    <tr>
      <td>
        <pre>
<strong>[main.dts]</strong>

/dts-v1/;

/ {
  a: a {};
  b: b {};
  c: c {};
};
</pre>
  </td>

        <td>
          <pre>
<strong>[overlay_1.dts]</strong>

/dts-v1/;
/plugin/;

&amp;b { ref1 =  <&a>;
    e: e {
        prop = <0x0a>;
        phandle = <0x04>;
    };
};
</pre>
</td>

      <td>
<pre>
<strong>[overlay_2.dts]</strong>

/dts-v1/;
/plugin/;

/* invalid! */
<font color="red">&amp;e</font> {
    prop = <0x0b>;
};
</pre>
      </td>
    </tr>
  </table>


  <h4 id="example-valid-overlays">Example: Valid overlays</h4>


  <p>In this example, <code>overlay_2.dts</code> refers only to node
  <strong><code>b</code></strong> from the main DTS. When
  <code>overlay_1</code> is applied to the base DT, then followed by the
  application of <code>overlay_2</code>, the value of property
  <strong><code>prop</code></strong> in node <strong><code>e</code></strong>
  (set by <code>overlay_1.dts</code>) is overwritten by the value set by
  <code>overlay_2.dts</code>.</p>


  <table>
    <tr>
      <th width="33%">main.dts</th>

      <th>overlay_1.dts</th>

      <th>overlay_2.dts</th>

    </tr>


    <tr>
      <td>
      <pre>
<strong>[final.dts]</strong>

/dts-v1/;

/ {
  a: a {};
  b: b {};
  c: c {};
};
</pre>
        </td>

      <td>
      <pre>
<strong>[overlay_1.dts]</strong>

/dts-v1/;
/plugin/;


&amp;b { ref1 =  <&a>;
     e {
          prop = <0x0c>;
      };
};
</pre>
        </td>

      <td>
      <pre>
<strong>[overlay_2.dts]</strong>

/dts-v1/;
/plugin/;

/* valid */
<font color="blue">&amp;b</font> { ref1 =  <&c>;
     e {
          prop = <0x0d>;
      };
};
</pre>
      </td>
    </tr>
  </table>


  <h3 id="implementing-the-dtbo-partition">Implementing the DTBO partition</h3>


  <p>To implement the required DTBO partition, ensure the bootloader can do the
  following:</p>


  <ol>
    <li>Identify the board it is running on and select the corresponding
    overlay(s) to be applied.</li>


    <li>Append the <code>androidboot.dtbo_idx</code> parameter to the kernel
    command line.

      <ul>
        <li>The parameter must indicate, the zero-based indices of the DTOs
        from the DTBO partition image it applied to the base DT (in the same
        order).</li>


        <li>The indices must refer to the position of the overlay in the DTBO
        partition.</li>
      </ul>
    </li>
  </ol>


  <p>For details on the structure of the DTBO partition, refer to <a href=
  "https://source.android.com/devices/architecture/dto/">Device Tree
  Overlays</a> on source.android.com.</p>


  <h3 id="validating-the-dtbo-partition">Validating the DTBO partition</h3>


  <p>You can use VTS to verify the following:</p>


  <ul>
    <li>Existence of the kernel command line parameter
    <code>androidboot.dtbo_idx</code> (by checking that <code>Init</code> has
    automatically set up the corresponding <code>ro.boot.dtbo_idx</code> system
    property).</li>


    <li>Validity of the <code>ro.boot.dtbo_idx</code> system property (by
    checking that the property specifies at least one valid DTBO image
    index).</li>


    <li>Validity of the DTBO partition (also verifies the overlays in the DTBO
    partition that are applied to the base DT).</li>


    <li>Additional nodes or property changes in the resulting DT are presented
    to the Linux kernel.</li>
  </ul>


  <p>For example, in the following overlays and final DT, adding
  <code>androidboot.dtbo_idx=5,3</code> to the kernel command line passes
  validation but adding <code>androidboot.dtbo_idx=3,5</code> to the kernel
  command line does not pass validation.</p>


  <table>
    <tr>
      <th width="50%">Overlay DT at index 3</th>


      <th>Overlay DT at index 5</th>

<tr>
<td>
<pre>
<strong>[overlay_1.dts]</strong>

/dts-v1/;
/plugin/;

&amp;c <strong>{ prop = <0xfe>; }</strong>;
</pre>
      </td>

      <td>
      <pre>
<strong>[overlay_2.dts]</strong>

/dts-v1/;
/plugin/;

&amp;c { prop = <0xff>; };
</pre>
        </td>
    </tr>

<table>
    <tr>
   <th>Final DT</th>

   <tr>
   <td>
   <pre>
/dts-v1/;
/ {

	a {
		phandle = <0x1>;
	};

	b {
		phandle = <0x2>;
	};

	c {
		phandle = <0x3>;
		<strong>prop = <0xfe></strong>;
	};

	__symbols__ {
		a = "/a";
		b = "/b";
		c = "/c";
	};
};

  </table>
</body>
</html>
