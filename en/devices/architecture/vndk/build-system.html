<html devsite>
  <head>
    <title>VNDK Build System Support</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<p>
  In Android 8.1 and higher, the build system has built-in VNDK support. When
  VNDK support is enabled, the build system checks the dependencies between
  modules, builds a vendor-specific variant for vendor modules, and
  automatically installs those modules into designated directories.
</p>

<h2 id=vndk-build-support-example>VNDK build support example</h2>

<p>
  In this example, the <code>Android.bp</code> module definition defines a
  library named <code>libexample</code>. The <code>vendor_available</code>
  property indicates framework modules and vendor modules may depend on
  <code>libexample</code>:
</p>

<p>
  <img src="../images/treble_vndk_androidbp.png" alt="libexample vendor_available:true and vndk.enabled:true" />
</p>
<figcaption>
  <strong>Figure 1.</strong> VNDK support enabled
</figcaption>

<p>
  Both the framework executable <code>/system/bin/foo</code> and the vendor
  executable <code>/vendor/bin/bar</code> depend on <code>libexample</code> and
  have <code>libexample</code> in their <code>shared_libs</code> properties.
</p>

<p>
  If <code>libexample</code> is used by both framework modules and vendor
  modules, two variants of <code>libexample</code> are built. The core variant
  (named after <code>libexample</code>) is used by framework modules and the
  vendor variant (named after <code>libexample.vendor</code>) is used by vendor
  modules. The two variants are installed into different directories:
</p>

<ul>
  <li>The core variant is installed into
  <code>/system/lib[64]/libexample.so</code>.</li>
  <li>The vendor variant is installed into
  <code>/system/lib[64]/vndk/libexample.so</code> because
  <code>vndk.enabled</code> is <code>true</code>.</li>
</ul>

<p>
  For more details, see <a href="#module-definition">Module definition</a>.
</p>

<h2 id="configuring-build-support">Configuring build support</h2>

<p>
  To enable full build system support for a product device, add
  <code>BOARD_VNDK_VERSION</code> to <code>BoardConfig.mk</code>:
</p>

<pre class="prettyprint">BOARD_VNDK_VERSION := current</pre>

<p>
  This setting has a <strong>global</strong> effect: When defined in
  <code>BoardConfig.mk</code>, all modules are checked. As there is no mechanism
  to blacklist or whitelist an offending module, you should clean all
  unnecessary dependencies before adding <code>BOARD_VNDK_VERSION</code>. You
  can test and compile a module by setting <code>BOARD_VNDK_VERSION</code> in
  your environment variables:
</p>

<pre class="prettyprint">$ BOARD_VNDK_VERSION=current m module_name.vendor</pre>

<p>When <code>BOARD_VNDK_VERSION</code> is enabled, several default global
  header search paths are <em>removed</em>. These include:
</p>

<ul>
 <li><code>frameworks/av/include</code></li>
 <li><code>frameworks/native/include</code></li>
 <li><code>frameworks/native/opengl/include</code></li>
 <li><code>hardware/libhardware/include</code></li>
 <li><code>hardware/libhardware_legacy/include</code></li>
 <li><code>hardware/ril/include</code></li>
 <li><code>libnativehelper/include</code></li>
 <li><code>libnativehelper/include_deprecated</code></li>
 <li><code>system/core/include</code></li>
 <li><code>system/media/audio/include</code></li>
</ul>

<p>
  If a module depends on the headers from these directories, you must specify
  (explicitly) the dependencies with <code>header_libs</code>,
  <code>static_libs</code>, and/or <code>shared_libs</code>.
</p>

<h2 id="module-definition">Module definition</h2>

<p>
  To build Android with <code>BOARD_VNDK_VERSION</code>, you must revise the
  module definition in either <code>Android.mk</code> or
  <code>Android.bp</code>. This section describes different kinds of module
  definitions, several VNDK-related module properties, and dependency checks
  implemented in the build system.
</p>

<h3 id="vendor-modules">Vendor modules</h3>

<p>
  Vendor modules are vendor-specific executables or shared libraries that
  must be installed into a vendor partition. In <code>Android.bp</code> files,
  vendor modules must set vendor or proprietary property to <code>true</code>.
  In <code>Android.mk</code> files, vendor modules must set
  <code>LOCAL_VENDOR_MODULE</code> or <code>LOCAL_PROPRIETARY_MODULE</code> to
  <code>true</code>.
</p>

<p>
  If <code>BOARD_VNDK_VERSION</code> is defined, the build system disallows
  dependencies between vendor modules and framework modules and emits errors if:
</p>

<ul>
 <li>a module without <code>vendor:true</code> depends on a module with
 <code>vendor:true</code>, or</li>
 <li>a module with <code>vendor:true</code> depends on a
 non-<code>llndk_library</code> module that has neither
 <code>vendor:true</code> nor <code>vendor_available:true</code>.</li>
</ul>

<p>
  The dependency check applies to <code>header_libs</code>,
  <code>static_libs</code>, and <code>shared_libs</code> in
  <code>Android.bp</code>, and to <code>LOCAL_HEADER_LIBRARIES</code>,
  <code>LOCAL_STATIC_LIBRARIES</code> and <code>LOCAL_SHARED_LIBRARIES</code> in
  <code>Android.mk</code>.
</p>

<h3 id="ll-ndk">LL-NDK</h3>

<p>
  LL-NDK shared libraries are shared libraries with stable ABIs. Both framework
  and vendor modules share the same and the latest implementation. For each
  LL-NDK shared library, <code>Android.bp</code> contains a
  <code>llndk_library</code> module definition:
</p>

<pre class="prettyprint">
llndk_library {
    name: "libvndksupport",
    symbol_file: "libvndksupport.map.txt",
}
</pre>

<p>
  This module definition specifies a module name and a symbol file that
  describes the symbols visible to vendor modules. For example:
</p>

<pre class="prettyprint">
LIBVNDKSUPPORT {
  global:
    android_load_sphal_library; # vndk
    android_unload_sphal_library; # vndk
  local:
    *;
};
</pre>

<p>
  Based on the symbol file, the build system generates a stub shared library for
  vendor modules, which link with these libraries when
  <code>BOARD_VNDK_VERSION</code> is enabled. A symbol is included in the stub
  shared library only if it:
</p>

<ul>
 <li>Is not defined in the section end with <code>_PRIVATE</code> or
 <code>_PLATFORM</code>,</li>
 <li>Does not have <code>#platform-only</code> tag, and</li>
 <li>Does not have <code>#introduce*</code> tags or the tag matches with the
 target.</li>
</ul>

<aside class="note">
  <strong>Note</strong>: Vendors must not define their own LL-NDK shared
  libraries because vendor modules won't be able to find them in
  <a href="/setup/build/gsi">Generic System Image (GSI)</a>.
</aside>

<h3 id="vndk">VNDK</h3>

<p>
  In <code>Android.bp</code> files, <code>cc_library</code>,
  <code>cc_library_static</code>, <code>cc_library_shared</code>, and
  <code>cc_library_headers</code> module definitions support three VNDK-related
  properties: <code>vendor_available</code>, <code>vndk.enabled</code>, and
  <code>vndk.support_system_process</code>.
</p>

<p>
  If <code>vendor_available</code> or <code>vndk.enabled</code> is
  <code>true</code>, two variants (<em>core</em> and <em>vendor</em>) may be
  built. The core variant should be treated as a framework module and the vendor
  variant should be treated as a vendor module. If some framework modules depend
  on this module, the core variant is built. If some vendor modules
  depend on this module, the vendor variant is built. The build system enforces
  the following dependency checks:
</p>

<ul>
 <li>The core variant is always framework-only and inaccessible to vendor
 modules.</li>
 <li>The vendor variant is always inaccessible to framework modules.</li>
 <li>All dependencies of the vendor variant, which are specified in
 <code>header_libs</code>, <code>static_libs</code>, and/or
 <code>shared_libs</code>, must be either an <code>llndk_library</code> or a
 module with <code>vendor_available</code> or <code>vndk.enabled</code>.</li>
 <li>If <code>vendor_available</code> is <code>true</code>, the vendor variant
 is accessible to all vendor modules.</li>
 <li>If <code>vendor_available</code> is <code>false</code>, the vendor variant
 is accessible only to other VNDK or VNDK-SP modules (i.e., modules with
 <code>vendor:true</code> cannot link <code>vendor_available:false</code>
 modules).</li>
</ul>

<p>
  The default installation path for <code>cc_library</code> or
  <code>cc_library_shared</code> is determined by the following rules:
</p>

<ul>
 <li>The core variant is installed to <code>/system/lib[64]</code>.</li>
 <li>The vendor variant installation path may vary:
  <ul>
    <li>If <code>vndk.enabled</code> is <code>false</code>, the vendor variant
    is installed into <code>/vendor/lib[64]</code>.</li>
    <li>If <code>vndk.enabled</code> is <code>true</code>,
    <code>vndk.support_system_process</code> can be either <code>true</code> or
    <code>false</code>. If:
  <ul>
    <li><code>false</code>, the vendor variant is installed into
    <code>/system/lib[64]/vndk-${VER}</code>.</li>
    <li><code>true</code>, the vendor variant is installed to
    <code>/system/lib[64]/vndk-sp-${VER}</code>.</li>
    </ul>
   </li>
  </ul>
 </li>
</ul>

<p>
  The table below summarizes how the build system handles the vendor variants:
</p>
<table>
 <tr>
  <th>vendor_available</th>
  <th style="text-align: center">vndk<br>enabled</th>
  <th style="text-align: center">vndk<br>support_same_process</th>
  <th>Vendor variant descriptions</th>
 </tr>

 <tr>
  <td rowspan="4"><code>true</code></td>
  <td rowspan="2"><code>false</code></td>
  <td><code>false</code></td>
  <td>The vendor variants are <em>VND-ONLY</em>. Shared libraries are
  installed into <code>/vendor/lib[64]</code>.</td>
 </tr>

 <tr>
  <td><code>true</code></td>
  <td><em>Invalid</em> (Build error)</td>
 </tr>

 <tr>
  <td rowspan="2"><code>true</code></td>
  <td><code>false</code></td>
  <td>The vendor variants are <em>VNDK</em>. Shared libraries are installed
  to <code>/system/lib[64]/vndk-${VER}</code>.</td>
 </tr>

 <tr>
  <td><code>true</code></td>
  <td>The vendor variants are <em>VNDK-SP</em>. Shared libraries are
  installed to <code>/system/lib[64]/vndk-sp-${VER}</code>.</td>
 </tr>

 <tr>
  <td rowspan="4"><p><code>false</code></p></td>
  <td rowspan="2"><p><code>false</code></p></td>
  <td><p><code>false</code></p></td>
  <td><p>No vendor variants. This module is <em>FWK-ONLY</em>.</p></td>
 </tr>

 <tr>
  <td><code>true</code></td>
  <td><em>Invalid</em> (Build error)</td>
 </tr>

 <tr>
  <td rowspan="2"><code>true</code></td>
  <td><code>false</code></td>
  <td>The vendor variants are <em>VNDK-Private</em>. Shared libraries are
  installed to <code>/system/lib[64]/vndk-${VER}</code>.These must not be
  directly used by vendor modules.</td>
 </tr>

 <tr>
  <td><code>true</code></td>
  <td>The vendor variants are <em>VNDK-SP-Private</em>. Shared libraries are
  installed to <code>/system/lib[64]/vndk-sp-${VER}</code>. These must not be
  directly used by vendor modules.</td>
 </tr>
</table>

<aside class="note">
  <strong>Note</strong>: Vendors may set <code>vendor_available</code> to their
  modules but must not set <code>vndk.enabled</code> or
  <code>vndk.support_system_process</code> because the modules won't be able
  to find them in the <a href="/setup/build/gsi">Generic System Image (GSI)</a>.
</aside>

<h3 id="vndk-extensions">VNDK extensions</h3>

<p>
  VNDK extensions are VNDK shared libraries with additional APIs. Extensions are
  installed to <code>/vendor/lib[64]/vndk[-sp]</code> (without version suffix)
  and override the original VNDK shared libraries at runtime.
</p>

<h4 id="defining-vndk-extensions">Defining VNDK extensions</h4>

<p>
  In Android 9 and higher, <code>Android.bp</code> natively supports VNDK
  extensions. To build a VNDK extension, define another module with a
  <code>vendor:true</code> and an <code>extends</code> property:
</p>

<pre class="prettyprint">
cc_library {
    name: "libvndk",
    vendor_available: true,
    vndk: {
        enabled: true,
    },
}

cc_library {
    name: "libvndk_ext",
    vendor: true,
    vndk: {
        enabled: true,
        extends: "libvndk",
    },
}
</pre>

<p>
  A module with <code>vendor:true</code>, <code>vndk.enabled:true</code>, and
  <code>extends</code> properties defines the VNDK extension:</p>

<ul>
 <li>The <code>extends</code> property must specify a base VNDK shared library
  name (or VNDK-SP shared library name).</li>
  <li>VNDK extensions (or VNDK-SP extensions) are named after the base module
  names from which they extend. For example, the output binary of
  <code>libvndk_ext</code> is <code>libvndk.so</code> instead of
  <code>libvndk_ext.so</code>.</li>
  <li>VNDK extensions are installed into <code>/vendor/lib[64]/vndk</code>.</li>
  <li>VNDK-SP extensions are installed into
  <code>/vendor/lib[64]/vndk-sp</code>.</li>
  <li>The base shared libraries must have both <code>vndk.enabled:true</code>
  and <code>vendor_available:true</code>.</li>
</ul>

<p>
  A VNDK-SP extension must extend from a VNDK-SP shared library
  (<code>vndk.support_system_process</code> must be equal):
</p>

<pre class="prettyprint">
cc_library {
    name: "libvndk_sp",
    vendor_available: true,
    vndk: {
        enabled: true,
        support_system_process: true,
    },
}

cc_library {
    name: "libvndk_sp_ext",
    vendor: true,
    vndk: {
        enabled: true,
        extends: "libvndk_sp",
        support_system_process: true,
    },
}
</pre>

<p>
  VNDK extensions (or VNDK-SP extensions) may depend on other vendor shared
  libraries:
</p>

<pre class="prettyprint">
cc_library {
    name: "libvndk",
    vendor_available: true,
    vndk: {
        enabled: true,
    },
}

cc_library {
    name: "libvndk_ext",
    vendor: true,
    vndk: {
        enabled: true,
        extends: "libvndk",
    },
    shared_libs: [
        "libvendor",
    ],
}

cc_library {
    name: "libvendor",
    vendor: true,
}
</pre>

<aside class="note">
  <strong>Note:</strong> Similar to SP-HAL-Dep, VNDK-SP extensions and their
  dependencies (including vendor libraries) must be labeled as
  <code>same_process_hal_file</code> in sepolicy.
</aside>

<h4 id="using-vndk-extensions">Using VNDK extensions</h4>

<p>
  If a vendor module depends on additional APIs defined by VNDK extensions, the
  module must specify the name of the VNDK extension in its
  <code>shared_libs</code> property:
</p>

<pre class="prettyprint">
// A vendor shared library example
cc_library {
    name: "libvendor",
    vendor: true,
    shared_libs: [
        "libvndk_ext",
    ],
}

// A vendor executable example
cc_binary {
    name: "vendor-example",
    vendor: true,
    shared_libs: [
        "libvndk_ext",
    ],
}
</pre>

<p>
  If a vendor module depends on VNDK extensions, those VNDK extensions are
  installed to <code>/vendor/lib[64]/vndk[-sp]</code> automatically. If a module
  no longer depends on a VNDK extension, add a clean step to
  <code>CleanSpec.mk</code> to remove the shared library. For example:
</p>

<pre class="prettyprint">
$(call add-clean-step, rm -rf $(TARGET_OUT_VENDOR)/lib/libvndk.so)
</pre>

<h3 id="conditional-compilation">Conditional compilation</h3>

<p>
  This section describes how to deal with the <em>subtle differences</em> (e.g.
  adding or removing a feature from one of the variants) between the following
  three VNDK shared libraries:
</p>

<ul>
  <li>core variant (e.g. <code>/system/lib[64]/libexample.so</code>)</li>
  <li>vendor variant (e.g.
  <code>/system/lib[64]/vndk[-sp]-${VER}/libexample.so</code>)</li>
  <li>VNDK extension (e.g. <code>/vendor/lib[64]/vndk[-sp]/libexample.so</code>)
  </li>
</ul>

<h4 id="conditional-cflags">Conditional compiler flags</h4>

<p>
  The Android build system defines <code>__ANDROID_VNDK__</code> for vendor
  variants (including VNDK extensions) by default. You may guard the code
  with the C preprocessor guards:
</p>

<pre class="prettyprint">
void all() { }

#if !defined(__ANDROID_VNDK__)
void framework_only() { }
#endif

#if defined(__ANDROID_VNDK__)
void vndk_only() { }
#endif
</pre>

<p>
  In addition to <code>__ANDROID_VNDK__</code>, different <code>cflags</code> or
  <code>cppflags</code> may be specified in <code>Android.bp</code>. The
  <code>cflags</code> or <code>cppflags</code> specified in
  <code>target.vendor</code> is specific to the vendor variant. For example, the
  following code example is the <code>Android.bp</code> module definition for
  <code>libexample</code> and <code>libexample_ext</code>:
</p>

<pre class="prettyprint">
cc_library {
    name: "libexample",
    srcs: ["example.c"],
    vendor_available: true,
    vndk: {
        enabled: true,
    },
    target: {
        vendor: {
            cflags: ["-DLIBEXAMPLE_ENABLE_VNDK=1"],
        },
    },
}

cc_library {
    name: "libexample_ext",
    srcs: ["example.c"],
    vendor: true,
    vndk: {
        enabled: true,
        extends: "libexample",
    },
    cflags: [
        "-DLIBEXAMPLE_ENABLE_VNDK=1",
        "-DLIBEXAMPLE_ENABLE_VNDK_EXT=1",
    ],
}
</pre>

<p>
  Code listing of <code>example.c</code>:
</p>

<pre class="prettyprint">
void all() { }

#if !defined(LIBEXAMPLE_ENABLE_VNDK)
void framework_only() { }
#endif

#if defined(LIBEXAMPLE_ENABLE_VNDK)
void vndk() { }
#endif

#if defined(LIBEXAMPLE_ENABLE_VNDK_EXT)
void vndk_ext() { }
#endif
</pre>

<p>
  Exported symbols for each variant:
</p>

<table>
 <tr>
  <th>Installation path</th>
  <th>Exported symbols</th>
 </tr>

 <tr>
  <td><code>/system/lib[64]/libexample.so</code></td>
  <td><code>all</code>, <code>framework_only</code></td>
 </tr>

 <tr>
  <td><code>/system/lib[64]/vndk-${VER}/libexample.so</code></td>
  <td><code>all</code>, <code>vndk</code></td>
 </tr>

 <tr>
  <td><code>/vendor/lib[64]/vndk/libexample.so</code></td>
  <td><code>all</code>, <code>vndk</code>, <code>vndk_ext</code></td>
 </tr>
</table>

<p>
  The VNDK ABI compliance checker compares the ABI of VNDK and VNDK
  extensions to the ABI dumps under <code>prebuilts/abi-dumps/vndk</code>:
</p>

<ul>
  <li>Symbols exported by original VNDK shared libraries must be identical to
  (not the supersets of) the symbols defined in ABI dumps.</li>
  <li>Symbols exported by VNDK extensions must be supersets of the symbols
  defined in ABI dumps.</li>
</ul>

<h4 id="excluding">Excluding source files or shared libs</h4>

<p>
  To exclude source files from the vendor variant, add them to the
  <code>exclude_srcs</code> property. Similarly, to ensure shared libraries are
  not linked with the vendor variant, add those libraries to the
  <code>exclude_shared_libs</code> property. For example:
</p>

<pre class="prettyprint">
cc_library {
    name: "libcond_exclude_example",
    srcs: ["fwk.c", "both.c"],
    shared_libs: ["libfwk_only", "libboth"],
    target: {
        vendor: {
            exclude_srcs: ["fwk.c"],
            exclude_shared_libs: ["libfwk_only"],
        },
    },
}
</pre>

<p>
  In this example, the core variant of <code>libcond_exclude_example</code>
  includes the code from <code>fwk.c</code> and <code>both.c</code> and depends
  on the shared libraries <code>libfwk_only</code> and <code>libboth</code>. The
  vendor variant of <code>libcond_exclude_example</code> includes only the code
  from <code>both.c</code> because <code>fwk.c</code> is excluded by the
  <code>exclude_srcs</code> property. Similarly,
  <code>libcond_exclude_example</code> depends only on the shared library
  <code>libboth</code> because <code>libfwk_only</code> is excluded by the
  <code>exclude_shared_libs</code> property.
</p>

<h3 id="product-packages">Product packages</h3>

<p>
  In the Android build system, the variable <code>PRODUCT_PACKAGES</code>
  specifies the executables, shared libraries, or packages that should be
  installed into the device. The transitive dependencies of the specified
  modules are implicitly installed into the device as well.
</p>

<p>
  If <code>BOARD_VNDK_VERSION</code> is enabled, modules with
  <code>vendor_available</code> or <code>vndk.enabled</code> get special
  treatment. If a framework module depends on a module with
  <code>vendor_available</code> or <code>vndk.enabled</code>, the core variant
  is included in the transitive installation set. Similarly, if a vendor module
  depends on a module with <code>vendor_available</code> or
  <code>vndk.enabled</code>, the vendor variant is included in the transitive
  installation set.
</p>

<p>
  When the dependencies are invisible to the build system (e.g. shared libraries
  that may be opened with <code>dlopen()</code> in runtime), you should specify
  the module names in <code>PRODUCT_PACKAGES</code> to install those modules
  explicitly.
</p>

<p>
  If a module has <code>vendor_available</code> or <code>vndk.enabled</code>,
  the module name stands for its core variant. To explicitly specify the
  vendor variant in <code>PRODUCT_PACKAGES</code>, append a <code>.vendor</code>
  suffix to the module name. For example:
</p>

<pre class="prettyprint">
cc_library {
    name: "libexample",
    srcs: ["example.c"],
    vendor_available: true,
}
</pre>

<p>
  In this example, <code>libexample</code> stands for
  <code>/system/lib[64]/libexample.so</code> and <code>libexample.vendor</code>
  stands for <code>/vendor/lib[64]/libexample.so</code>. To install
  <code>/vendor/lib[64]/libexample.so</code>, add <code>libexample.vendor</code>
  to <code>PRODUCT_PACKAGES</code>:
</p>

<pre class="prettyprint">PRODUCT_PACKAGES += libexample.vendor</pre>

  </body>
</html>
