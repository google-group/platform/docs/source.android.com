<html devsite>
  <head>
    <title>Android Security Bulletin—May 2018</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p><em>Published May 7, 2018 | Updated July 11, 2018</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2018-05-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705">Check & update
your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.
</p>
<p>
The most severe of these issues is a critical security vulnerability in Media
framework that could enable a remote attacker using a specially crafted file to
execute arbitrary code within the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the <a href="#mitigations">Android and Google Play
Protect mitigations</a> section for details on the
<a href="/security/enhancements/index.html">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2018-05-01.html">May
2018 Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.
</p>


<h2 id="mitigations">Android and Google service mitigations</h2>

<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/index.html">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect">Google Play Protect</a>.
These capabilities reduce the likelihood that security vulnerabilities
could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect">Google Play Protect</a>
and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms">Google Mobile Services</a>, and is
especially important for users who install apps from outside of Google
Play.</li>
</ul>

<h2 id="2018-05-01-details">2018-05-01 security patch level vulnerability details</h2>

<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-05-01 patch level. Vulnerabilities are
grouped under the component that they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, like the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="android-runtime">Android runtime</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
to access data normally accessible only to locally installed applications with
permissions.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-13309</td>
    <td><a href="https://android.googlesource.com/platform/external/conscrypt/+/c26e60713035b52e123bdcc2fe5d69eb94a374f7">
    A-73251618</a></td>
    <td>ID</td>
    <td>High</td>
    <td>8.1</td>
  </tr>
</table>


<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass user interaction requirements in order to gain access to
additional permissions.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-13310</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/57bc6ca6dda12ef7925c69a75cbcdf3df05067fc">A-71992105</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2017-13311</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/2950276f61220e00749f8e24e0c773928fefaed8">A-73252178</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass user interaction requirements in order to gain access to
additional permissions.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-13312</td>
    <td>A-73085795<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-13313</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/4b3b618144050d80dbaa0228797b021d5df5e919">A-74114680</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass user interaction requirements in order to gain access to
additional permissions.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-13314</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/ef1335ebdf6862c6a30686603c7ee549dbb7b359">A-63000005</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2017-13315</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/35bb911d4493ea94d4896cc42690cab0d4dbb78f">A-70721937</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h2 id="2018-05-05-details">2018-05-05 security patch level vulnerability details</h2>

<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-05-05 patch level. Vulnerabilities are
grouped under the component that they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
like the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass operating system protections that isolate application
data from other applications.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-16643</td>
    <td>A-69916367<br />
        <a href="https://github.com/torvalds/linux/commit/a50829479f58416a013a4ccca791336af3c584c7">
Upstream kernel</a></td>
    <td>ID</td>
    <td>High</td>
    <td>USB driver</td>
  </tr>
</table>


<h3 id="nvidia-components">NVIDIA components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of the TEE.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-6289</td>
    <td>A-72830049<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>Critical</td>
    <td>TEE</td>
  </tr>
  <tr>
    <td>CVE-2017-5715</td>
    <td>A-71686116<a href="#asterisk">*</a></td>
    <td>ID</td>
    <td>High</td>
    <td>TLK</td>
  </tr>
  <tr>
    <td>CVE-2017-6293</td>
    <td>A-69377364<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>NVIDIA Tegra X1 TZ</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>
<p>The most severe vulnerability in this section could enable a proximate
attacker using a specially crafted file to execute arbitrary code within the
context of a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-3580</td>
    <td>A-72957507<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-2.0/commit/?id=55dcc8d5ae9611e772a7ba572755192cf5415f04">
QC-CR#2149187</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2018-5841</td>
    <td>A-72957293<br />
        <a href="https://source.codeaurora.org/quic/la/kernel/msm-4.9/commit/?id=c3b82b337b1bbe854d330fc248b389b1ff11c248">
QC-CR#2073502</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>DCC</td>
  </tr>
  <tr>
    <td>CVE-2018-5850</td>
    <td>A-72957135<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=58b04ed29f44a8b49e0fa2f22b2a0ecf87324243">
QC-CR#2141458</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2018-5846</td>
    <td>A-71501683<br />
        <a href="https://source.codeaurora.org/quic/la/kernel/msm-4.9/commit/?id=616bb99045d728088b445123dfdff8eadcc27090">
QC-CR#2120063</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Radio internet packet accelerator</td>
  </tr>
  <tr>
    <td>CVE-2018-5845</td>
    <td>A-71501680<br />
        <a href="https://source.codeaurora.org/quic/la/kernel/msm-4.9/commit/?id=a989f5c6398175cc7354b9cdf0424449f8dd9860">
QC-CR#2119081</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>GPU</td>
  </tr>
  <tr>
    <td>CVE-2018-5840</td>
    <td>A-71501678<br />
        <a href="https://source.codeaurora.org/quic/la/kernel/msm-4.9/commit/?id=5d1c951ee0e3b06f96736eed25869bbfaf77efbc">
QC-CR#2052024</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>GPU</td>
  </tr>
  <tr>
    <td>CVE-2017-18154</td>
    <td>A-62872238<a href="#asterisk">*</a><br />
        QC-CR#2109325</td>
    <td>EoP</td>
    <td>High</td>
    <td>Libgralloc</td>
  </tr>
  <tr>
    <td>CVE-2018-3578</td>
    <td>A-72956999<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=0f22d9370eb848ee751a94a7ff472c335e934890">
QC-CR#2145573</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2018-3565</td>
    <td>A-72957234<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=ab91234d52984a86a836561578c8ab85cf0b5f2f">
QC-CR#2138555</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2017-13077</td>
    <td>A-77481464<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2018-3562</td>
    <td>A-72957526<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-2.0/commit/?id=7e82edc9f1ed60fa99dd4da29f91c4ad79470d7e">QC-CR#2147955</a>
        [<a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=70a5ee609ef6199dfcd8cce6198edc6f48b16bec">2</a>]</td>
    <td>DoS</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
</table>

<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>
This section answers common questions that may occur after reading this
bulletin.</p>

<p><strong>1. How do I determine if my device is updated to address these issues?
</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">Check
and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2018-05-01 or later address all issues associated
with the 2018-05-01 security patch level.</li>
<li>Security patch levels of 2018-05-05 or later address all issues associated
with the 2018-05-05 security patch level and all previous patch levels.</li>
</ul>
<p>
Device manufacturers that include these updates should set the patch string
level to:
</p>
<ul>
<li>[ro.build.version.security_patch]:[2018-05-01]</li>
<li>[ro.build.version.security_patch]:[2018-05-05]</li>
</ul>
<p>
<strong>2. Why does this bulletin have two security patch levels?</strong>
</p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2018-05-01 security patch level must include all issues
associated with that security patch level, as well as fixes for all issues
reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2018-05-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Nexus devices available from the <a
href="https://developers.google.com/android/nexus/drivers">Google Developer
site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device/partner security bulletins, such as the Pixel&hairsp;/&hairsp;Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required in order to declare the latest security patch level on Android devices.
Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for declaring
a security patch level. Android device and chipset manufacturers are encouraged
to document the presence of other fixes on their devices through their own security
websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html">LGE</a>, or
<a href="/security/bulletin/pixel/">Pixel&hairsp;/&hairsp;Nexus</a>
security bulletins.
</p>
<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>May 7, 2018</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>May 9, 2018</td>
   <td>Bulletin revised to include AOSP links.</td>
  </tr>
  <tr>
   <td>1.2</td>
   <td>July 11, 2018</td>
   <td>Removed CVE-2017-5754 from 2018-05-05 SPL.</td>
  </tr>
</table>
</body>
</html>
