<html devsite>
  <head>
    <title>Android Security Bulletin—January 2018</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published January 2, 2018 | Updated January 29, 2018</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2018-01-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705">Check and update
your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before publication.
Source code patches for these issues have been released to the Android Open
Source Project (AOSP) repository and linked from this bulletin. This bulletin
also includes links to patches outside of AOSP.</p>
<p>
The most severe of these issues is a critical security vulnerability in Media
framework that could enable a remote attacker using a specially crafted file to
execute arbitrary code within the context of a privileged process. The <a
href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the <a href="#mitigations">Android
and Google Play Protect mitigations</a> section for details on the <a
href="/security/enhancements/index.html">Android
security platform protections</a> and Google Play Protect, which improve the
security of the Android platform.
</p>
<p>
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the January 2018 Pixel&hairsp;/&hairsp;Nexus
Security Bulletin.
</p>
<h2 id="announcements">Announcements</h2>
<aside class="note">
<p><strong>Note:</strong> CVE-2017-5715, CVE-2017-5753, and CVE-2017-5754, a set of
vulnerabilities related to speculative execution in processors, have been
publicly disclosed. Android is unaware of any successful reproduction of these
vulnerabilities that would allow unauthorized information disclosure on any
ARM-based Android device.
</p>
<p>
To provide additional protection, the update for CVE-2017-13218 included in this
bulletin reduces access to high-precision timers, which helps limits side
channel attacks (such as CVE-2017-5715, CVE-2017-5753, and CVE-2017-5754)
of all known variants of ARM processors.
</p>
<p>We encourage Android users to accept available security updates to their
devices. See the
<a href="https://security.googleblog.com/2018/01/todays-cpu-vulnerability-what-you-need.html">Google
security blog</a> for more details.</p>
</aside>
<p>
We have launched a new <a href="/security/bulletin/pixel/">Pixel / Nexus
Security Bulletin</a>, which contains information on additional security
vulnerabilities and functional improvements that are addressed on Pixel and
Nexus devices. Android device manufacturers may choose to address these issues
on their devices. See <a href="#common-questions-and-answers">Common questions
and answers</a> for additional information.
</p>
<h2 id="mitigations">Android and Google service mitigations</h2>
<p>
This is a summary of the mitigations provided by the <a
href="/security/enhancements/index.html">Android
security platform</a> and service protections such as <a
href="https://www.android.com/play-protect">Google Play Protect</a>. These
capabilities reduce the likelihood that security vulnerabilities could be
successfully exploited on Android.
</p><ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.
<li>The Android security team actively monitors for abuse through <a
href="https://www.android.com/play-protect">Google Play Protect</a> and warns
users about <a
href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms">Google Mobile Services</a>, and is
especially important for users who install apps from outside of Google
Play.</li></ul>
<h2 id="2018-01-01-security-patch-level—vulnerability-details">2018-01-01
security patch level—Vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-01-01 patch level. Vulnerabilities are
grouped under the component that they affect. There is a description of the
issue and a table with the CVE, associated references, <a href="#type">type
of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, like the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="android-runtime">Android runtime</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
to bypass user interaction requirements in order to gain access to additional
permissions.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-13176</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/4afa0352d6c1046f9e9b67fbf0011bcd751fcbb5">
        A-68341964</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2017-13177</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/b686bb2df155fd1f55220d56f38cc0033afe278c">
   A-68320413</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13178</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/646a18fef28d19ba5beb6a2e1c00ac4c2663a10b">
   A-66969281</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13179</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/47d4b33b504e14e98420943f771a9aecd6d09516">
   A-66969193</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13180</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/cf1e36f93fc8776e3a8109149424babeee7f8382">
   A-66969349</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13181</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/base/+/d64e9594d3d73c613010ca9fafc7af9782e9225d">
   A-67864232</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13182</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/f1652e1b9f1d2840c79b6bf784d1befe40f4799e">
   A-67737022</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13184</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/native/+/16392a119661fd1da750d4d4e8e03442578bc543">
   A-65483324</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-0855</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/d7d6df849cec9d0a9c1fd0d9957a1b8edef361b7">
   A-64452857</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
   <td>CVE-2017-13191</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/f5b2fa243b4c45a4cd885e85f49ae548ab88c264">
   A-64380403</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13192</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/52ca619511acbd542d843df1f92f858ce13048a5">
   A-64380202</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13193</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/b3f31e493ef6fa886989198da9787807635eaae2">
   A-65718319</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13195</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/066e3b1f9c954d95045bc9d33d2cdc9df419784f">
   A-65398821</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13196</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/f5b2fa243b4c45a4cd885e85f49ae548ab88c264">
   A-63522067</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13197</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/0a714d3a14d256c6a5675d6fbd975ca26e9bc471">
   A-64784973</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13199</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/base/+/42b2e419b48a26d2ba599d87e3a2a02c4aa625f4">
   A-33846679</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>8.0, 8.1</td>
  </tr>
</table>


<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2017-13208</td>
   <td><a href="https://android.googlesource.com/platform/system/core/+/b71335264a7c3629f80b7bf1f87375c75c42d868">
   A-67474440</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13209</td>
   <td><a href="https://android.googlesource.com/platform/system/libhidl/+/a4d0252ab5b6f6cc52a221538e1536c5b55c1fa7">
   A-68217907</a>
[<a href="https://android.googlesource.com/platform/system/tools/hidl/+/8539fc8ac94d5c92ef9df33675844ab294f68d61">2</a>]
[<a href="https://android.googlesource.com/platform/system/hwservicemanager/+/e1b4a889e8b84f5c13b76333d4de90dbe102a0de">3</a>]</td>
   <td>EoP</td>
   <td>High</td>
   <td>8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13210</td>
   <td><a href="https://android.googlesource.com/platform/system/media/+/e770e378dc8e2320679272234285456ca2244a62">
   A-67782345</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2017-13211</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/181144a50114c824cfe3cdfd695c11a074673a5e">
   A-65174158</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>8.0</td>
  </tr>
</table>

<h2 id="2018-01-05-security-patch-level—vulnerability-details">2018-01-05
security patch level—Vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-01-05 patch level. Vulnerabilities are
grouped under the component that they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
like the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="htc-components">HTC components</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
to cause a denial of service in a critical system process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-13214</td>
    <td>A-38495900<a href="#asterisk">*</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>Hardware HEVC decoder</td>
  </tr>
</table>


<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-14497</td>
    <td>A-66694921<br />
        <a href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=edbd58be15a957f6a760c4a514cd475217eb97fd">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>TCP packet processing</td>
  </tr>
  <tr>
    <td>CVE-2017-13215</td>
    <td>A-64386293<br />
        <a href="https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/commit/?h=v3.18.78&id=36c84b22ac8aa041cbdfbe48a55ebb32e3521704">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Skcipher</td>
  </tr>
  <tr>
    <td>CVE-2017-13216</td>
    <td>A-66954097<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Ashmem</td>
  </tr>
  <tr>
    <td>CVE-2017-13218</td>
    <td>A-68266545<a href="#asterisk">*</a></td>
    <td>ID</td>
    <td>High</td>
    <td>High-precision timers</td>
  </tr>
</table>


<h3 id="lg-components">LG components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-13217</td>
    <td>A-68269077<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Bootloader</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-13183</td>
    <td>A-38118127</td>
    <td>EoP</td>
    <td>High</td>
    <td>8.1</td>
  </tr>
</table>


<h3 id="nvidia-components">NVIDIA components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-0869</td>
    <td>A-37776156<a href="#asterisk">*</a><br />
        N-CVE-2017-0869</td>
    <td>EoP</td>
    <td>High</td>
    <td>Nvidia driver</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-15849</td>
    <td>A-66937641<br />
        <a href="https://source.codeaurora.org/quic/la/platform/hardware/qcom/display/commit/?id=0a59679b954c02b8996">
QC-CR#2046572</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Display</td>
  </tr>
  <tr>
    <td>CVE-2017-11069</td>
    <td>A-65468974<br />
        <a href="https://source.codeaurora.org/quic/la/kernel/lk/commit/?id=daf1fbae4be7bd669264a7907677250ff2a1f89b">
QC-CR#2060780</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Bootloader</td>
  </tr>
</table>


<h3 id="qualcomm-closed-source-components">Qualcomm closed-source
components</h3>
<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm AMSS security bulletin or security
alert. The severity assessment of these issues is provided directly by
Qualcomm.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-14911</td>
    <td>A-62212946<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14906</td>
    <td>A-32584150<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14912</td>
    <td>A-62212739<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14913</td>
    <td>A-62212298<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14915</td>
    <td>A-62212632<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2013-4397</td>
    <td>A-65944893<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-11010</td>
    <td>A-66913721<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
</table>


<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>
This section answers common questions that may occur after reading this
bulletin.
</p>
<p>
<strong>1. How do I determine if my device is updated to address these issues?
</strong>
</p>
<p>
To learn how to check a device's security patch level, see <a
href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">Check
& update your Android version</a>.
</p>
<ul>
<li>Security patch levels of 2018-01-01 or later address all issues associated
with the 2018-01-01 security patch level.</li>
<li>Security patch levels of 2018-01-05 or later address all issues associated
with the 2018-01-05 security patch level and all previous patch levels.</li>
</ul>
<p>
Device manufacturers that include these updates should set the patch string
level to:
</p>
<ul>
<li>[ro.build.version.security_patch]:[2018-01-01]</li>
<li>[ro.build.version.security_patch]:[2018-01-05]</li>
</ul>
<p>
<strong>2. Why does this bulletin have two security patch levels?</strong>
</p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2018-01-01 security patch level must include all issues
associated with that security patch level, as well as fixes for all issues
reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2018-01-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Nexus devices available from the <a
href="https://developers.google.com/android/nexus/drivers">Google Developer
site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device/partner security bulletins, such as the Pixel / Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required in order to declare the latest security patch level on Android devices.
Additional security vulnerabilities that are documented in the device/partner
security bulletins are not required for declaring a security patch level.
Android device and chipset manufacturers are encouraged to document the presence
of other fixes on their devices through their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb">Samsung</a>, <a
href="https://lgsecurity.lge.com/security_updates.html">LGE</a>, or <a
href="/security/bulletin/pixel/">Pixel&hairsp;/&hairsp;Nexus</a>
security bulletins.
</p>
<h2 id="versions">Versions</h2>
<table>
  <col width="15%">
  <col width="25%">
  <col width="60%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>January 2, 2018</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>January 3, 2018</td>
   <td>Bulletin updated with announcement about CVE-2017-13218.</td>
  </tr>
  <tr>
   <td>1.2</td>
   <td>January 5, 2018</td>
   <td>Bulletin revised to include AOSP links.</td>
  </tr>
  <tr>
    <td>1.3</td>
    <td>January 29, 2018</td>
    <td>CVE-2017-13225 moved to <a href="/security/bulletin/pixel/">Pixel / Nexus
      Security Bulletin</a>.</td>
</table>
</body></html>
