<html devsite>
  <head>
    <title>Android Security Bulletin—August 2017</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published August 7, 2017 | Updated August 23, 2017</em></p>

<p>The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of August 05, 2017 or later
address all of these issues. Refer to the <a
href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">Pixel
and Nexus update schedule</a> to learn how to check a device's security patch
level.</p>

<p>Partners were notified of the issues described in the bulletin at least a month
ago. Source code patches for these issues have been released to the Android Open
Source Project (AOSP) repository and linked from this bulletin. This bulletin also
includes links to patches outside of AOSP.</p>

<p>The most severe of these issues is a critical security vulnerability in media
framework that could enable a remote attacker using a specially crafted file to
execute arbitrary code within the context of a privileged process. The <a
href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.</p>

<p>We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the <a href="#mitigations">Android and Google Play
Protect mitigations</a> section for details on the <a
href="/security/enhancements/index.html">Android
security platform protections</a> and Google Play Protect, which improve the
security of the Android platform.</p>

<p>We encourage all customers to accept these updates to their devices.</p>

<p class="note"><strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the <a
href="#google-device-updates">Google device updates</a> section.</p>

<h2 id="announcements">Announcements</h2>
<ul>
  <li>This bulletin has two security patch level strings to provide Android
  partners with the flexibility to more quickly fix a subset of vulnerabilities
  that are similar across all Android devices. See <a
  href="#questions">Common questions and answers</a> for
  additional information:
   <ul>
     <li><strong>2017-08-01</strong>: Partial security patch level string. This
    security patch level string indicates that all issues associated with 2017-08-01
    (and all previous security patch level strings) are addressed.</li>
     <li><strong>2017-08-05</strong>: Complete security patch level string. This
    security patch level string indicates that all issues associated with 2017-08-01
    and 2017-08-05 (and all previous security patch level strings) are
    addressed.</li>
   </ul>
  </li>
</ul>

<h2 id="mitigations">Android and Google Play Protect mitigations</h2>

<p>This is a summary of the mitigations provided by the <a
href="/security/enhancements/index.html">Android
security platform</a> and service protections such as <a
href="https://www.android.com/play-protect">Google Play Protect</a>. These
capabilities reduce the likelihood that security vulnerabilities could be
successfully exploited on Android.</p>
<ul>
  <li>Exploitation for many issues on Android is made more difficult by
  enhancements in newer versions of the Android platform. We encourage all users
  to update to the latest version of Android where possible.</li>
  <li>The Android security team actively monitors for abuse through <a
  href="https://www.android.com/play-protect">Google Play Protect</a> and warns
  users about
  <a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
  Harmful Applications</a>. Google Play Protect is enabled by default on devices
  with <a href="http://www.android.com/gms">Google Mobile Services</a>, and is
  especially important for users who install apps from outside of Google Play.</li>
</ul>

<h2 id="2017-08-01-details">2017-08-01 security patch level—Vulnerability details</h2>

<p>In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2017-08-01 patch level. Vulnerabilities are
grouped under the component that they affect. There is a description of the
issue and a table with the CVE, associated references, <a
href="#type">type of vulnerability</a>, <a
href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, like the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.</p>

<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application using a specially crafted file to execute arbitrary code within the
context of a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
   <th>CVE</th>
   <th>References</th>
   <th>Type</th>
   <th>Severity</th>
   <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2017-0712</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/opt/net/wifi/+/e8beda2579d277fb6b27f1792c4ed45c136ee15a">A-37207928</a></td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
</table>
<h3 id="libraries">Libraries</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
an unprivileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
   <th>CVE</th>
   <th>References</th>
   <th>Type</th>
   <th>Severity</th>
   <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2017-0713</td>
   <td><a href="https://android.googlesource.com/platform/external/sfntly/+/a642e3543a4ffdaaf1456768968ae05a205ed4f4">A-32096780</a>
   [<a href="https://android.googlesource.com/platform/external/sfntly/+/fa6053736808e999483ca0a21fbe16a3075cf2c8">2</a>]</td>
   <td>RCE</td>
   <td>High</td>
   <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
</table>

<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of a
privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
   <th>CVE</th>
   <th>References</th>
   <th>Type</th>
   <th>Severity</th>
   <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2017-0714</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/26557d832fde349721500b47d51467c046794ae9">A-36492637</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0715</td>
   <td><a href="https://android.googlesource.com/platform/external/libavc/+/676c26e6a2ab3b75ab6e8fdd984547219fc1ceb5">A-36998372</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0716</td>
   <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/d5f52646974c29e6b7d51230b8ddae0c0a9430dc">A-37203196</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0718</td>
   <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/327496c59fb280273e23353061980dd72b07de1f">A-37273547</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0719</td>
   <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/f0afcf1943a1844e2a82ac3c5ab4d49427102cd1">A-37273673</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0720</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/84732aaa4255955b8fefc39efee9b369181a6861">A-37430213</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0721</td>
   <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/08a0d1ab6277770babbedab6ce7e7e2481869ad0">A-37561455</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0722</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/26557d832fde349721500b47d51467c046794ae9">A-37660827</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0723</td>
   <td><a href="https://android.googlesource.com/platform/external/libavc/+/fe5ade4c86e2f5a86eab6c6593981d02f4c1710b">A-37968755</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0745</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/b6ec3bbba36a3816a936f1e31984529b875b3618">A-37079296</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0724</td>
   <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/08a0d1ab6277770babbedab6ce7e7e2481869ad0">A-36819262</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0725</td>
   <td><a href="https://android.googlesource.com/platform/external/skia/+/59372f5412036ce87285e91fd2dd53e37ff990e4">A-37627194</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0726</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/8995285da14e1303ae7357bf8162cbec13e65b68">A-36389123</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0727</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/native/+/39dfabd43cbe57f92b771c0110a5c2d976b6c44f">A-33004354</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0728</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/314a0d038e6ae24bef80fff0b542965aed78ae96">A-37469795</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0729</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/96974782cd914120272a026ebc263dd38098f392">A-37710346</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0730</td>
   <td><a href="https://android.googlesource.com/platform/external/libavc/+/efd28f6c36d40d0a8dd92f344e1d9a992b315c36">A-36279112</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0731</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/c10183960909f074a265c134cd4087785e7d26bf">A-36075363</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0732</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/bf153fbedc2333b14e90826d22f08d10e832db29">A-37504237</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0733</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/base/+/6f357fd589e115a74aae25b1ac325af6121cdadf">A-38391487</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0734</td>
   <td><a href="https://android.googlesource.com/platform/external/libavc/+/f6650b34ef27d6da8cdccd42e0f76fe756a9375b">A-38014992</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0735</td>
   <td><a href="https://android.googlesource.com/platform/external/libavc/+/fe5ade4c86e2f5a86eab6c6593981d02f4c1710b">A-38239864</a>
   [<a href="https://android.googlesource.com/platform/external/libavc/+/490bed0e7dce49296d50bb519348c6a87de8a8ef">2</a>]</td>
   <td>DoS</td>
   <td>High</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0736</td>
   <td><a href="https://android.googlesource.com/platform/external/libavc/+/dfbbb54f14f83d45ad06a91aa76ed8260eb795d5">A-38487564</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0687</td>
   <td><a href="https://android.googlesource.com/platform/external/libavc/+/17b46beeae7421f76d894f14696ba4db9023287c">A-35583675</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0737</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/77e075ddd6d8cae33832add5225ee8f8c77908f0">A-37563942</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0805</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/77e075ddd6d8cae33832add5225ee8f8c77908f0">A-37237701</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0738</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/1d919d737b374b98b900c08c9d0c82fe250feb08">A-37563371</a>
   [<a href="https://android.googlesource.com/platform/hardware/qcom/audio/+/234848dd6756c5d636f5a103e51636d60932983c">2</a>]</td>
   <td>ID</td>
   <td>Moderate</td>
   <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
   <td>CVE-2017-0739</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/e6e353a231f746743866d360b88ef8ced367bcb1">A-37712181</a></td>
   <td>ID</td>
   <td>Moderate</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
</table>
<h2 id="2017-08-05-details">2017-08-05
security patch level—Vulnerability details</h2>
<p>In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2017-08-05 patch level. Vulnerabilities are
grouped under the component that they affect and include details such as the
CVE, associated references, <a href="#type">type of
vulnerability</a>, <a
href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
like the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.</p>

<h3 id="broadcom-components">Broadcom components</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
an unprivileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
   <th>CVE</th>
   <th>References</th>
   <th>Type</th>
   <th>Severity</th>
   <th>Component</th>
  </tr>
  <tr>
   <td>CVE-2017-0740</td>
   <td>A-37168488<a href="#asterisk">*</a><br />
       B-RB#116402</td>
   <td>RCE</td>
   <td>Moderate</td>
   <td>Networking driver</td>
  </tr>
</table>
<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
   <th>CVE</th>
   <th>References</th>
   <th>Type</th>
   <th>Severity</th>
   <th>Component</th>
  </tr>
  <tr>
   <td>CVE-2017-10661</td>
   <td>A-36266767<br />
<a href="https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/commit/?id=1e38da300e1e395a15048b0af1e5305bd91402f6">Upstream
kernel</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>File system</td>
  </tr>
  <tr>
   <td>CVE-2017-0750</td>
   <td>A-36817013<a href="#asterisk">*</a></td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>File system</td>
  </tr>
  <tr>
   <td>CVE-2017-10662</td>
   <td>A-36815012<br />
<a href="https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/commit/?id=b9dd46188edc2f0d1f37328637860bb65a771124">Upstream
kernel</a></td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>File system</td>
  </tr>
  <tr>
   <td>CVE-2017-10663</td>
   <td>A-36588520<br />
<a href="https://sourceforge.net/p/linux-f2fs/mailman/message/35835945/">Upstream
kernel</a></td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>File System</td>
  </tr>
  <tr>
   <td>CVE-2017-0749</td>
   <td>A-36007735<a href="#asterisk">*</a></td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>Linux kernel</td>
  </tr>
</table>
<h3 id="mediatek-components">MediaTek components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
   <th>CVE</th>
   <th>References</th>
   <th>Type</th>
   <th>Severity</th>
   <th>Component</th>
  </tr>
  <tr>
   <td>CVE-2017-0741</td>
   <td>A-32458601<a href="#asterisk">*</a><br />
       M-ALPS03007523</td>
   <td>EoP</td>
   <td>High</td>
   <td>GPU driver</td>
  </tr>
  <tr>
   <td>CVE-2017-0742</td>
   <td>A-36074857<a href="#asterisk">*</a><br />
       M-ALPS03275524</td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>Video driver</td>
  </tr>
</table>
<h3 id="qualcomm-components">Qualcomm components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
   <th>CVE</th>
   <th>References</th>
   <th>Type</th>
   <th>Severity</th>
   <th>Component</th>
  </tr>
  <tr>
   <td>CVE-2017-0746</td>
   <td>A-35467471<a href="#asterisk">*</a><br />
       QC-CR#2029392</td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>IPA driver</td>
  </tr>
  <tr>
   <td>CVE-2017-0747</td>
   <td>A-32524214<a href="#asterisk">*</a><br />
       QC-CR#2044821</td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>Proprietary Component</td>
  </tr>
  <tr>
   <td>CVE-2017-9678</td>
   <td>A-35258962<a href="#asterisk">*</a><br />
       QC-CR#2028228</td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>Video driver</td>
  </tr>
  <tr>
   <td>CVE-2017-9691</td>
   <td>A-33842910<a href="#asterisk">*</a><br />
       QC-CR#1116560</td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>MobiCore driver (Trustonic)</td>
  </tr>
  <tr>
   <td>CVE-2017-9684</td>
   <td>A-35136547<a href="#asterisk">*</a><br />
       QC-CR#2037524</td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>USB driver</td>
  </tr>
  <tr>
   <td>CVE-2017-9682</td>
   <td>A-36491445<a href="#asterisk">*</a><br />
       QC-CR#2030434</td>
   <td>ID</td>
   <td>Moderate</td>
   <td>GPU driver</td>
  </tr>
</table>

<h2 id="google-device-updates">Google device updates</h2>
<p>This table contains the security patch level in the latest over-the-air update
(OTA) and firmware images for Google devices. The Google device firmware images
are available on the <a
href="https://developers.google.com/android/nexus/images">Google Developer
site</a>.</p>

<table>
  <tr>
   <th>Google device</th>
   <th>Security patch level</th>
  </tr>
  <tr>
   <td>Pixel / Pixel XL</td>
   <td>August 05, 2017</td>
  </tr>
  <tr>
   <td>Nexus 5X</td>
   <td>August 05, 2017</td>
  </tr>
  <tr>
   <td>Nexus 6</td>
   <td>August 05, 2017</td>
  </tr>
  <tr>
   <td>Nexus 6P</td>
   <td>August 05, 2017</td>
  </tr>
  <tr>
   <td>Nexus 9</td>
   <td>August 05, 2017</td>
  </tr>
  <tr>
   <td>Nexus Player</td>
   <td>August 05, 2017</td>
  </tr>
  <tr>
   <td>Pixel C</td>
   <td>August 05, 2017</td>
  </tr>
</table>
<p>Google device updates also contain patches for these security
vulnerabilities, if applicable:</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
   <th>CVE</th>
   <th>References</th>
   <th>Type</th>
   <th>Severity</th>
   <th>Component</th>
  </tr>
  <tr>
   <td>CVE-2017-0744</td>
   <td>A-34112726<a href="#asterisk">*</a><br />
       N-CVE-2017-0744</td>
   <td>EoP</td>
   <td>Low</td>
   <td>Sound driver</td>
  </tr>
  <tr>
   <td>CVE-2017-9679</td>
   <td>A-35644510<a href="#asterisk">*</a><br />
       QC-CR#2029409</td>
   <td>ID</td>
   <td>Low</td>
   <td>SoC driver</td>
  </tr>
  <tr>
   <td>CVE-2017-9680</td>
   <td>A-35764241<a href="#asterisk">*</a><br />
       QC-CR#2030137</td>
   <td>ID</td>
   <td>Low</td>
   <td>SoC driver</td>
  </tr>
  <tr>
   <td>CVE-2017-0748</td>
   <td>A-35764875<a href="#asterisk">*</a><br />
       QC-CR#2029798</td>
   <td>ID</td>
   <td>Low</td>
   <td>Audio driver</td>
  </tr>
  <tr>
   <td>CVE-2017-9681</td>
   <td>A-36386593<a href="#asterisk">*</a><br />
       QC-CR#2030426</td>
   <td>ID</td>
   <td>Low</td>
   <td>Radio driver</td>
  </tr>
  <tr>
   <td>CVE-2017-9693</td>
   <td>A-36817798<a href="#asterisk">*</a><br />
       QC-CR#2044820</td>
   <td>ID</td>
   <td>Low</td>
   <td>Networking driver</td>
  </tr>
  <tr>
   <td>CVE-2017-9694</td>
   <td>A-36818198<a href="#asterisk">*</a><br />
       QC-CR#2045470</td>
   <td>ID</td>
   <td>Low</td>
   <td>Networking driver</td>
  </tr>
  <tr>
   <td>CVE-2017-0751</td>
   <td>A-36591162<a href="#asterisk">*</a><br />
       QC-CR#2045061</td>
   <td>EoP</td>
   <td>Low</td>
   <td>QCE driver</td>
  </tr>
  <tr>
   <td>CVE-2017-9692</td>
   <td>A-36731152<a href="#asterisk">*</a><br />
       QC-CR#2021707</td>
   <td>DoS</td>
   <td>Low</td>
   <td>Graphics driver</td>
  </tr>
</table>
<h2 id="acknowledgements">Acknowledgements</h2>
<p>We would like to thank these researchers for their contributions:</p>

<table>
  <col width="17%">
  <col width="83%">
  <tr>
   <th>CVEs</th>
   <th>Researchers</th>
  </tr>
  <tr>
   <td>CVE-2017-0741, CVE-2017-0742, CVE-2017-0751</td>
   <td>Baozeng Ding (<a href="https://twitter.com/sploving1">@sploving</a>),
Chengming Yang, and Yang Song of Alibaba Mobile Security Group</td>
  </tr>
  <tr>
   <td>CVE-2017-9682</td>
   <td>Billy Lau of Android Security</td>
  </tr>
  <tr>
   <td>CVE-2017-0739</td>
   <td><a href="mailto:shaodacheng2016@gmail.com">Dacheng Shao</a>, Hongli Han
(<a href="https://twitter.com/HexB1n">@HexB1n</a>), Mingjian Zhou (<a
href="https://twitter.com/Mingjian_Zhou">@Mingjian_Zhou</a>), and Xuxian Jiang
of <a href="http://c0reteam.org">C0RE Team</a></td>
  </tr>
  <tr>
   <td>CVE-2017-9691, CVE-2017-0744</td>
   <td>Gengjia Chen (<a
href="https://twitter.com/chengjia4574">@chengjia4574</a>) and <a
href="http://weibo.com/jfpan">pjf</a> of IceSword Lab, Qihoo 360 Technology Co.
Ltd.</td>
  </tr>
  <tr>
   <td>CVE-2017-0727</td>
   <td>Guang Gong (龚广) (<a
href="https://twitter.com/oldfresher">@oldfresher</a>) of Alpha Team, Qihoo 360
Technology Co. Ltd.</td>
  </tr>
  <tr>
   <td>CVE-2017-0737</td>
   <td><a href="mailto:arnow117@gmail.com">Hanxiang Wen</a>, Mingjian Zhou (<a
href="https://twitter.com/Mingjian_Zhou">@Mingjian_Zhou</a>), and Xuxian Jiang
of <a href="http://c0reteam.org">C0RE Team</a></td>
  </tr>
  <tr>
   <td>CVE-2017-0748</td>
   <td>Hao Chen and Guang Gong of Alpha Team of Qihoo 360 Technology Co. Ltd.</td>
  </tr>
  <tr>
   <td>CVE-2017-0731</td>
   <td>Hongli Han (<a href="https://twitter.com/HexB1n">@HexB1n</a>), Mingjian
Zhou (<a href="https://twitter.com/Mingjian_Zhou">@Mingjian_Zhou</a>), and
Xuxian Jiang of <a href="http://c0reteam.org">C0RE Team</a></td>
  </tr>
  <tr>
   <td>CVE-2017-9679</td>
   <td>Nathan Crandall (<a href="https://twitter.com/natecray">@natecray</a>) of
Tesla's Product Security Team</td>
  </tr>
  <tr>
   <td>CVE-2017-0726</td>
   <td><a href="mailto:jiych.guru@gmail.com">Niky1235</a> (<a
href="https://twitter.com/jiych_guru">@jiych_guru</a>)</td>
  </tr>
  <tr>
   <td>CVE-2017-9684, CVE-2017-9694, CVE-2017-9693, CVE-2017-9681,
       CVE-2017-0738, CVE-2017-0728</td>
   <td>Pengfei Ding (丁鹏飞), Chenfu Bao (包沉浮), and Lenx Wei (韦韬) of Baidu X-Lab
(百度安全实验室)</td>
  </tr>
  <tr>
   <td>CVE-2017-9680, CVE-2017-0740</td>
   <td><a href="mailto:sbauer@plzdonthack.me">Scott Bauer</a>
   (<a href="https://twitter.com/ScottyBauer1">@ScottyBauer1</a>)</td>
  </tr>
  <tr>
   <td>CVE-2017-0724</td>
   <td>Seven Shen (<a href="https://twitter.com/lingtongshen">@lingtongshen</a>)
of TrendMicro</td>
  </tr>
  <tr>
   <td>CVE-2017-0732, CVE-2017-0805</td>
   <td>Timothy Becker of CSS Inc.</td>
  </tr>
  <tr>
   <td>CVE-2017-10661</td>
   <td>Tong Lin (<a
href="mailto:segfault5514@gmail.com">segfault5514@gmail.com</a>), Yuan-Tsung Lo
(<a href="mailto:computernik@gmail.com">computernik@gmail.com</a>), and Xuxian
Jiang of <a href="http://c0reteam.org">C0RE Team</a></td>
  </tr>
  <tr>
   <td>CVE-2017-0712</td>
   <td>Valerio Costamagna (<a href="https://twitter.com/vaio_co">@vaio_co</a>)
and Marco Bartoli (<a href="https://twitter.com/wsxarcher">@wsxarcher</a>)</td>
  </tr>
  <tr>
   <td>CVE-2017-0716</td>
   <td>Vasily Vasiliev</td>
  </tr>
  <tr>
   <td>CVE-2017-0750, CVE-2017-0713, CVE-2017-0715, CVE-2017-10662CVE-2017-10663</td>
   <td>V.E.O (<a href="https://twitter.com/vysea">@VYSEa</a>) of <a
href="http://blog.trendmicro.com/trendlabs-security-intelligence/category/mobile/">Mobile
Threat Response Team</a>, <a href="http://www.trendmicro.com">Trend Micro</a></td>
  </tr>
  <tr>
   <td>CVE-2017-9678</td>
   <td>Yan Zhou of Eagleye team, SCC, Huawei</td>
  </tr>
  <tr>
   <td>CVE-2017-0749, CVE-2017-0746</td>
   <td>Yonggang Guo (<a href="https://twitter.com/guoygang">@guoygang</a>) of
IceSword Lab, Qihoo 360 Technology Co. Ltd.</td>
  </tr>
  <tr>
   <td>CVE-2017-0729</td>
   <td>Yongke Wang of <a href="http://xlab.tencent.com">Tencent's Xuanwu
Lab</a></td>
  </tr>
  <tr>
   <td>CVE-2017-0714, CVE-2017-0719, CVE-2017-0718, CVE-2017-0722,
       CVE-2017-0725, CVE-2017-0720, CVE-2017-0745</td>
   <td><a href="http://weibo.com/ele7enxxh">Zinuo Han</a> of Chengdu Security
Response Center, Qihoo 360 Technology Co. Ltd.</td>
  </tr>
</table>
<h2 id="questions">Common questions and answers</h2>
<p>This section answers common questions that may occur after reading this
bulletin.</p>

<p><strong>1. How do I determine if my device is updated to address these issues?
</strong></p>

<p>To learn how to check a device's security patch level, read the instructions on
the <a
href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">Pixel
and Nexus update schedule</a>.</p>
<ul>
  <li>Security patch levels of 2017-08-01 or later address all issues associated
  with the 2017-08-01 security patch level.</li>
  <li>Security patch levels of 2017-08-05 or later address all issues associated
  with the 2017-08-05 security patch level and all previous patch levels.
  </li>
</ul>
<p>Device manufacturers that include these updates should set the patch string
level to:</p>
<ul>
  <li>[ro.build.version.security_patch]:[2017-08-01]</li>
  <li>[ro.build.version.security_patch]:[2017-08-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>

<p>This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.</p>
<ul>
  <li>Devices that use the August 01, 2017 security patch level must include all
  issues associated with that security patch level, as well as fixes for all
  issues reported in previous security bulletins.</li>
  <li>Devices that use the security patch level of August 05, 2017 or newer must
  include all applicable patches in this (and previous) security
  bulletins.</li>
</ul>
<p>Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.</p>

<p id="type"><strong>3. What do the entries in the <em>Type</em> column mean?</strong></p>

<p>Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.</p>

<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p><strong>4. What do the entries in the <em>References</em> column mean?</strong></p>

<p>Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.</p>

<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk"><strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong></p>

<p>Issues that are not publicly available have a <a href="#asterisk">*</a> next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Nexus devices available from the <a
href="https://developers.google.com/android/nexus/drivers">Google Developer
site</a>.</p>

<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>August 7, 2017</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>August 8, 2017</td>
   <td>Bulletin revised to include AOSP links and acknowledgements.</td>
  </tr>
  <tr>
   <td>1.2</td>
   <td>August 14, 2017</td>
   <td>Bulletin revised to add CVE-2017-0687.</td>
  </tr>
  <tr>
   <td>1.2</td>
   <td>August 23, 2017</td>
   <td>Bulletin revised to add CVE-2017-0805.</td>
  </tr>
</table>
</body>
</html>
