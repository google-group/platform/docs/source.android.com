<html devsite>
  <head>
    <title>Android Security Bulletin—November 2017</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p><em>Published November 6, 2017 | Updated November 8, 2017</em></p>
<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2017-11-06 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="//support.google.com/pixelphone/answer/4457705">Check and update
your Android version</a>.
</p>
<p>
Android partners were notified of all issues in the 2017-11-01 and 2017-11-05
patch levels at least a month before publication. Android partners were notified
of all issues in the 2017-11-06 patch level within the last month. Source code
patches for these issues have been released to the Android Open Source Project
(AOSP) repository and linked from this bulletin. This bulletin also includes
links to patches outside of AOSP.
</p>
<p>
The most severe of these issues is a critical security vulnerability in Media
framework that could enable a remote attacker using a specially crafted file to
execute arbitrary code within the context of a privileged process. The <a
href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the <a href="#mitigations">Android and Google Play
Protect mitigations</a> section for details on the <a
href="/security/enhancements/index.html">Android
security platform protections</a> and Google Play Protect, which improve the
security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2017-11-01">November 2017
Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.
</p>
<h2 id="announcements">Announcements</h2>
<ul>
  <li>We have launched a new
  <a href="/security/bulletin/pixel/">Pixel&hairsp;/&hairsp;Nexus Security
  Bulletin</a>, which contains information on additional security
  vulnerabilities and functional improvements that are addressed on supported
  Pixel and Nexus devices. Android device manufacturers may choose to address
  these issues on their devices. See <a href="#questions">Common questions and
  answers</a> for additional information.</li>
  <li>Security patches for the KRACK vulnerabilities are provided under the
  2017-11-06 security patch level.</li>
</ul>
<h2 id="mitigations">Android and Google service mitigations</h2>
<p>
This is a summary of the mitigations provided by the <a
href="/security/enhancements/index.html">Android
security platform</a> and service protections such as <a
href="//www.android.com/play-protect">Google Play Protect</a>. These
capabilities reduce the likelihood that security vulnerabilities could be
successfully exploited on Android.
</p>
<ul>
  <li>Exploitation for many issues on Android is made more difficult by
  enhancements in newer versions of the Android platform. We encourage all users
  to update to the latest version of Android where possible.</li>
  <li>The Android security team actively monitors for abuse through <a
  href="//www.android.com/play-protect">Google Play Protect</a> and warns
  users about <a
  href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
  Harmful Applications</a>. Google Play Protect is enabled by default on devices
  with <a href="//www.android.com/gms">Google Mobile Services</a>, and is
  especially important for users who install apps from outside of Google
  Play.</li>
</ul>
<h2 id="2017-11-01-details">2017-11-01 security patch level—Vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2017-11-01 patch level. Vulnerabilities are
grouped under the component that they affect. There is a description of the
issue and a table with the CVE, associated references, <a
href="#type">type of vulnerability</a>, <a
href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, like the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>
<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass user interaction requirements in order to gain access to
additional permissions.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-0830</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/d05d2bac845048f84eebad8060d28332b6eda259">A-62623498</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0831</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/c510ecb3ec0eeca5425f5bc96fae80ea56f85be6">A-37442941</a>
    [<a href="https://android.googlesource.com/platform/packages/apps/Settings/+/94c52029653426846c50c639e7f6b5404cedd472">2</a>]</td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-0832</td>
    <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/0a2112249af3c8de52f4da9e89d740b20246d050">A-62887820</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0833</td>
    <td><a href="https://android.googlesource.com/platform/external/libavc/+/5df744afde273bc4d0f7a499581dd2fb2ae6cb45">A-62896384</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0834</td>
    <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/89b4c1cf9e2d18c27c2d9c8c7504e5e2d79ef289">A-63125953</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0835</td>
    <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/c07e83250dcdc3be3eca434c266472be8fddec5f">A-63316832</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0836</td>
    <td><a href="https://android.googlesource.com/platform/external/libhevc/+/6921d875c1176cc79a582dd7416e020bf011b53e">A-64893226</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0839</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/2bec2c3b1fd778b35f45ff4f8b385ff9208fe692">A-64478003</a></td>
    <td>ID</td>
    <td>High</td>
    <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0840</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/f630233ee42214b36e6862dc99114f2c2bdda018">A-62948670</a></td>
    <td>ID</td>
    <td>High</td>
    <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
</table>


<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-0841</td>
    <td><a href="https://android.googlesource.com/platform/system/core/+/47efc676c849e3abf32001d66e2d6eb887e83c48">A-37723026</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0842</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/b413f1b1365af4273647727e497848f95312d0ec">A-37502513</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
</table>


<h2 id="2017-11-05-details">2017-11-05 security patch level—Vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2017-11-05 patch level. Vulnerabilities are
grouped under the component that they affect and include details such as the
CVE, associated references, <a
href="#type">type
of vulnerability</a>, <a
href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
like the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-9077</td>
    <td>A-62265013<br />
        <a href="//git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=83eaddab4378db256d00d295bda6ca997cd13a52">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Networking subsystem</td>
  </tr>
  <tr>
    <td>CVE-2017-7541</td>
    <td>A-64258073<br />
        <a href="//git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=8f44c9a41386729fea410e688959ddaa9d51be7c">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
</table>


<h3 id="mediatek-components">MediaTek components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-0843</td>
    <td>A-62670819<a href="#asterisk">*</a><br />
        M-ALPS03361488</td>
    <td>EoP</td>
    <td>High</td>
    <td>CCCI</td>
  </tr>
</table>


<h3 id="nvidia-components">NVIDIA components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-6264</td>
    <td>A-34705430<a href="#asterisk">*</a><br />
        N-CVE-2017-6264</td>
    <td>EoP</td>
    <td>High</td>
    <td>GPU driver</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-11013</td>
    <td>A-64453535<br />
        <a href="//source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/prima/commit/?id=64297e4caffdf6b1a90807bbdb65a66b43582228">
QC-CR#2058261</a>
 [<a href="//source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=c9f8654b11a1e693022ad7f163b3bc477fea8ce8">2</a>]</td>
    <td>RCE</td>
    <td>Critical</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2017-11015</td>
    <td>A-64438728<br />
        <a
href="//source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=ec58bc99e29d89f8e164954999ef8a45cec21754">QC-CR#2060959</a>
[<a href="//source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=1ef6add65a36de6c4da788f776de2b5b5c528d8e">2</a>]</td>
    <td>RCE</td>
    <td>Critical</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2017-11014</td>
    <td>A-64438727<br />
        <a href="//source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=ec58bc99e29d89f8e164954999ef8a45cec21754">
QC-CR#2060959</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2017-11092</td>
    <td>A-62949902<a href="#asterisk">*</a><br />
        QC-CR#2077454</td>
    <td>EoP</td>
    <td>High</td>
    <td>GPU driver</td>
  </tr>
  <tr>
    <td>CVE-2017-9690</td>
    <td>A-36575870<a href="#asterisk">*</a><br />
        QC-CR#2045285</td>
    <td>EoP</td>
    <td>High</td>
    <td>QBT1000 driver</td>
  </tr>
  <tr>
    <td>CVE-2017-11017</td>
    <td>A-64453575<br />
        <a href="//source.codeaurora.org/quic/la/kernel/lk/commit/?id=41423b4ef59ea8ed871ab1acc0c9cf48fd1017e4">
QC-CR#2055629</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Linux boot</td>
  </tr>
  <tr>
    <td>CVE-2017-11028</td>
    <td>A-64453533<br />
        <a href="//source.codeaurora.org/quic/la/kernel/msm-4.4/commit/?id=fd70b655d901e626403f132b65fc03d993f0a09b">
QC-CR#2008683</a>
[<a href="//source.codeaurora.org/quic/la/kernel/msm-4.4/commit/?id=6724296d3f3b2821b83219768c1b9e971e380a9f">2</a>]</td>
    <td>ID</td>
    <td>High</td>
    <td>Camera</td>
  </tr>
</table>


<h2 id="2017-11-06-details">2017-11-06 security patch level—Vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2017-11-06 patch level. Vulnerabilities are
grouped under the component that they affect and include details such as the
CVE, associated references, <a
href="#type">type of vulnerability</a>, <a
href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
like the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>
<h3 id="11-06-system">System</h3>
<p>
The most severe vulnerability in this section could enable a proximate attacker
to bypass user interaction requirements before joining an unsecured Wi-Fi
network.
</p>
<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2017-13077</td>
   <td><a
href="//android.googlesource.com/platform/external/wpa_supplicant_8/+/c66556ca2473620df9751e73eb97ec50a40ffd3e">A-67737262</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
   <td>CVE-2017-13078</td>
   <td><a
href="//android.googlesource.com/platform/external/wpa_supplicant_8/+/10bfd644d0adaf334c036f8cda91a73984dbb7b9">A-67737262</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
   <td>CVE-2017-13079</td>
   <td><a
href="//android.googlesource.com/platform/external/wpa_supplicant_8/+/10bfd644d0adaf334c036f8cda91a73984dbb7b9">A-67737262</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
   <td>CVE-2017-13080</td>
   <td><a
href="//android.googlesource.com/platform/external/wpa_supplicant_8/+/10bfd644d0adaf334c036f8cda91a73984dbb7b9">A-67737262</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
   <td>CVE-2017-13081</td>
   <td><a
href="//android.googlesource.com/platform/external/wpa_supplicant_8/+/10bfd644d0adaf334c036f8cda91a73984dbb7b9">A-67737262</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
   <td>CVE-2017-13082</td>
   <td><a
href="//android.googlesource.com/platform/external/wpa_supplicant_8/+/f6e1f661b95908660c2bcf200266734c30803910">A-67737262</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
   <td>CVE-2017-13086</td>
   <td><a
href="//android.googlesource.com/platform/external/wpa_supplicant_8/+/c580b5560810c3348335b4b284a48773ceaa2301">A-67737262</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
   <td>CVE-2017-13087</td>
   <td><a
href="//android.googlesource.com/platform/external/wpa_supplicant_8/+/58c0e963554ac0be5628f3d2e5058e5c686c128a">A-67737262</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
   <td>CVE-2017-13088</td>
   <td><a
href="//android.googlesource.com/platform/external/wpa_supplicant_8/+/58c0e963554ac0be5628f3d2e5058e5c686c128a">A-67737262</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
</table>
<p>
<strong>Note</strong>: Android partners may also need to obtain fixes from
chipset manufacturers where applicable.
</p>
<h2 id="questions">Common questions and answers</h2>
<p>
This section answers common questions that may occur after reading this
bulletin.
</p>
<p>
<strong>1. How do I determine if my device is updated to address these issues?
</strong>
</p>
<p>
To learn how to check a device's security patch level, see <a
href="//support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">Check
& update your Android version</a>.
</p>
<ul>
  <li>Security patch levels of 2017-11-01 or later address all issues associated
  with the 2017-11-01 security patch level.</li>
  <li>Security patch levels of 2017-11-05 or later address all issues associated
  with the 2017-11-05 security patch level and all previous patch levels.</li>
  <li>Security patch levels of 2017-11-06 or later address all issues associated
  with the 2017-11-06 security patch level and all previous patch levels.
  </li>
</ul>
<p>
Device manufacturers that include these updates should set the patch string
level to:
</p>
<ul>
  <li>[ro.build.version.security_patch]:[2017-11-01]</li>
  <li>[ro.build.version.security_patch]:[2017-11-05]</li>
  <li>[ro.build.version.security_patch]:[2017-11-06]</li>
</ul>
<p>
<strong>2. Why does this bulletin have three security patch levels?</strong>
</p>
<p>
This bulletin has three security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
  <li>Devices that use the 2017-11-01 security patch level must include all issues
  associated with that security patch level, as well as fixes for all issues
  reported in previous security bulletins.</li>
  <li>Devices that use the 2017-11-05 security patch level must include all issues
  associated with that security patch level, the 2017-11-01 security patch level,
  as well as fixes for all issues reported in previous security bulletins.</li>
  <li>Devices that use the security patch level of 2017-11-06 or newer must
  include all applicable patches in this (and previous) security
  bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Nexus devices available from the <a
href="//developers.google.com/android/nexus/drivers">Google Developer
site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device/partner security bulletins, such as the Pixel&hairsp;/&hairsp;Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required in order to declare the latest security patch level on Android devices.
Additional security vulnerabilities that are documented in the device/partner
security bulletins are not required for declaring a security patch level.
Android device and chipset manufacturers are encouraged to document the presence
of other fixes on their devices through their own security websites, such as the
<a href="//security.samsungmobile.com/securityUpdate.smsb">Samsung</a>, <a
href="//lgsecurity.lge.com/security_updates.html">LGE</a>, or <a
href="/security/bulletin/pixel/">Pixel&hairsp;/&hairsp;Nexus</a> security bulletins.
</p>
<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>November 6, 2017</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>November 8, 2017</td>
   <td>Bulletin revised to include AOSP links.</td>
  </tr>
</table>

</body></html>
