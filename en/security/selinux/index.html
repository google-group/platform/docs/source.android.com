<html devsite>
  <head>
    <title>Security-Enhanced Linux in Android</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>As part of the Android <a href="/security/index.html">
security model</a>, Android uses Security-Enhanced Linux (SELinux) to enforce
mandatory access control (MAC) over all processes, even processes running with
root/superuser privileges (Linux capabilities). Many companies and organizations
have contributed to Android's
<a href="https://android.googlesource.com/platform/external/selinux/" class="external">SELinux
implementation</a>. With SELinux, Android can better protect and confine system
services, control access to application data and system logs, reduce the effects
of malicious software, and protect users from potential flaws in code on mobile
devices.</p>

<p>SELinux operates on the principle of default denial: Anything not explicitly
allowed is denied. SELinux can operate in two global modes:</p>
<ul>
<li><em>Permissive</em> mode, in which permission denials are logged but not
enforced.</li>
<li><em>Enforcing</em> mode, in which permissions denials are both logged
<strong>and</strong> enforced.</li>
</ul>

<p>Android includes SELinux in enforcing mode and a corresponding security
policy that works by default across AOSP. In enforcing mode, disallowed
actions are prevented and all attempted violations are logged by the kernel to
<code>dmesg</code> and <code>logcat</code>. When developing, you should
use these errors to refine your software and SELinux policies before enforcing
them. For more details, see <a href="/security/selinux/implement">Implementing
SELinux</a>.</p>

<p>SELinux also supports a <em>per-domain permissive</em> mode in which specific
domains (processes) can be made permissive while placing the rest of the system
in global enforcing mode. A domain is simply a label identifying a process or set
of processes in the security policy, where all processes labeled with the same
domain are treated identically by the security policy. Per-domain permissive
mode enables incremental application of SELinux to an ever-increasing portion of
the system and policy development for new services (while keeping the rest of
the system enforcing).</p>

<h2 id="background">Background</h2>

<p>The Android security model is based in part on the concept of
<a href="/security/app-sandbox">application sandboxes</a>. Each application
runs in its own sandbox. Prior to Android 4.3, these sandboxes were defined by
the creation of a unique Linux UID for each application at time of installation.
Android 4.3 and later uses SELinux to further define the boundaries of the
Android application sandbox.</p>

<p>In Android 5.0 and later, SELinux is fully enforced, building on the
permissive release of Android 4.3 and the partial enforcement of Android 4.4.
With this change, Android shifted from enforcement on a limited set of crucial
domains (<code>installd</code>, <code>netd</code>, <code>vold</code> and
<code>zygote</code>) to everything (more than 60 domains). Specifically:</p>

<ul>
<li>Everything is in enforcing mode in Android 5.x and higher.</li>
<li>No processes other than <code>init</code> should run in the
<code>init</code> domain.</li>
<li>Any generic denial (for a <code>block_device</code>,
<code>socket_device</code>, <code>default_service</code>, etc.) indicates that
device needs a special domain.</li>
</ul>

<p>Android 6.0 hardened the system by reducing the permissiveness of our
policy to include better isolation between users, IOCTL filtering, reduced
threat of exposed services, further tightening of SELinux domains, and
extremely limited <code>/proc</code> access.
</p>
<p>
Android 7.0 updated SELinux configuration to further lock down the
application sandbox and reduce attack surface. This release also broke up the
monolithic mediaserver stack into smaller processes to reduce the scope of
their permissions. For more details, see
<a href="https://android-developers.googleblog.com/2016/07/protecting-android-with-more-linux.html" class="external">Protecting
Android with more Linux kernel defenses</a> and
<a href="https://android-developers.googleblog.com/2016/05/hardening-media-stack.html" class="external">Hardening
the media stack</a>.
</p>
</p>
<p>
Android 8.0 updated SELinux to work with <a
href="/devices/architecture/#hidl">Treble</a>, which separates the lower-level
vendor code from the Android system framework. This release updated SELinux
policy to allow device manufacturers and SOC vendors to update their parts of
the policy, build their images (<code>vendor.img</code>, <code>boot.img</code>,
etc.), then update those images independent of the platform or vice versa.
</p>
<p>
While it is possible to have higher/newer platform (framework) version running
on the device, the opposite case is not supported; the vendor images
(<code>vendor.img/odm.img</code>) cannot have a newer version than the platform
(<code>system.img</code>). So, a newer platform version might introduce SELinux
compatibility issues because the platform SELinux policy is at a newer version
than vendor SELinux parts of the policy. The Android 8.0 model provides a method
to <a href="/security/selinux/compatibility">retain compatibility</a> to prevent
unnecessary simultaneous OTAs.
</p>

<h2 id=supporting_documentation>Additional resources</h2>

<p>For help constructing useful SELinux policies, refer to the following
resources:</p>

<ul><li><a href="https://events.linuxfoundation.org/sites/events/files/slides/abs2014_seforandroid_smalley.pdf" class="external">
Security Enhancements for Linux</a></li>

<li><a href="http://www.cs.columbia.edu/~lierranli/coms6998-7Spring2014/papers/SEAndroid-NDSS2013.pdf" class="external">
Security Enhanced (SE) Android: Bringing Flexible MAC to Android</a></li>

<li><a href="http://freecomputerbooks.com/books/The_SELinux_Notebook-4th_Edition.pdf" class="external">
The SELinux Notebook, 4th Edition</a></li>

<li><a href="http://selinuxproject.org/page/ObjectClassesPerms" class="external">
SELinux Object Classes and Permissions Reference</a></li>

<li><a href="https://www.nsa.gov/resources/everyone/digital-media-center/publications/research-papers/assets/files/implementing-selinux-as-linux-security-module-report.pdf" class="external">
Implementing SELinux as a Linux Security Module</a></li>

<li><a href="https://www.nsa.gov/resources/everyone/digital-media-center/publications/research-papers/assets/files/configuring-selinux-policy-report.pdf" class="external">
Configuring the SELinux Policy</a></li>

<li><a href="https://www.gnu.org/software/m4/manual/index.html" class="external">
GNU M4 - GNU Macro Processor Manual</a></li>

<li><a href="https://opensource.com/business/13/11/selinux-policy-guide" class="external">
Your visual how-to guide for SELinux policy enforcement</a></li>
</ul>

  </body>
</html>
