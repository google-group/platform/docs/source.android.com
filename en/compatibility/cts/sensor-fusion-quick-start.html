<html devsite>
  <head>
    <title>Sensor Fusion Box Quick Start Guide</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
    <!--
    Copyright 2018 The Android Open Source Project
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    -->
    <p>
      The sensor fusion test measures timestamp accuracy of camera and other
      sensors for Android phones. This page provides step-by-step directions on
      how to setup the Sensor Fusion test and Sensor Fusion Box for the first
      time.
    </p>
    <h2 id="required-tools">Required tools</h2>
    <p>
      Before getting started, ensure you have the following cables and cords
    available:</p>
    <figure id="sensor-fusion-test-component">
      <img src="/compatibility/cts/images/sensor_fusion_test_components.png" width="700" alt="Sensor fusion test components">
      <figcaption><b>Figure 1.</b> Components required for the sensor fusion test</figcaption>
    </figure>
    <ul>
      <li>USB A to B cable</li>
      <li>USB A to C cable (for test phone)</li>
      <li>12V power cord (for servo control box)</li>
      <li>12V power cord (for lighting, with switch)</li>
      <li>Interconnected cable (for lighting)</li>
      <li>Conversion cable (for lighting)</li>
    </ul>
    <h2 id="step-1-connect-lights">Step 1: Connect lights</h2>
    <p>
      To connect the lights:
    </p>
    <ol>
      <li>Use the interconnected cable to connect the two lights.</li>
      <li>Connect one light to the conversion cable.
        <figure id="sensor-fusion-connect-lights">
          <img src="/compatibility/cts/images/sensor_fusion_connect_lights.png" width="300" alt="Connect lights">
          <figcaption><b>Figure 2.</b> Connecting the lights to each other and one light to the conversion cable</figcaption>
        </figure>
      </li>
      <li>Thread the unconnected end of the conversion cable through the round
      hole that exits the box, then connect the end of that cable to the power
      cable for lighting.
        <table class="columns">
          <tr>
            <td><img src="/compatibility/cts/images/sensor_fusion_conversion_cable1.png" width="" alt="Conversion cable and power cable"></td>
            <td><img src="/compatibility/cts/images/sensor_fusion_conversion_cable2.png" width="" alt="Power cable for lighting"></td>
          </tr>
        </table>
      <b>Figure 3.</b> Lighting conversion cable exiting the box and connecting
      to power cable</li>
    </ol>
    <h2 id="step-2-attach-servo">Step 2: Attach servo</h2>
    <p>
      To attach the servo:
    </p>
    <ol>
      <li>Plug the servo connector into the servo control. Be sure to insert
        the connector oriented to the corresponding colors as labeled (Y =
        Yellow, R = Red, B = Black), as reversing the order could damage the
        motor.
        <figure id="sensor-fusion-servo-connector">
          <img src="/compatibility/cts/images/sensor_fusion_servo_connector.png" width="300" alt="Servo connecting to the servo control box">
          <figcaption><b>Figure 4.</b> Servo connecting to the servo control box</figcaption>
        </figure>
        <li>Connect the servo control with its power cord (the lighting and
        servo control have independent, dedicated power supplies).
          <table class="columns">
            <tr>
              <td><img src="/compatibility/cts/images/sensor_fusion_servo_control1.png" width="" alt="Servo control"></td>
              <td><img src="/compatibility/cts/images/sensor_fusion_servo_control2.png" width="" alt="Power to servo control"></td>
            </tr>
          </table>
          <b>Figure 5.</b> Connecting the servo control to its dedicated power
          cord
          <li>Use the USB A to B cable to connect the servo control box to the
            host (machine that is running the test).
            <table class="columns">
              <tr>
                <td><img src="/compatibility/cts/images/sensor_fusion_servo_control_box1.png" width="" alt="Connect servo control box"></td>
                <td><img src="/compatibility/cts/images/sensor_fusion_servo_control_box2.png" width="" alt="Connect servo control box to host"></td>
              </tr>
            </table>
          <b>Figure 6.</b> Connecting the servo control box to the host machine</li>
        </ol>
        <h2 id="step-3-attach-phone">Step 3: Attach phone</h2>
        <ol>
          <li>Set the phone on the fixture and clamp it down.<br>
            <table class="columns">
              <tr>
                <td><img src="/compatibility/cts/images/sensor_fusion_fixture1.png" width="" alt="Phone on fixture"></td>
                <td><img src="/compatibility/cts/images/sensor_fusion_fixture2.png" width="" alt="Clamping phone on fixture"></td>
              </tr>
            </table>
            <b>Figure 7.</b> Placing and clamping the phone on the fixture
            <p> The upside-down thumb screw provides back support while the
              other screw tightens the grip by turning right. For more help,
              refer to the video on loading the phone (included in the 
              <a href="/compatibility/cts/sensor_fusion_1.5.zip">Sensor
              Fusion Box zip file</a>). </p>
          </li>
          <li>Use a zip tie to hold the phone USB cord to the fixture plate and
            lead it outside the box through the exit hole. Plug the other end
            of the cord to the host running the test.
            <figure id="sensor-fusion-zip-ties">
              <img src="/compatibility/cts/images/sensor_fusion_zip_ties.png" width="300" alt="Phone USB cord with zip ties">
              <figcaption><b>Figure 8.</b> Phone USB cord held to fixture with
              zip ties</figcaption>
            </figure>
          </li>
        </ol>
        <h2 id="step-4-run-test-script">Step 4: Run test script</h2>
        <p>
          The main python executable for the test script is:
        </p>
      <pre class="prettyprint">python tools/run_all_tests.py device=ID camera=0 scenes=sensor_fusion rot_rig=default</pre>
      <p>You can also enter the actual rotator address at the command line
      using:</p>
    <pre class="prettyprint">rot_rig=VID:PID:CH</pre>
    <ul>
      <li>To determine the Vendor ID (VID) and Product ID (PID), use the Linux
      command <code>lsusb</code>.</li> <li>By default, the VID and PID are set
      to <code>04d8</code> and <code>fc73</code> with channel "1".</li>
    </ul>
    <h3 id="multiple-runs-different-formats">Multiple runs, different formats</h3>
    <p>To perform multiple runs with different formats, you can use a
      different script (however, the results will not be uploaded to
      <code>CtsVerifier.apk</code>). Sample test script: </p>
  <pre class="prettyprint">python tools/run_sensor_fusion_box.py device=FA7831A00278 camera=0 rotator=default img_size=640,360 fps=30 test_length=7</pre>
  <h3 id="permission-issues">Permission issues</h3>
  <p>To resolve permission issues related to controlling the motor through the
    USB port:</p>
  <ol>
    <li>Add the operator username to <code>dialout</code> group using:
    <pre class="prettyprint">sudo adduser $username dialout</pre></li>
    <li>Log out the operator.</li>
    <li>Log in the operator.</li>
  </ol>
  <h2>Adjusting the motor</h2>
  <p>
    You can adjust the speed of the motor and the distance the phone' travels
    using the resistance ports (labelled <strong>A</strong>,
    <strong>B</strong>, and <strong>T</strong>) on the side of the controller
    box.
  </p>
  <ol>
    <li>Ensure the phone fixture travels a full 90 degrees (from 12
      o'clock to 9 o'clock when looking at the phone) for each rotation.
      <ul>
        <li>To adjust the distance travelled, use the <strong>A</strong> and
          <strong>B</strong> screws (where <strong>A</strong> is the starting
          location
        and <strong>B</strong> is the final location).</li>
        <li>Upon first receiving the box, it is easiest to power up the box and
          determine the initial position. If the initial position on power-up
          is not close to 12 o'clock, unscrew the phone fixture (single Philips
          head screw in mount hole) and rotate the phone fixture to 12
          o'clock.</li>
      </ul>
    </li>
    <li>Adjust the rotation speed to travel a full rotation in 1.5s. Turning
      the resistor pot clockwise slows down the motion.
      <table class="columns">
        <tr>
          <td>
            <img src="/compatibility/cts/images/sensor_fusion_adjust.png" width="" alt="Adjust position and speed of servo">
          </td>
          <td>
            <ul>
              <li>A is the start position of the fixture.</li>
              <li>B is the end position of the fixture.</li>
              <li>T is the speed motor rotates.</li>
            </ul>
          </td>
        </tr>
      </table>
      <b>Figure 9.</b> How to adjust the position and speed of servo and phone
      fixture
    </li>
  </ol>
  <p>
    For more help, refer to the video of the sensor fusion box running (included
    in the <a href=/compatibility/cts/sensor_fusion_1.5.zip>Sensor Fusion Box
    zip file</a>).
  </p>
</body>
</html>
