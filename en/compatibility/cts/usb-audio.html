<html devsite>
<head>
  <title>USB Audio CTS Verifier Tests</title>
  <meta name="project_path" value="/_project.yaml" />
  <meta name="book_path" value="/_book.yaml" />
</head>

<body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


  <p>Several <a href="/compatibility/cts/">Android Compatibility Test Suite
  (CTS)</a> tests for <a href="/devices/audio/usb">Android USB audio</a>
  require the physical connection of USB audio
  peripherals. For these, additional CTS Verifier tests have been implemented.</p>


  <h2 id="nomenclature">Nomenclature</h2>


  <p>Throughout this document, the term "device" and "peripheral" are used in a
  very precise manner:</p>


  <ul>
    <li><em>Device</em> refers to the Android device.</li>


    <li><em>Peripheral</em> refers to an external USB audio peripheral
    connected to the Android device.</li>
  </ul>


  <h2 id="recommended-peripherals">Mandated peripherals</h2>


  <p>For the USB audio CTS Verifier Tests to know the attributes and
  capabilities they are verifying, it is necessary to specify a set of known
  peripherals to test against. For this reason, specific brands and types are
  mandated below. Some tests require a specifically mandated peripheral. Other
  tests simply require a USB audio peripheral that meets the requirements of
  those specific test. Note that any of the peripherals mandated for the USB
  Audio Peripherals Attributes Test will be compatible with the requirements
  for the Play and Record tests.</p>


  <h3 id="usb-audio-interface">USB audio interface</h3>


  <p>Use one of the peripherals here to conduct the USB Audio Peripheral
  Attributes test. They can also be used for the Play test and Record test.</p>


  <ul>
    <li><a href="http://www.presonus.com/products/audiobox-22VSL" class="external">Presonus
    AudioBox 22VSL</a>
    </li>


    <li><a href="https://www.presonus.com/products/audiobox-usb" class="external">Presonus
    AudioBox USB</a>
    </li>
  </ul>


  <p>Note that these two peripherals have been discontinued by the manufacturer
  and will be deprecated in a future CTS Verifier release.</p>


  <ul>
    <li><a href=
    "https://focusrite.com/usb-audio-interface/scarlett/scarlett-2i4" class="external">
    Focusrite Scarlett 2i4</a>
    </li>


    <li><a href=
    "http://www.musictribe.com/Categories/Behringer/Computer-Audio/Interfaces/UMC204HD/p/P0BK0" class="external">
    Behringer UMC204HD</a>
    </li>


    <li><a href=
    "https://www.roland.com/us/products/rubix24/" class="external">
    Roland Rubix 24</a>
    </li>


    <li><a href=
    "https://www.presonus.com/products/AudioBox-USB-96" class="external">
    PreSonus AudioBox 96</a>
    </li>
  </ul>


  <table>
    <tr>
      <td width="50%">A USB audio interface (A PreSonus AudioBox 22VSL).</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/usb_audio_interface.png" alt=
      "USB audio interface" width="300" id="usb_audio_interface">
      </td>
    </tr>
  </table>


  <h3 id="usb-headset">USB headset</h3>


  <p>The CTS Verifier USB Audio Buttons Test does not require a specific USB
  headset peripheral. It can be an instance of either of the following:</p>


  <ul>
    <li><span style="color:#202124;">A USB Headset peripheral that supports the
    <a href=
    "/devices/accessories/headset/usb-headset-spec">Android
    USB Headset Accessory Specification</a><span style="color:#202124;">. These
    headset peripherals are often indicated by the manufacturer as "Made for
    Android".</span></span>
    </li>


    <li>An analog headset which supports the
    <a href=
    "/devices/accessories/headset/usb-headset-spec">Android
    USB Headset Accessory Specification </a>connected to a USB to analog audio converter (such as the
    <a href=
    "https://store.google.com/us/product/usb_c_headphone_adapter" class="external">Google USB-C
    digital to 3.5 mm headphone adapter</a> or <a href=
    "https://www.htc.com/us/accessories-b/#!pid=htc-u11&amp;acc=usb-c-digital-to-3-5mm-htc-u11"
    class="external">HTC USB-C digital to 3.5mm audio jack adapter</a>).
    <p>Known examples of compatible analog headset peripherals include the
    <a href="https://www.bose.com/en_us/products/headphones/earphones/soundsport-in-ear-headphones-samsung-devices.html"
    class="external">
    Bose SoundSport® in-ear headphones – Samsung and Android™ devices</a>, and
    the <a href=
    "https://www.urbanears.com/ue_us_en/reimers#reimers-black-belt" class="external" >UrbanEars
    Reimers Black Belt made for Android</a> headsets. These analog headsets
    must be connected to the device USB port via a USB to Audio
      converter.</p>
    </li>
  </ul>


  <p>Note that in either case, the button must produce virtual key codes for all three of the
  required buttons (volume up, volume down, play/pause) for the test to succeed.
  Refer to the Software mapping section in <a href="/devices/accessories/headset/usb-headset-spec">
  Android USB Headset Accessory Specification</a> for the virtual key codes.</p>


  <table>
    <tr>
      <td width="50%">A USB headset.</td>

      <td width="50%"><img src="/compatibility/cts/images/usb_headset.png" alt=
      "USB headset" width="300" id="usb_headset">
      </td>
    </tr>
  </table>


  <h2 id="required-additional-hardware">Required additional hardware</h2>


  <p>Patch cables (for loopback) 2 short ¼" male to ¼" male patch cables to
  connect the outputs to the inputs of the USB</p>


  <table>
    <tr>
      <td width="50%">¼" male to ¼" male patch cables.</td>

      <td width="50%"><img src="/compatibility/cts/images/patch_cables.png"
      alt="patch cables" width="300" id="patch_cables">
      </td>
    </tr>
  </table>


  <p>USB peripheral cable</p>


  <table>
    <tr>
      <td width="50%">This cable (which typically comes with the peripheral),
      connects the USB audio peripheral to the host device.</td>

      <td width="50%"><img src="/compatibility/cts/images/peripheral_cable.png"
      alt="peripheral cable" width="300" id="peripheral_cable">
      </td>
    </tr>
  </table>


  <p>USB "On The Go" (OTG) adapter</p>


  <table>
    <tr>
      <td width="50%">A USB "On The Go" (OTG) adapter is required to physically
      connect the peripheral to the Android device and indicate to the Android
      device that it should assume the role of "host".</td>

      <td width="50%"><img src="/compatibility/cts/images/otg_adapter.png" alt=
      "OTG adapter" width="300" id="otg_adapter">
      </td>
    </tr>
  </table>


  <p>Analog headphones to monitor the output of the USB audio interface for the
  Play test.</p>


  <table>
    <tr>
      <td width="50%">Set of analog headphones.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/analog_headphones.png" alt="analog headphones"
      width="300" id="analog_headphones">
      </td>
    </tr>
  </table>


  <h2 id="tests">Tests</h2>


  <p>In each test, indicate test success by clicking the <strong>test
  pass</strong> (check mark) button. Otherwise, indicate test failure by
  clicking the <strong>test fail</strong> (exclamation point) button.</p>


  <h3 id="attributes-test">Attributes test</h3>


  <h4 id="abstract">Abstract</h4>


  <p>This test verifies that the attributes (supported sample-rates, channel
  configurations, sample formats, etc.) match the set of a-priori known attributes
  of the device.</p>


  <h4 id="process">Process</h4>


  <p>After invoking the test from the main menu, connect a USB audio
  peripheral. If the attributes match, the <strong>test pass</strong> (check
  mark) button will be enabled.</p>


  <table>
    <tr>
      <td width="50%">Select <em>USB Audio Peripheral Attributes
      Test.</em></td>

      <td width="50%"><img src="/compatibility/cts/images/attributes_test.png"
      alt="attributes test" width="300" id="attributes test">
      </td>
    </tr>


    <tr>
      <td width="50%">Summary of instructions is displayed.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/attributes_summary.png" alt=
      "attributes summary" width="300" id="attributes_summary">
      </td>
    </tr>


    <tr>
      <td width="50%">Pre-connect screen.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/attributes_preconnect.png" alt=
      "attributes preconnect" width="300" id="attributes_preconnect">
      </td>
    </tr>


    <tr>
      <td width="50%">USB audio peripheral connected to Android Device with
      peripheral cable and OTG adapter.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/attributes_connected.png" alt=
      "attributes connected" width="300" id="attributes_connected">
      </td>
    </tr>


    <tr>
      <td width="50%">Post-connect screen.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/attributes_postconnect.png" alt=
      "attributes post connection" width="300" id="attributes_postconnect">
      </td>
    </tr>
  </table>


  <h3 id="play-test">Play test</h3>


  <h4 id="abstract">Abstract</h4>


  <p>This test verifies that audio playback is functioning. It does this by
  generating a 1KHz test tone and presenting it in stereo (two-channels) to the
  USB audio peripheral.</p>


  <h4 id="process">Process</h4>


  <p>After invoking the test from the main menu, connect the USB audio
  interface, including the analog headset, to the headset output jack on the
  interface for monitoring.</p>


  <p>Press the <strong>PLAY</strong> button. If the test tone is heard in both
  channels of the headset, indicate test pass by clicking the <strong>test
  pass</strong> (check mark) button. If either or both of the channels do not
  play the tone, indicate test failure by clicking the <strong>test
  fail</strong> (exclamation point) button.</p>


  <h4 id="notes">Notes</h4>


  <table>
    <tr>
      <td width="50%">Select <em>USB Audio Peripheral Play Test.</em></td>

      <td width="50%"><img src="/compatibility/cts/images/play_test.png" alt=
      "play test" width="300" id="play_test">
      </td>
    </tr>


    <tr>
      <td width="50%">Summary of instructions is displayed.</td>

      <td width="50%"><img src="/compatibility/cts/images/play_summary.png"
      alt="play summary" width="300" id="play_summary">
      </td>
    </tr>


    <tr>
      <td width="50%">Pre-connect screen.</td>

      <td width="50%"><img src="/compatibility/cts/images/play_preconnect.png"
      alt="play preconnect" width="300" id="play_preconnect">
      </td>
    </tr>


    <tr>
      <td width="50%">
        Connect the USB audio peripheral to the Android Device.

        <p>The headphones are connected to the headphone output jack on the USB
        audio interface for monitoring.</p>
      </td>

      <td width="50%"><img src="/compatibility/cts/images/play_connected.png"
      alt="play connected" width="300" id="play_connected">
      </td>
    </tr>


    <tr>
      <td width="50%">Post-connect screen.</td>

      <td width="50%"><img src="/compatibility/cts/images/play_postconnect.png"
      alt="play post connection" width="300" id="play_postconnect">
      </td>
    </tr>
  </table>


  <h3 id="record-loopback-test">Record (loopback) test</h3>


  <h4 id="abstract">Abstract</h4>


  <p>This test verifies that audio recording is functioning. It does this by
  generating a tone at the outputs of the USB audio interface, which is then
  routed via patch cords to the inputs of the USB audio peripheral.</p>


  <h4 id="process">Process</h4>


  <p>After invoking the test from the main menu, connect the USB audio
  interface. Connect the analog outputs to the analog inputs with patch cables.
  Press the <strong>RECORD LOOPBACK</strong> button. If both the channels of
  the recorded test tone are shown in the view below, indicate test pass by
  clicking the <strong>test pass</strong> (check mark) button. If either or
  both of the channels does not display, indicate test failure by clicking the
  <strong>test fail</strong> (exclamation point) button.</p>


  <h4 id="notes">Notes</h4>


  <p>Ensure positive connection of both input and output jacks on the
  peripheral. It will be necessary to adjust the input levels to correctly
  display the recorded signal.</p>


  <table>
    <tr>
      <td width="50%">Select <em>USB Audio Peripheral Record Test.</em></td>

      <td width="50%"><img src="/compatibility/cts/images/record_test.png" alt=
      "record test" width="300" id="record_test">
      </td>
    </tr>


    <tr>
      <td width="50%">Summary of instructions is displayed.</td>

      <td width="50%"><img src="/compatibility/cts/images/record_summary.png"
      alt="record summary" width="300" id="record_summary">
      </td>
    </tr>


    <tr>
      <td width="50%">Pre-connect screen.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/record_preconnect.png" alt="record preconnect"
      width="300" id="record_preconnect">
      </td>
    </tr>


    <tr>
      <td width="50%">USB audio Interface with loopback connected to Android
      device.</td>

      <td width="50%"><img src="/compatibility/cts/images/record_connected.png"
      alt="record connected" width="300" id="record_connected">
      </td>
    </tr>


    <tr>
      <td width="50%">Connections on the back of the USB audio interface.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/record_connected_back.png" alt=
      "record connected in back" width="300" id="record_connected_back">
      </td>
    </tr>


    <tr>
      <td width="50%">Connections on the front of the USB audio interface.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/record_connected_front.png" alt=
      "record connected in front" width="300" id="record_connected_front">
      </td>
    </tr>


    <tr>
      <td width="50%">Post-connect screen.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/record_postconnect.png" alt=
      "record post connection" width="300" id="record_postconnect">
      </td>
    </tr>
  <tr>
      <td width="50%">Post-connect screen, with record test running.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/record_postconnect_running.png" alt=
      "record postconnect running" width="300" id="attributes_postconnect_running.">
      </td>
    </tr>
  </table>


  <h3 id="headset-buttons-test">Headset buttons test</h3>


  <h4 id="abstract">Abstract</h4>


  <p>This test verifies the <strong>media/transport</strong> buttons on the
  recommended headset are correctly recognized.</p>


  <h4 id="process">Process</h4>


  <p>After invoking the test from the main menu, connect the USB headset
  peripheral. Press each <strong>media/transport</strong> (play, pause, volume
  up & volume down) button on the headset. As each is recognized, it will be
  recognized in the test panel. When all buttons have been recognized, the
  <strong>test pass</strong> (check mark) button will be enabled. Click the
  <strong>test pass</strong> button to indicate success. If the full set of
  buttons is not recognized, indicate test failure by clicking the <strong>test
  fail</strong> (exclamation point) button.</p>


  <h4 id="notes">Notes</h4>


  <table>
    <tr>
      <td width="50%">
        The USB headset peripheral connected to the Android device.

        <p>Note the OTG adapter.</p>
      </td>

      <td width="50%"><img src=
      "/compatibility/cts/images/buttons_connected.png" alt="buttons connected"
      width="300" id="buttons_connected">
      </td>
    </tr>


    <tr>
      <td width="50%">Select <em>USB Audio Peripheral Buttons Test.</em></td>

      <td width="50%"><img src="/compatibility/cts/images/buttons_test.png"
      alt="buttons test" width="300" id="buttons_test">
      </td>
    </tr>


    <tr>
      <td width="50%">Summary of instructions is displayed.</td>

      <td width="50%"><img src="/compatibility/cts/images/buttons_summary.png"
      alt="buttons summary" width="300" id="buttons_summary">
      </td>
    </tr>


    <tr>
      <td width="50%">
        Peripheral connected, but no buttons recognized (yet).

        <p>Note that the expected (buttons which are known to the device
        profile) are indicated with  white text; those that are not part of
        the test peripheral are displayed in grey text.</p>
      </td>

      <td width="50%"><img src=
      "/compatibility/cts/images/buttons_not_recognized.png" alt=
      "buttons not recognized" width="300" id="buttons not recognized">
      </td>
    </tr>


    <tr>
      <td width="50%">Peripheral connected, and expected buttons
      recognized.</td>

      <td width="50%"><img src=
      "/compatibility/cts/images/buttons_recognized.png" alt=
      "buttons recognized" width="300" id="buttons recognized">
      </td>
    </tr>
  </table>
</body>
</html>
