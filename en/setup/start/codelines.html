<html devsite>
  <head>
    <title>Codelines, Branches, and Releases</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  The Android Open Source Project (AOSP) maintains a complete software stack to
  be ported by OEMs and other device implementors and run on their own hardware.
  To maintain the quality of Android, Google has contributed full-time
  engineers, product managers, user interface designers, quality assurance
  testers, and all the other roles required to bring modern devices to market.
</p>

<p>
  Accordingly, we maintain a number of codelines to clearly separate the current
  stable version of Android from unstable experimental work. We roll the open
  source administration and maintenance of the Android codelines into the larger
  product development cycle.
</p>

<h2 id="aosp-management">AOSP code management</h2>
<p>
  The chart below depicts the concepts behind AOSP code management and releases.
</p>

<img src="/images/code-lines.png" alt="codeline diagram" id="figure1" >
<figcaption><strong>Figure 1.</strong> AOSP code and releases</figcaption>

<ol>
  <li>
	  At any given moment, there is a current latest release of the Android
    platform. This typically takes the form of a branch in the tree.
  </li>
  <li>
	  Device builders and contributors work with the current latest release,
    fixing bugs, launching new devices, experimenting with new features, etc.
  </li>
  <li>
	  In parallel, Google works internally on the next version of the Android
    platform and framework according to the product's needs and goals. We
    develop the next version of Android by working with a device partner on a
    flagship device whose specifications are chosen to push Android in the 
    direction we believe it should go.
	</li>
  <li>
	  When the n+1th version is ready, it is published to the public source
    tree and become the new latest release.
  </li>
</ol>

<aside class="note"><strong>Note:</strong> We use the term <em>codelines</em>
  instead of <em>branches</em> simply because at any given moment there may be
  more than one branch for a given codeline. For instance, when a release is
  cut, it may or may not become a new branch based on the needs of the moment.
</aside>


<h2 id="terms-and-caveats">Terms and Caveats</h2>

<ul>
  <li>
	  A <em>release</em> corresponds to a formal version of the Android platform,
    such as 1.5, 2.1, and so on. A release of the platform corresponds to the
    version in the <code>SdkVersion</code> field of
    <code>AndroidManifest.xml</code> files and defined within
    <code>frameworks/base/api</code> in the source tree.
  </li>
  <li>
	  An <em>upstream</em> project is an open source project from which the
    Android stack pulls code. In addition to projects such as the Linux kernel
    and WebKit, we continue to migrate some semi-autonomous Android projects
    such as ART, the Android SDK tools, Bionic, and so on to work as
    upstream projects. Generally, these projects are developed entirely in the
    public tree. For some upstream projects, development is done by contributing
    directly to the upstream project itself. For details, see
    <a href="../contribute/submit-patches.html#upstream-projects">Upstream
    projects</a>. In both cases, snapshots are periodically pulled into
    releases.
  </li>
  <li>
	  At all times, a release codeline (which may actually consist of more than
    one actual branch in git) is considered the sole canonical source code for a
    given Android platform version. OEMs and other groups building devices
    should pull only from a release branch.
  </li>
  <li>
	  Experimental codelines are established to capture changes from the community
    so they can be iterated on with an eye toward stability.
  </li>
  <li>
	  Changes that prove stable are eventually be pulled into a release branch.
    This applies only to bug fixes, application improvements, and other changes
    that do not affect the APIs of the platform.
  </li>
  <li>
	  Changes are pulled into release branches from upstream projects
    (including the Android upstream projects) as necessary.
  </li>
  <li>
	  The n+1th version (the next major version of the framework and platform
    APIs) is developed by Google internally. For details, see
    <a href="#private-codelines">Private codelines</a>.
  </li>
  <li>
	  Changes are pulled from upstream, release, and experimental branches into
    Google's private branch as necessary.
  </li>
  <li>
	  When the platform APIs for the next version have stabilized and been fully
    tested, Google cuts a release of the next platform version (specifically, a
    new <code>SdkVersion</code>). This corresponds to the internal codeline
    being made a public release branch and the new current platform codeline.
  </li>
  <li>
	  When a new platform version is cut, a corresponding experimental codeline is
    created at the same time.
  </li>
</ul>

<h2 id="private-codelines">Private codelines</h2>
<p>
  The source management strategy above includes a codeline that Google keeps
  private to focus attention on the current public version of Android.
</p>
<p>
  OEMs and other device builders naturally want to ship devices with the latest
  version of Android. Similarly, application developers don't want to deal with
  more platform versions than strictly necessary. Meanwhile, Google retains
  responsibility for the strategic direction of Android as a platform and a
  product. Our approach focuses on a small number of flagship devices to drive
  features while securing protections of Android-related intellectual property. 
</p>
<p>
  As a result, Google frequently has possession of confidential information from
  third parties and must refrain from revealing sensitive features until
  securing the appropriate protections. In addition, there are real risks to the
  platform arising from having too many platform versions extant at once. For
  these reasons, we have structured the open source project (including
  third-party contributions) to focus on the currently-public stable version of
  Android. Deep development on the next version of the platform occurs in
  private until it's ready to become an official release.
</p>
<p>
  We recognize many contributors disagree with this approach and we respect
  their different points of view. However, this is the approach we feel is best
  and the one we've chosen to implement for Android.
</p>

  </body>
</html>
