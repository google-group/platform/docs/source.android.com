<html devsite>
  <head>
    <title>Overview</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  Working with Android code requires using <strong>Git</strong> (an open-source
  version control system) and <strong>Repo</strong> (a Google-built repository
  management tool that runs on top of Git).
</p>

<h2 id="git">Git</h2>

<p>
  Git is designed to handle large projects that are distributed over multiple
  repositories. Android uses Git for local operations such as local branching,
  commits, diffs, and edits. One of the challenges in setting up the Android
  project was figuring out how to best support the outside community&mdash;from
  the hobbyist community to large OEMs building mass-market consumer devices. We
  wanted components to be replaceable, and we wanted interesting components to
  have a life of their own outside of Android. We first chose a distributed
  revision control system, then narrowed it down to Git.
</p>

<p>
  For more details on Git, refer to
  <a href="https://git-scm.com/documentation" class="external">Git
  Documentation</a>.
</p>

<h2 id="repo">Repo</h2>
<p>
  Repo unifies Git repositories when necessary, performs uploads to the
  <a href="https://android-review.googlesource.com/">Gerrit revision control
  system</a>, and automates parts of the Android development workflow. Repo is
  not meant to replace Git, only to make it easier to work with Git in the
  context of Android. The repo command is an executable Python script that you
  can put anywhere in your path. In working with the Android source files, you
  use Repo for across-network operations. For example, with a single Repo
  command you can download files from multiple repositories into your local
  working directory.
</p>

<p>
  In most situations, you can use Git instead of Repo, or mix Repo and Git
  commands to form complex commands. However, using Repo for basic
  across-network operations will make your work much simpler. For more details
  on Repo, see the <a href="/setup/develop/repo">Repo Command Reference</a>.
</p>

<h2 id="other-tools">Other tools</h2>

<p>
  Other tools include
  <a href="https://gerrit-review.googlesource.com/Documentation/" class="external">Gerrit</a>,
  a web-based code review system for projects that use Git. Gerrit encourages
  more centralized use of Git by allowing all authorized users to submit
  changes, which are automatically merged if they pass code review. In addition,
  Gerrit makes reviewing easier by displaying changes side-by-side in the
  browser and enabling inline comments.
</p>

<p>
  Finally,
  <a href="http://developer.android.com/tools/studio/index.html" class="external">Android
  Studio</a> is the official integrated development environment (IDE) for
  Android application development.
</p>

<h2 id="workflow">Workflow</h2>

<p>
  Android development involves the following basic workflow:
</p>

<ol>
  <li>Start a new topic branch using <code>repo start</code>.
  </li>
  <li>Edit the files.
  </li>
  <li>Stage changes using <code>git add</code>.
  </li>
  <li>Commit changes using <code>git commit</code>.
  </li>
  <li>Upload changes to the review server using <code>repo upload</code>.
  </li>
</ol>

<h2 id="common-tasks">Common tasks</h2>

<p>
  Working with Git and Repo in the Android code repositories involves
  performing the following common tasks:
</p>

<table>
  <tr>
    <th>Command</th>
    <th>Description</th>
  </tr>
  <tr>
    <td><code>repo init</code></td>
    <td>Initializes a new client.</td>
  </tr>
  <tr>
    <td><code>repo sync</code></td>
    <td>Syncs client to repositories.</td>
  </tr>
  <tr>
    <td><code>repo start</code></td>
    <td>Starts a new branch.</td>
  </tr>
  <tr>
    <td><code>repo status</code></td>
    <td>Shows status of current branch.</td>
  </tr>
  <tr>
    <td><code>repo upload</code></td>
    <td>Uploads changes to the review server.</td>
  </tr>
  <tr>
    <td><code>git add</code></td>
    <td>Stages files.</td>
  </tr>
  <tr>
    <td><code>git commit</code></td>
    <td>Commits staged files.</td>
  </tr>
  <tr>
    <td><code>git branch</code></td>
    <td>Shows current branches.</td>
  </tr>
  <tr>
    <td><code>git branch [branch]</code></td>
    <td>Creates new topic branch.</td>
  </tr>
  <tr>
    <td><code>git checkout [branch]</code></td>
    <td>Switches HEAD to specified branch.</td>
  </tr>
  <tr>
    <td><code>git merge [branch]</code></td>
    <td>Merges [branch] into current branch.</td>
  </tr>
  <tr>
    <td><code>git diff</code></td>
    <td>Shows diff of unstaged changes.</td>
  </tr>
  <tr>
    <td><code>git diff --cached</code></td>
    <td>Shows diff of staged changes.</td>
  </tr>
  <tr>
    <td><code>git log</code></td>
    <td>Shows history of current branch.</td>
  </tr>
  <tr>
    <td><code>git log m/[codeline]..</code></td>
    <td>Shows commits that are not pushed.</td>
  </tr>
</table>

<p>
  For information about using Repo to download source, see
  <a href="/setup/build/downloading">Downloading the Source</a> and the
  <a href="/setup/develop/repo">Repo Command Reference</a>.
</p>

<h3 id="synchronizing-clients">Synchronizing clients</h3>

<p>
  To synchronize the files for all available projects:
</p>
<pre class="devsite-terminal devsite-click-to-copy">repo sync</pre>

<p>
  To synchronize the files for selected projects:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo sync <var>PROJECT0 PROJECT1 ... PROJECTN</var>
</pre>

<h3 id="creating-topic-branches">Creating topic branches</h3>

<p>
  Start a topic branch in your local work environment whenever you begin a
  change, such as when you begin work on a bug or new feature. A topic branch is
  <strong>not</strong> a copy of the original files; it is a pointer to a
  particular commit, which makes creating local branches and switching among
  them a lightweight operation. By using branches, you can isolate one aspect of
  your work from the others. For an interesting article about using topic
  branches, refer to
  <a href="http://www.kernel.org/pub/software/scm/git/docs/howto/separating-topic-branches.txt" class="external">Separating
  topic branches</a>.
</p>

<p>
  To start a topic branch using Repo, navigate to the project and run:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo start <var>BRANCH_NAME</var> .
</pre>

<p>
  The trailing period (.) represents the project in the current working
  directory.
</p>

<p>
  To verify the new branch was created:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo status .
</pre>

<h3 id="using-topic-branches">Using topic branches</h3>

<p>To assign the branch to a specific project:</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo start <var>BRANCH_NAME PROJECT_NAME</var>
</pre>

<p>For a list of all projects, refer to
  <a href="https://android.googlesource.com/" class="external">android.googlesource.com</a>.
If you've already navigated to the project directory, just use a period to
represent the current project.
</p>

<p>
  To switch to another branch in your local work environment:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
git checkout <var>BRANCH_NAME</var>
</pre>

<p>
  To view a list of existing branches:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
git branch
</pre>

<p>or</p>

<pre class="devsite-terminal devsite-click-to-copy">
repo branches
</pre>

<p>
  Both commands return the list of existing branches with the name of the
  current branch preceded by an asterisk (*).
</p>

<aside class="note"><strong>Note:</strong> A bug might cause <code>repo
sync</code> to reset the local topic branch. If <code>git branch</code> shows *
(no branch) after you run <code>repo sync</code>, run <code>git checkout</code>
again.</aside>

<h3 id="staging-files">Staging files</h3>

<p>
  By default, Git notices but does not track the changes you make in a project.
  To tell Git to preserve your changes, you must mark or <em>stage</em> those
  changes for inclusion in a commit.
</p>

<p>
  To stage changes:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
git add
</pre>

<p>
  This command accepts arguments for files or directories within the project
  directory. Despite the name, <code>git add</code> does not simply add files to
  the git repository; it can also be used to stage file modifications and
  deletions.
</p>

<h3 id="viewing-client-status">Viewing client status</h3>

<p>
  To list the state of files:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo status
</pre>

<p>
  To view uncommitted edits (local edits that are <strong>not</strong> marked for commit):
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo diff
</pre>

<p>
  To view committed edits (located edits that <strong>are marked</strong> for
  commit), ensure you are in the project directory then run <code>git
  diff</code> with the <code>cached</code> argument:
</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">cd <var>~/WORKING_DIRECTORY/PROJECT</var></code>
<code class="devsite-terminal">git diff --cached</code>
</pre>

<h3 id="committing-changes">Committing changes</h3>

<p>
  A <em>commit</em> is the basic unit of revision control in Git and consists of
  a snapshot of directory structure and file contents for the entire project. To
  create a commit in Git:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
git commit
</pre>

<p>
  When prompted for a commit message, provide a short (but helpful) message for
  changes submitted to AOSP. If you do not add a commit message, the commit is
  aborted.
</p>

<h3 id="uploading-changes-to-gerrit">Uploading changes to Gerrit</h3>

<p>
  Update to the latest revision, then upload the change:
</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">repo sync</code>
<code class="devsite-terminal">repo upload</code>
</pre>

<p>
  This command returns a list of changes you have committed and prompts you to
  select the branches to upload to the review server. If there is only one
  branch, you will see a simple <code>y/n</code> prompt.
</p>

<h3 id="resolving-sync-conflicts">Resolving sync conflicts</h3>

<p>
  If the <code>repo sync</code> command returns sync conflicts:
</p>

<ol>
  <li>View the files that are unmerged (status code = U).</li>
  <li>Edit the conflict regions as necessary.</li>
  <li>Change to the relevant project directory. Add and commit the affected
    files, then rebase the changes:
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">git add .</code>
<code class="devsite-terminal">git commit</code>
<code class="devsite-terminal">git rebase --continue</code>
</pre>
  </li>
  <li>After the rebase completes, start the entire sync again:
<pre class="devsite-terminal devsite-click-to-copy">
repo sync <var>PROJECT0 PROJECT1 ... PROJECTN</var>
</pre>
  </li>
</ol>

<h3 id="cleaning-up-client-files">Cleaning up clients</h3>
<p>
  After merging changes to Gerrit, update your local working directory then use
  <code>repo prune</code> to safely remove stale topic branches:
</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">repo sync</code>
<code class="devsite-terminal">repo prune</code>
</pre>

<h3 id="deleting-clients">Deleting clients</h3>
<p>
  Because all state information is stored in your client, you only need to
  delete the directory from your filesystem:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
rm -rf <var>WORKING_DIRECTORY</var>
</pre>

<p>
  Deleting a client <em>permanently deletes</em> any changes you have not yet
  uploaded for review.
</p>

  </body>
</html>
