<html devsite>
  <head>
    <title>Submitting Patches</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<p>This page describes the full process of submitting a patch to the AOSP,
including
reviewing and tracking changes with <a
href="https://android-review.googlesource.com/">Gerrit</a>.</p>
<h3 id="prerequisites">Prerequisites</h3>
<ul>
<li>
<p>Before you follow the instructions on this page, you need to <a
href="../build/initializing.html">
initialize your build environment</a>, <a
href="../build/downloading.html">download the source</a>, <a
href="https://android.googlesource.com/new-password">create a
password</a>, and follow the instructions on the password generator page.</p>
</li>
<li>
<p>For details about Repo and Git, see <a
href="../develop/index.html">Developing</a>.</p>
</li>
<li>
<p>For information about the different roles you can play within the Android
Open Source community, see <a href="../start/roles.html">Project
roles</a>.</p>
</li>
<li>
<p>If you plan to contribute code to the Android platform, be sure to read
the <a href="../start/licenses.html">AOSP's licensing
information</a>.</p>
</li>
<li>
<p>Note that changes to some of the upstream projects used by Android should be
made directly to that project, as described in <a
href="#upstream-projects">Upstream Projects</a>.</p>
</li>
</ul>

<h2 id="for-contributors">For contributors</h2>

<h3 id="authenticate-with-the-server">Authenticate with the server</h3>
<p>Before you can upload to Gerrit, you need to <a
href="https://android.googlesource.com/new-password">establish a password</a>
that will identify you with the server. Follow the instructions on the password
generator page. You need to do this only once. See <a
href="../build/downloading.html#using-authentication">Using
Authentication</a> for additional details.</p>
<h3 id="start-a-repo-branch">Start a repo branch</h3>
<p>For each change you intend to make, start a new branch within the relevant
git repository:</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo start <var>NAME</var> .
</pre>
<p>You can start several independent branches at the same time in the same
repository. The branch NAME is local to your workspace and will not be included
on Gerrit or the final source tree.</p>
<h3 id="make-your-change">Make your change</h3>
<p>Once you have modified the source files (and validated them, please) commit
the changes to your local repository:</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">git add -A</code>
<code class="devsite-terminal">git commit -s</code>
</pre>
<p>Provide a detailed description of the change in your commit message. This
description will be pushed to the public AOSP repository, so please follow our
guidelines for writing changelist descriptions: </p>
<ul>

<li>
<p>Start with a one-line summary (50 characters maximum), followed by a
blank line.
This format is used by git and Gerrit for various displays.</p>
</li>

<li>
<p>Starting on the third line, enter a longer description, which must
hard-wrap at 72 characters maximum. This description should focus on what
issue the change solves, and how it solves it. The second part is somewhat
optional when implementing new features, though desirable.</p>
</li>
<li>
<p>Include a brief note of any assumptions or background information that
may be important when another contributor works on this feature next year.</p>
</li>
</ul>

<p>Here is an example commit message:</p>
<pre class="devsite-click-to-copy">short description on first line

more detailed description of your patch,
which is likely to take up multiple lines.
</pre>

<p>A unique change ID and your name and email as provided during <code>repo
init</code> will be automatically added to your commit message. </p>

<h3 id="upload-to-gerrit">Upload to Gerrit</h3>
<p>Once you have committed your change to your personal history, upload it
to Gerrit with</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo upload
</pre>
<p>If you have started multiple branches in the same repository, you will
be prompted to select which one(s) to upload.</p>
<p>After a successful upload, repo will provide you the URL of a new page on
<a href="https://android-review.googlesource.com/">Gerrit</a>. Visit this
link to view
your patch on the review server, add comments, or request specific reviewers
for your patch.</p>
<h3 id="uploading-a-replacement-patch">Uploading a replacement patch</h3>
<p>Suppose a reviewer has looked at your patch and requested a small
modification. You can amend your commit within git, which will result in a
new patch on Gerrit with the same change ID as the original.</p>
<aside class="note"><b>Note:</b> If you have made other commits since uploading this patch,
  you will need to manually move your git HEAD.</aside>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">git add -A</code>
<code class="devsite-terminal">git commit --amend</code>
</pre>
<p>When you upload the amended patch, it will replace the original on Gerrit
and in your local git history.</p>

<h3 id="resolving-sync-conflicts">Resolving sync conflicts</h3>
<p>If other patches are submitted to the source tree that conflict with
yours, you will need to rebase your patch on top of the new HEAD of the
source repository. The easy way to do this is to run</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo sync
</pre>
<p>This command first fetches the updates from the source server, then
attempts to automatically rebase your HEAD onto the new remote HEAD.</p>
<p>If the automatic rebase is unsuccessful, you will have to perform a
manual rebase.</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo rebase
</pre>
<p>Using <code>git mergetool</code> may help you deal with the rebase
conflict. Once you have successfully merged the conflicting files,</p>
<pre class="devsite-terminal devsite-click-to-copy">
git rebase --continue
</pre>
<p>After either automatic or manual rebase is complete, run <code>repo
upload</code> to submit your rebased patch.</p>

<h3 id="after-a-submission-is-approved">After a submission is approved</h3>
<p>After a submission makes it through the review and verification process,
Gerrit automatically merges the change into the public repository. Other
users will be able to run <code>repo sync</code> to pull the update into
their local client.</p>

<h2 id="upstream-projects">Upstream Projects</h2>
<p>Android makes use of a number of other open source projects, such as the
Linux kernel and WebKit, as described in
<a href="/setup/code-lines.html">Codelines, Branches, and
Releases</a>. For most projects under <code>external/</code>, changes should
be made upstream and then the Android maintainers informed of the new upstream
release containing these changes. It may also be useful to upload patches
that move us to track a new upstream release, though these can be difficult
changes to make if the project is widely used within Android like most of the
larger ones mentioned below, where we tend to upgrade with every release.</p>
<p>One interesting special case is bionic. Much of the code there is from BSD,
so unless the change is to code that's new to bionic, we'd much rather see an
upstream fix and then pull a whole new file from the appropriate BSD. (Sadly
we have quite a mix of different BSDs at the moment, but we hope to address
that in future, and get into a position where we track upstream much more
closely.)</p>
<h3 id="icu4c">ICU4C</h3>
<p>All changes to the ICU4C project at <code>external/icu4c</code> should
be made upstream at
<a href="http://site.icu-project.org/">icu-project.org/</a>.
See <a href="http://site.icu-project.org/bugs">Submitting ICU Bugs and
Feature Requests</a> for more.</p>

<h3 id="llvmclangcompiler-rt">LLVM/Clang/Compiler-rt</h3>
<p>All changes to LLVM-related projects (<code>external/clang</code>,
<code>external/compiler-rt</code>,
<code>external/llvm</code>) should be made upstream at
<a href="http://llvm.org/">llvm.org/</a>.</p>

<h3 id="mksh">mksh</h3>
<p>All changes to the MirBSD Korn Shell project at <code>external/mksh</code>
should be made upstream
either by sending an email to miros-mksh on the mirbsd.org domain (no
subscription
required to submit there) or (optionally) at <a
href="https://launchpad.net/mksh">Launchpad</a>.
</p>
<h3 id="openssl">OpenSSL</h3>
<p>All changes to the OpenSSL project at <code>external/openssl</code>
should be made upstream at
<a href="http://www.openssl.org">openssl.org</a>.</p>
<h3 id="v8">V8</h3>
<p>All changes to the V8 project at <code>external/v8</code> should be
submitted upstream at
<a href="https://code.google.com/p/v8">code.google.com/p/v8</a>. See <a
href="https://code.google.com/p/v8/wiki/Contributing">Contributing to V8</a>
for details.</p>
<h3 id="webkit">WebKit</h3>
<p>All changes to the WebKit project at <code>external/webkit</code> should
be made
upstream at <a href="http://www.webkit.org">webkit.org</a>. The process
begins by filing a WebKit bug.
This bug should use <code>Android</code> for the <code>Platform</code>
and <code>OS</code>
fields only if the bug is specific to Android. Bugs are far more likely to
receive the reviewers'
attention once a proposed fix is added and tests are included. See
<a href="http://webkit.org/coding/contributing.html">Contributing Code to
WebKit</a> for details.</p>
<h3 id="zlib">zlib</h3>
<p>All changes to the zlib project at <code>external/zlib</code> should be
made upstream at
<a href="http://zlib.net">zlib.net</a>.</p>


  </body>
</html>
