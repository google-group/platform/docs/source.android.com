<html devsite>
  <head>
    <title>Requirements</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  Before you download and build the Android source, ensure your system meets
  the following requirements then see <a href="initializing.html">Establishing
  a Build Environment</a> for installation instructions by operating system.
</p>

<h2 id=hardware-requirements>Hardware requirements</h2>
<p>
  Your development workstation should meet or exceed these hardware
  requirements:
</p>
<ul>
  <li>A 64-bit environment is required for Android 2.3.x (Gingerbread) and
    higher versions, including the master branch. You can compile older versions
    on 32-bit systems.
  </li>
  <li>At least 250GB of free disk space to checkout the code and an extra 150GB
    to build it. If you conduct multiple builds, you will need even more space.
    <aside class="note"><strong>Note:</strong> If you are checking out a mirror
      you will need more space as full Android Open Source Project (AOSP) mirrors
      contain all Git repositories that have ever been used.</aside>
  </li>
  <li>If you are running Linux in a virtual machine, you need at
    least 16GB of RAM/swap.
  </li>
</ul>

<h2 id=software-requirements>Software requirements</h2>
<p>
  The <a href="https://android.googlesource.com/" class="external">Android Open
  Source Project (AOSP)</a> <code>master</code> branch is traditionally
  developed and tested on Ubuntu Long Term Support (LTS) releases, but other
  distributions may be used. See the list below for recommended versions.
</p>
<p>
  Your workstation must have the software listed below. See
  <a href="initializing.html">Establishing a Build Environment</a> for
  additional required packages and the commands to install them.
</p>

<h3 id=latest-version>OS and JDK</h3>
<p>
  If you are developing against the AOSP <code>master</code> branch, use one
  of these operating systems: Ubuntu 14.04 (Trusty) or Mac OS v10.10 (Yosemite)
  or later with Xcode 4.5.2 and Command Line Tools.
</p>
<p>
  For the Java Development Kit (JDK), note the <code>master</code> branch of
  Android in AOSP comes with a prebuilt version of OpenJDK; so no additional
  installation is required. Older versions require a separate install.
</p>
<p>See <a href="#older-versions">Packages for older versions</a>.

<h3 id=packages>Key packages</h3>
<ul>
  <li>Python 2.6 to 2.7 from <a href="http://www.python.org/download/" class="external">python.org</a></li>
  <li>GNU Make 3.81 to 3.82 from <a href="http://ftp.gnu.org/gnu/make/" class="external">gnu.org</a></li>
  <li>Git 1.7 or newer from <a href="http://git-scm.com/download" class="external">git-scm.com</a></li>
</ul>

<h3 id=binaries>Device binaries</h3>
<p>
  Download previews, factory images, drivers, over-the-air (OTA) updates, and
  other blobs below. For details, see
  <a href="building.html#obtaining-proprietary-binaries">Obtaining proprietary
  binaries</a>.
</p>
<ul>
  <li><a href="https://developers.google.com/android/blobs-preview" class="external">Preview
    binaries (blobs)</a>. For AOSP <code>master</code> branch development.
  </li>
  <li><a href="https://developers.google.com/android/images" class="external">Factory
    images</a>. For supported devices running tagged AOSP release branches.
  </li>
  <li><a href="https://developers.google.com/android/drivers" class="external">Binary
    hardware support files</a>. For devices running tagged AOSP release
    branches.
  </li>
</ul>

<h3 id=toolchain>Build toolchain</h3>
<p>
  Android 8.0 and higher support only
  <a href="https://developer.android.com/ndk/guides/standalone_toolchain.html#working_with_clang" class="external">Clang/LLVM</a>
  for building the Android platform. Join the
  <a href="https://groups.google.com/forum/#!forum/android-llvm" class="external">android-llvm</a>
  group to pose questions and get help. Report NDK/compiler issues at the
  <a href="https://github.com/android-ndk/ndk" class="external">NDK GitHub</a>.
</p>
<p>
  For the
  <a href="https://developer.android.com/ndk/guides/index.html" class="external">Native
  Development Kit (NDK)</a> and legacy kernels, GCC 4.9 included in the AOSP
  master branch (under <code>prebuilts/</code>) may also be used.
</p>

<h3 id=older-versions>Packages for older versions</h3>
<p>
  This section details operating systems and JDK packages for older versions of
  Android.
</p>

<h4 id=operating-systems>Operating systems</h4>
<p>
  Android is typically built with a GNU/Linux or Mac OS operating system. It is
  also possible to build Android in a virtual machine on unsupported systems
  such as Windows.
</p>

<p>We recommend building on GNU/Linux instead of another operating system. The
  Android build system normally uses ART, running on the build machine, to
  pre-compile system dex files. Since ART is able to run only on Linux, the
  build system skips this pre-compilation step on non-Linux operating systems,
  resulting in an Android build with reduced performance.
</p>

<h5 id=linux>GNU/Linux</h5>
<ul>
  <li>Android 6.0 (Marshmallow) - AOSP master: Ubuntu 14.04 (Trusty)</li>
  <li>Android 2.3.x (Gingerbread) - Android 5.x (Lollipop): Ubuntu 12.04
  (Precise)</li>
  <li>Android 1.5 (Cupcake) - Android 2.2.x (Froyo): Ubuntu 10.04 (Lucid)</li>
</ul>
<h5 id=mac>Mac OS (Intel/x86)</h5>
<ul>
  <li>Android 6.0 (Marshmallow) - AOSP master: Mac OS v10.10 (Yosemite) or
    higher with Xcode 4.5.2 and Command Line Tools
  </li>
  <li>Android 5.x (Lollipop): Mac OS v10.8 (Mountain Lion) with Xcode 4.5.2
    and Command Line Tools
  </li>
  <li>Android 4.1.x-4.3.x (Jelly Bean) - Android 4.4.x (KitKat): Mac OS v10.6
    (Snow Leopard) or Mac OS X v10.7 (Lion) and Xcode 4.2 (Apple's Developer
    Tools)
  </li>
  <li>Android 1.5 (Cupcake) - Android 4.0.x (Ice Cream Sandwich): Mac OS
    v10.5 (Leopard) or Mac OS X v10.6 (Snow Leopard) and the Mac OS X v10.5
    SDK
  </li>
</ul>

<h4 id=jdk>JDK</h4>
<p>
  See <a href="initializing.html#installing-the-jdk">Installing the JDK</a>
  for the prebuilt path and installation instructions for older versions.
</p>
<ul>
  <li>Android 7.0 (Nougat) - Android 8.0 (Oreo): Ubuntu -
    <a href="http://openjdk.java.net/install/" class="external">OpenJDK 8</a>,
    Mac OS -
    <a href="http://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8-2177648.html#jdk-8u45-oth-JPR" class="external">jdk
    8u45 or newer</a>
  </li>
  <li>Android 5.x (Lollipop) - Android 6.0 (Marshmallow): Ubuntu -
    <a href="http://openjdk.java.net/install/" class="external">OpenJDK 7</a>,
    Mac OS -
    <a href="https://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase7-521261.html#jdk-7u71-oth-JPR" class="external">jdk-7u71-macosx-x64.dmg</a>
  </li>
  <li>Android 2.3.x (Gingerbread) - Android 4.4.x (KitKat): Ubuntu -
    <a href="http://www.oracle.com/technetwork/java/javase/archive-139210.html" class="external">Java
    JDK 6</a>, Mac OS - <a href="http://support.apple.com/kb/dl1572" class="external">Java JDK
    6</a>
  </li>
  <li>Android 1.5 (Cupcake) - Android 2.2.x (Froyo): Ubuntu -
    <a href="http://www.oracle.com/technetwork/java/javase/archive-139210.html" class="external">Java
    JDK 5</a>
  </li>
</ul>

<h4 id=make>Make</h4>
<p>
  To avoid build errors, Android 4.0.x (Ice Cream Sandwich) and earlier must
  <a href="initializing.html#reverting-from-make-382">revert from make 3.82</a>.
</p>

  </body>
</html>
