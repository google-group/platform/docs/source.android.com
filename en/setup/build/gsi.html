<html devsite>
  <head>
    <title>Generic System Image (GSI)</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
    {% include "_versions.html" %}
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  A generic system image (GSI) is a system image with adjusted configurations
  for Android devices. It is considered a "pure Android" implementation with
  unmodified Android Open Source Project (AOSP) code that any Android device
  running Android 8.1 or higher can run successfully.
</p>

<p>
  GSI compliance is part of the <a href="/compatibility/overview">Android
  Compatibility program</a>. The system image of an Android device is replaced
  with a GSI then tested with the <a href="/compatibility/vts/">Vendor Test
  Suite (VTS)</a> and the <a href="/compatibility/cts/">Compatibility Test Suite
  (CTS)</a> to ensure the device implements vendor inferfaces correctly with the
  latest version of Android.
</p>

<aside class="note"><strong>Note:</strong>
  This article describes GSI topics for Android OEM and ROM developers; Android
  application developers should refer to
  <a href="https://developer.android.com/topic/generic-system-image/">developer.android.com</a>
  for developer-focused GSI details.
</aside>

<p>
  To get started with GSIs, review the following sections for details on
  <a href="#gsi-configuration-and-variances">GSI configurations</a> (and allowed
  variances), <a href="#gsi-types">types</a> (Android GSI and Legacy GSI), and
  <a href="#vendor-binaries-and-vndk-dependencies">vendor binaries and VNDK
  dependencies</a>. When you are ready to use a GSI,
  <a href="#building-gsis">download and build the GSI</a> for your device
  target then <a href="#flashing-gsis">flash the GSI</a> to an Android
  device.
</p>

<h2 id="gsi-configuration-and-variances">GSI configuration and variances</h2>

<p>
  The current GSI has the following configuration:
</p>

<ul>
  <li><strong>Treble</strong>. A GSI includes full support for the
    <a href="/devices/architecture/#hidl">HIDL-based architectural changes</a>
    (also known as "Treble") introduced in Android 8.0, including support for
    <a href="/reference/hidl/">HIDL interfaces</a>. You can use a GSI on any
    Android device that uses HIDL vendor interfaces. (For more details, see
    <a href="/devices/architecture/#resources">Architecture resources</a>.)</li>
  <li><strong>Verify Boot.</strong> A GSI does not include a verify boot
    solution (<a href="/security/verifiedboot/">vboot 1.0</a>,
    <a href="/security/verifiedboot/avb">AVB</a>, etc.). To flash a GSI to an
    Android device, it must have a method for disabling verify boot.</li>
  <li><strong>Build variant</strong>. A GSI always uses a <code>userdebug</code>
    build variant to enable running VTS and CTS. After replacing the system
    image with GSI, you can root the device then test with a
    <code>user</code>-build vendor image and a <code>userdebug</code>-build
  system image.</li>
  <li><strong>File system and image format</strong>. A GSI uses an ext4 file
    system with sparse image format.</li>
</ul>

<p>
  The current GSI includes following major variances:
</p>

<ul>
  <li><strong>Version</strong>. Support for Android 8.0, Android 8.1, and
    Android {{ androidPVersionNumber }}.</li>
  <li><strong>CPU architecture</strong>. Support for different CPU instructions
    (ARM, x86, etc.) and CPU bitness (32-bit or 64-bit).</li>
  <li><strong>Partition layout</strong>. Can use
    <a href="/devices/bootloader/system-as-root">system-as-root</a> or
    non-system-as-root partition layout.</li>
  <li>Support for binder interface bitness.</li>
</ul>

<h2 id="gsi-types">GSI types</h2>

<p>
  The GSI used for compliance testing is determined by the Android version the
  device launches with. Android {{ androidPVersionNumber }} supports the
  following GSIs:
</p>

<table>
  <tr>
   <th>GSI name</th>
   <th>Description</th>
   <th>Product name</th>
  </tr>
  <tr>
   <td>Android GSI</td>
   <td>For devices launching with Android {{ androidPVersionNumber }}</td>
   <td><code>aosp_$arch</code></td>
  </tr>
  <tr>
   <td>Legacy GSI</td>
   <td>For devices launching with Android 8.0 or Android 8.1</td>
   <td><code>aosp_$arch_a(b)</code></td>
  </tr>
</table>

<p>
  All GSIs are built from the Android {{ androidPVersionNumber }} codebase.
</p>

<h3 id="changes-in-p-gsis">Android {{ androidPVersionNumber }} GSI changes</h3>

<p>
  Devices launching with Android {{ androidPVersionNumber }} must use Android
  {{ androidPVersionNumber }} GSIs for compliance testing, which includes the
  following major changes from earlier GSIs:
</p>

<ul>
  <li><strong>Merges GSI and emulator</strong>. GSIs are built from the system
    images of emulator products, e.g. <code>aosp_arm64</code>,
    <code>aosp_x86</code>, etc.</li>
  <li><strong>System-as-root</strong>. In previous versions of Android, devices
    that did not support A/B updates could mount the system image under
    <code>/system</code> directory. In Android {{ androidPVersionNumber }}, the
    root of the system image is mounted as the root of the device.</li>
    <li><strong>64-bit binder interface</strong>. In Android 8.x, 32-bit GSIs
      used the 32-bit binder interface. Android {{ androidPVersionNumber }} does
      not support 32-bit binder interface, so both 32-bit GSIs and 64-bit GSIs
      use the 64-bit binder interface.</li>
    <li><strong>VNDK enforcement</strong>. In Android 8.1, VNDK was optional. In
      Android {{ androidPVersionNumber }}, VNDK is mandatory, meaning the
      <code>BOARD_VNDK_RUNTIME_DISABLE</code> must <strong>not</strong> be set
      (<code>BOARD_VNDK_RUNTIME_DISABLE :=  # must not be set</code>).</li>
    <li><strong>Compatible system property</strong>. Android
      {{ androidPVersionNumber }} enables the access check for compatible system
      property (<code>PRODUCT_COMPATIBLE_PROPERTY_OVERRIDE := true</code>).</li>
</ul>

<p>
  To test devices launching with Android {{ androidPVersionNumber }} with
  cts-on-gsi, use the <a href="#p-gsi-build-targets">build targets for the
  Android {{ androidPVersionNumber }} GSI</a>.
</p>

<h3 id="changes-in-legacy-gsis">Android {{ androidPVersionNumber }} Legacy GSI
changes</h3>

<p>
  Devices upgrading to Android {{ androidPVersionNumber }} can use Legacy GSI
  product named with suffix <code>_ab</code> or <code>_a</code> (e.g.
  <code>aosp_arm64_ab</code>, <code>aosp_x86_a</code>) for compliance testing.
  This GSI supports the following upgrade use cases:
</p>

<ul>
  <li>Devices with an Android 8.1 vendor interface implementation</li>
  <li>Devices updated to the Android {{ androidPVersionNumber }} vendor
    interface implementation</li>
</ul>

<p>
  Legacy GSIs are build from the Android {{ androidPVersionNumber }} source
  tree but contain the following backward-compatible configurations for upgraded
  devices:
</p>

<ul>
  <li><strong>Non system-as-root</strong>. Devices that do not support
    system-as-root can continue to use <code>_a</code> products (e.g.,
    <code>aosp_arm_a</code>).</li>
  <li><strong>32-bit userspace + 32-bit binder interface</strong>. 32-bit GSIs
    can continue to use the 32-bit binder interface.</li>
  <li><strong>8.1 VNDK</strong>. Devices can use the included 8.1 VNDK.</li>
  <li><strong>Mount directories</strong>. Some legacy devices use directories as
    mount-pointers (e.g. <code>/bluetooth</code>, <code>/firmware/radio</code>,
    <code>/persist</code>, etc.).</li>
</ul>

<p>
  To test devices upgrading to Android {{ androidPVersionNumber }} with
  cts-on-gsi, use the <a href="#legacy-gsi-build-targets">build targets for
  Legacy GSI</a>.
</p>

<aside class="note">
  <strong>Note:</strong> If a pre-Android {{ androidPVersionNumber }} device
  implements the Android {{ androidPVersionNumber }} vendor interface and meets
  all requirements introduced in Android {{ androidPVersionNumber }}, don't use
  the Legacy GSIs; instead use Android {{ androidPVersionNumber }} GSIs for VTS
  and cts-on-gsi.
</aside>

<h3 id="changes-to-keymaster-behavior">Android {{ androidPVersionNumber }}
Keymaster changes</h3>

<p>
  In earlier versions of Android, devices implementing Keymaster 3 or lower were
  required to verify the version info (<code>ro.build.version.release</code> and
  <code>ro.build.version.security_patch</code>) reported by the running system
  matched the version info reported by bootloader. Such information was
  typically obtained from the boot image header.
</p>

<p>
  In Android {{ androidPVersionNumber }}, this requirement has changed to enable
  vendors to boot a GSI. Specifically, Keymaster should not perform verification
  since the version info reported by the GSI may not match the version info
  reported by vendor's bootloader. For devices implementing Keymaster 3 or
  lower, vendors must modify the Keymaster implementation to skip verification
  (or upgrade to Keymaster 4). For details on Keymaster, refer to
  <a href="/security/keystore/">Hardware-backed Keystore</a>.
</p>

<h2 id="vendor-binaries-and-vndk-dependencies">Vendor binaries and VNDK
dependencies</h2>

<p>
  Devices upgrading to Android {{ androidPVersionNumber }} have different
  upgrade paths depending on the version of vendor binaries in use on the device
  and the VNDK-related configurations used to build the device. The following
  table summarizes the Legacy GSI support for upgraded devices:
</p>

<table>
  <tr>
   <th>Use case</th>
   <th>Vendor<br>binaries<br>version</th>
   <th><code>BOARD_VNDK_VERSION</code></th>
   <th><code>BOARD_VNDK_RUNTIME_DISABLE</code></th>
   <th>Legacy GSI<br>system binaries version</th>
   <th>Legacy GSI support</th>
  </tr>
  <tr>
   <td>0</td>
   <td>8.0</td>
   <td>(any)</td>
   <td>(N/A)</td>
   <td>{{ androidPVersionNumber }}</td>
   <td>No</td>
  </tr>
  <tr>
   <td>1.a</td>
   <td>8.1</td>
   <td>(empty)</td>
   <td>(any)</td>
   <td>{{ androidPVersionNumber }}</td>
   <td>No</td>
  </tr>
  <tr>
   <td>1.b</td>
   <td>8.1</td>
   <td><code>current</code></td>
   <td><code>true</code></td>
   <td>{{ androidPVersionNumber }}</td>
   <td>No</td>
  </tr>
  <tr>
   <td>2</td>
   <td>8.1</td>
   <td><code>current</code></td>
   <td>(empty)</td>
   <td>{{ androidPVersionNumber }}</td>
   <td>Yes</td>
  </tr>
  <tr>
   <td>3</td>
   <td>{{ androidPVersionNumber }}</td>
   <td><code>current</code></td>
   <td><code>true</code></td>
   <td>{{ androidPVersionNumber }}</td>
   <td>Yes</td>
  </tr>
  <tr>
   <td>4</td>
   <td>{{ androidPVersionNumber }}</td>
   <td><code>current</code></td>
   <td>(empty)</td>
   <td>{{ androidPVersionNumber }}</td>
   <td>Yes</td>
  </tr>
</table>

<p>
  The most common supported use case is #2, where the Legacy GSI supports
  devices running Android 8.1 that were built with
  <code>BOARD_VNDK_VERSION</code> but built without
  <code>BOARD_VNDK_RUNTIME_DISABLE</code> (i.e., runtime enforcement was NOT
  disabled).
</p>

<p>
  The two unsupported use cases are #1.a and #1.b, where the Legacy GSI does NOT
  support devices running Android 8.1 that were not built with
  <code>BOARD_VNDK_VERSION</code> or built with
  <code>BOARD_VNDK_RUNTIME_DISABLE</code> (i.e. runtime enforcement WAS
  disabled). These devices are not supported because their vendor binaries
  depend on Android 8.1 non-VNDK shared libraries, which are not included in
  Legacy GSIs. To make these devices compatible with the Legacy GSI, vendors
  must do one of the following:
</p>

<ul>
  <li>Enable <code>BOARD_VNDK_VERSION</code> without
    <code>BOARD_VNDK_RUNTIME_DISABLE</code> (use case #2)
    <br><br>OR<br><br></li>
  <li>Port/upgrade the vendor binaries to depend on the shared libraries from
  Android {{ androidPVersionNumber }} (use case #3 and use case #4).</li>
</ul>

<h2 id="building-gsis">Building GSIs</h2>

<p>
  Starting with Android {{ androidPVersionNumber }}, each Android version has a
  GSI branch named <code><var>DESSERT</var>-gsi</code> on AOSP (for example,
  <code>pie-gsi</code> is the GSI branch on Android
  {{ androidPVersionNumber }}). GSI branches include the content of Android with
  all <a href="/security/bulletin/">security patches</a> and
  <a href="#contributing-to-gsis">GSI patches</a> applied.
</p>

<p>
  To build a GSI, set up the Android source tree by
  <a href="/setup/build/downloading">downloading</a> from a GSI branch and
  <a href="/setup/build/building#choose-a-target">choosing a GSI build
  target</a>. Use the build target tables below to determine the correct GSI
  version for your device. After the build completes, the GSI is the system
  image (e.g. <code>system.img</code>) and appears in the output folder
  <code>out/target/product/<strong>generic_arm64_ab</strong></code>. The build
  also outputs <code>vbmeta.img</code> , which you can use to disable verify
  boot on the devices using <a href="/security/verifiedboot/avb">Android
  Verified Boot</a>.
<p>

<p>
  For example, to build the Legacy GSI build target
  <code>aosp_arm64_ab-userdebug</code> on GSI branch <code>pie-gsi</code>, run
  the following commands:
</p>

<pre class="prettyprint">
$ repo init -u https://android.googlesource.com/platform/manifest -b pie-gsi
$ repo sync -cq
$ source build/envsetup.sh
$ lunch aosp_arm64_ab-userdebug
$ make -j4
</pre>

<h3 id="p-gsi-build-targets">Android {{ androidPVersionNumber }} GSI build
targets</h3>

<p>
  The following GSI build targets are for devices launching with Android
  {{ androidPVersionNumber }}. Due to a reduction in variances between
  architectures, Android {{ androidPVersionNumber }} includes only four GSI
  products.
</p>

<table>
  <tr>
   <th>GSI name</th>
   <th>CPU arch</th>
   <th>Binder interface bitness</th>
   <th>System-as-root</th>
   <th>Product name</th>
  </tr>
  <tr>
   <td><code>aosp_arm</code></td>
   <td><code>ARM</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm64</code></td>
   <td><code>ARM64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm64-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86</code></td>
   <td><code>x86</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_64</code></td>
   <td><code>x86-64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86_64-userdebug</code></td>
  </tr>
</table>

<h3 id="legacy-gsi-build-targets">Android {{ androidPVersionNumber }} Legacy
GSI build targets</h3>

<p>
  The following Legacy GSI build targets are for devices upgrading to Android
  {{ androidPVersionNumber }}. Legacy GSI names include the suffix
  <code>_ab</code> or <code>_a</code> to distinguish them from Android
  {{ androidPVersionNumber }} GSI names.
</p>

<table>
  <tr>
   <th>GSI name</th>
   <th>CPU arch</th>
   <th>Binder interface bitness</th>
   <th>System-as-root</th>
   <th>Product name</th>
  </tr>
  <tr>
   <td><code>aosp_arm_a</code></td>
   <td><code>ARM</code></td>
   <td><code>32</code></td>
   <td><code>N</code></td>
   <td><code>aosp_arm_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm_ab</code></td>
   <td><code>ARM</code></td>
   <td><code>32</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>**NA</code></td>
   <td><code>ARM</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td></td>
  </tr>
  <tr>
   <td><code>aosp_arm_64b_ab</code></td>
   <td><code>ARM</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm_64b_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm64_a</code></td>
   <td><code>ARM64</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td><code>aosp_arm64_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm64_ab</code></td>
   <td><code>ARM64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm64_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_a</code></td>
   <td><code>x86</code></td>
   <td><code>32</code></td>
   <td><code>N</code></td>
   <td><code>aosp_x86_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_ab</code></td>
   <td><code>x86</code></td>
   <td><code>32</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>**NA</code></td>
   <td><code>x86</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td></td>
  </tr>
  <tr>
   <td><code>**NA</code></td>
   <td><code>x86</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td></td>
  </tr>
  <tr>
   <td><code>aosp_x86_64_a</code></td>
   <td><code>x86-64</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td><code>aosp_x86_64_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_64_ab</code></td>
   <td><code>x86-64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86_64_ab-userdebug</code></td>
  </tr>
</table>
<em>** Could be added by request</em>

<aside class="aside">
  <strong>Note:</strong> These build targets will likely be removed in a
  future version of Android.
</aside>

<h2 id="flashing-gsis">Flashing GSIs Requirements</h2>

<p>
  Android devices can have different designs, so no single command or set of
  instructions for flashing a GSI to a specific device is possible. You can
  check with the manufacturer of the Android device for explicit flashing
  instructions or use the following general steps as guidelines:
</p>

<ol>
  <li>Ensure the device has the following:
    <ul>
      <li>Support for HIDL-HAL interfaces.</li>
      <li>A method for unlocking devices (so they can be flashed using
        <code>fastboot</code>).</li>
      <li>A method for disabling verify boot (e.g.
        <a href="/security/verifiedboot/">vboot 1.0</a>,
        <a href="/security/verifiedboot/avb">AVB</a>, etc.).</li>
      <li>Unlock the device to make it flashable via <code>fastboot</code>. (To
        ensure you have the latest version of <code>fastboot</code>, build it
        from Android source tree.)</li>
    </ul>
  </li>
  <li>Disable verify boot.</li>
  <li>Erase the current system partition, then flash the GSI to system
    partition.</li>
  <li>Wipe userdata and clear data from other necessary partitions (e.g.
    metadata).</li>
  <li>Reboot the device.</li>
</ol>

<p>
  For example, to flash a GSI to any Pixel device:
</p>

<ol>
  <li><a href="/setup/build/running#booting-into-fastboot-mode">Boot to
    bootloader mode</a> and
    <a href="/setup/build/running#unlocking-the-bootloader">unlock the
    bootloader</a>.</li>
  <li>Disable verify boot (AVB) by flashing <code>vbmeta.img</code>:
<pre class="prettyprint">$ fastboot flash vbmeta vbmeta.img</pre></li>
  <li>Erase and flash the GSI to system partition:
<pre class="prettyprint">
$ fastboot erase system
$ fastboot flash system system.img
</pre></li>
  <li>Wipe userdata and clear other necessary partitions:
<pre class="prettyprint">$ fastboot -w</pre></li>
  <li>Reboot:
<pre class="prettyprint">$ fastboot reboot</pre></li>
</ol>

<h2 id="contributing-to-gsis">Contributing to GSIs</h2>

<p>
  Android welcomes your contributions to GSI development. You can get involved
  and help improve the GSI by:
</p>

<ul>
  <li><strong>Creating a GSI patch</strong>. Because
    <code><var>DESSERT</var>-gsi</code> is <strong>not</strong> a development
    branch and accepts only cherrypicks from the AOSP master branch, to submit a
    GSI patch, you must:
    <ol>
      <li>Submit the patch to the
        <a href="https://android-review.googlesource.com" class="external">AOSP</a>
        master branch.</li>
      <li>Cherrypick the patch to <code><var>DESSERT</var>-gsi</code>.</li>
      <li>File a bug to get the cherrypick reviewed.</li>
    </ol>
  </li>
  <li><strong>Reporting GSI bugs</strong> or making other suggestions. Review
    the instructions in
    <a href="/setup/contribute/report-bugs#platform">Reporting Bugs</a>, then
    browse or file GSI bugs (look for "Generic System Image" in the Platform
    table).</li>
</ul>

</body>
</html>
